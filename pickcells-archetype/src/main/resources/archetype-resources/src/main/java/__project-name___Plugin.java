package ${package};

import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.pickcells.api.app.modules.ActivationListener;
import org.pickcellslab.pickcells.api.app.modules.Task;
import org.slf4j.LoggerFactory;

		// This annotation is used by the framework to discover our plugin 
@Module // (more at https://pickcellslab.frama.io/docs/developers/architecture/foundationj/dependency_injection/)
public class ${project-name}_Plugin implements Task {

// This example implements Task -> the plugin will be available
// to the user via a button in the taskbar of the main interface of the PickCells workbench


	@Override
	public String name() {
		// TODO This is the name of this new plugin
		return "${project-name}_Plugin";
	}

	@Override
	public String description() {
		// TODO Describe here what the plugin does. HTML can be used
		return "${project-name}_Plugin Description";
	}

	@Override
	public Icon icon() {
		// TODO This will define which icon to display in the menu bar of pickcells
		return new ImageIcon(getClass().getResource("/icons/default_icon_32.png"));
	}

	@Override
	public String[] categories() {
		// TODO You can tag your plugin with as many categories as you wish
		return new String[] { "Uncategorised" };
	}

	@Override
	public void launch() throws Exception {
		// TODO This is where your plugin does its job...
		// This method is called when the user clicks on the button launcher in the menu bar

		// NB: Logging can be done via the slf4j API
		LoggerFactory.getLogger(getClass()).info(name() + " launched!");
	}



	@Override
	public boolean isActive() {
		// TODO If your plugin needs some conditions to be met before being activable
		// declare here whether it activable or not
		return true;
	}

	@Override
	public void start() {
		// TODO Notifies this module that the workbench has just been launched
		// any required initialisation can be done here.
		
	}

	@Override
	public void registerListener(ActivationListener lst) {
		// TODO deprecated - do not use
		
	}

	@Override
	public String authors() {
		// TODO Comma separated list of authors
		return "Your Name";
	}

	@Override
	public String licence() {
		// TODO The acronym of the code license
		return null;
	}

	@Override
	public URL url() {
		// TODO A url where more info on the plugin may be found
		return null;
	}


}