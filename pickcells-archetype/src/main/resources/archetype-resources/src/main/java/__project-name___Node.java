package ${package};

import java.util.Objects;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;
import org.slf4j.LoggerFactory;


@Data(typeId = "${project-name}Node") // Will register your class amongst known DataTypes
@SameScopeAs(Workbench.class)
public class ${project-name}_Node extends DataNode implements NodeItem{

	@Override
	public Stream<AKey<?>> minimal() {
		// TODO Here you define the properties that are required by default
		// when recreating your java object from the database.
		return Stream.of(idKey);
	}


	// Override specific methods
	@Override
	public <T> void setAttribute( AKey<T> key, T v) {
		Objects.requireNonNull(v);
		attributes.put(key, v);
		LoggerFactory.getLogger(getClass()).info("Attribute "+key.name+" added !");
	}


	// Or add your own
	public String sayHi() {
		LoggerFactory.getLogger(getClass()).debug(typeId()+" says Hi!");
		return "Hi!";
	}

}