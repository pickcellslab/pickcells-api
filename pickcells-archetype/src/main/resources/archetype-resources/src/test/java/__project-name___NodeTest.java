package ${package};

import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.pickcellslab.foundationj.datamodel.AKey;

/**
 * Unit test for a custom NodeItem
 */
@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
public class ${project-name}_NodeTest {
  
		
	@BeforeAll
	public void setup() {
		// Test Setup
		// This method can be removed if no setup is required
	}
	
	@AfterAll
	public void cleanup() {
		// Test Cleanup
		// This method can be removed if no cleanup is required
	}
	
	
	@Test
	public void testSayHi() {
		${project-name}_Node testNode = new ${project-name}_Node();
		Assert.assertEquals("Hi!", testNode.sayHi());
	}
	
	
	@Test
	public void testPropertySet() {
				
		AKey<String> testKey = AKey.get("TestKey", String.class);
		String testValue = "TestValue";
		
		// Test property set
		${project-name}_Node testNode = new ${project-name}_Node();
		testNode.setAttribute(testKey, testValue);
		Assert.assertTrue(testNode.getAttribute(testKey).isPresent());
		Assert.assertTrue(testNode.getAttribute(testKey).get().equals(testValue));
				
		
	}
	
	
	
}
