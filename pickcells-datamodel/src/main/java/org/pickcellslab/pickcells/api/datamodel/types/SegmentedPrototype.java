package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;

/**
 * {@link SegmentedPrototype}s are {@link SegmentedObject}s which can act as a factory to create other {@link SegmentedPrototype} instances
 * of the same type but with a distinct {@link #label()} than the creating instance.
 */
public interface SegmentedPrototype extends SegmentedObject{

	
	/**
	 * This factory method allows a SegmentedObject to act as a prototype to create other
	 * SegmentedObject of the same kind but with a different label.
	 * @param label The label found in a Segmentation image
	 * @return A new Instance of the same class initialised with the specified label
	 */
	public SegmentedPrototype create(float label);
	
	
	@Override
	public default Optional<LabelsImage> origin() {
		return  getLinks(Direction.OUTGOING,Links.CREATED_FROM).map(l->(LabelsImage)l.target()).findAny();
	}
}
