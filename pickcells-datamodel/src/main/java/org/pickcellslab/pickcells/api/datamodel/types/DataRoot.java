package org.pickcellslab.pickcells.api.datamodel.types;

import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;

@Tag(creator = "PickCells core", description = "Defines an element of the Data model which can conceptually"
		+ " define a root for a dataset", tag = DataModel.DATAROOT)
public interface DataRoot {

}
