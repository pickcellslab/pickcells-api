package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;

@Data(typeId="Cluster")
@SameScopeAs(Image.class)
public class Cluster extends DataNode {

	private static final AKey<Integer> size = AKey.get("size",Integer.class);
	private static final AKey<Integer> number = AKey.get("cluster id",Integer.class);
	private static final List<AKey<?>> minimal = new ArrayList<>(2);
	static{
		minimal.add(WritableDataItem.idKey);
		minimal.add(size);
		minimal.add(number);
	}
	
	@SuppressWarnings("unused")
	private Cluster(){
		//Database
	}
	
	public Cluster(Image i, Clustering parent, List<ImageLocated> nodes, int id){		
		
		
		//TODO add features : principal components...
		setAttribute(size,nodes.size());
		setAttribute(number,id);
		
		new DataLink("FOUND_IN", this, i, true);
		new DataLink(Clustering.links,parent,this,true);
		
		for(ImageLocated n : nodes){
			new DataLink("REGROUP", this, n, true);
		}
		
		
		
	}
	
	

	@Override
	public Stream<AKey<?>> minimal() {
		return minimal.stream();
	}

	@Override
	public String toString() {
		Set<Link> links = this.typeMap.get(Clustering.links);
		if(links != null)
			return links.iterator().next()+" number "+getAttribute(number).get();
		else
		return "Cluster from unknown "+getAttribute(number).get();
	}

}
