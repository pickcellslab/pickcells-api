package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;

/**
 *
 * Models a boundary in an image, the inner or outer surface of any type of structure.
 * Other objects can be in contact with this boundary, have an anchor point, be inside 
 * or outside (note that a convention must be found when the boundary does not
 * refer to a closed space). A boundary is renderable and therefore possesses a mesh. 
 * 
 * @author Guillaume Blin
 *
 */
@Data(typeId="Boundary", icon="/icons/boundary_icon.png")
@SameScopeAs(Image.class)
public class Boundary extends DataNode{


	/**
	 * The type of the links connecting a cluster and its members
	 */
	public static final String bound_link = "DELINEATE";

	private static final String prefix = "sfc";

	private static AKey<String> ownerID = AKey.get("Owner ID", String.class);
	private static AKey<Float> out_label = AKey.get("Outer Label",Float.class);
	private static AKey<Float> in_label = AKey.get("Inner Label",Float.class);
	private static AKey<double[]> centroid = AKey.get("Centroid", double[].class);
	private static AKey<float[]> bbMin = AKey.get("bbMin", float[].class);
	private static AKey<float[]> bbMax = AKey.get("bbMax", float[].class);

	protected static final List<AKey<?>> keys = new ArrayList<>(5);

	static{	
		keys.add(idKey);
		keys.add(ownerID);
		keys.add(out_label);
		keys.add(in_label);
		keys.add(centroid);
	}

	private List<float[]> mesh;



	@SuppressWarnings("unused")
	private Boundary(){
		//For the database
	}

	/**
	 * Creates a new Boundary with the provided mesh and 
	 * @param mesh
	 * @param out_label
	 * @throws IOException 
	 */
	public Boundary(SegmentedObject owner, List<float[]> mesh, float outLabel, File folder) throws IOException{
	
		setAttribute(ownerID,owner.getClass().getSimpleName()+owner.getAttribute(WritableDataItem.idKey).get());
		setAttribute(out_label,outLabel);
		
		//Compute the centroid
		SummaryStatistics[] stats = new SummaryStatistics[mesh.get(0).length];
		for(int s = 0; s<stats.length; s++){
			stats[s] = new SummaryStatistics();
		}
		mesh.forEach(m->{
			for(int s = 0; s<stats.length; s++){
				stats[s].addValue(m[s]);
			}
		});
		
		double[] center = new double[stats.length];
		float[] min = new float[stats.length];
		float[] max = new float[stats.length];
		for(int s = 0; s<stats.length; s++){
			center[s] = stats[s].getMean();
			min[s] = (float) stats[s].getMin();
			max[s] = (float) stats[s].getMax();
		}
		
		setAttribute(centroid,center);
		setAttribute(bbMin,min);
		setAttribute(bbMax,max);
		setAttribute(in_label,owner.label().get());
		new DataLink(Boundary.bound_link,this,owner,true);
		this.mesh = mesh;


		final FileOutputStream out = new FileOutputStream(createPath(folder, this));
		final ObjectOutputStream oos = new ObjectOutputStream( out );
		new Thread(()->{

			try {

				oos.writeObject(mesh);
				/*
				String s = ModelExtension.toXML(mesh);
				PrintWriter pw = new PrintWriter(out);	
				pw.print(s);
				pw.close();
				 */
				oos.close();
				out.close();				


			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).start();

	}


	private static String createPath(File folder, NodeItem node){
		return folder.getPath()+File.separator+prefix+
				"_"+
				node.getAttribute(ownerID).get()+
				node.getAttribute(in_label).get().intValue()+
				node.getAttribute(out_label).get().intValue()+
				".msh";
	}


	/*
	private String readFile(String path) throws IOException{

		BufferedReader br = new BufferedReader(new FileReader(path));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}
	 */


	public List<float[]> mesh(File folder) throws IOException{
		if(mesh == null){
			mesh = mesh(folder, this);
		}
		return mesh;
	}


	public static List<float[]> mesh(File folder, NodeItem node) throws IOException{

		System.out.println("Path to boundary"+createPath(folder, node));
		
		FileInputStream st = new FileInputStream(createPath(folder, node));
		
		
		
		ObjectInputStream in = new ObjectInputStream(st);
		List<float[]> mesh = null;
		try {

			mesh = (List<float[]>) in.readObject();

		} catch (ClassNotFoundException e) {
			throw new IOException("Class not found!",e);
		}
		finally{
			in.close();
		}

		return mesh;
	}




	@Override
	public Stream<AKey<?>> minimal() {
		return keys.stream();
	}


	@Override
	public String toString() {
		return this.getAttribute(in_label).get()+" - Boundary - "+this.getAttribute(out_label).get();
	}


}
