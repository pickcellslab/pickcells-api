package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;

@Data(typeId="Speckle")
@SameScopeAs(Image.class)
public class Speckle extends DataNode implements ImageDot {

	protected static final List<AKey<?>> keys = new ArrayList<>(6);

	static{	
		keys.add(idKey);
		keys.add(Keys.bbMin);
		keys.add(Keys.bbMax);
		keys.add(Keys.location);
	}


	


	@Override
	public long[] location() {
		return getAttribute(Keys.location).get();
	}



	@Override
	public double[] centroid() {
		return getAttribute(Keys.centroid).get();
	}


	

	@Override
	public Speckle create(Image image, long[] l) {
		Speckle c = new Speckle();
		new DataLink(origin, c, image, true);
		c.setAttribute(Keys.location, l);
		c.setAttribute(Keys.centroid, image.calibrated(l,false,false));		
		return c;
	}

	@Override
	public String toString() {
		return "Speckle " + Arrays.toString(location());
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return keys.stream();
	}


	@Override
	public Image image() {
		return (Image) typeMap.get(origin)
				.stream()
				.map(l->l.target())
				.filter(n->				
				Image.class.isAssignableFrom(n.getClass())
						).findFirst().get();
	}

}
