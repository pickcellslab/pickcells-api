package org.pickcellslab.pickcells.api.datamodel.conventions;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;

/**
 * A Convenience Repertoire of {@link AKey} commonly used by {@link WritableDataItem}. These keys maybe used as 
 * conventions between distinct modules.
 * @author Guillaume Blin
 *
 */
public abstract class Keys {
	
	/**
	 * The name of an item
	 */
	public static final  AKey< String> name = AKey.get("name", String.class);
	
	/**
	 * A generic key to obtain a type of some sort
	 */
	public static final  AKey< String> type = AKey.get("type", String.class);
		
	
	/**
	 * This key represents a confidence score of some sort, for instance an association between two
	 * {@link CellComponents} has confidence score for the association calculated by the module that computed
	 * the association.
	 */
	public static final  AKey< Double> confidence = AKey.get("confidence", Double.class);
	
	/**
	 * This key represents a distance between 2 objects. This key is used for instance in links of
	 * type ADJACENT_TO to store the distance between 2 adjacent object
	 */
	public static final  AKey< Double> distance = AKey.get("distance", Double.class);
	
	
	/**
	 * This AKey can be found in some Links which connect objects that can be rendered in a 3d scene. 
	 * The attribute it refers to corresponds to the anchor point in space of the link to the source.
	 */
	public static final  AKey< float[]> sourceAnchor = AKey.get("Source Anchor Point", float[].class);
	
	/**
	 * This AKey can be found in some Links which connect objects that can be rendered in a 3d scene. 
	 * The attribute it refers to corresponds to the anchor point in space of the link to the target.
	 */
	public static final  AKey< float[]> targetAnchor = AKey.get("Target Anchor Point", float[].class);

	public static final AKey<Boolean> isborder = AKey.get("Is Border", Boolean.class);
	
	
	/**
	 * Found in {@link SegmentedObject} and points to coordinates of the bounding box minimum of the object in image space
	 */
	public static  AKey<long[]> bbMin = AKey.get("bounding box minimum", long[].class);
	
	/**
	 * Found in {@link SegmentedObject} and points to coordinates of the bounding box maximum of the object in image space
	 */
	public static  AKey<long[]> bbMax = AKey.get("bounding box maximum", long[].class);

	/**
	 * An attribute representing a vector in euclidian space. This is a key used in polarity analysis for example
	 */
	public static  AKey<double[]> vector = AKey.get("Vector",double[].class);
	
	
	/**
	 * The Center of an object in image coordinates, omitting the channel dimension (ordering is the actual image dimension ordering)
	 */
	public static AKey<long[]> location = AKey.get("Location", long[].class);
	
	/**
	 * The centroid of an object in calibrated space, omitting the channel dimension (ordering is xyzt)
	 */
	public static AKey<double[]> centroid = AKey.get("Centroid", double[].class);
	
	/**
	 * Second moments of an object shape in real space
	 */
	public static AKey<double[]> second = AKey.get("Second Moment", double[].class);
	
	/**
	 * Volume of an object in real space
	 */
	public static AKey<Double> volume = AKey.get("volume", Double.class);
	
	public static final AKey<Integer> trackID = AKey.get("Track ID", Integer.class);
	
	
}
