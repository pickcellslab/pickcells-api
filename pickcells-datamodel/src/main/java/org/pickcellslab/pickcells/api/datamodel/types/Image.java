package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;


/**
 * 
 * A {@link DataNode} dedicated to store information regarding an image imported in an {@link Experiment}
 * in PickCells. It stores for example the dimensions of the image, the calibration of the voxels
 * and provides method to translate raw image coordinates into real space coordinates (taking into account
 * the number of dimensions of the image).
 * 
 * @author Guillaume Blin
 *
 */
@Data(typeId="Image", icon="/icons/image_icon.png")
@Scope(name="WORKBENCH", parent="IMAGE-RESOURCES", policy=ScopePolicy.SIBLING)
public final class Image extends DataNode implements DataRoot{


	/** 
	 * The type of the {@link Link} to the {@link RawFile} it was created from
	 */
	public static String RAW_FILE_LINK = "OBTAINED_FROM";

	/**
	 * An int[5] which represents the "remapping" of indices from xyczt (mapped to -1 when the dimension is absent)
	 * For example if the original image is xyztc, then the array is
	 * {0,1,3,4,2}, if the image is xyz, then the array is {0,1,-1,2,-1}...
	 */
	private static   AKey<int[]> order = AKey.get("order", int[].class);
	/**
	 * A double[5] storing voxel sizes, ordered as xyczt, the default value is 1 if the dimension does not exist.
	 */
	private static   AKey<double[]> calibration = AKey.get("calibration", double[].class);

	/**
	 * A long[5] storing the dimensions of the image using the image dimension ordering
	 */
	private static   AKey<long[]> dimensions = AKey.get("dimensions", long[].class);

	public static   AKey<String> sizeUnit = AKey.get("size unit", String.class);	
	public static   AKey<String> timeUnit = AKey.get("time unit", String.class);

	public static   AKey<Integer> bits = AKey.get("bit depth per channel", Integer.class);

	public static   AKey<String[]> cNames = AKey.get("channel names", String[].class);
	public static   AKey<String[]> cLuts = AKey.get("channel luts", String[].class);


	public static   AKey<String> loc = AKey.get("location", String.class);


	private MinimalImageInfo info;


	/**
	 * Image dimensions
	 */
	//WARNING any change here will result in incompatibilities with previous versions!
	public static final int x=0, y=1, c=2, z=3, t=4; 



	private static Collection<AKey<?>> minimal = new ArrayList<>();
	static{
		minimal.add(Keys.name);
		minimal.add(loc);
		minimal.add(cNames);
		minimal.add(dimensions);	
		minimal.add(bits);	
		minimal.add(order);
		minimal.add(calibration);
	}


	private Image(){
		//Only accessible by the database or by an ImageBuilder;
	}


	/**
	 * @return The absolute file path (including name of the file and its extension) 
	 */
	public String path(){
		return this.getAttribute(loc).get()+File.separator+this.getAttribute(Keys.name).get();
	}


	/**
	 * @return {@code true} if this image is a z-stack {@code false otherwise}
	 */
	public boolean isZStack(){
		return this.getAttribute(dimensions).get()[z]!=1;
	}
	/**
	 * @return {@code true} if this image is a time lapse {@code false otherwise}
	 */
	public boolean isTimeLapse(){
		return this.getAttribute(dimensions).get()[t]!=1;
	}

	/**
	 * @return the width in pixels of this image
	 */
	public long width(){
		return this.getAttribute(dimensions).get()[x];
	}
	/**
	 * @return the bit depth per channel of this image
	 */
	public int bitDepth(){
		return this.getAttribute(bits).get();
	}
	/**
	 * @return the height in pixels of this image
	 */
	public long height(){
		return this.getAttribute(dimensions).get()[y];
	}
	/**
	 * @return the depth in pixels of this image
	 */
	public long depth(){
		return this.getAttribute(dimensions).get()[z];
	}
	/**
	 * @return The number of time frame in this image
	 */
	public long frames(){
		return this.getAttribute(dimensions).get()[t];
	}


	/**
	 * @return The time interval between 2 frames 
	 */
	public double frameInterval(){
		return this.getAttribute(calibration).get()[t];
	}
	/**
	 * @return the width of one pixel of this image (unit is given by pxUnit())
	 */
	public double pxWidth(){
		return this.getAttribute(calibration).get()[x];
	}
	/**
	 * @return the height of one pixel of this image (unit is given by pxUnit())
	 */
	public double pxHeight(){
		return this.getAttribute(calibration).get()[y];
	}
	/**
	 * @return the depth of one pixel of this image (unit is given by pxUnit())
	 */
	public double pxDepth(){
		return this.getAttribute(calibration).get()[z];
	}

	/**
	 * Computes the volume (or area if 2d) of a voxel
	 * @return the computed value
	 */
	public double voxelVolume(){
		double v = 1;
		double[] cal = getAttribute(Image.calibration).get();
		for(int i=0; i<cal.length-1; i++)//-1 to avoid time
			if(cal[i]!=-1)
				v*=cal[i];
		return v;
	}


	/**
	 * @return The names of the channels in this Image (in order)
	 */
	public String[] channels(){
		String[] names = this.getAttribute(cNames).get();
		return Arrays.copyOf(names, names.length);
	}

	/**
	 * @return The names of the luts for each channel in order. Note that names match the names of enum in the image module
	 */
	public String[] luts(){
		String[] luts = this.getAttribute(cLuts).get();
		return Arrays.copyOf(luts, luts.length);
	}


	/**
	 * @return  An int[5] which represents the "remapping" of indices from xyczt. 
	 * For example if the original image is xyztc, then the array is
	 * {0,1,4,2,3}, if the image is xyz, then the array is {0,1,-1,3,-1}...
	 *
	 */
	public int[] order(){
		int[] order = (int[]) this.attributes.get(Image.order);
		return Arrays.copyOf(order, order.length);
	}



	/**
	 * @return The {@link SegmentationResult} created from this image
	 */
	public List<SegmentationResult> segmentations(){
		return getLinks(Direction.INCOMING, Links.COMPUTED_FROM)
				.filter(l-> SegmentationResult.class.isAssignableFrom(l.source().getClass()))
				.map(l -> (SegmentationResult)l.source())
				.collect(Collectors.toList());
	}




	/**
	 * @param channel The name of the channel of interest
	 * @return An {@link Optional} holding the SegmentationResult corresponding to the specified channel
	 */
	public Optional<SegmentationResult> getSegmentation(String type){

		if(null == type)
			return Optional.empty();

		return getLinks(Direction.INCOMING, Links.COMPUTED_FROM)
				.filter(l-> {
					if(SegmentationResult.class.isAssignableFrom(l.source().getClass()))
						for(String asso : ((SegmentationResult)l.source()).associatiedObjects())
							if(asso.equals(type))
								return true;
					return false;
				})
				.map(l -> (SegmentationResult)l.source())
				.findFirst();		
	}



	/**
	 * @param includeChannel Whether or not to include the channel dimension in the count
	 * @param includeTime	Whether or not to include the time dimension in the count
	 * @return The number of dimensions in the image taking into consideration the specified parameters. 
	 */
	public int numDimensions(boolean includeChannel, boolean includeTime){
		int[] o = this.order();
		if(!includeChannel)
			o[Image.c] = -1;
		if(!includeTime)
			o[Image.t] = -1;
		int numDimensions = 0;
		for(int i = 0; i<o.length; i++)
			if(o[i]!=-1)
				numDimensions++;
		return numDimensions;
	}

	/**
	 * @return The index of the channel dimension in coordinate arrays or -1 if no channels found
	 */
	public int channelIndex(){
		return getAttribute(order).get()[c];
	}

	/**
	 * @return The index of the depth dimension in coordinate arrays or -1 if image is 2d
	 */
	public int depthIndex(){
		return getAttribute(order).get()[z];
	}

	/**
	 * @return The index of the time dimension in coordinate arrays or -1 if the image is not a time lapse
	 */
	public int timeIndex(){
		return getAttribute(order).get()[t];
	}





	/**
	 * @return A copy of the double[5] array holding the calibration values for this image in each dimension in xyczt order
	 */
	public double[] calibration(){
		return Arrays.copyOf((double[])this.attributes.get(calibration), 5);
	}

	/**
	 * @return A copy of the long[5] array holding the size of each dimension for this image in xyczt order
	 */
	public long[] dimensions(){
		return Arrays.copyOf((long[])this.attributes.get(dimensions), 5);
	}


	/**
	 * @param includeChannel Whether or not to include the channel dimension
	 * @param includeTime	Whether or not to include the time dimension
	 * @return A double[] holding the voxel size in each requested dimension. NB: The length of the returned array depends on
	 * the actual number of dimensions in the image. For example if you choose to include time bot not channel and the image is xyct,
	 * the length will be 4; if the image is xyt, the length will be 3, if the image is xycz, the length will be 3 and if the image is xy, the length will be 2
	 * This logic is consistent across the methods in Image.  
	 * @deprecated Use {@link MinimalImageInfo} instead
	 */
	public double[] calibration(boolean includeChannel, boolean includeTime){

		int[] o = this.order();
		if(!includeChannel)
			o = Image.removeDimension(o, Image.c);
		if(!includeTime)
			o = Image.removeDimension(o, Image.t);

		int numDimensions = 0;
		for(int i = 0; i<o.length; i++)
			if(o[i]!=-1)
				numDimensions++;


		double[] cal = (double[]) this.attributes.get(calibration);
		double[] fcal = new double[numDimensions];

		for(int i = 0; i<o.length; i++){
			if(o[i] == -1)
				continue;			
			fcal[o[i]] = cal[i]; 
		}		
		return fcal;

	}



	/**
	 * @param includeChannel Whether or not to include the channel dimension
	 * @param includeTime	Whether or not to include the time dimension
	 * @return A long[] holding the number of voxels in each requested dimension. NB: The length of the returned array depends on
	 * the actual number of dimensions in the image. For example if you choose to include time bot not channel and the image is xyct,
	 * the length will be 4; if the image is xyt, the length will be 3, if the image is xycz, the length will be 3 and if the image is xy, the length will be 2
	 * This logic is consistent across the methods in Image.  
	 * @see #numDimensions(boolean, boolean)
	 * @see #calibration(boolean, boolean)
	 * @deprecated Use {@link MinimalImageInfo} instead
	 */
	public long[] dimensions(boolean includeChannel, boolean includeTime){

		//int[] orig = this.order();
		int[] o = this.order();
		if(!includeChannel)
			o = Image.removeDimension(o, Image.c);
		if(!includeTime)
			o = Image.removeDimension(o, Image.t);

		//System.out.println(Arrays.toString(o));

		int numDimensions = 0;
		for(int i = 0; i<o.length; i++)
			if(o[i]!=-1)
				numDimensions++;


		long[] dims = new long[numDimensions];
		long[] allDims = (long[]) this.attributes.get(dimensions);

		for(int i = 0; i<allDims.length; i++){
			if(o[i] == -1)
				continue;
			dims[o[i]] = allDims[i]; 
		}

		return dims;
	}



	/**
	 * Computes the calibrated coordinate from a given image coordinate.
	 * @param point The coordinate to be calibrated
	 * @param includeChannel Whether or not to include the channel dimension
	 * @param includeTime	Whether or not to include the time dimension
	 * @return A long[] holding the number of voxels in each requested dimension. NB: The length of the returned array depends on
	 * the actual number of dimensions in the image. For example if you choose to include time bot not channel and the image is xyct,
	 * the length will be 4; if the image is xyt, the length will be 3, if the image is xycz, the length will be 3 and if the image is xy, the length will be 2
	 * This logic is consistent across the methods in Image.  
	 * @see #numDimensions(boolean, boolean)
	 * @see #calibration(boolean, boolean)
	 * @deprecated Use {@link MinimalImageInfo} instead
	 */
	public double[] calibrated(long[] point, boolean includeChannel, boolean includeTime) throws IllegalArgumentException{

		if(point.length!=numDimensions(includeChannel, includeTime))
			throw new IllegalArgumentException("Wrong input array length : "+point.length+"; should be "+numDimensions(includeChannel, includeTime));

		double[] calibration = calibration(includeChannel, includeTime);
		double[] result = new double[numDimensions(includeChannel, includeTime)];

		for(int i = 0; i<calibration.length; i++)
			result[i] = (double)point[i]*calibration[i];

		return result;
	}



	/**
	 * Computes the calibrated coordinate from a given image coordinate.
	 * @param point The coordinate to be calibrated
	 * @param includeChannel Whether or not to include the channel dimension
	 * @param includeTime	Whether or not to include the time dimension
	 * @return A long[] holding the number of voxels in each requested dimension. NB: The length of the returned array depends on
	 * the actual number of dimensions in the image. For example if you choose to include time bot not channel and the image is xyct,
	 * the length will be 4; if the image is xyt, the length will be 3, if the image is xycz, the length will be 3 and if the image is xy, the length will be 2
	 * This logic is consistent across the methods in Image.  
	 * @see #numDimensions(boolean, boolean)
	 * @see #calibration(boolean, boolean)
	 * @deprecated Use {@link MinimalImageInfo} instead
	 */
	public float[] floatCalibrated(long[] point, boolean includeChannel, boolean includeTime) throws IllegalArgumentException{


		if(point.length!=numDimensions(includeChannel, includeTime))
			throw new IllegalArgumentException("Wrong input array length : "+point.length+"; should be "+numDimensions(includeChannel, includeTime));

		double[] calibration = calibration(includeChannel, includeTime);
		float[] result = new float[numDimensions(includeChannel, includeTime)];

		for(int i = 0; i<calibration.length; i++)
			result[i] = (float)point[i]*(float)calibration[i];

		return result;
	}






	/**
	 * Computes the calibrated coordinate from a given image coordinate.
	 * @param point The coordinate to be calibrated
	 * @param includeChannel Whether or not to include the channel dimension
	 * @param includeTime	Whether or not to include the time dimension
	 * @return A long[] holding the number of voxels in each requested dimension. NB: The length of the returned array depends on
	 * the actual number of dimensions in the image. For example if you choose to include time bot not channel and the image is xyct,
	 * the length will be 4; if the image is xyt, the length will be 3, if the image is xycz, the length will be 3 and if the image is xy, the length will be 2
	 * This logic is consistent across the methods in Image.  
	 * @see #numDimensions(boolean, boolean)
	 * @see #calibration(boolean, boolean)
	 * @deprecated Use {@link MinimalImageInfo} instead
	 */
	public double[] calibrated(double[] point, boolean includeChannel, boolean includeTime) throws IllegalArgumentException{

		if(point.length!=numDimensions(includeChannel, includeTime))
			throw new IllegalArgumentException("Wrong input array length : "+point.length+"; should be "+numDimensions(includeChannel, includeTime));

		double[] calibration = calibration(includeChannel, includeTime);
		double[] result = new double[numDimensions(includeChannel, includeTime)];

		for(int i = 0; i<calibration.length; i++)
			result[i] = point[i]*calibration[i];

		return result;
	}




	/**
	 * Computes the calibrated coordinate from a given image coordinate.
	 * @param point The coordinate to be calibrated
	 * @param includeChannel Whether or not to include the channel dimension
	 * @param includeTime	Whether or not to include the time dimension
	 * @return A long[] holding the number of voxels in each requested dimension. NB: The length of the returned array depends on
	 * the actual number of dimensions in the image. For example if you choose to include time bot not channel and the image is xyct,
	 * the length will be 4; if the image is xyt, the length will be 3, if the image is xycz, the length will be 3 and if the image is xy, the length will be 2
	 * This logic is consistent across the methods in Image.  
	 * @see #numDimensions(boolean, boolean)
	 * @see #calibration(boolean, boolean)
	 * @deprecated Use {@link MinimalImageInfo} instead
	 */
	public float[] floatCalibrated(float[] point, boolean includeChannel, boolean includeTime) throws IllegalArgumentException{

		if(point.length!=numDimensions(includeChannel, includeTime))
			throw new IllegalArgumentException("Wrong input array length : "+point.length+"; should be "+numDimensions(includeChannel, includeTime));

		double[] calibration = calibration(includeChannel, includeTime);
		float[] result = new float[numDimensions(includeChannel, includeTime)];

		for(int i = 0; i<calibration.length; i++)
			result[i] = (float)point[i]*(float)calibration[i];

		return result;
	}









	@Override
	public String toString() {
		return getAttribute(Keys.name).get();
	}



	@Override
	public Stream<AKey<?>> minimal() {
		return minimal.stream();
	}


	/**
	 * @return The {@link MinimalImageInfo} for this {@link Image}
	 */
	public MinimalImageInfo getMinimalInfo(){
		if(info == null){
			int[] order = (int[]) this.attributes.get(Image.order);
			String xyUnit = getAttribute(sizeUnit).orElse("Unknown");
			String tUnit = getAttribute(timeUnit).orElse("Unknown");
			String[] units = new String[5];
			if(order[Image.x] != -1) units[order[Image.x]] = xyUnit;
			if(order[Image.y] != -1) units[order[Image.y]] = xyUnit;
			if(order[Image.z] != -1) units[order[Image.z]] = xyUnit;
			if(order[Image.c] != -1) units[order[Image.c]] = "lut";
			if(order[Image.t] != -1) units[order[Image.t]] = tUnit;

			info = new MinimalImageInfo(					
					(long[])this.attributes.get(dimensions),
					order,
					(double[])this.attributes.get(calibration),
					units);
		}
		return info;
	}



	/**
	 * Allows to create an Image Instance from a given NodeItem, only if the given NodeItem is also defining an image
	 * @param item
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Optional<Image> createInstance(DataRegistry registry, NodeItem item){
		if(registry.classOf(item.typeId()) == Image.class){
			Image i = new Image();
			item.getValidAttributeKeys().forEach(k->i.setAttribute((AKey)k, item.getAttribute(k).get()));
			return Optional.of(i);
		}else
			return Optional.empty();		
	} 





	/**
	 * A convenience method to create an int[5] as per {@link #order()} from an original order with one dimension removed
	 * @param order The original int[5] containing the positions of each dimension
	 * @param d one of {@link #x}, {@link #y}, {@link #c}, {@link #z}, {@link #t}.
	 * @return A new int[5] with one of the dimensions removed
	 * @deprecated Use {@link MinimalImageInfo} instead
	 */
	public static int[] removeDimension(int[] order, int d){

		if(order[d] == -1)
			return order;

		int[] updated = new int[order.length];
		int c = order[d];	
		for(int i = 0; i<order.length; i++){
			if(order[i]>c)
				updated[i] = order[i] -1;
			else
				updated[i] = order[i];
		}
		updated[d] = -1;

		return updated;
	}



	/**
	 * @param ordering as {@link Image#order()}
	 * @return the number of dimensions to be expected from an image giving the provided ordering.
	 */
	public static int numDimensions(int[] ordering) {
		int numD = 5;
		for(int i = 0; i<ordering.length; i++)
			if(ordering[i] == -1)	numD--;
		return numD;		
	}





	/**
	 * Formats the provided coordinate according to the given order with the specified options (includeChanne, includeTime).
	 * Removes dimensions if required.
	 * @param p The coordinates to format. Must have the same length as valid dimensions in order (!=-1).
	 * @param order An int[5] as described in {@link #order()}
	 * @param includeChannel
	 * @param includeTime
	 * @return A reformatted coordinate
	 */
	public static long[] format(long[] p, int[] order, boolean includeChannel, boolean includeTime){

		int[] o = order;
		if(!includeChannel)
			o = Image.removeDimension(o, Image.c);
		if(!includeTime)
			o = Image.removeDimension(o, Image.t);

		//System.out.println(Arrays.toString(o));

		int numDimensions = 0;
		for(int i = 0; i<o.length; i++)
			if(o[i]!=-1)
				numDimensions++;


		long[] dims = new long[numDimensions];

		for(int i = 0; i<5; i++){
			if(o[i] == -1)
				continue;
			dims[o[i]] = p[order[i]]; 
		}

		return dims;
	}





	public static class ImageBuilder{

		private Image img = new Image();


		private SortedMap<Integer, String> cPos = new TreeMap<>();
		private Map<String, String> cLut = new HashMap<>();
		private double[] cal = new double[5];
		private int[] ordering = new int[5];
		private long[] dims = new long[5];


		private boolean rawFileSet;
		private boolean locSet;





		/**
		 * Initialise the building process of an {@link Image}
		 * @param name The name of the image file (including the extension)
		 * @param width The width (in px) of the image
		 * @param height The height (in px) of the image
		 * @param bitdepth The bitdepth (exponent) of the image
		 */
		public ImageBuilder(String name, long width, long height, int bitdepth){			

			Objects.requireNonNull(name);
			Objects.requireNonNull(ordering);

			Arrays.fill(cal, 1);//neutral calibration by default
			Arrays.fill(ordering, -1);// -1 means does not exist by default
			Arrays.fill(dims, 1);// only 1 by default

			dims[x] = width;
			dims[y] = height;

			img.setAttribute(Keys.name, name);
			img.setAttribute(Image.bits, bitdepth);

		}

		/**
		 * @param pos The index of the channel dimension in the image.
		 * @return The updated builder
		 */
		public ImageBuilder setChannelDimension(int pos){
			ordering[c] = pos;
			return this;
		}

		/**
		 * @param position The indice of the channel in the channel dimension
		 * @param name
		 * @param lut
		 * @return
		 */
		public ImageBuilder setChannel(int position, String name, String lut){
			cPos.put(position, name);
			cLut.put(name, lut);
			return this;
		}

		public ImageBuilder setUnits(double pxWidth, double pxHeight, String unit){
			Objects.requireNonNull(unit);
			cal[x] = pxWidth;
			cal[y] = pxHeight;
			ordering[x] = 0;
			ordering[y] = 1;
			img.setAttribute(Image.sizeUnit, unit);
			return this;
		}

		public ImageBuilder setRawFile(RawFile raw){
			new DataLink(RAW_FILE_LINK, img, raw, true);
			rawFileSet = true;
			return this;			
		}

		/**
		 * @param location The URL or path of the parent directory of the image.
		 * @return The updated Builder
		 */
		public ImageBuilder setLocation(String location){
			img.setAttribute(Image.loc, location);
			locSet = true;
			return this;
		}

		/**
		 * @param sliceNumber The number of slices in the stack
		 * @param pxDepth The voxel size in the z dimension
		 * @param pos The position of the dimension in the actual image
		 * @return The updated builder
		 */
		public ImageBuilder setZ(long sliceNumber, double pxDepth, int pos){
			cal[z] = pxDepth;
			ordering[z] = pos;
			dims[z] = sliceNumber;
			return this;
		}

		public ImageBuilder setT(long frameNumber, double timeFrame, int pos, String unit){
			cal[t] = timeFrame;
			ordering[t] = pos;
			dims[t] = frameNumber;
			img.setAttribute(Image.timeUnit, unit);
			return this;
		}



		public Image build() throws IllegalStateException{

			//Check if mandatory setters have been done
			if(! (ordering[x]==x && rawFileSet && locSet))
				throw new IllegalStateException("Attempt to build an Image without setting mandatory fields"
						+ " (setUnits(), setRawFile() or setLocation())");


			//Check channels state
			if(!(cPos.size()>0 && cPos.firstKey()==0 && cPos.lastKey()==cPos.size()-1))
				throw new IllegalStateException("Attempt to build an Image with inadequate channels definition");

			//Fill the channels properties
			String[] names = new String[cPos.size()];
			String[] luts = new String[cPos.size()];
			for(int i = 0; i<cPos.size(); i++){
				names[i] = cPos.get(i);
				luts[i] = cLut.get(names[i]);
			}
			img.setAttribute(cNames, names);
			img.setAttribute(cLuts, luts);

			if(names.length!=1)
				dims[c] = names.length;//Number of channels in dimension

			//If more than one channel, make sure the position of the channel dimension was set.
			if(cPos.size()>1){
				if(ordering[c]==-1)
					throw new IllegalStateException("You have given info for multiple channels, but forgot to provide the dimension index!");
			}else if(ordering[c]!=-1)
				throw new IllegalStateException("You have provided info for only 1 channel, yet the position for the channel dimension was provided!");


			//Finally, check that dimensions ordering does not contain twice the same entry (not -1)
			//And that there is no discontinuity in dimensions (if 3 is found then the sum is 6 not considering -1 entries)
			int[] counts = new int[ordering.length];
			int sum = 0;
			int n = 0;
			for(int i=0; i< ordering.length; i++)
				if(ordering[i]!=-1){
					counts[ordering[i]]++;
					sum+=ordering[i];
					if(ordering[i]>n) n = ordering[i];
				}
			for(int i=0; i< counts.length; i++)
				if(counts[i]>1)
					throw new IllegalStateException("Error in the position of dimensions (duplicates) : "+Arrays.toString(ordering));

			//Check triangle number
			if(sum != n*(n+1)/2)
				throw new IllegalStateException("Error in the position of dimensions (continuity) : "+Arrays.toString(ordering));

			//Set attributes for dimensions
			img.setAttribute(Image.calibration,cal);
			img.setAttribute(Image.order, ordering);
			img.setAttribute(Image.dimensions, dims);

			return img;

		}


	}







}
