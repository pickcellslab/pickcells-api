package org.pickcellslab.pickcells.api.datamodel.types;

import java.io.File;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;

/**
 * 
 * Represents a <a href="https://pickcellslab.frama.io/docs/getstarted/walkthrough/#identify-nuclei">segmentation image</a> as a {@link NodeItem} in the graph database. 
 * 
 */
@Data(typeId="SegmentationResult", icon="/icons/segres_icon.png")
@SameScopeAs(Image.class)
public class SegmentationResult extends DataNode implements LabelsImage{

	
	private static final AKey<String> method = AKey.get("method", String.class);
	private static final AKey<Integer> bits = AKey.get("bit depth", Integer. class);
	
	private static final List<AKey<?>> keys = new ArrayList<>(4);
	static{	
		keys.add(idKey);
		keys.add(segType);
		keys.add(Keys.name);
		keys.add(bits);
		keys.add(LabelsImage.associated);
	}
		
	
	
	/**
	 * Creates a new {@link SegmentationResult} instance
	 * 
	 * @param img The {@link Image} the segmentation result has been computed from.
	 * @param filename The name of the segmentation image on the file system (including extension)
	 * @param type see {@link LabelsImage#segmentationType()}. Ensure that this type is unique.
	 * @param bitDepth The bit depth of the segmentation image
	 * @param methodDescription A description of the method that generated this new instance
	 */
	public SegmentationResult( Image img,  String filename, String type,  int bitDepth,  String methodDescription){
		this(img, img.getAttribute(Image.loc).get(), filename, type, bitDepth, methodDescription);
	}
	
	/**
	 * Creates a new {@link SegmentationResult} instance
	 * 
	 * @param img The {@link Image} the segmentation result has been computed from.
	 * @param parentDir the path to the parent directory where the segmentation image is located
	 * @param filename The name of the segmentation image on the file system (including extension)
	 * @param type see {@link LabelsImage#segmentationType()}. Ensure that this type is unique.
	 * @param bitDepth The bit depth of the segmentation image
	 * @param methodDescription A description of the method that generated this new instance
	 */
	public SegmentationResult( Image img, String parentDir, String filename, String type,  int bitDepth,  String methodDescription){
		Link l = new DataLink(Links.COMPUTED_FROM, this, img, true);
		l.setAttribute(method, methodDescription);
		setAttribute(Keys.name, filename);
		setAttribute(Image.loc, parentDir);
		setAttribute(segType, type);
		setAttribute(bits, bitDepth);
	}



	@SuppressWarnings("unused")
	private SegmentationResult(){
		//For the database
	}

	@Override
	public String path(){
		return getAttribute(Image.loc).orElse(origin().getAttribute(Image.loc).get())+File.separator+fileName();
	}
	
	
	@Override
	public String fileName(){
		return getAttribute(Keys.name).get();
	}
	
	@Override
	public String segmentationType(){
		return getAttribute(segType).orElse(fileName().split("_")[0]);
	}
	

	@Override
	public Image origin(){
		return (Image) getLinks(Direction.OUTGOING, Links.COMPUTED_FROM).iterator().next().target();
	}


	
	@Override
	public int bitDepth(){
		return getAttribute(AKey.get("bit depth", Integer. class)).get();
	}


	@Override
	public String toString() {
		return "Segmentation Result: "+fileName();
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return keys.stream();
	}



	@Override
	public String linkType() {
		return Links.CREATED_FROM;
	}



	


}
