package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;

@Data(typeId="Clustering")
@SameScopeAs(Image.class)
public class Clustering extends DataNode implements DataRoot{
	
	
	public static String links = "FOUND";

	protected static final List<AKey<?>> keys = new ArrayList<>(3);
	private static final AKey<String> param = AKey.get("parameters", String.class);	
	static{	
		keys.add(idKey);
		keys.add(Keys.name);
		keys.add(Keys.type);
		keys.add(param);
	}
	
	@SuppressWarnings("unused")
	private Clustering(){
		//Database
	}
	
	public Clustering(String name, String type, String parameters){
		setAttribute(Keys.name, name);
		setAttribute(Keys.type, type);
		setAttribute(param, parameters);
	}
	
	
	

	@Override
	public Stream<AKey<?>> minimal() {
		return keys.stream();
	}

	@Override
	public String toString() {
		return getAttribute(Keys.name).get() +"("+getAttribute(Keys.type).get()+")";
	}

}
