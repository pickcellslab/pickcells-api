package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;

@Data(typeId="RawFile", icon="/icons/rawfile_icon.png")
@SameScopeAs(Image.class)
public class RawFile extends DataNode{
	
	
	private static Collection<AKey<?>> min = new ArrayList<>(4);
	static{
		min.add(idKey);
		min.add(AKey.get("name", String.class));
		min.add(AKey.get("type", String.class));
		min.add(AKey.get("path", String.class));
		min.add(AKey.get("original dimensions", long[].class));
	}
	
	
	public RawFile( String name,  String type,  String path, long[] oriDims){
		setAttribute(AKey.get("name", String.class), name);
		setAttribute(AKey.get("type", String.class), type);
		setAttribute(AKey.get("path", String.class), path);
		setAttribute(AKey.get("original dimensions", long[].class), oriDims);
	}
	
	@SuppressWarnings("unused")
	private RawFile(){
		//For the database
	}
	
	
	public Image getImage(){
		return (Image) getLinks(Direction.INCOMING, Image.RAW_FILE_LINK).iterator().next().source();
	}
	
	public String getName(){
		return getAttribute(AKey.get("name", String.class)).get();
	}
	public String getType(){
		return getAttribute(AKey.get("type", String.class)).get();
	}
	public String getPath(){
		return getAttribute(AKey.get("path", String.class)).get();
	}
	
	public int[] originalDims(){
		return getAttribute(AKey.get("original dimensions", int[].class)).get();
	}
	
	@Override
	public String toString() {
		return "RawFile ("+getName()+")";
	}
	

	@Override
	public Stream<AKey<?>> minimal() {
		return min.stream();
	}
	
}
