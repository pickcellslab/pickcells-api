package org.pickcellslab.pickcells.api.datamodel.conventions;

import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.types.GenericSegmented;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedPrototype;

/**
 * Regroups conventions regarding the data model in PickCells
 * 
 * @author Guillaume Blin
 *
 */
public final class DataModel {

	private DataModel(){
		//cannot be instantiated
	}



	/**
	 * A tag which can be included in the description of a {@link MetaReadable} to indicate that this readable can
	 * considered as directional data. We mean by this a normalised 3 dimensional vector. Using this tag allows 
	 * the feature to be displayed and used in Directional plots or analyses.
	 */
	public static final String DIRECTIONAL = "DIRECTIONAL";


	/**
	 * A tag which can be included in the description of a {@link MetaReadable} to indicate that this readable can
	 * considered as a categorical type of data. This will allow the readable to be considered when a {@link MetaCategory}
	 * needs to be created.
	 */
	public static final String CATEGORICAL = "CATEGORICAL";


	public static final String DATAROOT = "DATASET_ROOT";



	/**
	 * @param type The declaredType as {@link NodeItem#declaredType()}
	 * @return An instance of a {@link SegmentedPrototype} for the given type (same as returned by SegmentedResult)
	 */
	public static SegmentedPrototype getPrototype(DataRegistry registry, String type){

		return registry.knownTypes(SegmentedPrototype.class).filter(dt->dt.getSimpleName().equals(type))
				.map(dt-> {
					try {
						return (SegmentedPrototype) dt.newInstance();
					} catch (InstantiationException | IllegalAccessException e) {
						e.printStackTrace();
						return null;
					}
				})
				.findAny().orElse(new GenericSegmented(type));

	}



}
