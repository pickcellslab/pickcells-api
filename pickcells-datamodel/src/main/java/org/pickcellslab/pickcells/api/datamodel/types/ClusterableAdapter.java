package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

import org.apache.commons.math3.ml.clustering.Clusterable;

/**
 * An adapter for objects to become {@link Clusterable}
 * 
 * @author Guillaume Blin
 *
 */
public class ClusterableAdapter<T> implements Clusterable{

	
	private final T o;
	private final Function<T, double[]> getter;


	/**
	 * Constructs a new {@link Clusterable} from the given object and the given coordinate provider 
	 * @param o The object to make Clusterable
	 * @param getter The Function capable of obtaining some coordinates for the {@link #getPoint()} method
	 */
	public ClusterableAdapter(T o, Function<T,double[]> getter){
		Objects.requireNonNull(o);
		Objects.requireNonNull(getter);
		this.o = o;
		this.getter = getter;
	}
	
	
	@Override
	public double[] getPoint() {
		return getter.apply(o);
	}
	
	
	/**
	 * @return The adapted object
	 */
	public T object(){
		return o;
	}
	
	
	/**
	 * @return The Function capable of obtaining some coordinates for the {@link #getPoint()} method
	 */
	public Function<T,double[]> function(){
		return getter;
	}
	
	
	
	
	

}
