package org.pickcellslab.pickcells.api.datamodel.types;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;

@Data(typeId="RNA", icon="/icons/rna_icon.png")
@SameScopeAs(Image.class)
public class RNA extends DataNode implements ImageDot {

	protected static final List<AKey<?>> keys = new ArrayList<>(6);

	static{	
		keys.add(idKey);
		keys.add(Keys.bbMin);
		keys.add(Keys.bbMax);
		keys.add(Keys.location);
		keys.add(Keys.centroid);
	}


	


	@Override
	public long[] location() {
		return getAttribute(Keys.location).get();
	}



	@Override
	public double[] centroid() {
		return getAttribute(Keys.centroid).get();
	}


	

	@Override
	public RNA create(Image image, long[] l) {
		RNA c = new RNA();
		new DataLink(origin, c, image, true);
		c.setAttribute(Keys.location, l);
		c.setAttribute(Keys.centroid, image.calibrated(l,false,false));		
		return c;
	}

	@Override
	public String toString() {
		return "RNA " + Arrays.toString(location());
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return keys.stream();
	}


	@Override
	public Image image() {
		return (Image) typeMap.get(origin)
				.stream()
				.map(l->l.target())
				.filter(n->				
				Image.class.isAssignableFrom(n.getClass())
						).findFirst().get();
	}
}
