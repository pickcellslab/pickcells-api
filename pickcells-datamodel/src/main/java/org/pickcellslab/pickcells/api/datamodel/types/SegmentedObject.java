package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.Optional;

import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;

/**
 * {@link SegmentedObject}s are {@link NodeItem}s created from the segmentation of an image.
 * <br>
 * As such they possess a label which is unique within the image where they are defined. They also possess a location as 
 * {@link SegmentedObject} extends {@link ImageLocated}. In addition, {@link SegmentedObject}s store features which describe the shape  / volume 
 * they represent. Finally, they can also be used with ImgIO to create thumbnails, meshes, contours etc to be displayed in visualisation modules. 
 *
 */
@Tag(tag = "SEGMENTED", creator = "PickCell Core Data Model", description = "An object extracted from a segmented image")
public interface SegmentedObject extends ImageLocated {

	public static AKey<Float> labelKey = AKey.get("label", Float.class);
	

	public static AKey<double[]> centroid = AKey.get("Centroid",double[].class);

	

	/**
	 * @return The {@link LabelsImage} from which this SementedObject was derived from
	 */
	public Optional<LabelsImage> origin();

	
		
	/**
	 * @return The label corresponding to this object inside the image
	 */
	public default Optional<Float> label(){
		return this.getAttribute(labelKey);
	}
	
	

	/**
	 * The centroid of the Segmented object in image coordinates (no channel, no time, 2D or 3D). 
	 * @see org.apache.commons.math3.ml.clustering.Clusterable#getPoint()
	 */
	public default double[] getPoint(){
		return this.getAttribute(centroid).get();
	}
	
	
	public default Image image(){
		Image image = null;
		LabelsImage l = origin().orElse(null);
		if(l!=null)
			image = l.origin();
		return image;
	}
	
	


}
