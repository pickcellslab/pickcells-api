package org.pickcellslab.pickcells.api.datamodel.types;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;


/**
 * {@link ImageLocated} objects are {@link NodeItem}s which possess a location in an {@link Image}
 * 
 */
public interface ImageLocated extends NodeItem {
	
	public static AKey<Integer> frameKey = AKey.get("frame", Integer.class);
	
	/**
	 * @return The index of the frame where this ImageLocated is found
	 */
	public default int frame(){
		return this.getAttribute(frameKey).orElse(0);
	}
	
	/**
	 * @return The location of the point in image coordinates and image dimensions order (excluding channel and time).
	 * @see #frame()
	 */
	public long[] location();
	
	/**
	 * @return The location of the object in real space in xyzt order (excluding channel) 
	 */
	public double[] centroid();
	
	/**
	 * @return The {@link Image) in which this {@link ImageDot} can be found
	 */
	public Image image();
	
	
}
