package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;

/**
 * An immutable helper class to store and manipulate image dimensions order. 
 * 
 * @author Guillaume Blin
 *
 */
public final class MinimalImageInfo {

	private final long[] dims;
	private final int[] order;
	private final double[] cal;
	private final String[] units;

	private long[] imgdims;
	private double[] imgcal;
	private String[] imgunits;


	/**
	 * @param order An int[5] as described by {@link Image#order}. 
	 * @param dims A long[] storing the number of pixels in each dimension of the image (in image coordinate)
	 */
	public MinimalImageInfo(int[] order, long[] dims) {

		assert order.length==5 : "order must be of length 5";

		this.order = Arrays.copyOf(order, order.length);

		// Convert dims to our convention
		this.dims = convert(dims, 1l);

		this.cal = new double[]{1,1,1,1,1};
		//Create default units
		this.units = new String[5];
		if(order[Image.x] != -1) units[order[Image.x]] = "um";
		if(order[Image.y] != -1) units[order[Image.y]] = "um";
		if(order[Image.z] != -1) units[order[Image.z]] = "um";
		if(order[Image.c] != -1) units[order[Image.c]] = "lut";
		if(order[Image.t] != -1) units[order[Image.t]] = "min";

	}



	/**
	 * @param order An int[5] as described by {@link Image#order}. 
	 * @param dims A long[] storing the number of pixels in each dimension of the image (in image coordinates)
	 * @param cal A double[] storing the scale factor for each dimension of the image (in image coordinates)
	 */
	public MinimalImageInfo(int[] order, long[] dims, double[] cal) {

		assert order.length==5 : "order be of length 5";
		assert dims.length == cal.length : "dims and cal must have the same length";


		this.order = Arrays.copyOf(order, order.length);
		this.dims = convert(dims, 1l);
		this.cal = convert(cal, 1d);

		//Create default units
		this.units = new String[5];
		if(order[Image.x] != -1) units[order[Image.x]] = "um";
		if(order[Image.y] != -1) units[order[Image.y]] = "um";
		if(order[Image.z] != -1) units[order[Image.z]] = "um";
		if(order[Image.c] != -1) units[order[Image.c]] = "lut";
		if(order[Image.t] != -1) units[order[Image.t]] = "min";

	}






	/**
	 * @param order An int[5] as described by {@link Image#order}. 
	 * @param dims A long[] storing the number of pixels in each dimension of the image (in image coordinates)
	 * @param cal A double[] storing the scale factor for each dimension of the image (in image coordinates)
	 * @param units A String[] storing the units for each dimension of the image (in image coordinates)
	 */
	public MinimalImageInfo(int[] order, long[] dims, double[] cal, String[] units) {

		assert order.length==5 : "order be of length 5";
		assert dims.length == cal.length && cal.length == units.length : "dims, cal and units must have the same length";

		this.order = Arrays.copyOf(order, order.length);

		this.dims = convert(dims, 1l);
		this.cal = convert(cal, 1d);
		this.units = convert(units, "None");
	}



	/**
	 * Here all parameters must be the "converted values", of length 5. -> Package protected to provide access to Image
	 * @param dims
	 * @param order
	 * @param cal
	 * @param units
	 */
	MinimalImageInfo(long[] dims, int[] order, double[] cal, String[] units) {
		this.dims = dims;
		this.order = order;
		this.cal = cal;
		this.units = units;
	}


	/**
	 * @param d One of {@link Image#x}, y, c, z, t
	 * @return The index of the given dimension in image coordinates or -1 if the desired dimension does not exist
	 */
	public final int index(int d){
		return order[d];
	}

	/**
	 * @param d One of {@link Image#x}, y, c, z, t
	 * @return The number of pixels in the given dimension
	 */
	public final long dimension(int d){
		if(order[d]==-1)
			return 1;
		return dims[d];
	}

	/**
	 * @param d One of {@link Image#x}, y, c, z, t
	 * @return The scale of the given dimension
	 */
	public final double calibration(int d){
		if(order[d]==-1)
			return 1;
		return cal[d];
	}

	/**
	 * @param d One of {@link Image#x}, y, c, z, t
	 * @return The unit system of the given dimension
	 */
	public final String unit(int d){
		if(order[d]==-1)
			return "None";
		return units[d];
	}


	public final int numDimensions(){
		return imgDims().length;
	}


	/**
	 * @return A long[] storing the number of pixels in each dimension in image order. The
	 * length of the returned array is the number of dimensions in the actual image, not to be mistaken with
	 * {@link Image#dimensions()}
	 */
	public final long[] imageDimensions(){
		return Arrays.copyOf(imgDims(), imgdims.length);
	}


	public final double[] imageCalibration(){
		return Arrays.copyOf(imgCal(), imgcal.length);
	}

	public final String[] imageUnits(){
		return Arrays.copyOf(imgUnits(), imgunits.length);
	}

	/**
	 * @param index Index of the desired dimension in image coordinates
	 * @return One of {@link Image#x}, y, c, z, t or -1 if the dimension is absent
	 */
	public final int axisType(int index){
		for(int i = 0; i<order.length; i++){
			if(order[i]==index)
				return i;
		}
		return -1;
	}



	private final long[] imgDims(){
		if(imgdims == null){
			int numDimensions = 0;
			for(int i = 0; i<order.length; i++)
				if(order[i]!=-1)
					numDimensions++;


			imgdims = new long[numDimensions];

			for(int i = 0; i<dims.length; i++){
				if(order[i] == -1)
					continue;
				imgdims[order[i]] = dims[i]; 
			}
		}
		return imgdims;
	}




	private final double[] imgCal(){
		if(imgcal == null){
			int numDimensions = 0;
			for(int i = 0; i<order.length; i++)
				if(order[i]!=-1)
					numDimensions++;


			imgcal = new double[numDimensions];

			for(int i = 0; i<dims.length; i++){
				if(order[i] == -1)
					continue;
				imgcal[order[i]] = cal[i]; 
			}
		}
		return imgcal;
	}



	private String[] imgUnits(){
		if(imgunits == null){
			int numDimensions = 0;
			for(int i = 0; i<order.length; i++)
				if(order[i]!=-1)
					numDimensions++;


			imgunits = new String[numDimensions];

			for(int i = 0; i<dims.length; i++){
				if(order[i] == -1)
					continue;
				imgunits[order[i]] = units[i]; 
			}
		}
		return imgunits;
	}








	/**
	 * @param point A location in uncalibrated image coordinates
	 * @return The calibrated coordinates.
	 */
	public double[] calibrated(long[] point){

		double[] calibration = imgCal();
		if(point.length!=calibration.length)
			throw new IllegalArgumentException("Illegal point length ("+point.length+"), should be "+calibration.length);

		double[] result = new double[point.length];		
		for(int i = 0; i<calibration.length; i++)
			result[i] = (double)point[i]*calibration[i];

		return result;
	}


	/**
	 * @param point A location in uncalibrated image coordinates
	 * @return The calibrated coordinates.
	 */
	public float[] floatcalibrated(long[] point){

		double[] calibration = imgCal();
		if(point.length!=calibration.length)
			throw new IllegalArgumentException("Illegal point length ("+point.length+"), should be "+calibration.length);

		float[] result = new float[point.length];		
		for(int i = 0; i<calibration.length; i++)
			result[i] = (float) ((float)point[i]*calibration[i]);

		return result;
	}




	/**
	 * @param d One of {@link Image#x}, y, c, z, t.
	 * @return A new {@link MinimalImageInfo} with the given dimension removed or this instance if the dimension
	 * did not exist in the first place
	 */
	public MinimalImageInfo removeDimension(int d){

		if(order[d] == -1)
			return this;

		int[] updated = new int[order.length];
		int c = order[d];	
		for(int i = 0; i<order.length; i++){
			if(order[i]>c)
				updated[i] = order[i] -1;
			else
				updated[i] = order[i];
		}
		updated[d] = -1;

		long[] newdims = Arrays.copyOf(dims, dims.length);			newdims[d] = 1;
		double[] newcal = Arrays.copyOf(cal, cal.length);			newcal[d] = 1;
		String[] newunits = Arrays.copyOf(units, units.length);		newunits[d] = null;

		return new MinimalImageInfo(newdims, updated, newcal, newunits);
	}









	public MinimalImageInfo addDimension(int d, int pos, double cal, long dim, String unit) throws IllegalArgumentException{

		if(pos > 4 || pos<0)
			throw new IllegalArgumentException("Position "+pos+" is invalid");

		// Check if this dimension does not already exist
		if(order[d]!=-1)
			throw new IllegalArgumentException("Dimension "+d+" already exists");

		int[] neworder = Arrays.copyOf(order, order.length);

		// Increment position of all dimensions >= pos
		for(int i = 0; i<neworder.length; i++)
			if(neworder[i]>=pos)
				neworder[i]++;

		// Add new dimension, pos, cal and unit
		order[d] = pos;
		long[] newdims = Arrays.copyOf(dims, dims.length);			newdims[d] = dim;
		double[] newcal = Arrays.copyOf(this.cal, this.cal.length);	newcal[d] = cal;
		String[] newunits = Arrays.copyOf(units, units.length);		newunits[d] = unit;

		return new MinimalImageInfo(newdims, neworder, newcal, newunits);

	}



	public MinimalImageInfo crop(long[] cropDims) {
		imgDims();
		assert cropDims.length == imgdims.length : "Illegal crop dimensionality : "+cropDims.length+" should be "+imgdims.length;
		return new MinimalImageInfo(order, cropDims, imgCal(), imgUnits());
	}


	public MinimalImageInfo resize(int d, long size)throws IllegalArgumentException{
		if(order[d]==-1)
			throw new IllegalArgumentException("Attempting to resize a dimension which does not exist");
		long[] newdims = Arrays.copyOf(dims, dims.length);
		newdims[d]+=size;
		if(newdims[d]<1)
			throw new IllegalArgumentException("Resizing results in a size < 1");
		return new MinimalImageInfo(newdims, order, cal, units);
	}




	public MinimalImageInfo duplicate(){
		return new MinimalImageInfo(dims, order, cal, units);
	}




	private long[] convert(long[] arr, long defaultValue) {
		long[] converted = new long[5];
		for(int i = 0; i<order.length; i++)
			if(order[i]!=-1)
				converted[i] = arr[order[i]];	
			else
				converted[i] = defaultValue;
		return converted;
	}

	private double[] convert(double[] arr, double defaultValue) {
		double[] converted = new double[5];
		for(int i = 0; i<order.length; i++)
			if(order[i]!=-1)
				converted[i] = arr[order[i]];	
			else
				converted[i] = defaultValue;
		return converted;
	}

	private String[] convert(String[] arr, String defaultValue) {
		String[] converted = new String[5];
		for(int i = 0; i<order.length; i++)
			if(order[i]!=-1)
				converted[i] = arr[order[i]];	
			else
				converted[i] = defaultValue;
		return converted;
	}



	@Override
	public String toString(){
		String s = "Minimal Image Info:"+
				"\nOrder "+Arrays.toString(order)+
				"\nDimensions "+Arrays.toString(dims)+
				"\nCalibration "+Arrays.toString(cal)+
				"\nDimensions "+Arrays.toString(units);
		return s;

	}



}
