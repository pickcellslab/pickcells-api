package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Optional;

import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;

/**
 * Defines an object representing a cell component such as an organelle, a membrane, a vesicle...
 * @see Cell
 * @author Guillaume Blin
 *
 */
@Tag(tag = "CELL_COMPONENT", creator = "PickCells Core Data Model", 
description = "An object representing a cell component such as an organelle, a membrane, a vesicle...")
public abstract class CellComponent extends DataNode {
	
	
	/**
	 * @return The maximum number of this CellComponent per cell that is allowed
	 * All the CellComponent belonging to the same class need to return the same value
	 */
	public abstract long maxNumberPerCell();
	




	/**
	 * @return {@code true} if this CellComponent is associated to a {@link Cell},
	 * {@code false} otherwise
	 */
	public final boolean isWithinACell() {
		return getCell().isPresent();
	}


	public final Optional<Cell> getCell(){
		return this.getLinks(Direction.OUTGOING, Links.CONTAINS)
				.map(l->(Cell)l.target()).findFirst();		
	}


	final void setCell(Link l) {
		this.addLink(l);
	}

	final void wasDetached(Link l) {
		this.removeLink(l, false);
	}


}
