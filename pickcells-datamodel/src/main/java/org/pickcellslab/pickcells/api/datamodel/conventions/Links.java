package org.pickcellslab.pickcells.api.datamodel.conventions;

import org.pickcellslab.pickcells.api.datamodel.types.CellComponent;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public abstract class Links {

	public static final String[] ALL_CONVENTIONAL = {
		"INCLUDES", "CONTAINS", "BELONGS_TO", "ASSOCIATED", "CREATED_FROM", "COMPUTED_FROM", "ADJACENT_TO", "TRACK"
	};
	
	/**
	 * This type of link is similar to {@link #CONTAINS} except that it indicates a relationship which is either
	 * a shortcut (Experiment includes Image while Experiment contains Sample contains Image), or a potentially transient
	 * relationship (Image includes Cluster, a Cluster being an entity that might be deleted at a later time)
	 */
	public static final String INCLUDES = ALL_CONVENTIONAL[0];
	
	/**
	 * This type of Link is used to define the hierarchy of the PickCells dataset:
	 * Experiment contains Samples contains Image contains Cell contains CellComponent.
	 */
	public static final String CONTAINS = ALL_CONVENTIONAL[1];
	/**
	 * This type of link is used to define a classification relationship. For example,
	 * Cell belongs to class X
	 */
	public static final String BELONGS = ALL_CONVENTIONAL[2];
	/**
	 * This type of link is used by Cell objects to point towards a {@link CellComponent}
	 * These links also store a confidence value
	 */
	public static final String ASSOCIATED = ALL_CONVENTIONAL[3];

	/**
	 * Defines a relation where the source has been created from the target
	 */
	public static final String CREATED_FROM = ALL_CONVENTIONAL[4];	
	/**
	 * Defines a relation where the source has been computed from the target
	 * (used by SegmentationResult)
	 */
	public static final String COMPUTED_FROM = ALL_CONVENTIONAL[5];
	
	/**
	 * Defines a relation where the source is a neighbour of its target and vice et versa (non directional)
	 * These links should also store a distance value
	 */
	public static final String ADJACENT_TO = ALL_CONVENTIONAL[6];

	/**
	 * Defines a time relationship between a source at time t to a target at time t+1;
	 */
	public static final String TRACK = ALL_CONVENTIONAL[7];
	
}
