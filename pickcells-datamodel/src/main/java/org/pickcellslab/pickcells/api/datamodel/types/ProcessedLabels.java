package org.pickcellslab.pickcells.api.datamodel.types;

import java.io.File;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;

/**
 * This class defines images storing the labels of objects which were deduced from a {@link SegmentationResult}
 * 
 * @author Guillaume Blin
 *
 */
@Deprecated
public class ProcessedLabels extends DataNode implements LabelsImage{

	
	public static final String TO_SEG = "DEDUCED_FROM";
	
	private static final AKey<String> method = AKey.get("method", String.class);
	private static final AKey<Integer> bits = AKey.get("bit depth", Integer. class);
	
	protected static final List<AKey<?>> keys = new ArrayList<>(3);
	static{	
		keys.add(idKey);
		keys.add(Keys.name);
		keys.add(segType);
		keys.add(bits);
		keys.add(LabelsImage.associated);
	}
		
	

	/**
	 * @param seg The {@link SegmentationResult} which was used to create the ProcessedLabels object
	 * @param filename The name of the image file on the disk
	 * @param bitDepth the bit depth of this Labelled image
	 * @param method A description of the method used to create this labelled image
	 */
	public ProcessedLabels(SegmentationResult seg, String filename, String type, int bitDepth, String method){

		Link l = new DataLink(TO_SEG, this, seg, true);
		l.setAttribute(ProcessedLabels.method, method);

		setAttribute(Keys.name, filename);
		setAttribute(Keys.type, type);
		setAttribute(bits, bitDepth);
	}



	@SuppressWarnings("unused")
	private ProcessedLabels(){
		//For the database
	}

	@Override
	public String fileName(){
		return getAttribute(Keys.name).get();
	}

	@Override
	public Image origin(){		
		return ((SegmentationResult) getLinks(Direction.OUTGOING, Links.COMPUTED_FROM).iterator().next().target()).origin();
	}


	
	@Override
	public int bitDepth(){
		return getAttribute(AKey.get("bit depth", Integer. class)).get();
	}


	@Override
	public String toString() {
		return "Processed Labels: "+fileName();
	}

	@Override
	public Stream<AKey<?>> minimal() {
		return keys.stream();
	}



	@Override
	public String linkType() {
		return "IDENTIFIED_IN";
	}



	@Override
	public String segmentationType() {
		return getAttribute(segType).get();
	}



	@Override
	public String path() {
		return origin().getAttribute(Image.loc).get()+File.separator+fileName();
	}





}
