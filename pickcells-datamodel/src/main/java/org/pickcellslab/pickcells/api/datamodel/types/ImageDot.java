package org.pickcellslab.pickcells.api.datamodel.types;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;

/**
 * This interface marks {@link NodeItem} objects representing a point in an image
 * @author Guillaume Blin
 *
 */
public interface ImageDot extends ImageLocated{

	
	/**
	 * The type of links which connect an ImageDot to its image of origin
	 */
	public static final String origin = "DETECTED_IN";
	
	public static final AKey<double[]> signal = AKey.get("Signal Strength",double[].class);
	
	
	
	/**
	 * @return A value representative of the signal strength of this ImageDot
	 */
	public default double[] signal() {
		return getAttribute(signal).get();
	}
	
	

	/**
	 * This factory method allows an {@link ImageDot} to act as a prototype to create other
	 * ImageDot of the same type but at a different location.
	 * @param image The image the ImageDot in which it was identified
	 * @param location The location of the ImageDot in image coordinates (channel ommited)
	 * @return A new Instance of the same class initialised with the specified image and location
	 */
	public ImageDot create(Image image, long[] location);
	
	
	
	
}
