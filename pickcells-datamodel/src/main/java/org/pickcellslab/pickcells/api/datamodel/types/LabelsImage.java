package org.pickcellslab.pickcells.api.datamodel.types;

import java.io.File;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;

import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;


/**
 * Represents an image on the disk which stores labels of segmented object from an {@link Image}. Note by convention, this {@link LabelImage}
 * has the same dimensions as its origin omitting the channel dimension (not multispectral)
 * 
 * @author Guillaume Blin
 *
 */
@Tag(creator = "PickCells Core", description = "This tag labels elements of the model which describe an image on the disk"
		+ " \nwhich contains objects labeled with a unique intensity value", tag = "LABEL_IMAGE")
public interface LabelsImage extends NodeItem {

	public static AKey<String> segType = AKey.get("Segmentation Type",String.class);
	
	public static AKey<String[]> associated = AKey.get("Associated Objects",String[].class);

	/**
	 * @return The color image this LabelsImage was derived from
	 */
	public Image origin();

	
	/**
	 * @return The absolute file path (including name of the file and its extension) 
	 */
	public String path();
	
	
	/**
	 * @return The name of the file on the disk (not the full path, just the name (including extension))
	 */
	public String fileName();

	/**
	 * @return A String which defines a group of segmentation results based on what the segmentation result labels ('nuclei' for example).
	 */
	public String segmentationType();	
	
	/**
	 * @return The type of the links connecting SegmentedObject contained in this LabelsImage
	 */
	public String linkType();

	/**
	 * @return The bit depth of the labels coding
	 */
	public int bitDepth();

	
	public default String[] associatiedObjects(){

		String[] s = getAttribute(associated).orElse(null);
		if(s==null)
			return new String[0];

		String[] r = new String[s.length];
		for(int i = 0; i<s.length; i++){
			// For legacy reasons process associated types to get simple name
			r[i] = s[i].substring(s[i].lastIndexOf(".")+1);
		}		
		return r;
	}



	public default void addAssociated(String type){
		
		String[] s = getAttribute(associated).orElse(null);
		if(s==null){
			setAttribute(associated, new String[]{type});
			return;
		}
		
		//check that it does not already contains this type
		for(String strg : s)
			if(strg.equals(type))
				return;
		
		//Add to the array
		String[] n = Arrays.copyOf(s, s.length+1);
		n[s.length] = type;
		setAttribute(associated,n);		
		
	}

	
	public default long[] dimensions() {
		Image i = origin();
		long[] oDims = i.dimensions();
		return Image.format(oDims, i.order(), false, true);
	}

}
