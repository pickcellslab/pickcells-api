package org.pickcellslab.pickcells.api.datamodel.types;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.DefaultData;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;

/**
 * This class is the representation of a Cell, it is a container for {@link CellComponent}. Cell objects
 * are linked to CellComponent via links of type {@link Links#POSSESS}. This type of link is reserved for
 * the above relationship and therefore cannot be used to link a Cell to anything else than a CellComponent. 
 * @author Guillaume Blin
 *
 */
public class Cell extends DefaultData implements NodeItem{

	/**
	 * Collection of the Links pointing to components
	 */
	private Map<Class<? extends CellComponent>, LinkedList<Link>> components = new HashMap<>();	
	/**
	 * This HashMap stores the links by direction ({@code <Direction, Set<Link>>})
	 */
	protected Map<Direction, Set<Link>> directionMap = new HashMap<>(2);
	/**
	 * This HashMap stores the links by type ({@code <link type, Set<Link>>})
	 */
	protected Map<String, Set<Link>> typeMap = new HashMap<>();


	public Cell(CellComponent first){
		//TODO
	}

	@SuppressWarnings("unused")
	private Cell(){
		//For the database
	}


	/**
	 * @param type The type of the CellComponents to be retrieved
	 * @return All the CellComponents of the specified type
	 * @see {@link DataModel}
	 */
	public Collection<CellComponent> getComponents(String type){	
		if(components.get(type) != null)
			return components.get(type).stream()
					.map(l->(CellComponent)l.source())
					.collect(Collectors.toList());	
		else return Collections.emptyList();
	}



	/**
	 * @param type The desired CellComponent Type
	 * @return An {@link Optional} holding the {@link CellComponent} with the specified type for 
	 * which the association confidence value is the highest
	 */
	public Optional<CellComponent> mostConfident(String type){		
		if(components.get(type) != null)
			return Optional.of((CellComponent)components.get(type).peekFirst().source());	
		else return Optional.empty();		
	}

	/**
	 * @param type The desired CellComponent Type
	 * @return An {@link Optional} holding the {@link CellComponent} with the specified type for 
	 * which the association confidence value is the lowest
	 */
	public Optional<CellComponent> leastConfident(String type){		
		if(components.get(type) != null)
			return Optional.of((CellComponent)components.get(type).peekLast().source());	
		else return Optional.empty();		
	}

	/**
	 * @param type The type of the CellComponent to count
	 * @return The number of {@link CellComponent} of the specified type contained in this Cell
	 */
	public int count(String type){
		if(components.get(type) != null)
			return components.get(type).size();
		else return 0;
	}

	/**
	 * {@inheritDoc}
	 * . Please note that adding a {@link CellComponent} as a source will throw an unchecked Exception.
	 * Please use {@link #addComponent(CellComponent, double)} instead.
	 */
	@Override
	public boolean addLink(Link link) {
		// test if link points to a CellComponent
		if(isCellComponent(link.target()))
			throw new IllegalArgumentException("A CellComponent cannot be the target of a link where a Cell is a source");
		if(isCellComponent(link.source())){
			//test that type is possess
			if(link.declaredType() != Links.CONTAINS)
				throw new IllegalArgumentException("A Link that assigns a CellComponent to a Cell must be of type "+Links.CONTAINS);
			//test if link as a confidence value
			if(!link.getValidAttributeKeys().anyMatch(k->k.equals(Keys.confidence)))
				throw new IllegalArgumentException("A Link that assigns a CellComponent to a Cell must possess a confidence attribute");

			//Passed the tests so assign			
			return addComponent(link);

		}else		
			return addNonComponentLink(link);
	}

	/**
	 * Sets the specified {@link CellComponent} in this Cell at the specified position
	 * @param component The CellCOmponent to assign to the Cell
	 * @param confidence the confidence value for this association
	 * @return An {@link Optional} holding the CellComponent with the lowest confidence value if
	 * the maximum number of the component type has been reached for this Cell.
	 */
	public Optional<CellComponent> addComponent(CellComponent component, double confidence){

		//Check that this component is not already in this Cell
		if(component.getCell().orElse(null) == this)
			return Optional.empty();

		//Init the Optional
		Optional<CellComponent> removed = null;

		//create the link
		Link l = new DataLink(Links.CONTAINS, this, component, false);
		l.setAttribute(Keys.confidence, confidence); 

		//Get the list to store the link
		LinkedList<Link> compLinks = components.get(component.getClass());
		if(compLinks == null)
			compLinks = new LinkedList<>();

		//If max has been reached, detach the lowest confidence
		if(compLinks.size() == component.maxNumberPerCell()){

			//If the provided confidence is higher than the lowest
			//then remove lowest, add the provided and sort for next time
			if(confidence > compLinks.peekLast().getAttribute(Keys.confidence).get()){

				Link r= compLinks.pollLast();
				removed = Optional.of((CellComponent)r.source());
				((CellComponent)r.source()).wasDetached(r);

				compLinks.add(l);
				compLinks.sort((l1,l2)-> l1.getAttribute(Keys.confidence).get().compareTo(l2.getAttribute(Keys.confidence).get()));
				component.setCell(l);

				directionMap.get(Direction.OUTGOING).remove(r);				
				directionMap.get(Direction.OUTGOING).add(l);

				typeMap.get(Links.CONTAINS).remove(r);
				typeMap.get(Links.CONTAINS).add(l);				

			}//Otherwise just return the provided component
			else
				return Optional.of(component);
		}//If still room for the component
		else{//Just add and sort for next time

			compLinks.add(l);
			compLinks.sort((l1,l2)-> l1.getAttribute(Keys.confidence).get().compareTo(l2.getAttribute(Keys.confidence).get()));
			component.setCell(l);

			directionMap.get(Direction.OUTGOING).add(l);
			typeMap.get(Links.CONTAINS).add(l);

		}

		return removed;
	}

	private boolean addComponent(Link l){

		if(addNonComponentLink(l)){//then it was never included before

			CellComponent component = (CellComponent) l.source();

			//Get the list to store the link
			LinkedList<Link> compLinks = components.get(component.getClass());
			if(compLinks == null)
				compLinks = new LinkedList<>();

			//If max has been reached, detach the lowest confidence
			if(compLinks.size() == component.maxNumberPerCell()){

				//If the provided confidence is higher than the lowest
				//then remove lowest, add the provided and sort for next time
				if(l.getAttribute(Keys.confidence).get() > compLinks.peekLast().getAttribute(Keys.confidence).get()){

					Link r= compLinks.pollLast();
					((CellComponent)r.source()).wasDetached(r);

					compLinks.add(l);
					compLinks.sort((l1,l2)-> l1.getAttribute(Keys.confidence).get().compareTo(l2.getAttribute(Keys.confidence).get()));
					component.setCell(l);

					directionMap.get(Direction.OUTGOING).remove(r);				
					directionMap.get(Direction.OUTGOING).add(l);

					typeMap.get(Links.CONTAINS).remove(r);
					typeMap.get(Links.CONTAINS).add(l);				

					return true;

				}//Otherwise just return the provided component
				else
					return false;
			}//If still room for the component
			else{//Just add and sort for next time

				compLinks.add(l);
				compLinks.sort((l1,l2)-> l1.getAttribute(Keys.confidence).get().compareTo(l2.getAttribute(Keys.confidence).get()));
				component.setCell(l);

				directionMap.get(Direction.OUTGOING).add(l);
				typeMap.get(Links.CONTAINS).add(l);

				return true;
			}		

		}else
			return false;

	}

	private boolean addNonComponentLink(Link link){

		boolean success = false;
		boolean typeWasPreviouslySeen =  typeMap.get(link.declaredType()) != null;

		if(!typeWasPreviouslySeen){  
			Set<Link> typeSet  = new LinkedHashSet<>();
			typeMap.put(link.declaredType(), typeSet);
		}

		if (link.source().equals(this)){
			success = directionMap.get(Direction.OUTGOING).add(link);        
			typeMap.get(link.declaredType()).add(link);
		}

		else if (link.target().equals(this)){
			success = directionMap.get(Direction.INCOMING).add(link);
			typeMap.get(link.declaredType()).add(link);
		}

		return success;

	}

	private boolean isCellComponent(Object o){
		return CellComponent.class.isAssignableFrom(o.getClass());
	}



	/**
	 * {@inheritDoc}
	 * . Please note that using this method to assign a {@link CellComponent} may throw an unchecked Exception
	 * depending on the state of this Cell objects and the rules defined by the CellComponent.
	 * Please use {@link #setComponent(CellComponent, int)} instead. Also the type cannot be Links.CONTAINS which is 
	 * reserved for CellComponents
	 */
	@Override
	public Link addOutgoing(String type, NodeItem target) {

		//Test if link type is possess
		if(type.equals(Links.CONTAINS))
			throw new IllegalArgumentException("It is forbidden to assign a link of type: "+Links.CONTAINS+" without a confidence value");
		//Test if target is CellComponent
		if(isCellComponent(target))
			throw new IllegalArgumentException("Cannot assign a CellComponent to a Cell without a confidence value. Please use"+
					" addComponent(CellComponent component, double confidence) instead");	

		return new DataLink(type, this, target, true);
	}

	/**
	 * {@inheritDoc}
	 * . Please note that adding a {@link CellComponent} as a source will throw an unchecked Exception.
	 * Please use {@link #setComponent(CellComponent, int)} instead.
	 */
	@Override
	public Link addIncoming(String type, NodeItem source) {

		//Test if link type is possess
		if(type.equals(Links.CONTAINS))
			throw new IllegalArgumentException("It is forbidden to assign a link of type: "+Links.CONTAINS+" without a confidence value");
		//Test if source is CellComponent
		if(isCellComponent(source))
			throw new IllegalArgumentException("Cannot assign a link where a CellComponent is the source and the Cell is a target. Please use"+
					" addComponent(CellComponent component, double confidence) instead");		
		return new DataLink(type, source, this, true);
	}


	@Override
	public Stream<Link> getLinks(Direction direction, String... linkTypes) {

		if(linkTypes.length == 0){
			if(direction != Direction.BOTH)
				return directionMap.get(direction).stream();			
			else 
				return Stream.concat(directionMap.get(Direction.INCOMING).stream(), directionMap.get(Direction.OUTGOING).stream());

		}
		else{

			final Set<String> types = new HashSet<>(Arrays.asList(linkTypes));

			if(direction != Direction.BOTH)
				return directionMap.get(direction).stream().filter(l->types.contains(l.declaredType()));	

			else 
				return Stream.concat(directionMap.get(Direction.INCOMING).stream(), directionMap.get(Direction.OUTGOING).stream()).filter(l->types.contains(l.declaredType()));

		}

	}

	@Override
	public boolean removeLink(Link link, boolean updateNeighbour) {
		boolean removed = typeMap.get(link.declaredType()).remove(link);		
		if(removed){
			removed = directionMap.get(Direction.INCOMING).remove(link);
			if(!removed){
				removed = directionMap.get(Direction.OUTGOING).remove(link);
				if(updateNeighbour)
					link.target().removeLink(link, false);
			}else if(updateNeighbour)
				link.source().removeLink(link, false);

			if(link.declaredType().equals(Links.CONTAINS)){
				CellComponent comp = (CellComponent) link.source();
				components.get(comp.getClass()).remove(link);
				comp.wasDetached(link);
			}

		}
		return removed;
	}


	@Override
	public Collection<Link> removeLink(Predicate<Link> predicate, boolean updateNeighbour) {
		List<Link> remove = getLinks(Direction.BOTH).filter(predicate).collect(Collectors.toList());
		remove.forEach(l->this.removeLink(l, updateNeighbour));
		return remove;
	}

	@Override
	public Stream<Link> links() {

		if(typeMap.isEmpty())
			return Stream.empty();

		final Iterator<Set<Link>> it = typeMap.values().iterator();		
		Stream<Link> all = it.next().stream();
		while(it.hasNext())
			all = Stream.concat(all, it.next().stream());

		return all;

	}

	
	@Override
	public String toString() {
		return "Cell";
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(idKey);
	}

	@Override
	public int getDegree(Direction direction, String linkType) {
		if(Direction.BOTH == direction){
			Set<Link> set = typeMap.get(linkType);
			if(null == set)
				return 0;
			else return set.size();
		}
		else 
			return directionMap.get(direction).stream().filter(l->l.declaredType().equals(linkType)).collect(Collectors.counting()).intValue();
	}

	@Override
	public String typeId() {
		return getClass().getTypeName();
	}

	@Override
	public boolean hasTag(String tag) {
		throw new RuntimeException("Not implemented");
	}

	


}
