package org.pickcellslab.pickcells.api.datamodel;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.RawFile;

/**
 * Unit test for {@link Image}
 */
public class ImageTest {


	static final long x = 10, y = 25, c = 2, z = 4, t = 19;

	static final Image xycz = 
			new Image.ImageBuilder("XYCZ",x,y,3)
			.setChannel(0, "Channel 1", "Red")
			.setChannel(1, "Channel 2", "Green")
			.setChannel(2, "Channel 3", "Blue")
			.setRawFile(new RawFile("Fake Raw file","fake Path","tif", new long[]{}))
			.setChannelDimension(2)
			.setUnits(0.2, 0.2, "microns")
			.setLocation("Fake location")
			.setZ(z, 0.8, 3)
			.build();

	static final Image xyz = 
			new Image.ImageBuilder("XYZ",x,y,3)
			.setUnits(0.2, 0.2, "microns")
			.setChannel(0, "Channel 1", "Red")// only one channel			
			.setZ(z, 0.8, 2)
			.setRawFile(new RawFile("Fake Raw file","fake Path","tif", new long[]{}))
			.setLocation("Fake location")
			.build();

	static final Image xytc = 
			new Image.ImageBuilder("XYTC",x,y,3)
			.setChannel(0, "Channel 1", "Red")
			.setChannel(1, "Channel 2", "Green")
			.setChannel(2, "Channel 3", "Blue")
			.setChannelDimension(3)
			.setUnits(0.2, 0.2, "microns")	
			.setT(t, 3, 2, "min")
			.setRawFile(new RawFile("Fake Raw file","fake Path","tif", new long[]{}))
			.setLocation("Fake location")
			.build();

	static final Image xytz = 
			new Image.ImageBuilder("XYTZ",x,y,3)
			.setUnits(0.2, 0.2, "microns")
			.setChannel(0, "Channel 1", "Red")// only one channel			
			.setZ(z, 0.8, 3)
			.setT(t, 3, 2, "min")
			.setRawFile(new RawFile("Fake Raw file","fake Path","tif", new long[]{}))
			.setLocation("Fake location")
			.build();



	@Test
	public void testBuilderOrdering(){

		int[] xyczOrder = xycz.order();
		assertTrue(xyczOrder[0]==0);
		assertTrue(xyczOrder[1]==1);
		assertTrue(xyczOrder[2]==2);
		assertTrue(xyczOrder[3]==3);
		assertTrue(xyczOrder[4]==-1);

		int[] xyzOrder = xyz.order();
		assertTrue(xyzOrder[0]==0);
		assertTrue(xyzOrder[1]==1);
		assertTrue(xyzOrder[2]==-1);
		assertTrue(xyzOrder[3]==2);
		assertTrue(xyzOrder[4]==-1);

		int[] xytOrder = xytc.order();
		assertTrue(xytOrder[0]==0);
		assertTrue(xytOrder[1]==1);
		assertTrue(xytOrder[2]==3);
		assertTrue(xytOrder[3]==-1);
		assertTrue(xytOrder[4]==2);

		int[] xytzOrder = xytz.order();
		assertTrue(xytzOrder[0]==0);
		assertTrue(xytzOrder[1]==1);
		assertTrue(xytzOrder[2]==-1);
		assertTrue(xytzOrder[3]==3);
		assertTrue(xytzOrder[4]==2);

	}



	/**
	 * Test exception thrown when a position is entered twice
	 */
	@Test(expected = IllegalStateException.class)
	public void testBuilderDuplicate(){

		new Image.ImageBuilder("XYTZ",512,512,3)
		.setUnits(0.2, 0.2, "microns")
		.setChannel(0, "Channel 1", "Red")// only one channel			
		.setZ(20, 0.8, 3)
		.setT(90, 3, 3, "min")
		.setRawFile(new RawFile("Fake Raw file","fake Path","tif", new long[]{}))
		.setLocation("Fake location")
		.build();
	}


	/**
	 * Test exception thrown when dimensions are not continuous (0,1,3) instead of (0,1,2).
	 */
	@Test(expected = IllegalStateException.class) 
	public void testBuilderContinuity(){

		new Image.ImageBuilder("XYZ",512,512,3)
		.setUnits(0.2, 0.2, "microns")
		.setChannel(0, "Channel 1", "Red")// only one channel			
		.setZ(20, 0.8, 3)
		.setRawFile(new RawFile("Fake Raw file","fake Path","tif", new long[]{}))
		.setLocation("Fake location")
		.build();
	}








	@Test
	public void testDimensions(){

		long[] xyczD = xycz.dimensions(true, true);
		assertTrue(xyczD[0] == x);
		assertTrue(xyczD[1] == y);		
		assertTrue(xyczD[2] == 3);
		assertTrue(xyczD[3] == z);
		assertTrue(xyczD.length == 4);
		
		long[] xyczDFalse = xycz.dimensions(true, false);
		assertTrue(xyczDFalse[0] == x);
		assertTrue(xyczDFalse[1] == y);		
		assertTrue(xyczDFalse[2] == 3);
		assertTrue(xyczDFalse[3] == z);
		assertTrue(xyczD.length == 4);
		
		
		long[] xyczDNC = xycz.dimensions(false, false);
		assertTrue(xyczDNC[0] == x);
		assertTrue(xyczDNC[1] == y);
		assertTrue(xyczDNC[2] == z);
		assertTrue(xyczDNC.length == 3);
		
		System.out.println("xyz "+Arrays.toString(xyz.dimensions()));
		long[] xyzD = xyz.dimensions(true, true);	System.out.println("xyzD "+Arrays.toString(xyzD));
		assertTrue(xyzD.length == 3);
		assertTrue(xyzD[0] == x);
		assertTrue(xyzD[1] == y);
		assertTrue(xyzD[2] == z);
		
		xyzD = xyz.dimensions(false, false);	System.out.println("xyzD "+Arrays.toString(xyzD));
		assertTrue(xyzD[0] == x);
		assertTrue(xyzD[1] == y);
		assertTrue(xyzD[2] == z);


		System.out.println("xytc "+Arrays.toString(xytc.dimensions()));
		long[] xytcD = xytc.dimensions(true, true);	System.out.println("xytcD "+Arrays.toString(xytcD));
		assertTrue(xytcD[0] == x);
		assertTrue(xytcD[1] == y);
		assertTrue(xytcD[2] == t);		
		assertTrue(xytcD[3] == 3);
		
		xytcD = xytc.dimensions(false, true); System.out.println("xyzD "+Arrays.toString(xyzD));
		assertTrue(xytcD[0] == x);
		assertTrue(xytcD[1] == y);
		assertTrue(xytcD[2] == t);




		long[] xytzD = xytz.dimensions(true, true);
		assertTrue(xytzD[0] == x);
		assertTrue(xytzD[1] == y);		
		assertTrue(xytzD[2] == t);
		assertTrue(xytzD[3] == z);		
		
		xytzD = xytz.dimensions(false, true);
		assertTrue(xytzD[0] == x);
		assertTrue(xytzD[1] == y);	
		assertTrue(xytzD[2] == t);
		assertTrue(xytzD[3] == z);

	}


	@Test
	public void testRemoveDimension(){
		// xyczt -> remove c
		int[] xyczt = {0,1,2,3,4};
		int[] xyzt = Image.removeDimension(xyczt, Image.c);
		assertTrue(xyzt[Image.c] == -1);
		assertTrue(xyzt[Image.z] == 2);
		assertTrue(xyzt[Image.t] == 3);

		// xycz -> remove c
		int[] xycz = {0,1,2,3,-1};
		int[] xyz = Image.removeDimension(xycz, Image.c);
		assertTrue(xyz[Image.c] == -1);
		assertTrue(xyz[Image.z] == 2);
		assertTrue(xyz[Image.t] == -1);
	}


	
	@Test
	public void testFormat(){
		
		int[] xytcz = new int[]{0,1,3,4,2}; 
		long[] pos = new long[]{10,10,3,1,5};
		long[] f1 = Image.format(pos, xytcz, false, false);
		long[] f2 = Image.format(pos, xytcz, false, true);
		
		System.out.println("f1 "+Arrays.toString(f1));
		assertTrue(f1.length == 3);
		assertTrue(f1[2] == 5);
		
		System.out.println("f2 "+Arrays.toString(f2));
		assertTrue(f2.length == 4);
		assertTrue(f2[2] == 3);
		
	}
	
	


	/**
	 * Check Exception thrown when channel requested but not present in input
	 */
	@Test(expected = IllegalArgumentException.class) 
	public void testOrderingExceptions1(){
		long[] xyczCoords = new long[]{x,y,c,z};
		xycz.calibrated(xyczCoords, false, true);
	}

	

	/**
	 * Check Exception thrown when length of provided array is wrong
	 */
	@Test(expected = IllegalArgumentException.class) 
	public void testOrderingExceptions3(){
		long[] xyzCoords = new long[]{x,y};
		xyz.calibrated(xyzCoords, false, false);
	}






}
