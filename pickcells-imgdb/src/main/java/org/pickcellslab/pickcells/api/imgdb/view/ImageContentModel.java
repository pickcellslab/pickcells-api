package org.pickcellslab.pickcells.api.imgdb.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dataviews.mvc.AbstractModel;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.RawFile;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.util.UsefulQueries;


public class ImageContentModel extends AbstractModel<Path<NodeItem,Link>>{




	private final ProviderFactoryFactory pff;
	private SegmentationManager sm;
	private DataAccess access;
	private Image currentImage;
	private File home;
	

	
	
	public ImageContentModel(DataAccess access, ProviderFactoryFactory pff) {
		this.pff = pff;
		this.access = access;
		try {
			home = new File(UsefulQueries.experimentFolder(access));
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public DataRegistry dataRegistry() {
		return access.dataRegistry();
	}



	@Override
	protected void dataSetHasChanged() {
		// Check which image we are dealing with
		List<MetaClass> mcs = new ArrayList<>();
		for(int i = 0; i<getDataSet().numQueries(); i++)
			mcs.addAll(getDataSet().queryInfo(i).get(QueryInfo.INCLUDED));

		try {


			currentImage = access.queryFactory().regenerate(RawFile.class).toDepth(2)
					.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
					.and(Image.RAW_FILE_LINK, Direction.INCOMING)
					.includeAllNodes().regenerateAllKeys().doNotGetAll().useFilter(getDataSet().queryInfo(0).get(QueryInfo.FIXED_ROOT)).run()
					.getOneDependency(Image.class).get();

			sm = new SegmentationManager(access.dataRegistry(), pff, currentImage, mcs);



		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	public SegmentationManager getSegmentationManager() {
		return sm;
	}

	
	public Image currentImage(){
		return currentImage;
	}
	
	public File containingFolder(){
		return home;
	}
	
	
	public static Predicate<MetaQueryable> renderableTypes(DataRegistry registry){
		return mq ->{
			if(mq instanceof MetaClass)
				return ImageLocated.class.isAssignableFrom(((MetaClass) mq).itemClass(registry));
			else
			{
				MetaLink ml = (MetaLink) mq;
				return 
						ImageLocated.class.isAssignableFrom(ml.source().itemClass(registry)) &&
						ImageLocated.class.isAssignableFrom(ml.target().itemClass(registry));
			}

		};
	}



	public static MetaReadable<?> fixedRootKey(DataAccess access) {
		return access.metaModel().getMetaModel("RawFile").metaKey(Keys.name).get();
	}


}
