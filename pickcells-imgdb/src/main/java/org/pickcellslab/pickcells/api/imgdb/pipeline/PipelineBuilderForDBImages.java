package org.pickcellslab.pickcells.api.imgdb.pipeline;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.JOptionPane;

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DefaultProcessingPipeline;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.ImgWriteManagerFactory;
import org.pickcellslab.pickcells.api.img.pipeline.InputsHandler;
import org.pickcellslab.pickcells.api.img.pipeline.MultiChannelImgWriteManagerFactory;
import org.pickcellslab.pickcells.api.img.pipeline.PipelineBuilder;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingPipeline;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingPipelineNode;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingRunner;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingStep;
import org.pickcellslab.pickcells.api.img.pipeline.SingleChannelImgWriteManagerFactory;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.RealType;

/**
 * An extension of {@link PipelineBuilder} which offers the possibility to process all images currently registered in a database
 * 
 * @author Guillaume Blin
 *
 * @param <T> The {@link Type} of the input image
 * @param <R> The {@link Type} of the result image
 */
public class PipelineBuilderForDBImages<T extends NativeType<T> & RealType<T>, R extends NativeType<R> & RealType<R>>
extends PipelineBuilder<T,R>{


	
	@Override
	public <N extends RealType<N> & NativeType<N>> PipelineBuilderForDBImages<T,N> step(ImgProcessing<R,N> step, String description){
		return (PipelineBuilderForDBImages<T, N>) super.step(step, description);
	}
	
	@Override
	public <N extends RealType<N> & NativeType<N>> PipelineBuilderForDBImages<T,N> steps(List<ImgProcessing<R,N>> steps, String description){
		return (PipelineBuilderForDBImages<T, N>) super.steps(steps, description);
	}
	
	
	

	public Optional<ProcessingPipeline<T,R>> buildForDBImages(
			UITheme theme,
			NotificationFactory notifier,
			ImgIO io,
			DataAccess access,
			Icon icon,
			String title){


		// Ask user which image to use for preview
		try {

			//Get images names
			//Read one image to obtain the name and luts of the channels
			List<String> rt =
					access.queryFactory().read(DataRegistry.typeIdFor(Image.class))
					.makeList(Keys.name).inOneSet().getAll().run();

			if(rt.isEmpty()){
				JOptionPane.showMessageDialog(null, "There are no images in the database");
				return Optional.empty();
			}

			//Display a dialog to allow the user to choose which image to load for preview
			String choice = (String) JOptionPane.showInputDialog(
					null,
					"Choose an image for preview",
					title,
					JOptionPane.PLAIN_MESSAGE,
					icon,
					rt.toArray(),
					rt.get(0)
					);


			if(choice == null)
				return Optional.empty();


			//Get all images from the database
			RegeneratedItems r = access.queryFactory().regenerate(DataRegistry.typeIdFor(Image.class)).toDepth(0).traverseAllLinks()
					.includeAllNodes().regenerateAllKeys().getAll();


			//Get the image chosen for preview
			Image preview = r.getTargets(Image.class)
					.filter(p->choice.equals(p.getAttribute(Keys.name).orElse(null)))
					.findAny().orElse(null);

			Img<T> previewedImage = io.open(preview);

			Pair<ProcessingPipelineNode, ProcessingStep<T, RandomAccessibleInterval<R>>> pipeline =
					this.showGuiAndGetPipeline(theme, notifier, io, previewedImage, preview.getMinimalInfo(), preview.order());

			if(pipeline != null){

				ProcessingPipelineNode<?,R> node = pipeline.getKey();
				
				// Create the runner
				ProcessingRunner<T,R, DBImageInput<T>> tm = new DBImageInputManager<>();

				//Create the ImgWriteManagerFactory
				ImgWriteManagerFactory<R> fctry = null;
				if(isFlattened)
					fctry = new SingleChannelImgWriteManagerFactory<>();
				else 
					fctry = new MultiChannelImgWriteManagerFactory<>();

				
				// Create the PreviewedImageHandler
				InputsHandler<T,R, DBImageInput<T>> handler = new PreviewedDBImageHandler<>(
						io,
						r.getTargets(Image.class).collect(Collectors.toList()),
						preview,
						previewedImage,
						node.getProcessedImageInfo(),
						node.getProcessedImage(),
						wasCropped);

				
				// Now return the ProcessingPipeline
				return Optional.of(new DefaultProcessingPipeline<>(theme, notifier, pipeline.getSecond(), node.getProcessedImageType(), tm, fctry, handler));
				
			}
			else
				return Optional.empty();
			
			

		}catch(DataAccessException e){
			//TODO
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return null;
	}


}
