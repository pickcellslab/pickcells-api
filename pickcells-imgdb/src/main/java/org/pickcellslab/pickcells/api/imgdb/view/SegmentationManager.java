package org.pickcellslab.pickcells.api.imgdb.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.providers.NotInProductionException;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.api.img.providers.SegmentationManagementException;

/**
 * 
 * Loads {@link ImageSegmentationProvider} on requests
 * 
 * @author Guillaume Blin
 *
 */

public class SegmentationManager {


	private final Map<String,SegmentationImageProvider<?>> segs = new HashMap<>();
	private final Map<String,LabelsImage> labels = new HashMap<>();
	private final ProviderFactory factory;
	private final ArrayList<MetaClass> included = new ArrayList<>();

	
	public SegmentationManager(DataRegistry registry, ProviderFactoryFactory io, Image i, List<MetaClass> included) throws Exception {
		if(!included.isEmpty()){
			factory = io.create(4);
			for(MetaClass mc : included){
				//System.out.println("Included : " + mc);
				if(SegmentedObject.class.isAssignableFrom(mc.itemClass(registry))){
					SegmentationResult sr = i.getSegmentation(mc.itemDeclaredType()).get();
					factory.addToProduction(sr);
					labels.put(mc.itemDeclaredType(),sr);
					this.included.add(mc);
				}
			}
		}
		else
			factory = null;
	}


	public Set<String> getIncluded(){
		return Collections.unmodifiableSet(segs.keySet());
	}
	
	
	public MetaClass getAssociated(String type){
		for(MetaClass mc : included)
			if(mc.itemDeclaredType().equals(type))
				return mc;
		return null;
	}


	public SegmentationImageProvider<?> getFor(String type) throws SegmentationManagementException{			

		try {

			SegmentationImageProvider<?> s = segs.get(type);	
			if(s == null){
				LabelsImage label = labels.get(type);
				if(label == null)
					throw new SegmentationManagementException("This type of object was never added to the production line "+type);				
				s = factory.get(label);
				
				//System.out.println("SegmentationManager -> s is null ? "+(s==null));
				//s.setPrototype(label, (SegmentedObject) clazz.newInstance());
				segs.put(type, s);				
			}

			
			
			return s;

		} catch (InterruptedException | ExecutionException e) {
			throw new SegmentationManagementException(e);
		} catch (NotInProductionException e) {
			throw new SegmentationManagementException(e);
		} 
	}
}


