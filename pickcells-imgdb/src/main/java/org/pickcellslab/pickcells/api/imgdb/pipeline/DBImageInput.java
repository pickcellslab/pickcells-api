package org.pickcellslab.pickcells.api.imgdb.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingInput;

import net.imglib2.RandomAccessibleInterval;

public class DBImageInput<T> implements ProcessingInput<T> {

	
	private final RandomAccessibleInterval<T> img;
	private final Image image;

	DBImageInput(RandomAccessibleInterval<T> img, Image image) {
		this.img = img;
		this.image = image;
	}
	
	
	@Override
	public RandomAccessibleInterval<T> getImg() {
		return img;
	}

	@Override
	public MinimalImageInfo getInfo() {
		return image.getMinimalInfo();
	}

	@Override
	public String getName() {
		String name = image.getAttribute(Keys.name).get();
		return name.substring(0, name.lastIndexOf('.'));
	}

	@Override
	public File getFolder() {
		return new File(image.path()).getParentFile();
	}
	
	public Image getAssociatedImage(){
		return image;
	}

}
