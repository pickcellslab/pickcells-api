package org.pickcellslab.pickcells.api.imgdb.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.ExtendedImageInfo;
import org.pickcellslab.pickcells.api.img.pipeline.ImgWriteManagerFactory;
import org.pickcellslab.pickcells.api.img.pipeline.InputsHandler;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingRunner;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingStep;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;

class PreviewedDBImageHandler<I,O extends Type<O>> implements InputsHandler<I, O, DBImageInput<I>> {

	private final ImgIO io;
	private final Image preview;
	private final List<Image> list;
	private RandomAccessibleInterval<I> prevImg;
	private MinimalImageInfo prevResultInfo;
	private RandomAccessibleInterval<O> prevResult;
	private final boolean wasCropped;

	public PreviewedDBImageHandler(
			ImgIO io,
			List<Image> list,
			Image preview,
			RandomAccessibleInterval<I> prevImg,
			MinimalImageInfo prevResultInfo,
			RandomAccessibleInterval<O> prevResult,
			boolean wasCropped) {
		this.io = io;
		this.list = list;
		this.preview = preview;		
		this.prevImg = prevImg;
		this.prevResultInfo = prevResultInfo;
		this.prevResult = prevResult;
		this.wasCropped = wasCropped;
		list.remove(preview);
	}

	@Override
	public <E extends Type<E>> InputsHandler<I, E, DBImageInput<I>> addStep(Function<RandomAccessibleInterval<O>, RandomAccessibleInterval<E>> step) {

		if(!wasCropped)// Add step only if we need to compute the pipeline for the original image
			return new PreviewedDBImageHandler<>(io, list, preview, null, prevResultInfo, step.apply(prevResult), wasCropped);
		else
			return new PreviewedDBImageHandler<>(io, list, preview, prevImg, null, null, wasCropped);

	}
	
	

	@Override
	public String nameOfPreview() {
		return preview.getAttribute(Keys.name).get();
	}
	
	

	@Override
	public ExtendedImageInfo<O> getPreviewedResult(ProcessingStep<I, RandomAccessibleInterval<O>> pipeline,
			ImgIO io,
			Function<String, String> namingConvention, ImgWriteManagerFactory<O> fctry,
			ProcessingRunner<I, O, DBImageInput<I>> tm, O outType) throws IOException {


		ExtendedImageInfo<O> result;
		final File outFolder = new File(preview.getAttribute(Image.loc).get());
		String name = preview.getAttribute(Keys.name).get();
		name = name.substring(0, name.lastIndexOf('.'));
		if(wasCropped){
			System.out.println("Running on preview image");			
			DBImageInput<I> input = new DBImageInput<>(prevImg, preview);			
			result = tm.run(pipeline, io, input, outType, outFolder, namingConvention.apply(name), fctry);
		}
		else{//save prevResult
			final String resultName = namingConvention.apply(name);
			final String ext = io.standard((RandomAccessibleInterval)prevResult);
			io.save(prevResultInfo, (RandomAccessibleInterval)prevResult, outFolder.getPath()+File.separator+resultName+ext);
			result = new ExtendedImageInfo<>(prevResultInfo, outFolder, resultName, ext, outType, preview);
		}
		prevImg=null;	prevResult=null;	prevResultInfo=null;	
		return result;
	}

	
	
	@Override
	public Iterator<DBImageInput<I>> iterator() {
		return new Iterator<DBImageInput<I>>(){

			final Iterator<Image> it = list.iterator();

			@Override
			public boolean hasNext() {
				return it.hasNext();
			}

			@Override
			public DBImageInput<I> next() {
				try{
					final Image image = it.next();
					return new DBImageInput(io.open(image), image);
				}
				catch(IOException e){
					throw new RuntimeException(e);
				}
			}

		};
	}


}
