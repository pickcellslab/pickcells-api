package org.pickcellslab.pickcells.api.app.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dataviews.fitting.Distribution;

/**
 * Objects which display {@link Distribution}s allowing the user to pick one and delegate
 * what to do with the picked distribution to {@link DistributionPickConsumer} 
 * 
 * @author Guillaume Blin
 *
 */
public interface DistributionPicker<T> {

	public void addDistributionPickListener(DistributionPickConsumer<T> l);
	
	public void removeDistributionPickListener(DistributionPickConsumer<T> l);
}
