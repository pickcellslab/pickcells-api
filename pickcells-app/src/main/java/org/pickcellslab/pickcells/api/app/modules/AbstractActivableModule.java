package org.pickcellslab.pickcells.api.app.modules;

/*-
 * #%L
 * foundationj-modules
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashSet;
import java.util.Set;

public abstract class AbstractActivableModule implements Activable {


	private Set<ActivationListener> lstrs = new HashSet<>();

	@Override
	public void registerListener(ActivationListener lst) {
		lstrs.add(lst);
	}

	/**
	 * Notifies {@link ActivationListener}s that this module changed its active state
	 */
	public void fireIsNowActive() {
		lstrs.forEach(l->l.isNowActive(this, isActive()));
	}

}
