package org.pickcellslab.pickcells.api.app.modules;

import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.pickcells.api.presentation.Sandbox;

/**
 * A Marker interface for Modules which provides the user with some function which can be accomplished
 * without being connected to the database
 * Such modules will be launchable via the sandbox desktop.
 * 
 * @author Guillaume Blin
 *
 */
@Modular
@SameScopeAs(Sandbox.class)
public interface DesktopModule extends Launchable, Documented, WithIcon{

}
