package org.pickcellslab.pickcells.api.app.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.dbm.access.DataPointer;

public class AbstractSeriesPicker implements SeriesPicker {

	protected final List<SeriesPickConsumer> sPickLstrs = new ArrayList<>();


	@Override
	public void addSeriesPickListener(SeriesPickConsumer l) {
		if(null!=l)
			sPickLstrs.add(l);
	}

	@Override
	public void removeSeriesPickListener(SeriesPickConsumer l) {
		sPickLstrs.remove(l);
	}

	
	protected void fireSeriesPick(String seriesId, DataPointer v){
		sPickLstrs.forEach(l->l.seriesPicked(seriesId, v));
	}
	
}
