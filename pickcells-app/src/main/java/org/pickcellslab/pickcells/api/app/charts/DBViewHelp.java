package org.pickcellslab.pickcells.api.app.charts;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.util.Optional;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.mvc.ReconfigurableView;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableDimensionsModel;
import org.pickcellslab.foundationj.dataviews.mvc.SelectableDimensionBroker;
import org.pickcellslab.pickcells.api.app.picking.DistributionPicker;
import org.pickcellslab.pickcells.api.app.picking.SeriesPicker;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPicker;
import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;

/**
 * A helper class to create a toolbar for a Chart or any data view 
 * 
 * @author Guillaume Blin
 *
 */
@Core
@SameScopeAs(Workbench.class)
public interface DBViewHelp {


	public <M extends RedefinableDimensionsModel<T,B,D>, B extends SelectableDimensionBroker<T,D>,D,T> Optional<Dimension<T,D>> 
	displayDimensionChoice(ReconfigurableView<M,T,B,D> view, String axis);
	
	
	public AppearanceFactory<Color> simpleColorFactory();
		

	public DBViewToolBarBuilder newToolBar();

	
	public void registerSingleConsumersFor(SingleDataPicker picker);

	
	public void registerSeriesConsumersFor(SeriesPicker picker);



	public <T> void registerDistributionConsumerFor(DistributionPicker<T> picker);
		
	


}

