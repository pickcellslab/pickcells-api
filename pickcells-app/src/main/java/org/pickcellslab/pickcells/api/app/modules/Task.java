package org.pickcellslab.pickcells.api.app.modules;

import org.pickcellslab.pickcells.api.presentation.workbench.WorkbenchElement;

/**
 * A marker interface for modules which are {@link Launchable}  and {@link Activable}.
 * 
 * @author Guillaume Blin
 *
 */
public interface Task extends Launchable, Activable, Categorized, Documented, WithIcon, WorkbenchElement{

}
