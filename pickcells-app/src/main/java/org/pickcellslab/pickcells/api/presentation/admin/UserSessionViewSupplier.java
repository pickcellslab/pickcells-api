package org.pickcellslab.pickcells.api.presentation.admin;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.pickcellslab.pickcells.api.presentation.SwingSupplier;

@Core
@Scope(name="USER-SESSION", parent="ADMIN-ACCESS", policy=ScopePolicy.SINGLETON)
public interface UserSessionViewSupplier extends SwingSupplier {

	
}
