package org.pickcellslab.pickcells.api.app.picking;

import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;

@Modular
@SameScopeAs(Workbench.class)
public interface SeriesPickConsumer{
	
	public void seriesPicked(String seriesId, DataPointer v);
	
	public String name();

}
