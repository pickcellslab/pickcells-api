package org.pickcellslab.pickcells.api.presentation;

import javax.swing.JComponent;

public interface SwingSupplier {

	public JComponent getPanel();
	
}
