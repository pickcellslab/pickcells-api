package org.pickcellslab.pickcells.api.app.modules;

import javax.swing.Icon;

public interface WithIcon {

	/**
	 * @return An {@link Icon} which can be used to display a launcher button in the GUI
	 */
	public Icon icon();
	
}
