package org.pickcellslab.pickcells.api.app.charts;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer.Mode;
import org.pickcellslab.foundationj.dataviews.fitting.DistributionFitterFactory;
import org.pickcellslab.foundationj.dataviews.mvc.AbstractDimensionalModel;
import org.pickcellslab.foundationj.dataviews.mvc.FittableModel;
import org.pickcellslab.foundationj.dataviews.mvc.TransformFactory;
import org.pickcellslab.foundationj.dataviews.mvc.VectorDimensionBroker;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;

public class VectorChartModel <T> extends AbstractDimensionalModel<T,VectorDimensionBroker<T>,double[]> implements FittableModel<T,double[],double[]> {

	
	
	

	private final Predicate<Integer> allowedDimensionality;
	private final List<DistributionFitterFactory<?,double[],?,?>> fitters;
	private final TransformFactory<T, double[]> transFactory;


	public VectorChartModel(Predicate<Integer> allowedDimensionality,
			List<DistributionFitterFactory<?,double[],?,?>> fitters, 
			TransformFactory<T,double[]> transFactory) {
		Objects.requireNonNull(allowedDimensionality,"predicate is null");
		Objects.requireNonNull(fitters,"fitters is null");
		Objects.requireNonNull(transFactory,"factory type is null");
		this.transFactory = transFactory;
		this.allowedDimensionality = allowedDimensionality;	
		this.fitters = fitters;
	}

	boolean dimensionalityRespected(int dimensionality){
		return this.allowedDimensionality.test(dimensionality);
	}

	
	@Override
	public VectorDimensionBroker<T> createBroker(List<Dimension<T, double[]>> dims) {
		
		if(dims.size() == 0)
			throw new RuntimeException("provided dimensions empty!");

		VectorDimensionBroker<T> broker = new VectorDimensionBroker<T>(dims, getDataSet(), transFactory, fitters);
		return broker;
		
		
	}

	@Override
	public Predicate<DimensionContainer<T, ?>> dimensionPredicate() {
		return p ->{
			
			if(!allowedDimensionality.test(p.numDimensions()))
				return false;
				
			Dimension<T,?> d = p.getRaw();
			
			System.out.println("Tested dimension --> "+ d.description());
			return d.dataType() == dType.NUMERIC && d.info().startsWith(DataModel.DIRECTIONAL);
		};
	}

	@Override
	protected void dataSetHasChanged() {}

	@Override
	public Mode getDimensionMode() {
		return Mode.RAW;
	}

	@Override
	public boolean allowsNonFittableDimensions() {
		return false;
	}

	@Override
	public List<DimensionContainer<T, double[]>> possibleDimensions(int queryIndex) {
		return this.getDataSet().possibleDimensions(queryIndex, dimensionPredicate());
	}


}
