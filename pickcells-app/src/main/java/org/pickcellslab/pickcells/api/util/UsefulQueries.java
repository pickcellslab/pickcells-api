package org.pickcellslab.pickcells.api.util;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.stream.Collectors;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.F;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Boundary;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.ProcessedLabels;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;

public final class UsefulQueries {


	private UsefulQueries() {/*static methods only*/}



	public static String experimentFolder(DataAccess access) throws DataAccessException{
		access.dataRegistry();
		return access.queryFactory().read("Experiment").makeList(AKey.get("dbpath", String.class)).inOneSet().getAll().run().get(0);
	}



	/**
	 * Obtain the list of available images in the database attached to their {@link LabelsImage}
	 * @param access
	 * @return
	 * @throws Exception
	 */
	public static List<Image> imagesWithSegmentations(DataAccess access) throws Exception{
		//load images (data types) to run the computers for each one of them
		//reconstruct also segresult and processedlabels
		return
				access.queryFactory()
				.regenerate(DataRegistry.typeIdFor(Image.class))
				.toDepth(2)
				.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
				.and(ProcessedLabels.TO_SEG, Direction.INCOMING)
				.includeAllNodes().regenerateAllKeys()
				.getAll()
				.getTargets(Image.class).collect(Collectors.toList());
	}



	/**
	 * Obtain 1 image attached to the {@link ImageLocated} objects of class clazz that have been identified in the desired image.
	 * @param access A {@link DataAccess} connected to the database
	 * @param clazz The type of {@link ImageLocated} objects to attach to the image during the regeneration process.
	 * @param imageID The database id of the image of interest
	 * @return a {@link RegeneratedItem} containing the {@link Image} of interest and all the ImageLocated object of the desired class.
	 * @throws Exception
	 */
	public static RegeneratedItems imageToImageLocated(DataAccess access, MetaClass mc, int imageID) throws Exception{

		//Make sure that the istem class extends ImageLocated
		final Class<?> clazz = mc.itemClass(access.dataRegistry());
		final String type = mc.itemDeclaredType();

		assert ImageLocated.class.isAssignableFrom(clazz) : "The provided MetaClass does not represent an ImageLocated type";

		if(ImageDot.class.isAssignableFrom(clazz))
			return
					access.queryFactory()
					.regenerate(DataRegistry.typeIdFor(Image.class))
					.toDepth(1)
					.traverseLink(ImageDot.origin, Direction.INCOMING)
					.withOneEvaluation(P.isNodeWithDeclaredType(type), Decision.INCLUDE_AND_STOP, Decision.EXCLUDE_AND_CONTINUE)
					.regenerateAllKeys()
					.doNotGetAll()
					.useFilter(F.select(DataItem.idKey).equalsTo(imageID))
					.run();
		else
			return 
					access.queryFactory()
					.regenerate(DataRegistry.typeIdFor(Image.class))
					.toDepth(2)
					.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
					.and(Links.CREATED_FROM, Direction.INCOMING)
					.withEvaluations(P.isNodeWithDeclaredType(type), Decision.INCLUDE_AND_STOP, Decision.EXCLUDE_AND_CONTINUE)
					.addEvaluation(P.isSubType(LabelsImage.class), Decision.INCLUDE_AND_CONTINUE)
					.done()
					.regenerateAllKeys()
					.doNotGetAll()
					.useFilter(F.select(DataItem.idKey).equalsTo(imageID))
					.run();

	}


	/**
	 * Obtain 1 image attached to the {@link ImageLocated} objects of class clazz that have been identified in the desired image.
	 * @param access A {@link DataAccess} connected to the database
	 * @param clazz The type of {@link ImageLocated} objects to attach to the image during the regeneration process.
	 * @param p An {@link ExplicitPredicate} for filtering the attached ImageLocated
	 * @param imageID The database id of the image of interest
	 * @return a {@link RegeneratedItem} containing the {@link Image} of interest and all the ImageLocated associated accepted by the specified predicate
	 * @throws Exception
	 */
	public static RegeneratedItems imageToImageLocated(DataAccess access, MetaClass mc, ExplicitPredicate<? super DataItem> p, int imageID) throws Exception{

		//Make sure that the istem class extends ImageLocated
		final Class<?> clazz = mc.itemClass(access.dataRegistry());
		final String type = mc.itemDeclaredType();


		if(ImageDot.class.isAssignableFrom(clazz))
			return
					access.queryFactory()
					.regenerate(DataRegistry.typeIdFor(Image.class))
					.toDepth(1)
					.traverseLink(ImageDot.origin, Direction.INCOMING)
					.withOneEvaluation(p.merge(Op.Bool.AND, P.isNodeWithDeclaredType(type)), Decision.INCLUDE_AND_CONTINUE, Decision.EXCLUDE_AND_CONTINUE)
					.regenerateAllKeys()
					.doNotGetAll()
					.useFilter(F.select(DataItem.idKey).equalsTo(imageID))
					.run();
		else
			return
					access.queryFactory()
					.regenerate(DataRegistry.typeIdFor(Image.class))
					.toDepth(2)
					.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
					.and(Links.CREATED_FROM, Direction.INCOMING)
					.withEvaluations( p.merge(Op.Bool.AND, P.isNodeWithDeclaredType(type)), Decision.INCLUDE_AND_CONTINUE, Decision.EXCLUDE_AND_CONTINUE)
					.lastEvaluation(P.isSubType(SegmentationResult.class), Decision.INCLUDE_AND_CONTINUE)
					.regenerateAllKeys()
					.doNotGetAll()
					.useFilter(F.select(DataItem.idKey).equalsTo(imageID))
					.run();

	}

	
	
	
	
	
	
	/**
	 * Obtain 1 image attached to the {@link ImageLocated} objects of class clazz that have been identified in the desired image.
	 * @param access A {@link DataAccess} connected to the database
	 * @param clazz The type of {@link ImageLocated} objects to attach to the image during the regeneration process.
	 * @param p An {@link ExplicitPredicate} for filtering the attached ImageLocated
	 * @param imageID The database id of the image of interest
	 * @return a {@link RegeneratedItem} containing the {@link Image} of interest and all the ImageLocated associated accepted by the specified predicate
	 * @throws Exception
	 */
	public static RegeneratedItems imageToImageLocated(DataAccess access, MetaClass mc, int imageID, int depth, String additionalLinks, Direction dir) throws Exception{

		//Make sure that the istem class extends ImageLocated
		final Class<?> clazz = mc.itemClass(access.dataRegistry());
		final String type = mc.itemDeclaredType();


		if(ImageDot.class.isAssignableFrom(clazz))
			return
					access.queryFactory()
					.regenerate(DataRegistry.typeIdFor(Image.class))
					.toDepth(depth)
					.traverseLink(ImageDot.origin, Direction.INCOMING)
					.withOneEvaluation(P.isNodeWithDeclaredType(type), Decision.INCLUDE_AND_CONTINUE, Decision.EXCLUDE_AND_CONTINUE)
					.regenerateAllKeys()
					.doNotGetAll()
					.useFilter(F.select(DataItem.idKey).equalsTo(imageID))
					.run();
		else
			return 
					access.queryFactory()
					.regenerate(DataRegistry.typeIdFor(Image.class))
					.toDepth(depth)
					.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
					.and(Links.CREATED_FROM, Direction.INCOMING)
					.and(additionalLinks, dir)
					.withEvaluations(P.isNodeWithDeclaredType(type), Decision.INCLUDE_AND_CONTINUE, Decision.EXCLUDE_AND_CONTINUE)
					.addEvaluation(P.isSubType(LabelsImage.class), Decision.INCLUDE_AND_CONTINUE)
					.done()
					.regenerateAllKeys()
					.doNotGetAll()
					.useFilter(F.select(DataItem.idKey).equalsTo(imageID))
					.run();

	}

	
	
	
	
	
	
	
	



	/**
	 * Obtain 1 image attached to the {@link Boundary} objects that have been identified in the desired image.
	 * @param access A {@link DataAccess} connected to the database
	 * @param clazz The type of {@link ImageLocated} objects to attach to the image during the regeneration process.
	 * @param p An {@link ExplicitPredicate} for filtering the attached Boundaries.
	 * @param imageID
	 * @return a {@link RegeneratedItem} containing the {@link Image} of interest and all the ImageLocated associated accepted by the specified predicate
	 * @throws Exception
	 */
	public static RegeneratedItems imageToBoundary(DataAccess access, ExplicitPredicate<? super NodeItem> p, int imageID) throws Exception{

		return
				access.queryFactory()
				.regenerate(DataRegistry.typeIdFor(Image.class))
				.toDepth(3)
				.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
				.and(Links.CREATED_FROM, Direction.INCOMING)
				.and(Boundary.bound_link, Direction.INCOMING)
				.withOneEvaluation(
						P.isSubType(Boundary.class, NodeItem.class).merge(Op.Bool.AND, p.negate())
						, Decision.EXCLUDE_AND_CONTINUE, Decision.INCLUDE_AND_CONTINUE)
				.regenerateAllKeys()
				.doNotGetAll()
				.useFilter(F.select(DataItem.idKey).equalsTo(imageID))
				.run();

	}




}
