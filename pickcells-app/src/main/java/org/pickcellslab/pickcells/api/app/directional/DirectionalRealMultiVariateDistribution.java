package org.pickcellslab.pickcells.api.app.directional;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.dataviews.fitting.MultiVariateRealDistribution;

/**
 * 
 * A {@link MultiVariateRealDistribution} representing directional data
 * 
 * @author Guillaume Blin
 *
 */
public interface DirectionalRealMultiVariateDistribution extends MultiVariateRealDistribution, NodeItem {

	public static AKey<String> info = AKey.get("info",String.class);
	
	/**
	 * Adds Information regarding this distribution
	 * @param info
	 */
	public void setInfo(String info);
	
	/**
	 * @return An HTML text describing this distribution (fitted parameters, additional info...)
	 */
	public String getDescription();
	
}
