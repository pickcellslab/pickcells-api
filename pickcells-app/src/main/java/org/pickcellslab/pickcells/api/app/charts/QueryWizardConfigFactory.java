package org.pickcellslab.pickcells.api.app.charts;

import org.pickcellslab.foundationj.dbm.queries.config.QueryWizardConfig;

@FunctionalInterface
public interface QueryWizardConfigFactory<T,D> {

	QueryWizardConfig<T,D> createConfig();
	
}
