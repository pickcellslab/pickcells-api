package org.pickcellslab.pickcells.api.app.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Function;

import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;
/**
 * Listens for events where a {@link Distribution} has been selected
 * 
 * @author Guillaume Blin
 *
 */
@Modular
@SameScopeAs(Workbench.class)
public interface DistributionPickConsumer<T> {

	
	public <D> void distributionPicked(Distribution<D> d, MetaQueryable p, Function<T, D> f);
	
	public String name();
	
}
