package org.pickcellslab.pickcells.api.presentation;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.scope.ScopeEventListener;


@Core
@Scope
public interface PickCellsPresentation extends ScopeEventListener {

	public enum Perspective{
		WELCOME, WORKBENCH, SANDBOX, OVERVIEW
	}

	
	public enum Controls{
		WELCOME, WORKBENCH, SANDBOX, OVERVIEW, LOGOFF, LOGIN
	}


	public void addPresentationListener(PickCellsPresentationListener l);



	public void setPerspective(Perspective view);

	public void goToExperiment(String expName);

	public boolean showConfirm(String msg);
	
	public void setControlEnabled(Controls control);

	
}
