package org.pickcellslab.pickcells.api.app.modules;

import java.net.URL;

/**
 * 
 * Define objects which are well documented and provide information about authors, license, documentaion url etc...
 * 
 * @author Guillaume Blin
 *
 */
public interface Documented {

	/**
	 * @return The name of the documented object
	 */
	public String name();
	
	/**
	 * @return Comma separated list of authors
	 */
	public String authors();
	
	/**
	 * @return The name of the license (GPLv3, BSD etc...)
	 */
	public String licence();
	
	/**
	 * @return The description of the documented object
	 */
	public String description();
	
	/**
	 * @return A url where additional info about the documented object may be found, null permitted
	 */
	public URL url();
	
}
