package org.pickcellslab.pickcells.api.presentation;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.pickcellslab.foundationj.annotations.SameScopeAs;

/**
 * 
 * A marker interface for the sandbox {@link Scope}. Do not implement, PickCells already provides an implementation for this interface.
 * Just use this interface when specifying {@link SameScopeAs} for your own {@link Modular} interfaces
 * 
 * @author Guillaume Blin
 *
 */
@Core
@Scope(name="SANDBOX", parent="IMAGE-RESOURCES", policy=ScopePolicy.SINGLETON)
public interface Sandbox extends SwingSupplier{

}
