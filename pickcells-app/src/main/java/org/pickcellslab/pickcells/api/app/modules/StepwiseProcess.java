package org.pickcellslab.pickcells.api.app.modules;

/*-
 * #%L
 * foundationj-modules
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;

import javax.swing.Icon;

/**
 * A stepwise process capable of notifying a {@link ProgressPublisher} about its progress
 * 
 * @author Guillaume Blin
 *
 */
public interface StepwiseProcess {

	/**
	 * @return An Array of icons to be displayed for each step. Entries in the array may be null however
	 * the length of the array must be equal to the number of steps.
	 */
	public Icon[] icons();
	
	/**
	 * @return An Array with the name of each step. null entries not permitted!
	 */
	public String[] steps();
			
	
	public void addPublisher(ProgressPublisher p);
	
	public void removePublisher(ProgressPublisher p);
	
	public Collection<ProgressPublisher> getPublishers();
	
	public void stop();
	
}
