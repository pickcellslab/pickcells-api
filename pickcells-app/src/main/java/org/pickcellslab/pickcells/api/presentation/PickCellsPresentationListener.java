package org.pickcellslab.pickcells.api.presentation;

import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;

public interface PickCellsPresentationListener {

			
	public void sandboxClicked();
	
	public void loginClicked();
		
	public void logOffClicked();

	public void showOverviewClicked();
	
	public void showWorkbenchClicked();

	public void showWelcomeClicked();

	public void closeWorkbenchClicked(Workbench wb);
	
	
}
