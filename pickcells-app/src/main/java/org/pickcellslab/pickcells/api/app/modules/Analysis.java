package org.pickcellslab.pickcells.api.app.modules;

/**
 * A marker interface for Analysis modules
 * An Analysis is a launcher for resource intensive tasks.
 * It extends {@link StepwiseProcess}, {@link Activable} and {@link Launchable}.
 *  
 * @author Guillaume Blin
 *
 */
public interface Analysis extends StepwiseProcess, Task{

	@Override
	public void launch() throws AnalysisException;
}
