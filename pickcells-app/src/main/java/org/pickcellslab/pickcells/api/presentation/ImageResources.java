package org.pickcellslab.pickcells.api.presentation;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;

@Core
@Scope(name="IMAGE-RESOURCES", parent=Scope.APPLICATION, policy=ScopePolicy.SINGLETON)
public interface ImageResources {

}
