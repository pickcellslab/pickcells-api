package org.pickcellslab.pickcells.api.presentation.workbench;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.pickcellslab.pickcells.api.presentation.SwingSupplier;

/**
 * 
 * A marker interface for the workbench {@link Scope}. Do not implement, PickCells already provides an implementation for this interface.
 * Just use this interface when specifying {@link SameScopeAs} for your own {@link Modular} interfaces  
 * 
 * @author Guillaume Blin
 *
 */
@Core
@Scope(name="WORKBENCH", parent="IMAGE-RESOURCES", policy=ScopePolicy.SIBLING)
public interface Workbench extends SwingSupplier{

	public String title();
	
}
