package org.pickcellslab.pickcells.api.app.charts;

import org.pickcellslab.foundationj.dbm.access.DataAccessException;

/**
 * Should be thrown when an Exception occurs during the creation of a visualisation
 * 
 * @author Guillaume Blin
 *
 */
public class ViewFactoryException extends Exception {

	public ViewFactoryException(String string) {
		super(string);
	}

	public ViewFactoryException(String string, DataAccessException e) {
		super(string, e);
	}

}
