package org.pickcellslab.pickcells.api.app.modules;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.graph.Node;
import org.pickcellslab.foundationj.optim.staging.Agent;
import org.pickcellslab.foundationj.optim.staging.Director;
import org.pickcellslab.foundationj.optim.staging.Enableable;
import org.pickcellslab.foundationj.optim.staging.Satisfiable;



/**
 * A marker interface for Nodes linked by {@link Dependency} edges
 * 
 * @author Guillaume Blin
 *
 */
public interface Demand<T> extends Node<Dependency<T>>,  Agent<T>, Director<T>, Enableable, Satisfiable{

}
