package org.pickcellslab.pickcells.api.app.modules;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JComponent;

public class DefaultUIDocument implements UIDocument {

	private final JComponent scene;
	private final String id;
	private final List<DocumentClosedListener> lstrs = new ArrayList<>();
	private final Icon icon;

	public DefaultUIDocument(JComponent scene, String id, Icon icon) {
		this.scene = scene;
		this.id = id;
		this.icon = icon;
	}
	
	@Override
	public void close() {
		lstrs.forEach(l->l.closed(this));
	}

	@Override
	public String id() {
		return id;
	}

	@Override
	public JComponent scene() {
		return scene;
	}

	@Override
	public void addDocumentClosedListener(DocumentClosedListener l) {
		lstrs.add(l);
	}

	@Override
	public void removeDocumentClosedListener(DocumentClosedListener l) {
		lstrs.remove(l);
	}

	@Override
	public Icon icon() {
		return icon;
	}

}
