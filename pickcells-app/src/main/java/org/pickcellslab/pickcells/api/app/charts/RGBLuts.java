package org.pickcellslab.pickcells.api.app.charts;


/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.util.Random;

public enum RGBLuts implements RGBLut{


	Grays{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 0 ].length; i++ ){
				gray[ 0 ][ i ] = ( byte ) i;
				gray[ 1 ][ i ] = ( byte ) i;
				gray[ 2 ][ i ] = ( byte ) i;					
			}
			return gray;
		}
	},
	Red{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 0 ].length; i++ )
				gray[ 0 ][ i ] = ( byte ) i;
			return gray;
		}
	},
	Green{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ )
				gray[ 1 ][ i ] = ( byte ) i;
			return gray;
		}
	},
	Blue{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 2 ].length; i++ )
				gray[ 2 ][ i ] = ( byte ) i;
			return gray;
		}
	},
	Cyan{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ ){
				gray[ 1 ][ i ] = ( byte ) i;
				gray[ 2 ][ i ] = ( byte ) i;
			}
			return gray;
		}
	},
	Yellow{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ ){
				gray[ 0 ][ i ] = ( byte ) i;
				gray[ 1 ][ i ] = ( byte ) i;
			}
			return gray;
		}
	},
	Magenta{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ ){
				gray[ 0 ][ i ] = ( byte ) i;
				gray[ 2 ][ i ] = ( byte ) i;
			}
			return gray;
		}	

	},	
	RANDOM{
		@Override
		public byte[][] data() {
			Random r = new Random();
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 1; i < gray[ 1 ].length-1; i++ ){
				gray[ 0 ][ i ] = ( byte ) r.nextInt(255);
				gray[ 1 ][ i ] = ( byte ) r.nextInt(255);
				gray[ 2 ][ i ] = ( byte ) r.nextInt(255);
			}
			gray[ 0 ][ 255 ] = ( byte ) r.nextInt(255);
			gray[ 1 ][ 255 ] = ( byte ) r.nextInt(255);
			gray[ 2 ][ 255 ] = ( byte ) r.nextInt(255);
			return gray;
		}	

	},	 


	SPECTRUM{

		public byte[][] data(){
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for (int i = 0; i < 256; i++) {
				final int c = Color.HSBtoRGB(i / 255f, 1f, 1f);
				gray[0][i] =  (byte) ((c)&0xFF);
				gray[1][i]  = (byte) ((c>>8)&0xFF);
				gray[2][i] =  (byte) ((c>>16)&0xFF);
			}
			return gray;
		}


	},


	FIRE{

		

		public byte[][] data(){
			
			final int[] r = { 0, 0, 1, 25, 49, 73, 98, 122, 146, 162, 173, 184, 195, 207,
					217, 229, 240, 252, 255, 255, 255, 255, 255, 255, 255, 255,
					255, 255, 255, 255, 255, 255 };
			final int[] g = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 35, 57, 79, 101,
					117, 133, 147, 161, 175, 190, 205, 219, 234, 248, 255, 255,
					255, 255 };
			final int[] b = { 0, 61, 96, 130, 165, 192, 220, 227, 210, 181, 151, 122, 93,
					64, 35, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 35, 98, 160, 223,
					255 };
			
			
			byte[][] map = new byte[3][r.length];

			// cast elements
			for (int i = 0; i < r.length; i++) {
				map[0][i] = (byte) r[i];
				map[1][i] = (byte) g[i];
				map[2][i] = (byte) b[i];
			}

			return map;
		}



	}






}

