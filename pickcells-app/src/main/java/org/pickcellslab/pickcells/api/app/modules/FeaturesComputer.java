package org.pickcellslab.pickcells.api.app.modules;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.graph.DefaultNode;
import org.pickcellslab.foundationj.optim.staging.Enableable;
import org.pickcellslab.foundationj.optim.staging.Satisfiable;
import org.pickcellslab.pickcells.api.presentation.workbench.Workbench;

/**
 * An abstract class for plugins capable of computing shape descriptors for shapes defined 
 * in a segmentation result image.
 * A {@link FeaturesCoputer} may further implement {@link ChannelImageConsumer} or/and {@link SegmentationImageConsumer}
 * if it requires injection of images data, PickCells will provide those when required. 
 * 
 * @author Guillaume Blin
 *
 */

@Modular
@SameScopeAs(Workbench.class )
public abstract class FeaturesComputer
extends DefaultNode<Dependency<Collection<AKey<?>>>> 
implements Documented, Runnable, Demand<Collection<AKey<?>>>{


	public static final String DONE = "done";



	/**
	 * @return a Map of the features names and their associated description that this FeatureComputer
	 * can create
	 */
	public abstract Map<AKey<?>,String> featuresDescription();

	protected PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	private boolean isSatisfied = false;
	private boolean isEnabled = true;
	private boolean isDone = false;



	@Override
	public final double evaluate(Collection<AKey<?>> contract) {
		Set<AKey<?>> set = new HashSet<>(this.requirements());
		set.removeAll(contract);		
		if(set.isEmpty()){
			//calculate penalty due to excess	
			double nExcess = contract.size()-requirements().size();
			if(nExcess == 0) return 1;
			return 2-(1/Math.exp(2/nExcess));
		}		
		//If not all satisfied, then there are no benefits -> must return a number below 1
		double percent = (double)set.size()/(double)requirements().size();
		double nExcess = contract.size()-requirements().size()+set.size();
		return 1-percent-nExcess;
	}



	/**
	 * Set to true when this computer was already run on a specific type of segmented object 
	 * @param b
	 */
	public final void setDone(boolean b){
		if(isDone!=b){
			isSatisfied = b;
			isDone = b;
			pcs.firePropertyChange(DONE, !isDone, isDone);
		}
	}

	public final boolean isDone(){
		return isDone;
	}

	@Override
	public final void setSatisfied(boolean b, boolean silent) {
		isSatisfied = b;
		if(!silent){
			System.out.println("FeaturesComputer "+name()+" is firing new satisfaction : "+isSatisfied);			
			pcs.firePropertyChange(Satisfiable.PROPERTY, !isSatisfied, isSatisfied);
		}
	}



	@Override
	public final boolean isSatisfied() {
		return isSatisfied;
	}



	@Override
	public final void setEnabled(boolean b) {
		if(isEnabled!=b){
			isEnabled = b;
			this.setEnabled(isEnabled);
			pcs.firePropertyChange(Enableable.PROPERTY, !isEnabled, isEnabled);
		}	
	}



	@Override
	public final boolean isEnabled() {
		return isEnabled;
	}



	@Override
	public final void addPropertyChangeListener(PropertyChangeListener l) {
		pcs.addPropertyChangeListener(l);		
	}


	
	



}
