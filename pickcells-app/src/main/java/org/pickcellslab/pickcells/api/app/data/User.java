package org.pickcellslab.pickcells.api.app.data;

import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.datamodel.NodeItem;

/**
 * 
 * Defines a User of the application.
 * <br><br>
 * Note to implementers: Implementations need to be annotated with @{@link Data} and @{@link Scope}
 * 
 *
 */
public interface User extends NodeItem{

	public enum Priviledge {
		ADMIN, USER
	}
	
	Priviledge privilege();

	String userName();

	String creationDate();
		
	String toString();


}