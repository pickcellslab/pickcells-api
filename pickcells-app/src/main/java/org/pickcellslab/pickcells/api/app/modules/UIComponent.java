package org.pickcellslab.pickcells.api.app.modules;

import org.pickcellslab.pickcells.api.presentation.workbench.WorkbenchElement;

public interface UIComponent extends WorkbenchElement {

	public void start();
	
	public void stop();
	
	public void dispose();
	
}
