package org.pickcellslab.pickcells.api.app.charts;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;




public class DataToFileAction<T extends DataItem> implements Action<T,Void>{




	private final List<Dimension<T, ?>> dims;
	private final String path;
	private final String prefix;

	public DataToFileAction(List<Dimension<T,?>> dims, String path, String prefix) throws FileNotFoundException {

		this.dims = dims;
		this.path = path;
		this.prefix = prefix;
	}


	public <E,O> DataToFileAction(Dimension<T,E> dim, Function<E,O> f, Class<O> type, String path, String prefix) throws FileNotFoundException {

		this.dims = new ArrayList<>(1);
		dims.add(
				new Dimension<T,O>(){

					@Override
					public dType dataType() {
						return dType.valueOf(type);
					}

					@Override
					public Class<O> getReturnType() {
						return type;
					}

					@Override
					public O apply(T t) {
						return f.apply(dim.apply(t));
					}

					@Override
					public int index() {
						return 0;
					}

					@Override
					public int length() {
						return 0;
					}

					@Override
					public String name() {
						return dim.name();
					}

					@Override
					public String info() {
						return dim.info();
					}

				});
		this.path = path;
		this.prefix = prefix;

	}



	@Override
	public ActionMechanism<T> createCoordinate(Object key) throws DataAccessException {		
		File f = new File(path+File.separator+prefix+"_"+Objects.toString(key));
		try {
			return new ToFileMechanism(dims,f);
		} catch (FileNotFoundException e) {
			throw new DataAccessException("File not found",e);
		}
	}

	@Override
	public Set<Object> coordinates() {
		return Collections.emptySet();
	}

	@Override
	public Void createOutput(Object key) throws IllegalArgumentException {
		return null;
	}

	@Override
	public String description() {
		return "Printing series to file";
	}





	class ToFileMechanism implements ActionMechanism<T>{

		private final PrintWriter os;
		private final List<Dimension<T, ?>> dims;

		public ToFileMechanism(List<Dimension<T,?>> dims, File file) throws FileNotFoundException {

			this.os = new PrintWriter(file);
			this.dims = dims;

			//Print Headers
			for (Dimension<T,?> dim : dims) {
				os.print(dim.name()+"\t");
			}
		}


		@Override
		public void performAction(T i) throws DataAccessException {

			// Print a new row
			os.println();	
			for (Dimension<T,?> dim : dims) {
				os.print(AKey.asString(i, dim)+"\t");
			}

		}


		public void lastAction(){
			os.flush();
			os.close();
		}
	}


}
