package org.pickcellslab.pickcells.api.app.charts;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.dataviews.mvc.CameleonView;
import org.pickcellslab.foundationj.dataviews.mvc.DataView;
import org.pickcellslab.foundationj.dataviews.mvc.FitView;
import org.pickcellslab.foundationj.dataviews.mvc.FittableDimensionBroker;
import org.pickcellslab.foundationj.dataviews.mvc.ReconfigurableView;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableDimensionsModel;

/**
 * 
 * Defines a {@link DataView} which displays numerical data in a form which allows the data to be fitted with a distribution
 * 
 * @author Guillaume Blin
 *
 * @param <M> The type of DataViewModel used by the chart
 * @param <T> The type of database objects which will be displayed by this view *
 * @param <B> The type of dimension broker used by the chart and the model
 * @param <D> The type of coordinate returned by Dimensions
 * @param <R> The type of coordinates used for fitting
 * @param <A> The type of Appearance object handled by the chart
 */
public interface FittableDBChart<M extends RedefinableDimensionsModel<T,B,D>,T,B extends FittableDimensionBroker<T,R,D>,D,R,A> extends CameleonView<A>, FitView<A>,
ReconfigurableView<M,T,B,D> {

}
