package org.pickcellslab.pickcells.api.app.directional;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dataviews.fitting.MixtureModel;


public interface VonMisesMixture extends NodeItem, MixtureModel<double[],VonMisesDistribution>, DirectionalRealMultiVariateDistribution{

		
	/**
	 * The number of mixtures in this model
	 */
	public static AKey<Integer> k = AKey.get("Number of Mixtures",Integer.class);
		
	public static AKey<double[]> theta = AKey.get("theta",double[].class);
	
	public static AKey<double[]> proba = AKey.get("probabilities",double[].class);
	
	/**
	 * The bayesian inference criterion associated with this mixture
	 */
	public static AKey<Double> bic = AKey.get("bic",Double.class);
	
	
	
	@Override
	public default Stream<AKey<?>> minimal() {
		List<AKey<?>> min = new ArrayList<>();
		min.add(k); min.add(theta); min.add(bic);
		min.add(WritableDataItem.idKey); min.add(proba);
		min.add(AKey.get("name", String.class));
		return min.stream();
	}
	
}
