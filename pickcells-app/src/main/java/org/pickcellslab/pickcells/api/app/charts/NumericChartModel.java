package org.pickcellslab.pickcells.api.app.charts;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer;
import org.pickcellslab.foundationj.datamodel.dimensions.DimensionContainer.Mode;
import org.pickcellslab.foundationj.dataviews.fitting.DistributionFitterFactory;
import org.pickcellslab.foundationj.dataviews.mvc.AbstractDimensionalModel;
import org.pickcellslab.foundationj.dataviews.mvc.DataViewModel;
import org.pickcellslab.foundationj.dataviews.mvc.FittableModel;
import org.pickcellslab.foundationj.dataviews.mvc.ScalarDimensionBroker;

/**
 * 
 * The default {@link DataViewModel} for numerical charts. This model supports redefinition of its dimensions as well as distribution fitting 
 * 
 * @author Guillaume Blin
 *
 * @param <T>
 */
public class NumericChartModel<T,D> extends AbstractDimensionalModel<T,ScalarDimensionBroker<T,D>,Number> implements FittableModel<T,D,Number>{


	private final Predicate<Integer> allowedDimensionality;
	private final List<DistributionFitterFactory<?,D,?,?>> fitters;


	public NumericChartModel(Predicate<Integer> allowedDimensionality, List<DistributionFitterFactory<?,D,?,?>> fitters) {
		Objects.requireNonNull(allowedDimensionality,"predicate is null");
		Objects.requireNonNull(fitters,"fitter type is null");
		this.allowedDimensionality = allowedDimensionality;	
		this.fitters = fitters;
	}

	boolean dimensionalityRespected(int dimensionality){
		return this.allowedDimensionality.test(dimensionality);
	}



	@Override
	public Predicate<DimensionContainer<T,?>> dimensionPredicate() {
		return d -> d.getDimension(0).dataType() == dType.NUMERIC;
	}

	@Override
	public ScalarDimensionBroker<T,D> createBroker(List<Dimension<T,Number>> dims) {

		if(dims.size() == 0)
			throw new RuntimeException("provided dimensions empty!");

		ScalarDimensionBroker<T,D> broker = new ScalarDimensionBroker<T,D>(dims, getDataSet(), fitters);
		return broker;
	}

	@Override
	protected void dataSetHasChanged() {}

	@Override
	public final Mode getDimensionMode() {
		return Mode.DECOMPOSED;
	}

	@Override
	public boolean allowsNonFittableDimensions() {
		return false;
	}

	@Override
	public List<DimensionContainer<T, Number>> possibleDimensions(int queryIndex) {
		return this.getDataSet().possibleDimensions(queryIndex, dimensionPredicate());
	}

	
	
	

	




}
