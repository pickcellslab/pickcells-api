package org.pickcellslab.pickcells.api.app.modules;

/**
 * An object that can be launched
 * 
 * @author Guillaume Blin
 *
 */
public interface Launchable {

	/**
	 * Notifies the {@link Launchable} that a launch is requested 
	 */
	public void launch() throws Exception;
	
		
}
