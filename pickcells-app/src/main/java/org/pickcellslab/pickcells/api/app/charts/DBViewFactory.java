package org.pickcellslab.pickcells.api.app.charts;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Icon;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dataviews.mvc.DataView;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.pickcells.api.app.modules.Documented;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;
import org.pickcellslab.pickcells.api.presentation.workbench.WorkbenchElement;

/**
 * Implement this interface to allow the user to create a new type of {@link DataView}.
 * <br>Do not forget to annotate your class with {@link Module}
 * 
 * @author Guillaume Blin
 *
 */
public interface DBViewFactory extends Documented, WorkbenchElement{

	public enum ViewType{
		Numerical, Categorical
	}
	
	public ViewType getType();
			
	public UIDocument createView(DataAccess access, DBViewHelp helper) throws ViewFactoryException;

	public Icon icon();	
	

}


