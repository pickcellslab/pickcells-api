package org.pickcellslab.pickcells.api.app.data;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.annotations.Tag;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;

/*-
 * #%L
 * foundationj-dbm
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.NodeItem;


@Tag(creator = "PickCells", description = "Identifies a data item as root in a graph", tag = "Root")
public abstract class Experiment extends DataNode implements NodeItem {

	public static final AKey<String> nameKey = AKey.get("name", String.class);
	public static final AKey<String> descriptionKey = AKey.get("description", String.class);
	public static final AKey<String> dbNameKey = AKey.get("dbname", String.class);
	public static final AKey<String> dbPathKey = AKey.get("dbpath", String.class);
	
	
	
	protected Experiment() {/*Database Use*/}
	
	protected Experiment(String name, String location, String description){
		this.setAttribute(nameKey, name);
		this.setAttribute(dbNameKey, name+"_db");
		this.setAttribute(dbPathKey, location);
		this.setAttribute(descriptionKey, description);
		this.setAttribute(declaredTypeKey, "Experiment");
	}
	
	
	@Override
	public String declaredType(){
		return "Experiment";
	}

	public String getName() {
		return getAttribute(nameKey).get();
	}

	public String getDescription() {
		return getAttribute(descriptionKey).get();
	}

	public String getDBName() {
		return getAttribute(dbNameKey).get();
	}

	public String getDBPath() {
		return getAttribute(dbPathKey).get();
	}
	
	
	@Override
	public Stream<AKey<?>> minimal() {
		return Stream.of(nameKey, descriptionKey, dbNameKey, dbPathKey, idKey);
	}

}
