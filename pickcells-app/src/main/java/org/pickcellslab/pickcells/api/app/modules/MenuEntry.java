package org.pickcellslab.pickcells.api.app.modules;

import org.pickcellslab.pickcells.api.presentation.workbench.WorkbenchElement;



public interface MenuEntry extends WorkbenchElement, Launchable, Activable, Documented {

	/**
	 * @return A string array holding the menu hierarchy for the placement of this {@link MenuEntry}
	 */
	public String[] menuPath();
	
}
