package org.pickcellslab.pickcells.api.app.modules;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.graph.Edge;


/**
 * An {@link Edge} modeling a dependency between {@link Demand} nodes
 * 
 * @author Guillaume Blin
 *
 */
public class Dependency<T> implements Edge<Demand<T>> {

	

	private Demand<T> source;
	private Demand<T> target;


	/**
	 * Creates a new Silent Link with the specified type, source and target.
	 * @param type
	 * @param source
	 * @param target
	 * @parma a flag to determine if the link should automatically be registered in the source and target during instantiation
	 */
	public Dependency(Demand<T> source, Demand<T> target, boolean addAutomatically) {
		
		Objects.requireNonNull(source);
		Objects.requireNonNull(target);
		
		this.source = source;
		this.target = target;
		if(addAutomatically){
			source.addLink(this);
			target.addLink(this);
		}
	}


	@Override
	public Demand<T> source() {
		return source;
	}

	@Override
	public Demand<T> target() {
		return target;
	}

	@Override
	public void delete() {		
		source.removeLink(this, true);
		target = null;
		source = null;	
	}


}
