package org.pickcellslab.pickcells.api.app.charts;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.FunctionDimension;
import org.pickcellslab.foundationj.dataviews.mvc.DimensionalDataSet;
import org.pickcellslab.foundationj.dataviews.mvc.TransformFactory;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;

public class VectorRotationFactory<T extends DataItem> implements TransformFactory<T, double[]> {

	private List<Dimension<T,double[]>> dims;


	private final static Vector2D base = new Vector2D(1,0);

	private static final BiFunction<double[], double[], double[]> rot2D = (vector,reference)->{

		Vector2D ref = new Vector2D(reference);


		double s = ref.crossProduct(ref,base);// sine of angle
		double c = ref.dotProduct(base); // cosine of angle

		//Rotation matrix
		RealMatrix R = MatrixUtils.createRealMatrix(2,2);
		R.setEntry(0, 0, c); 	R.setEntry(0, 1, -s);
		R.setEntry(1, 0, s); 	R.setEntry(1, 0, c);

		return R.operate(vector);
	};





	//An Identity matrix used in rotations
	private static final RealMatrix I = MatrixUtils.createRealDiagonalMatrix(new double[]{1,1,1});

	private static final BiFunction<double[], double[], double[]> rot3D = (vector,reference)->{
		//Rotate -> Define the matrix of rotation to change base
		//Adapted from : http://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
		Vector3D ref = new Vector3D(reference);

		Vector3D v = ref.crossProduct(Vector3D.PLUS_K);//cross product
		double s = v.getNorm(); // sine of angle
		double c = ref.dotProduct(Vector3D.PLUS_K); // cosine of angle

		//skew-symmetric cross-product matrix of v
		RealMatrix V = MatrixUtils.createRealMatrix(3,3);
		V.setEntry(0, 1, -v.getZ()); 	V.setEntry(0, 2, v.getY());
		V.setEntry(1, 0, v.getZ()); 	V.setEntry(1, 2, -v.getX());
		V.setEntry(2, 0, -v.getY()); 	V.setEntry(2, 1, v.getX());

		RealMatrix R = I.add(V).add(V.power(2).scalarMultiply((1d-c)/Math.pow(s,2)));

		return R.operate(vector);
	};



	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void setDataSet(DimensionalDataSet<T, double[]> dataset) {
		//this.dataset = dataset;
		dims = (List)dataset.possibleDimensions(0, d->((MetaReadable)d).description().contains(DataModel.DIRECTIONAL)).stream().map(d->d.getRaw()).collect(Collectors.toList());
	}

	@Override
	public int numAvailableTransforms() {
		if(dims == null)
			return 0;
		return dims.size();
	}

	@Override
	public String transformName(int i) {
		if(dims == null)
			return "Not Available";
		return dims.get(i).name();
	}

	@Override
	public String transformDescription(int i) {
		if(dims == null)
			return "Not Available";
		return dims.get(i).toHTML();
	}

	@Override
	public Optional<Dimension<T, double[]>> create(Dimension<T, double[]> untransformed, int choice) {
		if(dims == null)
			return Optional.empty();
		final Dimension<T, double[]> refDim = dims.get(choice);
		if(refDim.length() == 3){
			Function<T,double[]> f = i ->{
				double[] ref = refDim.apply(i);
				if(null == ref)
					return null;
				double[] vct = untransformed.apply(i);
				if(null == vct)
					return null;
				return rot3D.apply(vct, ref);
			};
			return Optional.of(new FunctionDimension<>(AKey.get(transformName(choice),double[].class), f, transformDescription(choice)));
		}
		else if(refDim.length() == 2){
			Function<T,double[]> f = i ->{
				double[] ref = refDim.apply(i);
				if(null == ref)
					return null;
				double[] vct = untransformed.apply(i);
				if(null == vct)
					return null;
				return rot2D.apply(vct, ref);
			};
			return Optional.of(new FunctionDimension<>(AKey.get(transformName(choice),double[].class), f, transformDescription(choice)));
		}
		else throw new IllegalArgumentException("Unsupported dimensionality : "+refDim.length());
	}

}
