package org.pickcellslab.pickcells.api.app.modules;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * An abstract {@link Analysis} which provides a default implementation for registering {@link ProgressPublishers}
 * 
 * @author Guillaume Blin
 *
 */
public abstract class AbstractAnalysis extends AbstractActivableModule implements Analysis{


	protected final List<ProgressPublisher> publishers = new ArrayList<>(4);
	
	@Override
	public void addPublisher(ProgressPublisher p) {
		publishers.add(p);
	}


	@Override
	public void removePublisher(ProgressPublisher p) {
		publishers.remove(p);
	}
	
	/**
	 * Notifies publishers that the given step is now being performed
	 * @param step index in array returned by {@link #steps()}
	 */
	protected void setStep(int step){
		publishers.forEach(p->p.setStep(this, step));
	}
	
	/**
	 * Notifies publishers about the current progress accomplished to reach the next step
	 * @param progress
	 */
	protected void setProgress(float progress){
		publishers.forEach(p->p.setProgress(this,progress));
	}
	
	
	/**
	 * Notifies publishers that the process failed to reach its goal
	 * @param message
	 */
	protected void setFailure(String message){
		publishers.forEach(p->p.failure(this, message));
	}
	
	/**
	 * Notifies publishers that this process was cancelled
	 */
	protected void setCancelled(){
		publishers.forEach(p->p.cancelled(this));
	};
	
	@Override
	public Collection<ProgressPublisher> getPublishers(){
		return publishers;
	}
	
}
