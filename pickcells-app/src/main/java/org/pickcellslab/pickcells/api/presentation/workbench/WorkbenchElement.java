package org.pickcellslab.pickcells.api.presentation.workbench;

import org.pickcellslab.foundationj.annotations.Modular;
import org.pickcellslab.foundationj.annotations.SameScopeAs;

/**
 * A Marker interface for elements of the workbench. This is the root interface for all components
 * used in the workbench
 * 
 * @author Guillaume Blin
 *
 */
@Modular
@SameScopeAs(Workbench.class)
public interface WorkbenchElement {

}
