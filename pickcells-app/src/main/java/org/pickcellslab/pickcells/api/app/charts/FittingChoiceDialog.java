package org.pickcellslab.pickcells.api.app.charts;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.dataviews.mvc.FittableDimensionBroker;
import org.pickcellslab.foundationj.dataviews.mvc.FittableModel;

public class FittingChoiceDialog extends JDialog{

	private boolean wasCancelled = true;

	/**
	 * true if there is no dataset in the model yet
	 */
	private boolean noDataset;
	
	private String chosenQuery;
	
	private int chosenMethod;

	private String prefix;


	public <T,R,D> FittingChoiceDialog(FittableModel<T,R,D> model) {

		if(model.getDataSet() == null){
			noDataset = true;
			return;
		}

		
		
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.PAGE_AXIS));
		setContentPane(main);
		
		


		//=================================================================================================
		// Definition of Controllers
		
				
		
		// Choice of query to fit		
		JLabel lblSeriesToFit = new JLabel("Series to fit:");
		JComboBox<String> seriesBox = new JComboBox<>();
		
		
		int numQueries = model.getDataSet().numQueries();



		for(int q = 0; q<numQueries; q++){
			String queryId = model.getDataSet().queryID(q);
			FittableDimensionBroker<T,R,D> broker = model.dimensionBroker(queryId);
			if(broker!=null)			
				seriesBox.addItem(queryId);			
		}

		if(seriesBox.getModel().getSize() == 0){
			noDataset = true;
			return;
		}
		
		JPanel seriesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		seriesPanel.add(lblSeriesToFit);
		seriesPanel.add(seriesBox);
		
		main.add(seriesPanel);
		
		

		// Choice of fitting method
		JLabel lblMethod = new JLabel("Method");

		JComboBox<String> methodBox = new JComboBox<>();
		
		
		FittableDimensionBroker<T,R,D> broker = model.dimensionBroker(seriesBox.getSelectedItem().toString());
		
		for(int f = 0; f<broker.numFittingMethods(); f++)
			methodBox.addItem(broker.getFittingMethod(f).getName());
		
		seriesBox.addActionListener(l->{
			methodBox.removeAllItems();
			for(int f = 0; f<broker.numFittingMethods(); f++)
				methodBox.addItem(broker.getFittingMethod(f).getName());
		});
				
		
		JPanel methodPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		methodPanel.add(lblMethod);
		methodPanel.add(methodBox);
		
		main.add(methodPanel);
		
		
		/*
		
		//Choice of dimensions
		List<Dimension<?,?>> dims = new ArrayList<>();
		for(int d = 0; d<broker.numFittableDimensions(); d++)
			dims.add(broker.fittableDimension(d));
		
		DimensionChoice<?> dimChoice = new DimensionChoice(dims);
		main.add(dimChoice);
		*/
		
		
		
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(l->{
			
			chosenQuery = (String) seriesBox.getSelectedItem();
			chosenMethod = methodBox.getSelectedIndex();			
			if(chosenQuery == null || chosenMethod == -1){
				JOptionPane.showMessageDialog(this.getParent(), "Please choose one method and one series");
				return;
			}			
			wasCancelled = false;			
			this.dispose();
		});
		
		JButton btnCancel = new JButton("Cancel");		
		btnCancel.addActionListener(l->this.dispose());
			
		

		JPanel btnPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		btnPanel.add(btnOk);
		btnPanel.add(btnCancel);
		
		main.add(btnPanel);
		
		

		setTitle("Fitting Options...");
	

		
		// TODO Auto-generated constructor stub
	}



	@Override
	public void setVisible(boolean b){
		//Override here in order to display a warning message instad if there is no dataset in the model
		if(b && noDataset){
			JOptionPane.showMessageDialog(this.getParent(), "There is no dataset loaded yet");
			return;
		}
		else
			super.setVisible(b);
	}



	public boolean wasCancelled(){
		return wasCancelled;
	}



	public String getChosenQuery() {
		return chosenQuery;
	}



	public int getChosenMethod() {
		return chosenMethod;
	}


	






}
