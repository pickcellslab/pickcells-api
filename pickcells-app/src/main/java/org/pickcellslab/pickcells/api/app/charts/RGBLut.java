package org.pickcellslab.pickcells.api.app.charts;

public interface RGBLut {

	public byte[][] data();
	
}
