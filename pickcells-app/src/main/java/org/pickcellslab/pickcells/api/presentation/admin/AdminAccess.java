package org.pickcellslab.pickcells.api.presentation.admin;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;

@Core
@Scope(name="ADMIN-ACCESS", parent="", policy=ScopePolicy.SINGLETON)
public interface AdminAccess {

}
