package org.pickcellslab.pickcells.api.app.charts;

import java.awt.event.ActionListener;
import java.util.function.Consumer;

import javax.swing.Icon;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.dataviews.mvc.CameleonView;
import org.pickcellslab.foundationj.dataviews.mvc.DataView;
import org.pickcellslab.foundationj.dataviews.mvc.DataViewModel;
import org.pickcellslab.foundationj.dataviews.mvc.DimensionBroker;
import org.pickcellslab.foundationj.dataviews.mvc.FittableModel;
import org.pickcellslab.foundationj.dataviews.mvc.ReconfigurableView;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableDimensionsModel;
import org.pickcellslab.foundationj.dataviews.mvc.TransformableDimensionBroker;

public interface DBViewToolBarBuilder {

	

	public DBViewToolBarBuilder addButton(Icon icon, String tooltip, ActionListener l);
	
	public <T,D> DBViewToolBarBuilder addChangeDataSetButton(QueryWizardConfigFactory<T,D> configFactory, DataViewModel<T> model) ;


	public <M extends RedefinableDimensionsModel<T,B,D>, B extends DimensionBroker<T,D>,D,T> DBViewToolBarBuilder addChangeDimensionsButton(
			ReconfigurableView<M,T,B,D> view);


	public <M extends RedefinableDimensionsModel<T,B,D>, B extends DimensionBroker<T,D>,D,T extends DataItem> DBViewToolBarBuilder addExportDataButton(
			ReconfigurableView<M,T,B,D> view);

	public <V extends CameleonView<A> & DataView<?,?>, A> DBViewToolBarBuilder addCameleonControl(V view, AppearanceFactory<A> factory);

	public <T,R,D> DBViewToolBarBuilder addDistributionFittingControl(FittableModel<T,R,D> model);

	public <M extends RedefinableDimensionsModel<T,B,D>, B extends TransformableDimensionBroker<T,D>,D,T> 
	DBViewToolBarBuilder addTransformDimensionControl(ReconfigurableView<M,T,B,D> view, String tooltip);

	/**
	 * Adds a group of toggle buttons allowing the user to choose between objects a bit like radio buttons
	 * @param icons The icons to display for each button
	 * @param choices The objects corrsponding to the provided icons
	 * @param tooltips The tooltips for each button
	 * @param consumer The consumer which will process the object when selected
	 * @return The updated DBViewToolBarBuilder
	 */
	public <T> DBViewToolBarBuilder addToggleControl(Icon[] icons, String[] tooltips, T[] choices, Consumer<T> consumer);
	
	
	public JPanel build();


	
}
