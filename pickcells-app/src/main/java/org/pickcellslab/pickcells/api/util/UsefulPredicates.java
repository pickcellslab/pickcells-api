package org.pickcellslab.pickcells.api.util;

import java.util.function.Predicate;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;
import org.pickcellslab.pickcells.api.datamodel.types.DataRoot;

public class UsefulPredicates {

	/**
	 * A useful {@link Predicate} which can be used to determine if a {@link MetaReadable} should be considered as {@link #DIRECTIONAL}
	 */
	public static Predicate<MetaReadable<?>> directionalMeta = d -> d.dataType() == dType.NUMERIC && d.numDimensions() == 3 && d.description().contains("DIRECTIONAL");

	/**
	 * A useful {@link Predicate} which can be used to determine if a {@link MetaQueryable} can be considered as
	 * a root for a dataset.
	 * @see DataRoot
	 */
	public static Predicate<MetaQueryable> datasetTypes = (d) -> {
		if(MetaClass.class.isAssignableFrom(d.getClass()))
			((MetaClass)d).tags().forEach(t->t.toString().equals(DataModel.DATAROOT));
		return false;
	};

	
}
