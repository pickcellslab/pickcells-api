package org.pickcellslab.pickcells.api.app.modules;

/*-
 * #%L
 * foundationj-modules
 * %%
 * Copyright (C) 2016 - 2017 PickCellsLab
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * A ProgressPublichser listens to progresses made by a {@link StepwiseProcess} while the module runs. It can thus report 
 * in the form of a graphical log for instance on the progress made by the listened module.
 * 
 * @author Guillaume Blin
 *
 */
public interface ProgressPublisher {
	
	/**
	 * Registers the given {@link StepwiseProcess}. Should automatically add itself as a listener. 
	 * @param p The {@link StepwiseProcess} to register
	 */
	public void register(StepwiseProcess p);
	
	
	public void setStep(StepwiseProcess source, int step);
	
	public void setProgress(StepwiseProcess source, float progress);
	
	public void cancelled(StepwiseProcess source);
	
	public void failure(StepwiseProcess source, String message);
	
	
	public List<StepwiseProcess> activeProcesses();
	
	
	
	
}
