package org.pickcellslab.pickcells.api.app.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;

public class AbstractTotiPicker<T> extends AbstractSeriesAndSinglePicker implements DistributionPicker<T>{

	protected List<DistributionPickConsumer<T>> distriLstrs = new ArrayList<>();


	@Override
	public void addDistributionPickListener(DistributionPickConsumer<T> l) {
		if(l!=null)
			distriLstrs.add(l);
	}

	@Override
	public void removeDistributionPickListener(DistributionPickConsumer<T> l) {
		distriLstrs.remove(l);
	}
	
	
	protected <D> void fireDistributionPicked(Distribution<D> d, MetaQueryable v, Function<T,D> f){
		Objects.requireNonNull(d, "Distribution is null");
		Objects.requireNonNull(v, "Vignette is null");
		Objects.requireNonNull(f, "Function is null");
		distriLstrs.forEach(l->l.distributionPicked(d, v, f));
	}

}
