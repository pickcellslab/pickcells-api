package org.pickcellslab.pickcells.api.presentation.workbench;

public interface WorkbenchListener {

	
	public void closeExperimentClicked(String expName);
	
}
