+++

title ="Create Plugins"
weight = 3

date = "2018-04-15"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Mike Jackson"
lastmod = "2018-08-02"

repo = "pickcells-api"
fileUrl = "https://framagit.org/pickcellslab/pickcells-api/tree/docs/docs/content/developers/plugins/_index.md"

tags = ["develop", "intro"]

+++

In this section you will find documentation on how to create your own plugin for PickCells.

If you are new to creating plugins for PickCells, you should also look at how to [set up a plugin development environment](/developers/plugins/plugin_setup).

There are several plugin types that you can create. The flowchart below is designed to help you find the type that you need.  

{{<mermaid align="left">}}

graph LR

db_access(I need access to the database ?)
db_access -->|yes| db_create{I want to create}

db_create-->db_vis(Visualisations)
db_create-->db_analyse(Analysis Methods)
db_create-->db_image(Image Interactions)

db_vis-->mod_docfctry[Document Factory]

db_analyse-->db_rsrc(Resource Intensive ?)

db_rsrc-->|no|db_launch(With a launcher)
db_rsrc-->|yes|mod_analysis[Analysis]

db_launch-->|In the Menu|mod_utility[Utility]
db_launch-->|In theTask Bar|mod_task[Simple Task]

db_image-->mod_mgr[Annotation Manager]



db_access-->|no| no_db{I want to provide}
no_db-->|generic charts| mod_chart[Chart Factory]
no_db-->|new object descriptors| mod_feature[Feature Computer]
no_db-->|image processing| mod_todo[Not yet available as a plugin. <br> Please see Contributing to PickCells]
no_db-->|something else| mod_desk[Desktop Module]


click mod_desk "{{< ref "desktop_module.md" >}}" "Link to the tutorial"
click mod_docfctry "{{< ref "document_factory.md" >}}" "Link to the tutorial"
click mod_chart "{{< ref "chart_factory.md" >}}" "Link to the tutorial"
click mod_feature "{{< ref "feature_computer.md" >}}" "Link to the tutorial"
click mod_analysis "{{< ref "analysis.md#analysis" >}}" "Link to the tutorial"
click mod_utility "{{< ref "analysis.md#utility" >}}" "Link to the tutorial"
click mod_task "{{< ref "analysis.md#simple-task" >}}" "Link to the tutorial"
click mod_mgr "{{< ref "annotation_manager.md" >}}" "Link to the tutorial"

classDef module fill:#FFD37D, stroke:#0C090A, stroke-width:2px, color:#2B65EC, font-weight: bold;
class mod_desk,mod_docfctry,mod_chart,mod_analysis,mod_utility,mod_task,mod_feature,mod_mgr module;

classDef type fill:#FFE5B3, stroke:#0C090A, color:#0C090A;
class db_vis,db_analyse,db_image type;

classDef question fill:#FFFBF3, stroke:#0C090A, color:#0C090A;
class db_access,no_db,mod_todo,db_create,db_rsrc,db_launch question;

{{< /mermaid >}}

In the above flowchart, plugin nodes are in dark orange and you can click on the nodes to access the corresponding tutorial.

PickCells supports the following types of plugin:

* __Feature Computer:__ A Feature Computer computes new descriptors for Segmented Objects.
* __Chart Factory:__ These Factories provide runtime implementations for simple charts. Unlike data visualisations created by a Document Factory, simple charts are destined to be available for analysis modules to be used to preview results or to show the histogram of an image region for example.
* __Desktop Module:__ These plugins are launched from the Utility Desktop. They do not deal with the data present in the database and can be used independently from an Experiment.
* __Utility:__  Utility plugins are launched from the Utilities menu. They can perform data related tasks but are by convention not resource intensive.
* __Simple Task:__ Simple Tasks are similar to Utility plugins except that they get more visibility from a user perspective as the launcher is located in the task bar with a large icon.
* __Analysis:__ An Analysis, like a Simple Task posesses a launcher in the task bar. Analyses can perform any forms of data processing, image segmentation, etc. They update the user with their progress using stepwise progress bars and any registered listeners.
* __Document Factory:__ These modules create a launcher in the 'visualisation' toolbar. They can create new windows to display any kind of data visualisation
* __Annotation Manager:__ These modules provide image interaction functionalities. For example editing segmented shapes, or lineage tracks.
