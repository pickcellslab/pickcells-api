+++

title ="Set up a Plugin Development Environment"
weight = 1

date = "2018-04-18"
creatorDisplayName = "Guillaume Blin"

lastmodifierdisplayname = "Guillaume Blin"
lastmod = "2019-01-08"

repo = "pickcells-api"
fileUrl = "https://framagit.org/pickcellslab/pickcells-api/tree/docs/docs/content/developers/plugins/plugin_setup.md"

tags = ["develop", "tutorial"]

+++

## Contents

* [Introduction](#introduction)
* [Set up your environment](#set-up-your-environment)
* [Import the archetype catalog](#import-the-archetype-catalog)
* [Create a project using the archetype](#create-a-project-using-the-archetype)
* [Enable annotation processing](#enable-annotation-processing)
* [Understanding the project's structure](#understanding-the-project-s-structure)
  - [src/main/java](#src-main-java)
  - [src/main/resources](#src-main-resources)
  - [src/test/java](#src-test-java)
  - [target/](#target)
  - [pom.xml](#pom-xml)
* [Where Next?](#where-next)

---

## Introduction

This tutorial explains how to setup your environment in order to create a new plugin for PickCells and provides explanations about the diverse components of a plugin project.

Before you start, please note that there exists a number of projects which are maintained independently because of the third-party libraries they depend on. Here is a non-exhaustive list of library specific projects for PickCells:

* [pickcells-j3d](https://framagit.org/pickcellslab/pickcells-j3d): visualisation modules based on [Java3D](https://en.wikipedia.org/wiki/Java_3D).
* [pickcells-jzy3d](https://framagit.org/pickcellslab/pickcells-jzy3d): visualisation modules based on [jzy3D](http://www.jzy3d.org/).
* [pickcells-rjava](https://framagit.org/pickcellslab/pickcells-rjava): Allows R integration into PickCells using [rJava](https://cran.r-project.org/web/packages/rJava/index.html).
* [pickcells-jfree](https://framagit.org/pickcellslab/pickcells-jfree): visualisation modules based on [JFreeChart](http://www.jfree.org/jfreechart/).
* [pickcells-prefuse](https://framagit.org/pickcellslab/pickcells-prefuse): visualisation modules based on [Prefuse](http://prefuse.org/).

If you are planning on using one of the above libraries, you should access the corresponding repository as these projects already contain utility classes and a dependency management system which you will find useful. If unsure of what to do then please ask on the [PickCells Colony forum](https://colony.pickcellslab.org/).

If your were planning on using one of the above libraries, you should fork, develop and contribute the corresponding repository as these projects already contain utility classes and a dependency management system which you will find useful.

If you would like to contribute to the PickCells framework itself, please see [Develop PickCells core](/developers/dev_core/).

If you wish to create a feature which uses the PickCells API but none of the libraries mentioned above or if you just want to experiment, then you are in the right place! {{< icon name="thumbs-up" size="large" >}}


---

## Set up your environment

This tutorial assumes you have already downloaded, installed and run PickCells, as described in our documentation on how to [Get Started](/getstarted/).

Install Java 8 as described in our documentation on how to [Install PickCells](/getstarted/install/).

Install the [Eclipse](https://www.eclipse.org/) integrated development environment. We recommend installing the latest version of the [Eclipse IDE for Java Developers](https://www.eclipse.org/downloads/eclipse-packages/), as this includes a [Maven](https://maven.apache.org/) integration package, [m2e](https://projects.eclipse.org/projects/technology.m2e).

Maven is used as PickCells' build and dependency management tool. Maven supports [archetypes](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html), which are template Maven projects.

We will now use Eclipse to create a Maven project which will contain the desired directory structure for a PickCells plugin and some template code. This should only take a few minutes to accomplish thanks to a Maven archetype designed specifically for this purpose.


---

## Import the archetype catalog

An archetype catalog is a small [XML](https://en.wikipedia.org/wiki/XML) file which will tell Maven which archetypes are available. To import the PickCells archetype catalog, in Eclipse go to Window > preferences.

In the left panel of the dialog navigate to Maven > archetype and click to 'Add remote catalog'.

In the 'Catalog file' text box, enter the following URL:

```
https://archiva.pickcellslab.org/repository/pickcells-releases/archetype-catalog.xml
```



Enter a custom description in the box underneath.

![catalog](/developers/plugins/maven_load_catalog.png?classes=border,align-left,shadow "Catalog Import Dialog")


---

## Create a project using the archetype

Now we can create a new Maven project using the newly available archetype. To do so, go to File > New... and select Maven Project:

![new maven](/developers/plugins/new_maven_project.png?classes=border,align-left,shadow "New Maven Project Dialog")

You will then be asked where the new project should be created. You are free to choose a different location than the default one (which should be a new folder of your current workspace location).

After this, the next dialog will ask you to choose an archetype. You should be able to choose the pickcells-archetype catalog that we imported previously. When you select the pickcells-archetype catalog, the pickcells-archetype should appear underneath as shown in the screenshot below. Select it and then click 'Next'.

![pickcells archetype](/developers/plugins/pickcells_archetype.png?classes=border,align-left,shadow "Select Archetype Dialog")

The final dialog allows you to define the 'Maven coordinates' for your project. In brief, once built, your project will produce a binary file (most likely a Java jar file) which, in Maven terms, is called an artifact. In order to identify this artifact, Maven uses a coordinate system which consists of a '_groupId_', an '_artifactId_' and a '_version_'. This allows Maven to manage versions and dependencies when building any project. For more information on this, see [Maven Coordinates](https://maven.apache.org/pom.html#Maven_Coordinates). The documentation also includes best naming practices for your coordinates.

Here we have create an `Example` ('_artifactId_) project which belongs to the 'family' or group 'org.pickcellslab.plugins` ('_groupId') and with a '_version_' of `0.1`.

![coordinates](/developers/plugins/new_example.png?classes=border,align-left,shadow "Define Coordinates Dialog")

Notice that an additional field is requested, 'project_name'.

This field is specific to the pickcells-archetype. It will be used to generate template code in our new project. Here we are using the name `Example` again. This does not have to be the same as the artifactId. You should use a name which does not contain characters which are illegal in class definitions as this name will be used for naming template classes in the project (we describe these classes in 'Understanding the project's structure' below).

When you click 'Finish', a new project should appear in your workspace with the following structure:

![archetyped project](/developers/plugins/archetyped_project.png?classes=border,align-left,shadow "Project Structure")





---

## Enable annotation processing

Java annotations are used to enable the framework to discover the plugins at runtime and also to ensure that a few conventions are respected.  

As some of the annotations are not standard java annotations, custom annotation processing must be enabled to allow eclipse to display potential warnings or errors in the editor.

To do so, right-click on the project in the project explorer panel and select Build Path -> Configure Build Path...

Then navigate to Java Compiler -> Annotation Processing and tick all boxes:

![Custom Annotation Processing](/developers/plugins/annotation_proc_1.png?classes=border,align-left,shadow)

Then in Factory Path, enable project specific settings and click 'Add external jar' to add the annotation processors which are part of the foundationj-wiring.jar artefact. 

![Factory Path](/developers/plugins/annotation_proc_2.png?classes=border,align-left,shadow)

NB: The foundationj-wiring jar file should be located in your local maven repository

```
~/.m2/repository/org/pickcellslab/foundationj/foundationj-wiring/0.1.0-SNAPSHOT/foundationj-wiring-0.1.0-SNAPSHOT.jar
```


That's it, you are now ready to start coding {{< icon name="thumbs-up" size="large" >}}!

If you wish to get started quickly and are familiar with Maven conventions, you may skip the next section and check out the [Where Next?](#where-next) section further below.


---

## Understanding the project's structure

The following sections explain the structure of the project created with the method described above. If you are familiar with Maven, you will notice that the directory structure follows the Maven [standard directory layout](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html)

### src/main/java

This is where your source code is located. By default, this folder already contains two template classes: 

![main java](/developers/plugins/main_java.png?classes=border,align-left,shadow)

Note that the names of these classes  (`{project_name}_Node` and `{project_name}_Plugin`) are generated based on the value that we entered in the 'project_name' field when creating the project. The classes are as follows:

* `Example_Node`: This is a template `NodeItem` object. For more information on NodeItems and the property graph data structure used in PickCells, please refer to the following tutorial {**TODO** link to proper tutorial}
* `Example_Plugin`:  This object implements `SimpleTask` and can be used as a template to generate a 'SimpleTask'-type of plugin for PickCells. For a tutorial on creating plugins for PickCells, please see {**TODO** link to proper tutorial}

### src/main/resources

This is the folder for your resources which need to be included at runtime:

![main resources](/developers/plugins/main_resources.png?classes=border,align-left,shadow)

By default, an `icons/` subfolder is present and it contains a `.png` file. This is here to illustrate where you may store your icons so they are available at runtime. For example, our `Example_Plugin` class mentioned previously implements `SimpleTask`. One of its methods uses the default icon:

```java
@Override
    public Icon icon() {
        // This will define which icon to display in the menu bar of pickcells for this module
        return new ImageIcon(getClass().getResource("/icons/default_icon_32.png"));
}
```

### src/test/java

This folder is where unit tests are located. For an introduction to unit testing concepts, see [unit tests](https://en.wikibooks.org/wiki/Introduction_to_Software_Engineering/Testing/Unit_Tests) in [Introduction to Software Engineering](https://en.wikibooks.org/wiki/Introduction_to_Software_Engineering) on Wikibooks.

![test java](/developers/plugins/test_java.png?classes=border,align-left,shadow "Unit Tests Source")

In PickCells we use the [JUnit 5](https://junit.org/junit5/) unit testing framework in our archetyped project for writing unit tests. The dependencies are already configured and ready to use.

A template test for the `Example_Node` has already been created to illustrate how testing can be performed. For instance, our `Example_Node` class declares the following method:

```java
public String sayHi() {
	LoggerFactory.getLogger(getClass()).debug(typeId()+" says Hi!");
	return "Hi!";
}
```
and our `Example_NodeTest` has the following method:

```java
	@Test
	public void testSayHi() {
		Example_Node testNode = new Example_Node();
		Assert.assertEquals("Hi!", testNode.sayHi());
	}
```

The `@Test` annotation allows JUnit to find and run the method as a JUnit test. In Eclipse, you can right-click on the method declaration to run this specific method as a test. The test should pass and the console should display the logging message.  

This is a very simple method just to show that the framework for unit testing is readily available in the archetyped project.

We recommend to write rigourous and automated unit tests for your plugins. For information on writing tests with JUnit 5, see the [JUnit 5 Tutorial](https://howtodoinjava.com/junit-5-tutorial/).



### target/

The target folder is where the built artifacts from your project will be located. To create the artifact issue the following Maven command (either in a terminal window or from within Eclipse):

```
mvn clean install
```

This command will update, test, build and install your project and create following layout in the target folder:

![mvn install](/developers/plugins/target.png?classes=border,align-left,shadow "Maven Installation")

Notice the `example-0.1.jar` file. This is the jar file that you can drop into the plugins folder of the PickCells installation to see the result of your code.  If you do this without changing anything from the archetyped project, you will obtain something similar to this in the bottom left corner of the workbench (in `PickCells-0.7.3-SNAPSHOT`):

![plugin presence](/developers/plugins/plugin_presence_annotated.png?classes=border,align-left,shadow "Plugin Presence")

### pom.xml

The last file is an important one. The `pom.xml` file is your Maven configuration file which defines your artifact coordinates, build procedure, dependencies, etc.

With the archetype, we have tried to make it easy to get setup. For example, the dependency to [JUnit 5](https://junit.org/junit5/) is already defined. If you open the pom file in your editor, you will find the corresponding line.

```xml
<!-- Unit Testing -->
<dependency>
    <groupId>org.junit.platform</groupId>
    <artifactId>junit-platform-runner</artifactId>
    <version>1.1.0</version>
    <scope>test</scope>
</dependency>
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-engine</artifactId>
    <version>5.1.0</version>
    <scope>test</scope>
</dependency>
```

In PickCells we also use [Mockito](http://site.mockito.org/) for mocking some objects in our tests, the dependencies are not included by default. However you could add this by adding the following lines in the `dependencies` section:

```xml
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-core</artifactId>
    <version>2.16.0</version>
    <scope>test</scope>
</dependency>
```

Note the `scope` tag. This enables the dependency only during the test phase and not in your final build.

More on Maven POM files can be found in Maven's [Introduction to the POM](https://maven.apache.org/guides/introduction/introduction-to-the-pom.html).



---

## Where Next?


Once your environment is setup, you may start learning about the PickCells API by:

  * Having a look at Tutorial code in the [pickcells-tutorials repository](https://framagit.org/pickcellslab/pickcells-tutorials) (**/src folder not the /docs folder**)
  * Learning about general concepts on the [documentation website](https://pickcellslab.frama.io/docs/). A good place to start is the [Architecture and Concept](https://pickcellslab.frama.io/docs/developers/architecture/) page in the develop section.


