package org.pickcellslab.pickcells.api.geometry.bvh;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiConsumer;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.math3.util.Pair;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.pickcellslab.foundationj.datamodel.tools.BinaryTreeBFS;

public class BVHTest {

	static List<float[]> points = new ArrayList<>();

	@BeforeClass
	public static void setup(){

		//Create a random list of points in 2 dimensions
		/*
		int n = 500;

		for(int i = 0; i<n; i++){
			float[] p = new float[2];
			p[0] = (float) (Math.random()*250)+125;
			p[1] = (float) (Math.random()*250)+125;
			points.add(p);
		}
		 */

		int n = 360;

		for(int i = 0; i<n; i++){
			float[] p = new float[2];
			float r = (float) (Math.sin(0.1*i)*40)+50;
			float phi = (float) (i*Math.PI/180);
			p[0] = (float) ((float) r*Math.cos(phi)+250f);
			p[1] = (float) ((float) r*Math.sin(phi)+250f);
			points.add(p);
		}
	}			


	@AfterClass
	public static void clean(){
		points = null;
	}



	//@Test
	public void testBVHUI() throws InterruptedException{

		DrawArea draw = new DrawArea();
		Painter painter = new Painter(points);
		draw.setPainter(painter);
		draw.setPreferredSize(new Dimension(500,500));

		JFrame f = new JFrame("BVH Test... ");
		f.setContentPane(draw);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);


		BVHNode<BVHNodeObject<List<float[]>>,List<float[]>> root = new BVHNodeObject<>(points, null);
		BinaryTreeBFS<BVHNode<BVHNodeObject<List<float[]>>,List<float[]>>> bfs = new BinaryTreeBFS<>(root);

		/*
		int c = 0;
		for(BVHNode n : bfs){
			painter.addBVH(n);
			draw.repaint();
			Thread.sleep(500);
			System.out.println(c++);
		}	
		 */

		final Iterator<List<BVHNode<BVHNodeObject<List<float[]>>, List<float[]>>>> itr = bfs.depthAsListIterator();
		int c = 0;
		byte[][] lut = lut();
		while(itr.hasNext()){			
			List<BVHNode<BVHNodeObject<List<float[]>>,List<float[]>>> list = itr.next();
			painter.addBVH((List)list);

			int r = lut[0][c*16] & 0xff;
			int g = lut[1][c*16] & 0xff;
			int b = lut[2][c*16] & 0xff;

			System.out.println(r+";"+g+";"+b);

			painter.setColor(new Color(r, g, b));
			draw.repaint();
			Thread.sleep(1000);
			System.out.println(c++);
		}




	}

	
	
	@Test
	public void testBVHSplit(){
		
		BVH<String> bvh = new BVH<>(points, "Test");
		Pair<BVH<String>,BVH<String>> p = bvh.split();
		System.out.println("Before Split -> "+bvh.size());
		System.out.println("Left -> "+p.getFirst().size());
		System.out.println("Right -> "+p.getSecond().size());
	}
	
	@Test
	public void testBVHCrumbs(){
		
		BVH<String> bvh = new BVH<>(points, "Test");
		
		List<BVH<String>> list = new ArrayList<>();
		list.add(bvh);
		
		bvh.crumbs((c)-> assertTrue(c.size() <= 3) , 3);

	}
	
	


	private class Painter implements BiConsumer<DrawArea,Graphics>{

		private List<float[]> samples = new ArrayList<>();
		private List<BVHNode<?,?>> bvh = new ArrayList<>();
		private Color color = Color.green;

		private Painter(List<float[]> samples){
			this.samples = samples;
		}


		@Override
		public void accept(DrawArea d, Graphics g) {

			System.out.println("Painting!");
			//clear
			//g.setColor( Color.white ); 
			//g.fillRect (0, 0, d.getWidth(), d.getHeight()); 


			g.setColor(Color.black);
			// First draw the sample
			for(float[] p : samples)
				g.drawOval((int)p[0], (int)p[1], 2, 2);

			g.setColor(color);
			for(BVHNode<?,?> n : bvh){
				g.drawOval((int)n.center()[0], (int)n.center()[1], 2, 2);
				drawCircle(g, (int)n.center()[0], (int)n.center()[1], (int)n.radius());
			}
		}

		public void drawCircle(Graphics cg, int xCenter, int yCenter, int r) {
			cg.drawOval(xCenter-r, yCenter-r, 2*r, 2*r);
		}


		public void addBVH(List<BVHNode<?,?>> node){
			bvh = node;
		}

		public void setColor(Color c){
			this.color = c;
		}
	}






	@SuppressWarnings("serial")
	public static class DrawArea extends JPanel{

		private BiConsumer<DrawArea, Graphics> painter;

		@Override
		public void paint(Graphics g){
			if(null != painter)
				painter.accept(this,g);
			else super.paint(g);
		}

		public void setPainter(BiConsumer<DrawArea,Graphics> painter){
			this.painter = painter;
		}

	}



	private byte[][] lut() {
		final byte[][] gray = new byte[ 3 ][ 256 ];

		byte[] c = new byte[]{0,0,0};			
		for(int i = 0; i<16; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{1,(byte) 171,(byte) 171};
		for(int i = 16; i<32; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{1,1,(byte) 224};
		for(int i = 32; i<48; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{1,110,(byte) 254};
		for(int i = 48; i<64; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{1,(byte) 171,(byte) 254};
		for(int i = 64; i<80; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{1,(byte) 224,(byte) 254};
		for(int i = 80; i<96; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{1,(byte) 254,1};
		for(int i = 96; i<112; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{(byte) 190,(byte) 255,0};
		for(int i = 112; i<128; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{(byte) 255,(byte) 224,0};
		for(int i = 128; i<160; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{(byte) 255,(byte) 141,0};
		for(int i = 160; i<176; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{(byte) 250,94,0};
		for(int i = 176; i<192; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{(byte) 245,0,0};
		for(int i = 192; i<208; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{(byte) 245,0,(byte) 135};
		for(int i = 208; i<223; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{(byte) 222,0,(byte) 222};
		for(int i = 223; i<240; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		c = new byte[]{(byte) 255,(byte) 255,(byte) 255};
		for(int i = 240; i<256; i++){
			gray[0][i] = c[0];
			gray[1][i] = c[1];
			gray[2][i] = c[2];
		}

		return gray;
	}	




}
