package org.pickcellslab.pickcells.api.geometry.bvh;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.junit.AfterClass;
import org.junit.BeforeClass;




public class BVTTTest {

	static List<float[]> pointsA = new ArrayList<>();
	static List<float[]> pointsB = new ArrayList<>();
	static List<float[]> pointsC = new ArrayList<>();
	static List<float[]> pointsD = new ArrayList<>();
	static List<float[]> pointsE = new ArrayList<>();

	static int N = 3000;
	
	@BeforeClass
	public static void setup(){

		//Create a random list of points in 2 dimensions
		int n = 360;

		for(int i = 0; i<n; i+=4){
			float[] p = new float[2];
			float r = (float) (Math.sin(0.05*i)*20)+80;
			float phi = (float) (i*Math.PI/180);
			p[0] = (float) ((float) r*Math.cos(phi)+350f);
			p[1] = (float) ((float) r*Math.sin(phi)+250f);
			pointsA.add(p);
		}



		for(int i = 0; i<n; i+=4){
			float[] p = new float[2];
			float r = (float) (Math.sin(0.1*i)*10)+80;
			float phi = (float) (i*Math.PI/180);
			p[0] = (float) ((float) r*Math.cos(phi)+150f);
			p[1] = (float) ((float) r*Math.sin(phi)+150f);
			pointsB.add(p);
		}


		for(int i = 0; i<n; i+=4){
			float[] p = new float[2];
			float r = (float) (Math.sin(0.1*i)*10)+100;
			float phi = (float) (i*Math.PI/180);
			p[0] = (float) ((float) r*Math.cos(phi)+250f);
			p[1] = (float) ((float) r*Math.sin(phi)+350f);
			pointsC.add(p);
		}

		
		for(int i = 0; i<N; i++){
			float[] p = new float[2];
			p[0] = (float) ((float) Math.random() * 500);
			p[1] = (float) ((float) Math.random() * 500);
			pointsD.add(p);
		}
		
		
		for(int i = 0; i<n; i+=4){
			float[] p = new float[2];
			float r = (float) (Math.sin(0.05*i)*20)+80;
			float phi = (float) (i*Math.PI/180);
			p[0] = (float) ((float) r*Math.cos(phi)+350f);
			p[1] = (float) ((float) r*Math.sin(phi)+450f);
			pointsE.add(p);
		}
		

	}

	@AfterClass
	public static void clean(){
		pointsA = null;
		pointsB = null;
		pointsC = null;
		pointsD = null;
		pointsE = null;
	}


	//@Test
	public void testClosestSearch() throws InterruptedException{

		DrawArea draw = new DrawArea();
		Painter painter = new Painter(draw);		
		draw.setPainter(painter);
		draw.setPreferredSize(new Dimension(500,500));

		JFrame f = new JFrame("BVH Test... ");
		f.setContentPane(draw);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);



		BVH<Color> source = new BVH<>(pointsB,Color.BLUE);//new Color(105,105,105));
		List<BVH<Color>> targets = new ArrayList<>();
		targets.add(new BVH<>(pointsA,Color.YELLOW));
		targets.add(new BVH<>(pointsC,Color.RED));
		targets.add(new BVH<>(pointsE,Color.GREEN));

		painter.addBVH(source);
		painter.addBVH(targets.get(0));
		painter.addBVH(targets.get(1));
		painter.addBVH(targets.get(2));

		
		List<BVTTNode<Color, Color>> list = ClosestMeshFinder.closestPoints(source, targets, 
				0.05f, 200f, 500f,
				new RoundPainter(painter,Color.BLACK),
				new TestPainter(painter),
				new LeavePainter(painter,Color.MAGENTA),
				new PainterClearer(painter), 
				1);
		
		System.out.println("Number of BVTTNodes : "+list.size());

		painter.addBVTest(list);

		draw.repaint();
		Thread.sleep(20000);
	}
	
	
	
	//@Test
	public void testClosestSearchSinglePoint() throws InterruptedException{

		DrawArea draw = new DrawArea();
		Painter painter = new Painter(draw);		
		draw.setPainter(painter);
		draw.setPreferredSize(new Dimension(500,500));

		JFrame f = new JFrame("BVH Test... ");
		f.setContentPane(draw);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);



		BVH<Color> source = new BVH<Color>(pointsB.get(0),Color.BLACK);
		BVH<Color> target = new BVH<Color>(pointsC,Color.RED);

		painter.addBVH(source);
		painter.addBVH(target);


		painter.addBVTest(ClosestMeshFinder.closestPoints(source,target, 
				0.3f, 100f,
				new RoundPainter(painter,Color.BLACK),
				new TestPainter(painter),
				new LeavePainter(painter,Color.MAGENTA),
				new PainterClearer(painter), 
				300)
				);

		draw.repaint();
		Thread.sleep(10000);
	}
	
	
	
	
	//@Test
	public void testClosestSearchSinglePointInside() throws InterruptedException{

		DrawArea draw = new DrawArea();
		Painter painter = new Painter(draw);		
		draw.setPainter(painter);
		draw.setPreferredSize(new Dimension(500,500));

		JFrame f = new JFrame("BVH Test... ");
		f.setContentPane(draw);
		f.pack();
		f.setLocationRelativeTo(null);
		f.setVisible(true);



		BVH<Color> source = new BVH<Color>(pointsB.get(0),Color.BLACK);
		BVH<Color> target = new BVH<Color>(pointsD,Color.RED);

		painter.addBVH(source);
		painter.addBVH(target);


		painter.addBVTest(ClosestMeshFinder.closestPoints(source,target, 
				1f/N, 10f,
				new RoundPainter(painter,Color.BLACK),
				new TestPainter(painter),
				new LeavePainter(painter,Color.MAGENTA),
				new PainterClearer(painter), 
				50)
				);

		draw.repaint();
		Thread.sleep(10000);
	}
	
	
	


	
	
	
	private class RoundPainter implements Consumer<List<BVTTNode<Color,Color>>>{

		private Painter painter;
		private Color color;

		RoundPainter(Painter p, Color color){
			this.painter = p;
			this.color = color;
		}

		@Override
		public void accept(List<BVTTNode<Color, Color>> t) {
			t.forEach(n->n.addLabel(color));
			painter.addTemporary(t);	
			System.out.println("Round ->"+t.size());
			painter.update();
		}
	}

	private class LeavePainter implements Consumer<BVTTNode<Color,Color>>{

		private Painter painter;
		private Color color;

		LeavePainter(Painter p, Color color){
			this.painter = p;
			this.color = color;
		}

		@Override
		public void accept(BVTTNode<Color, Color> t) {
			t.addLabel(color);	
			painter.update();
			
		}
	}

	private class PainterClearer implements Consumer<Void>{

		private Painter painter;

		PainterClearer(Painter p){
			this.painter = p;
		}

		@Override
		public void accept(Void t) {
			painter.clearTemporary();
		}

	}

	private class TestPainter implements BiConsumer<BVTTNode<Color, Color>, Boolean>{

		private Painter painter;
		private BVTTNode<Color, Color> previous;

		TestPainter(Painter p){
			this.painter = p;
		}

		@Override
		public void accept(BVTTNode<Color, Color> t, Boolean u) {

			if(previous!=null)
				previous.addLabel(Color.BLACK);	
			painter.update();
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			painter.setTested(t);
			
			if(u)
				t.addLabel(Color.GREEN);	
			else
				t.addLabel(Color.RED);

			previous = t;
			painter.update();
			
		}

	}



	private class Painter implements BiConsumer<DrawArea,Graphics>{

		
		
		private List<BVH<Color>> samples = new ArrayList<>();
		private List<BVTTNode<Color,Color>> bvh = new ArrayList<>();
		
		private List<BVTTNode<Color,Color>> temp = new ArrayList<>();

		private DrawArea draw;
		private BVTTNode<Color, Color> tested;

		Painter(DrawArea d){
			this.draw = d;
		}


		public void setTested(BVTTNode<Color, Color> t) {
			tested = t;
			
		}


		@Override
		public void accept(DrawArea d, Graphics g) {

			//clear
			
				g.setColor( Color.white ); 
				g.fillRect (0, 0, d.getWidth(), d.getHeight()); 
		
			// First draw the sample
			for(BVH<Color> bvh : samples){
				g.setColor(bvh.getUserObject());
				for(float[] p : bvh.getAllPoints())
					g.drawOval((int)p[0], (int)p[1], 2, 2);
			}

			if(tested!=null){
				g.setColor(Color.ORANGE);
				g.drawOval((int)tested.source().center()[0], (int)tested.source().center()[1], 5, 5);
				g.drawOval((int)tested.target().center()[0], (int)tested.target().center()[1], 5, 5);
			}
			
			//Draw temp nodes
			for(BVTTNode<Color,Color> n : temp){
				g.setColor((Color) n.getLabel());
				drawCircle(g, (int)n.target().center()[0], (int)n.target().center()[1], (int)n.target().radius());
				drawCircle(g, (int)n.source().center()[0], (int)n.source().center()[1], (int)n.source().radius());

			}

			//Draw final choices
			for(BVTTNode<Color, Color> n : bvh){

				g.setColor(blend(n.source().getUserObject(),n.target().getUserObject()));

				BVHNode<?,Color> s = n.source();
				drawCircle(g, (int)s.center()[0], (int)s.center()[1], 5);

				BVHNode<?,Color> t = n.target();
				drawCircle(g, (int)t.center()[0], (int)t.center()[1], 5);
			}
		}

		private void drawCircle(Graphics cg, int xCenter, int yCenter, int r) {
			cg.drawOval(xCenter-r, yCenter-r, 2*r, 2*r);
		}


		public void addTemporary(List<BVTTNode<Color,Color>> nodes){
			temp.addAll(nodes);
		}

		public void clearTemporary(){
			temp.clear();
		}


		public void addBVH(BVH<Color> color){
			samples.add(color);
		}


		public void addBVTest(List<BVTTNode<Color,Color>> node){
			bvh = node;
		}

		public void update(){
			draw.repaint();
		}

	}




	public static Color blend(Color c0, Color c1) {
		double totalAlpha = c0.getAlpha() + c1.getAlpha();
		double weight0 = c0.getAlpha() / totalAlpha;
		double weight1 = c1.getAlpha() / totalAlpha;

		double r = weight0 * c0.getRed() + weight1 * c1.getRed();
		double g = weight0 * c0.getGreen() + weight1 * c1.getGreen();
		double b = weight0 * c0.getBlue() + weight1 * c1.getBlue();
		double a = Math.max(c0.getAlpha(), c1.getAlpha());

		return new Color((int) r, (int) g, (int) b, (int) a);
	}





	@SuppressWarnings("serial")
	public static class DrawArea extends JPanel{

		private BiConsumer<DrawArea, Graphics> painter;

		@Override
		public void paint(Graphics g){
			if(null != painter)
				painter.accept(this,g);
			else super.paint(g);
		}

		public void setPainter(BiConsumer<DrawArea,Graphics> painter){
			this.painter = painter;
		}


	}

}
