package org.pickcellslab.pickcells.api.geometry.bvh;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

import org.pickcellslab.foundationj.datamodel.graph.BinaryTreeNode;

public interface BVHNode<B extends BVHNode<B,T>, T> extends BinaryTreeNode<BVHNode<B,T>>{

	float radius();

	float[] center();

	List<float[]> points();

	T getUserObject();
	
	int size();
	

	/**
	 * @param n1
	 * @param n2
	 * @return The distance between the 2 hypersphere defined by the provided BVHNode. 
	 * The value will be negative if the 2 hyperspheres intersect 
	 * @throws NullPointerException if one of the nodes is Null
	 */
	public static float distance(BVHNode<?,?> n1, BVHNode<?,?> n2){				
		return distance(n1.center(), n2.center()) - (n1.radius() + n2.radius());
	}

	public static float distance(float[] a, float[] b){
		float sum = 0;
		for (int i = 0; i < a.length; i++) {
			final float dp = a[i] - b[i];
			sum += dp * dp;
		}
		return (float) Math.sqrt(sum);
	}

	
	
	
}
