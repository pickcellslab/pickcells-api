package org.pickcellslab.pickcells.api.geometry.triangulation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides static method to create triangulations between {@link NodeItem}s
 * 
 * @author Guillaume Blin
 *
 */
public class DataTriangulation {

	private static Logger log = LoggerFactory.getLogger(DataTriangulation.class);
	
	
	/**
	 * Creates the delaunay triangulation between {@link NodeItem}s. The result is stores as {@link Link}
	 * of a desired types between the connected {@link NodeItem} objects. The created links will contain
	 * the euclidean distance as an attribute ({@link Keys#distance})
	 * @param data The list of NodeItem to triangulate
	 * @param f A {@link Function} which retrieves the coordinate from the NodeItem
	 * @param dimensions the number of dimension in space
	 * @param max The maximum distance between object allowed to create a link
	 * @param linkType The type of the link to create
	 */
	public static <T extends NodeItem> void triangulate(List<T> data, Function<T,double[]> f, int dimensions, double max, String linkType){

		//Restructure the data into a float array to inject into the visad delaunay solver

		float[][] coordinates = new float[dimensions][data.size()];

		try{
			for(int i=0 ; i<data.size(); i++){
				double[] centroid = f.apply(data.get(i));	
				for(int j=0 ; j<centroid.length; j++)
					coordinates[j][i] = (float) centroid[j];
			}
		}catch(ArrayIndexOutOfBoundsException e){
			log.error("The dimensions of the data are inconsistent", e);
			return;
		}

		log.info("Number of segmented object to triangulate = "+data.size());
		log.info("Number of coordinates = "+coordinates[0].length);
		
		Delaunay delan = null;

		try {
			//delan = Delaunay.factory(coordinates, true); 
			//delan = new DelaunayWatson(coordinates);
			delan = new DelaunayClarkson(coordinates);
			//TODO test delan.improve(coordinates, 3);
		}
		catch (Exception e) {
			log.error("The triangulation failed",e);
			return;
		}
		log.info("Delaunay number of unique edges = "+delan.NumEdges);

		int[][] vertices = delan.Vertices;
		int[][] tri = delan.Tri;

		log.info("Number of vertices = "+vertices.length);
		log.info("Number of triangles = "+tri.length);

		LongAdder counter = new LongAdder();

		for(int v = 0; v<vertices.length; v++){

			T source = data.get(v);
			//Keep track of targets added in the previous simplex (they share edges obviously)
			Set<T> visited = new HashSet<>();
			visited.add(source);

			for(int t = 0; t<vertices[v].length; t++){//itr through all the triangles the vertex belongs to
				//values are indices in Tri

				for(int summits = 0; summits<tri[vertices[v][t]].length; summits++){
					T target = data.get(tri[vertices[v][t]][summits]);
					if(visited.add(target)){

						//Do not add the link in both direction
						if(target.getLinks(Direction.OUTGOING, linkType)
								.filter(l->l.target()==source)
								.count() == 0){

							double distance = computeDistance(f.apply(source),f.apply(target));
							if(distance<=max){
								counter.increment();
								Link l = new DataLink(linkType, source, target, true);
								l.setAttribute(Keys.distance, distance);
							}
						}
					}
				}
			}
		}
		log.info("Number of vertices = "+counter); 	
	}
	
	
	
	
	

	

	/**
	 * Computes the euclidian distance between to {@link WritableDataItem} using their centroids.
	 * The provided items must possess a value for AKey.get("Centroid", double[].class), otherwise,
	 * a NoSuchElementException will be thrown
	 */
	private static double computeDistance(double[] d1,	double[] d2) throws NoSuchElementException{
		double sum = 0;
		for(int d = 0; d<d1.length; d++)
			sum += Math.pow(d1[d]-d2[d], 2);
		return Math.sqrt(sum);
	}




}
