package org.pickcellslab.pickcells.api.geometry.bvh;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class MeshConversion {

	private MeshConversion(){};


	public static List<float[]> asList(float[] mesh){

		if(mesh.length % 3 != 0)
			throw new IllegalArgumentException("The length of the array must be a multiple of the provided number of dimension");
	
		List<float[]> list = new ArrayList<>(mesh.length/3);
		for(int p = 0; p<mesh.length; p+=3){
			float[] p1 = Arrays.copyOfRange(mesh, p, p+3);	 	
			list.add(p1);
		}
		System.out.println("MeshConversion: size of the mesh in traingles = "+list.size());
		return list;
	}

}
