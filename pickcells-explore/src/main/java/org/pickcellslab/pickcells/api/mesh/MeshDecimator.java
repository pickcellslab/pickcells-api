package org.pickcellslab.pickcells.api.mesh;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.stream.Collectors;

import javax.vecmath.Point3f;

/**
 * 
 * Provides methods to decimate mesh structures.
 * 
 * @author Guillaume Blin
 *
 */
public final class MeshDecimator {


	private MeshDecimator(){/*Not instantiable*/}


	/**
	 * @param mesh A list of {@link Point3f} representing a mesh (size multiple of 3)
	 * @param force The strength of the decimation process
	 * @return The decimated list of points representing the mesh.
	 * @throws DecimationException
	 */
	public static List<Point3f> decimate(List<Point3f> mesh, float force)throws DecimationException{

		List<Point3f> decimated = null;
		try{
			EdgeContraction ec = new EdgeContraction(new FullInfoMesh(mesh), false);
			ec.removeUntil(force);
			decimated = ec.getMeshes().get(0).getMesh();
		}catch (NullPointerException e){
			throw new DecimationException();
		}

		return decimated;

	}


	public static List<float[]> decimateFloats(List<float[]> mesh, float force)throws DecimationException{

		List<float[]> decimated = null;
		try{
			EdgeContraction ec = new EdgeContraction(
					new FullInfoMesh(mesh.stream().map(f->new Point3f(f)).collect(Collectors.toList())), false);
			ec.removeUntil(force);
			decimated = ec.getMeshes().get(0).getMesh().stream().map(p->{
				float[] f = new float[3];
				p.get(f);
				return f;
			}).collect(Collectors.toList());
		}
		catch (NullPointerException e){
			throw new DecimationException();			
		}

		return decimated;

	}


}
