package org.pickcellslab.pickcells.api.geometry.bvh;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.commons.math3.util.Pair;

/**
 * A BVH (Boundary Volume Hierarchy) is a space partitioning strategy which can be useful to speed up searches in space
 * such as nearest neighbours. This implementation, uses an hyperspherical bounding volume with a number of dimension
 * identical to the number of dimension of the space to partition.
 * 
 * @author Guillaume Blin
 *
 */
public class BVH<T> {

	private final BVHNode<?,T> root;





	/**
	 * Constructs a new BVH containing one point only (useful for finding closest points belonging to other objects
	 * from the given point)
	 * @param point
	 * @param userObject
	 */
	public BVH(float[] point, T userObject) {
		Objects.requireNonNull(point);
		List<float[]> single = new ArrayList<>(1);
		single.add(point);
		root = new BVHNodeObject<>(single, userObject);		
	}



	/**
	 * Constructs a new {@link BVH} for the provided list of points
	 * @param points The List of points. To be valid, all float[] in the list must have the same number of dimensions
	 * @param userObject An object to associate with this BVH. Maybe null
	 */
	public BVH(final List<float[]> points, T userObject){
		Objects.requireNonNull(points);
		if(points.size() == 0)
			throw new IllegalArgumentException("The BVH must contain at least one point");
		root = new BVHNodeObject<>(points, userObject);		
	}




	private BVH(BVHNode<?,T> root){
		this.root = root;
	}

	
	
	
	
	




	@SuppressWarnings({ "unchecked", "rawtypes", "hiding" })
	private <T extends List<E>,E> BVH(T userObject, Function<E,float[]> translation) {
		Objects.requireNonNull(userObject);
		Objects.requireNonNull(translation);

		root = new TranslatedBVHNode(userObject, translation);		
	}


		
	
	
	
	


	public static <T> BVH<List<T>> create(List<T> points, Function<T,float[]> translation){
		return new BVH<>(points,translation);
	}



	public static <T> BVH<List<T>> create(T point, Function<T,float[]> translation){
		List<T> l = new ArrayList<>(1);
		l.add(point);
		return new BVH<>(l,translation);
	}





	/**
	 * @return The root of the BVH
	 */
	BVHNode<?,T> getRoot(){
		return root;
	}



	public Pair<BVH<T>,BVH<T>> split(){
		BVHNode<?, T> left = root.getLeft();
		BVHNode<?,T> right = root.getRight();
		BVH<T> l = null;
		BVH<T> r = null;
		if(left != null)		
			l = new BVH<>(left);
		if(right!=null)
			r = new BVH<>(right);
		return new Pair<>(l,r);
	}


	


	/**
	 * Breaks this BVH in multiple BVH with the given maximum size
	 * @param consumer Allows to consume the crumb BVH before it is added to the collector
	 * @param crumbSize The maximum size of of the resulting crumbs
	 */
	public void crumbs(Consumer<BVH<T>> consumer, int crumbSize){

		if(crumbSize < 2)
			throw new IllegalArgumentException("Crumb Size is too small : " + crumbSize);

		if(this.size() <= crumbSize){
			consumer.accept(this);
			return;
		}


		Deque<BVHNode<?,T>> queue = new LinkedList<>();
		queue.add(root);

		while(!queue.isEmpty()){

			BVHNode<?,T> current = queue.removeFirst();

			if(!current.isLeaf()){

				BVHNode<?, T> l = current.getLeft();
				BVHNode<?, T> r = current.getRight();
				if(null != l)
					if(l.size() > crumbSize)
						queue.add(l);
					else{
						BVH<T> lb = new BVH<>(l);
						consumer.accept(lb);
					}
				if(null != r)
					if(r.size() > crumbSize)
						queue.add(r);
					else{
						BVH<T> rb = new BVH<>(r);
						consumer.accept(rb);
					};	
			}

		}

	}



	public float[] center(){
		return root.center();
	}


	public T getUserObject(){
		return root.getUserObject();
	}

	/**
	 * @return An unmodifiable List holding the coordinates of the points of this BVH
	 */
	public List<float[]> getAllPoints(){
		return root.points();
	}


	public int size(){
		return root.size();
	}



	@Override
	public int hashCode(){
		return getUserObject().hashCode();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object o){
		if(null == o) return false;
		if(o instanceof BVH)
			return ((BVH) o).getUserObject().equals(getUserObject());
		else return false;
	}


}
