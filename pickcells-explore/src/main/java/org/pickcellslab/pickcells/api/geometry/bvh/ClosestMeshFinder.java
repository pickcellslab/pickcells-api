package org.pickcellslab.pickcells.api.geometry.bvh;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ClosestMeshFinder {

	//private static final Logger log = LoggerFactory.getLogger(ClosestMeshFinder.class);



	/**
	 * Look for the closest pairs of points between the source and the targets	 
	 * @param source The source {@link BVH}
	 * @param targets The target BVH.
	 * @param accept A percentage of the total number of points (target) to report in the result
	 * @param minClip A threshold distance below which the pair of points will be associated regardless of the shortest distance identified
	 * @param maxClip A threshold distance above which the pair of points will discarded regardless of the shortest distance identified
	 * @return a List of {@link BVTTNode} holding the pair of BVHNode found to be the closest pairs within the accepted distance variation
	 */
	public static <S,T> List<BVTTNode<S,T>> closestPoints(BVH<S> source, BVH<T> target, float accept, float maxClip){

		final int stop =  (int) (accept * (float)target.getAllPoints().size()) + 1;

		//System.out.println("Stop ="+ stop);


		//Create a BVTT for each source / target BVH pair
		List<BVTTNode<S,T>> tests = new ArrayList<>();
		final List<BVTTNode<S,T>> next = new ArrayList<>();
		final List<BVTTNode<S,T>> result = new ArrayList<>();

		next.add(new BVTTNode<S,T>(source.getRoot(),target.getRoot()));

		//At each depth -> 2 passes
		// 1- look for shortest distance
		// Note : if one is negative keep all, the bounding volume might hide the actual distance

		// 2- Accept all nodes where distance is <= than min distance + accept * min distance and reject others (mark as discarded)
		// then go to next depth from those nodes until all are leaves 



		int count = 0;
		do{

			tests.clear();
			tests.addAll(next);
			next.clear();

			//Sort BVTTNodes by distance and keep only percentage
			if(tests.size() > stop){
				Collections.sort(tests, sorter);
				tests.subList(stop, tests.size()).forEach(b->b.setDiscarded());
				tests = tests.subList(0, stop);				
			}



			for(BVTTNode<S,T> n : tests){
				if(n.distance() > maxClip + n.target().radius()){
					n.setDiscarded();
				}
				else{

					if(!n.isLeaf()){
						next.add(n.getLeft());
						next.add(n.getRight());
					}
					else{
						result.add(n);
					}
				}
			}


			//Count points
			count = result.size();
			//for(BVTTNode<S, T> bvtt : next)
			//	count+=bvtt.target().points().size();
			//	System.out.println("Count "+count);

		} while(!next.isEmpty() && count <= stop);


		tests = null;
		result.addAll(next);


		return result;
	}



	private static Comparator<BVTTNode<?,?>> sorter = (b1,b2) ->{
		return Float.compare(b1.distance(), b2.distance());
	};



	/**
	 * Look for the closest pairs of points between the source and the targets	 
	 * @param source The source {@link BVH}
	 * @param targets The target BVH.
	 * @param accept A percentage of the total number of points (target) to report in the result
	 * @param minClip A threshold distance below which the pair of points will be associated regardless of the shortest distance identified
	 * @param maxClip A threshold distance above which the pair of points will discarded regardless of the shortest distance identified
	 * @return a List of {@link BVTTNode} holding the pair of BVHNode found to be the closest pairs within the accepted distance variation
	 */
	public static <S,T> List<BVTTNode<S,T>> closestPoints(BVH<S> source, BVH<T> target, float accept, float maxClip,
			Consumer<List<BVTTNode<S,T>>> newRound,
			BiConsumer<BVTTNode<S,T>,Boolean> resultTest,
			Consumer<BVTTNode<S,T>> leaved,
			Consumer<Void> updater,
			int delay) throws InterruptedException{



		final int stop =  (int) (accept * (float)target.getAllPoints().size()) + 1;

		System.out.println("Stop ="+ stop);


		//Create a BVTT for each source / target BVH pair
		List<BVTTNode<S,T>> tests = new ArrayList<>();
		final List<BVTTNode<S,T>> next = new ArrayList<>();
		final List<BVTTNode<S,T>> result = new ArrayList<>();

		next.add(new BVTTNode<S,T>(source.getRoot(),target.getRoot()));

		//At each depth -> 2 passes
		// 1- look for shortest distance
		// Note : if one is negative keep all, the bounding volume might hide the actual distance

		// 2- Accept all nodes where distance is <= than min distance + accept * min distance and reject others (mark as discarded)
		// then go to next depth from those nodes until all are leaves 



		int count = 0;
		do{

			tests.clear();
			tests.addAll(next);
			next.clear();

			newRound.accept(tests);
			Thread.sleep(delay);

			//Sort BVTTNodes by distance and keep only percentage
			if(tests.size() > stop){
				Collections.sort(tests, sorter);
				tests.subList(stop, tests.size()).forEach(b->b.setDiscarded());
				tests = tests.subList(0, stop);				
			}
			

			for(BVTTNode<S,T> n : tests){
				if(n.distance() > maxClip + n.target().radius()){
					n.setDiscarded();
					resultTest.accept(n,false);
					Thread.sleep(delay);
				}
				else{

					if(!n.isLeaf()){
						next.add(n.getLeft());
						next.add(n.getRight());
						resultTest.accept(n,true);
						Thread.sleep(delay);
					}
					else{
						result.add(n);
						leaved.accept(n);
						Thread.sleep(delay);
					}
				}
			}


			updater.accept(null);
			Thread.sleep(delay);

			//Count points
			count = result.size();
			//for(BVTTNode<S, T> bvtt : next)
			//	count+=bvtt.target().points().size();

			System.out.println("Count "+count);

		} while(!next.isEmpty() && count <= stop);


		//next = null;
		tests = null;
		result.addAll(next);

		return result;

	}




	/**
	 * Look for the closest pairs of points between the source and the targets	 
	 * @param source The source {@link BVH}
	 * @param targets The list of the targets BVH.
	 * @param mesh_accept A percentage of the total number of points from the target bvh below which no more subpartitioning will be done.  
	 * @param shape_accept  A percentage of the distance between the closest BVH which will be used to decide
	 * which other BVH include in the result.
	 * @param maxClip A threshold distance above which the pair of points will discarded regardless of the shortest distance identified
	 * @return a List of {@link BVTTNode} holding the pair of BVHNode found to be the closest pairs within the accepted distance variation
	 */
	public static <S,T> List<BVTTNode<S,T>> closestPoints(BVH<S> source, List<BVH<T>> targets, float mesh_accept, float shape_accept, float maxClip){

		//long start = System.currentTimeMillis();


		Objects.requireNonNull(source, "The source of the test cannot be null");
		Objects.requireNonNull(targets, "The list of targets of the test cannot be null");
		if(targets.size() == 0) throw new IllegalArgumentException("At least one target is required");


		//Create a BVTT for each source / target BVH pair		
		final List<BVTTNode<S,T>> result = new ArrayList<>();
		targets.stream().parallel().filter(t->t != null && !t.equals(source))
		.forEach(t-> result.addAll(ClosestMeshFinder.closestPoints(source, t, mesh_accept, maxClip)));

		// Find the smallest distance and filter out all the nodes with a distance > accept x min distance
		final double min = result.stream().mapToDouble(n->n.distance()).min().orElse(Double.NaN);
		if(!Double.isNaN(min)) {
			final double thresh = min + min*shape_accept;
			return result.stream().filter(n->n.distance()<=thresh).collect(Collectors.toList());
		}
		else
			return result;

	}





	/**
	 * Identifies the closest pairs of points between the source and the targets. The method accepts {@link Consumer} to be able
	 * to pefrom some desired operations at every step of the process (displaying the selected points at every step for example)
	 * @param source The {@link BVH} of the source object
	 * @param targets The {@link BVH} of the target object
	 * @param mesh_accept A percentage of the total number of points from the target bvh below which no more subpartitioning will be done.  
	 * @param shape_accept  A percentage of the distance between the closest BVH which will be used to decide
	 * which other BVH include in the result.
	 * @param maxClip A threshold distance above which the pair of points will discarded regardless of the shortest distance identified
	 * @param newRound A {@link Consumer} of the List of {@link BVTTNode} to be tested at each step
	 * @param resultTest A {@link Consumer} of {@link BVTTNode} which habe been either accepted or rejected after the test at each step 
	 * @param leaved A {@link Consumer} of {@link BVTTNode} which has been labeled as leaf
	 * @param updater A {@link Consumer} which is triggered when one step has finished
	 * @param delay delay in millisecond between each step.
	 * @return a List of {@link BVTTNode} holding the pair of BVHNode found to be the closest pairs within the accepted distance variation
	 * @throws InterruptedException
	 */
	public static <S,T> List<BVTTNode<S,T>> closestPoints(BVH<S> source, List<BVH<T>> targets, float mesh_accept, float shape_accept, float maxClip,
			Consumer<List<BVTTNode<S,T>>> newRound,
			BiConsumer<BVTTNode<S,T>,Boolean> resultTest,
			Consumer<BVTTNode<S,T>> leaved,
			Consumer<Void> updater,
			int delay) throws InterruptedException{

		//Create a BVTT for each source / target BVH pair		
		final List<BVTTNode<S,T>> result = new ArrayList<>();
		targets.stream().parallel().filter(t->t != null && !t.equals(source))
		.forEach(t->{
			try {
				result.addAll(ClosestMeshFinder.closestPoints(source, t, mesh_accept, maxClip, newRound, resultTest, leaved, updater, delay));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});


		// Find the smallest distance and filter out all the nodes with a distance > accept x min distance
		final double min = result.stream().mapToDouble(n->n.distance()).min().orElse(Double.NaN);
		if(!Double.isNaN(min)) {
			final double thresh = min + min*shape_accept;
			return result.stream().filter(n->n.distance()<=thresh).collect(Collectors.toList());
		}
		else
			return result;
	}


}
