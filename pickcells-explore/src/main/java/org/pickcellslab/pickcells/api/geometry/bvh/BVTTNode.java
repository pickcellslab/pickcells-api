package org.pickcellslab.pickcells.api.geometry.bvh;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.graph.BinaryTreeNode;

public class BVTTNode<S,T> implements BinaryTreeNode<BVTTNode<S,T>> {


	private final BVHNode<?,S> source;
	private final BVHNode<?,T> target;
	private final BVTTNode<S,T> parent;

	private boolean checked = false;
	private BVTTNode<S,T> left;
	private BVTTNode<S,T> right;

	private boolean isDiscarded = false;
	private Object label;



	BVTTNode(BVHNode<?,S> source, BVHNode<?,T> target){
		Objects.requireNonNull(source,"The provided source BVHNode must not be null");
		Objects.requireNonNull(target,"The provided target BVHNode must not be null");
		this.parent = null;
		this.source = source;
		this.target = target;
	}


	private BVTTNode(BVTTNode<S,T> parent, BVHNode<?,S> source, BVHNode<?,T> target){
		Objects.requireNonNull(source,"The provided source BVHNode must not be null");
		Objects.requireNonNull(target,"The provided target BVHNode must not be null");
		this.parent = parent;
		this.source = source;
		this.target = target;
	}


	public BVHNode<?,S> source(){
		return source;
	}

	public BVHNode<?,T> target(){
		return target;
	}

	public float distance(){
		return BVHNode.distance(source, target);
	}


	public boolean overlap(){
		return BVHNode.distance(source, target)<0;
	}

	public void setDiscarded(){
		isDiscarded = true;
	}

	public boolean isDiscarded(){
		return isDiscarded;
	}
	
	public void addLabel(Object label){
		this.label = label;
	}

	public Object getLabel(){
		return label;
	};
	
	@Override
	public boolean isLeaf() {
		return source.isLeaf() && target.isLeaf() || isDiscarded;
	}

	@Override
	public boolean isRoot() {
		return source.isRoot() && target.isRoot();
	}

	@Override
	public BVTTNode<S,T> getLeft() {
		if(!checked)	split();
		return left;
	}

	@Override
	public BVTTNode<S,T> getRight() {
		if(!checked)	split();
		return right;
	}

	@Override
	public BVTTNode<S,T> getParent() {
		return parent;
	}


	private void split(){
		if(!isLeaf()){
			if(!source.isLeaf() && !target.isLeaf()){			
				if(source.radius()>target.radius()){
					left = new BVTTNode<S,T>(this, source.getLeft(), target);
					right = new BVTTNode<S,T>(this, source.getRight(), target);
				}else{
					left = new BVTTNode<S,T>(this, source, target.getLeft());
					right = new BVTTNode<S,T>(this, source, target.getRight());
				}	
			}
			else if(source.isLeaf()){
				left = new BVTTNode<S,T>(this, source, target.getLeft());
				right = new BVTTNode<S,T>(this, source, target.getRight());
			}
			else if(target.isLeaf()){
				left = new BVTTNode<S,T>(this, source.getLeft(), target);
				right = new BVTTNode<S,T>(this, source.getRight(), target);
			}
		}
		checked = true;
	}



}
