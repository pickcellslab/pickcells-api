package org.pickcellslab.pickcells.api.geometry.bvh;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;


public class BVHNodeObject<T> implements  BVHNode<BVHNodeObject<T>,T>{

	private final List<float[]> points;
	private final float[] center;
	private final float radius;
	private final int maxVarDim;

	private final T userObject;
	private final BVHNodeObject<T> parent;
	private BVHNodeObject<T> left;
	private BVHNodeObject<T> right;
	private boolean checked = false;



	BVHNodeObject(List<float[]> points, T userObject){
		this(null,points, userObject);
	}




	private BVHNodeObject(BVHNodeObject<T> parent, List<float[]> points, T userObject){

		Objects.requireNonNull(points);
		this.parent = parent;
		this.points = points;
		this.userObject = userObject;


		float[] bbMin = new float[this.points.get(0).length];
		Arrays.fill(bbMin, Float.MAX_VALUE);
		float[] bbMax = new float[bbMin.length];
		Arrays.fill(bbMax, Float.MIN_VALUE);
		center = new float[bbMin.length];

		//Compute the bounding box
		for(float[] f : this.points){
			IntStream.range(0, bbMin.length)
			.forEach(i-> {
				if(f[i]<bbMin[i])	bbMin[i] = f[i];
				if(f[i]>bbMax[i])	bbMax[i] = f[i];
			});
		}

		//Find the dimension with highest variation and compute center
		float max = Float.MIN_VALUE;
		int mv = 0;
		for(int i = 0; i<bbMin.length; i++){
			center[i] = (bbMax[i]+bbMin[i])/2f;

			float var = bbMax[i]-bbMin[i];
			if(var>max) {
				max = var;
				mv = i;
			}
		}

		maxVarDim = mv;
		radius = BVHNode.distance(center, bbMin);
	}


	/* (non-Javadoc)
	 * @see net.pickcellslab.pickcells.api.geometry.bvh.BVHNode1#radius()
	 */
	@Override
	public float radius(){
		return radius;
	}

	/* (non-Javadoc)
	 * @see net.pickcellslab.pickcells.api.geometry.bvh.BVHNode1#center()
	 */
	@Override
	public float[] center(){
		return center;
	}


	@Override
	public boolean isLeaf() {
		return points.size() <= 1 || radius == 0;
	}

	@Override
	public boolean isRoot() {
		return parent == null;
	}

	@Override
	public BVHNodeObject<T> getLeft() {		
		split();
		return left;
	}


	@Override
	public BVHNodeObject<T> getRight() {
		split();
		return right;
	}

	@Override
	public BVHNodeObject<T> getParent() {
		return parent;
	}



	


	private synchronized void split() {
		if(checked)
			return;

		if(isLeaf()){
			checked = true;
			return;
		}

		List<float[]> left = new ArrayList<>(points.size()/2);
		List<float[]> right = new ArrayList<>(points.size()/2);
		for(float[] p : points){
			if(p[maxVarDim]<center[maxVarDim])
				left.add(p);
			else
				right.add(p);
		}

		/*
		if(left.size() == 0){
			System.out.println("max var dim = " + center[maxVarDim]);
			System.out.println("radius = " + radius);
			System.out.println("center = " + Arrays.toString(center));
		}else if (right.size() == 0){
			System.out.println("left size "+left.size());
			System.out.println("right is empty");
			System.out.println("max var dim = " +maxVarDim);
			System.out.println("radius = " + radius);
			System.out.println("center = " + Arrays.toString(center));
			for(float[] p : left){
				System.out.println(Arrays.toString(p));		
			}
		}
		*/

		//if(!left.isEmpty())
		this.left = new BVHNodeObject<T>(this, left, userObject);
		//if(!right.isEmpty())
		this.right = new BVHNodeObject<T>(this, right, userObject);

		checked  = true;

	}


	/* (non-Javadoc)
	 * @see net.pickcellslab.pickcells.api.geometry.bvh.BVHNode1#points()
	 */
	@Override
	public List<float[]> points(){
		return Collections.unmodifiableList(points);
	}


	
	

	/* (non-Javadoc)
	 * @see net.pickcellslab.pickcells.api.geometry.bvh.BVHNode1#getUserObject()
	 */
	@Override
	public T getUserObject(){
		return userObject;
	}




	@Override
	public int size() {
		return points.size();
	}

}
