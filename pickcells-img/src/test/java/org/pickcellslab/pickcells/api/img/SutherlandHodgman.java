package org.pickcellslab.pickcells.api.img;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.stream.Collectors;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Pair;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;

public class SutherlandHodgman extends JFrame {

	SutherlandHodgmanPanel panel;

	public static void main(String[] args) {
		JFrame f = new SutherlandHodgman();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

	public SutherlandHodgman() {
		Container content = getContentPane();
		content.setLayout(new BorderLayout());
		panel = new SutherlandHodgmanPanel();
		content.add(panel, BorderLayout.CENTER);
		setTitle("SutherlandHodgman");
		pack();
		setLocationRelativeTo(null);
	}
}

class SutherlandHodgmanPanel extends JPanel {

	List<long[]> subject, clipper, result, focus;
	long[] center;
	Vector2D va,vb;
	Vector2D ref = new Vector2D(1,0);


	public SutherlandHodgmanPanel() {


		setPreferredSize(new Dimension(600, 500));



		// these subject and clip points are assumed to be valid
		long[][] subjPoints = {{100, 150}, {200, 50}, {350, 150}, {350, 300},
				{250, 300}, {200, 250}, {150, 350}, {100, 250}, {100, 200}};

		//long[][] subjPoints = {{100, 150},  {350, 150}, {230, 350}, {250, 350}};

		//long[][] subjPoints = ImgGeometry.clockwiseKernel(100);

		//long[][] clipPoints = {{100, 100}, {300, 100}, {100, 300}, {350, 300}};

		//	 long[][] clipPoints = {{150, 150}, {170, 150}, {170, 170}, {150, 170}};
		//long[][] clipPoints = {{20, 20}, {40, 20}, {40, 40}, {20, 40}};
		long[][] clipPoints = {{100, 100}, {300, 100}, {300, 300}, {100, 300}};


		subject = new ArrayList<>(Arrays.asList(subjPoints));		
		clipper = new ArrayList<>(Arrays.asList(clipPoints));
		result = ImgGeometry.clipPolygon(subject,clipper);

		focus = subject;


		//Collections.reverse(clipper);

		//double overlap = 1 - (ImgGeometry.areaNonIntersecting(subject)-ImgGeometry.areaNonIntersecting(result))/ImgGeometry.areaNonIntersecting(subject);

		//System.out.println("overlap : "+ overlap);



		Pair<Vector2D,Vector2D> pair = ImgGeometry.fitEllipse(focus);
		va = pair.getFirst();
		vb = pair.getSecond();



		/*
		if(area(result) > 0.2 * area(subject) || area(result2) > 0.8 * area(clipper)){
			System.out.println("Clip forbiden");
		}
		 */

		Pair<Double,long[]> p = ImgGeometry.centroidArea(focus);
		center = p.getSecond();
		System.out.println("Area (centroidArea) = " + p.getFirst());
		System.out.println("Area2 = " + area(focus));
		System.out.println("Area (NonIntersecting) = " + ImgGeometry.areaNonIntersecting(focus));
		System.out.println("Roughness = " + getRoughness(focus, center));
		System.out.println("Roughness Circle = " + getRoughness(Arrays.asList(ImgGeometry.clockwiseKernel(500)), new long[2]));
	}





	public static double area(List<long[]> list) {

		if(list.isEmpty())
			return 0;

		boolean remove = false;
		if(!Arrays.equals(list.get(0),list.get(list.size()-1)))
			list.add(list.get(0));

		DoubleAdder adder = new DoubleAdder();
		ImgGeometry.FillPolygon(list, (l1,l2)->adder.add(ImgGeometry.distance(l1, l2)));
		if(remove)
			list.remove(list.size()-1);
		return adder.doubleValue();
	}



	public double getRoughness(List<long[]> list, long[] centroid){		

		final List<Double> ccd = toCCD(list, centroid);
		double m1 = ccd.stream().reduce(0.0, Double::sum)/(double)ccd.size();

		System.out.println("m1 = "+m1);
		
		final List<Double> mu2 = ccd.stream().map(sq-> FastMath.pow((sq-m1),2)).collect(Collectors.toList());
		double F1 = FastMath.sqrt(mu2.stream().reduce(0.0, Double::sum)/(double)ccd.size()) / m1;

		double F2 = mu2.stream().map(qu -> qu*qu).reduce(0.0, Double::sum)/(double)ccd.size();
		F2 = FastMath.pow(F2, 0.25) / m1;

		System.out.println("F1 = "+F1);
		System.out.println("F2 = "+F2);

		return  F2 - F1;		
	}

	public static List<Double> toCCD(List<long[]> contour, long[] centroid){		
		return contour.stream().map(l->ImgGeometry.distance(l, centroid)).collect(Collectors.toList());
	}




	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		//g2.translate(80, 60);
		g2.setStroke(new BasicStroke(2));
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);


		drawPolygon(g2, subject, Color.blue);
		drawPolygon(g2, clipper, Color.red);
		drawPolygon(g2, result, Color.green);


		g2.setColor(Color.CYAN);
		ImgGeometry.FillPolygon(result, (l1,l2)->g2.drawLine((int)l1[0],(int)l1[1],(int) l2[0],(int) l2[1]));

		System.out.println("vA = "+va);
		System.out.println("vB = "+vb);

		int x = (int) center[0];
		int y = (int) center[1];


		g2.setColor(Color.GRAY);
		g2.drawLine(x,y, (int)va.getX()+x,(int)va.getY()+y);
		g2.setColor(Color.ORANGE);
		g2.drawLine(x,y, (int)vb.getX()+x,(int)vb.getY()+y);

		g2.setColor(Color.BLACK);
		g2.rotate(Vector2D.angle(va, ref), x , y);
		g2.drawOval(x-(int)va.getNorm(), y-(int)vb.getNorm(), (int)va.getNorm()*2, (int)vb.getNorm()*2);

		g2.setColor(Color.RED);
		g2.drawOval((int)center[0], (int)center[1], 2, 2);
		System.out.println("Center:");
		System.out.println(Arrays.toString(center));

	}

	private void drawPolygon(Graphics2D g2, List<long[]> points, Color color) {
		g2.setColor(color);
		int len = points.size();
		Line2D line = new Line2D.Double();
		for (int i = 0; i < len; i++) {
			long[] p1 = points.get(i);
			long[] p2 = points.get((i + 1) % len);
			line.setLine(p1[0], p1[1], p2[0], p2[1]);
			g2.draw(line);
		}
	}
}
