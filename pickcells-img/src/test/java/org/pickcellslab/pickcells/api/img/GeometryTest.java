package org.pickcellslab.pickcells.api.img;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;

public class GeometryTest {

	@Test
	public void testClockWiseKernel(){

		System.out.println("*****************************");
		long[][] k = ImgGeometry.clockwiseKernel(3);
		for(int i = 0; i<k.length; i++)
			System.out.println(Arrays.toString(k[i]));


	}


	@Test
	public void testLinePath(){

		System.out.println("*****************************");
		long[] s = {1,8};
		long[] t = {5,2};

		ImgGeometry.linePath(s, t, l -> System.out.println(Arrays.toString(l)));


	}



	@Test
	public void testCurvature(){

		System.out.println("*****************************");
		List<Integer> cc = new ArrayList<>();

		cc.add(0);	cc.add(7);	cc.add(6);	cc.add(6);	cc.add(6);	cc.add(5);	cc.add(5);	cc.add(3);
		cc.add(3);	cc.add(2);	cc.add(1);	cc.add(2);	cc.add(3);	cc.add(2); 	cc.add(0);	

		/*
		cc.add(3);	cc.add(3);	cc.add(3);	cc.add(3);	cc.add(3);	cc.add(3);	cc.add(3);	cc.add(3);
		cc.add(5);	cc.add(5);	cc.add(5);	cc.add(5);	cc.add(5);	cc.add(5);	cc.add(5);	cc.add(5);
		cc.add(7);	cc.add(7);	cc.add(7);	cc.add(7);	cc.add(7);	cc.add(7);	cc.add(7);	cc.add(7);		
		cc.add(1);	cc.add(1);	cc.add(1);	cc.add(1);	cc.add(1);	cc.add(1);	cc.add(1);	cc.add(1);


		cc.add(2);	cc.add(1);	cc.add(2);	cc.add(2);	cc.add(1);	cc.add(2);	cc.add(2);	cc.add(1);
		cc.add(2);	cc.add(2);	cc.add(1);	cc.add(2);	cc.add(1);	cc.add(2);	cc.add(2);	cc.add(1);
		 */


		System.out.println("Chain Code size = "+cc.size());

		ImgGeometry.curvature(cc, 8).forEach(f-> System.out.println(" fi = "+f));



		List<long[]> coords = new ArrayList<>();

		ImgGeometry.linePath(new long[]{0,0}, new long[]{-50,2}, l-> coords.add(Arrays.copyOf(l, l.length)));

		cc = toChainCode(coords) ;
		System.out.println(" --------------------- Chain Code size = "+cc.size());
		cc.forEach(System.out::print);

		ImgGeometry.curvature(cc, 4).forEach(f-> System.out.println(" fi = "+f));


	}

	private static final long[][] p = {

			{0,1},
			{1,1},
			{1,0},
			{1,-1},
			{0,-1},
			{-1,-1},
			{-1,0},
			{-1,1}			

	};



	public static int getCode(long[] current, long[] next, long[] temp){
		temp[0] = next[0]-current[0];	temp[1] = next[1]-current[1];
		for(int f = 0; f<p.length; f++){
			if(Arrays.equals(p[f], temp))
				return f;
		}		
		throw new IllegalArgumentException("Illegal locations, current and next are not neighbour pixels -> "+Arrays.toString(temp));
	}


	public static List<Integer> toChainCode(List<long[]> track) {

		if(track.size()<2)
			return Collections.emptyList();

		List<Integer> cc = new ArrayList<>();
		long[] current = track.get(0);
		long[] temp = new long[2];
		for(int i = 1; i<track.size(); i++){
			cc.add(getCode(current, track.get(i), temp));
			current = track.get(i);
		}
		return cc;
	}






	@Test
	public void testAngle360(){




		System.out.println("*****************************");

		long[] orig = new long[]{1,0};

		List<long[]> list = new ArrayList<>();
		list.add(new long[]{0,1});
		list.add(new long[]{1,1});
		list.add(new long[]{0,-1});
		list.add(new long[]{-1,-1});
		list.add(new long[]{-1,0});


		double angle = ImgGeometry.angle2PI(orig, list.get(0));
		System.out.println(Math.toDegrees(angle));

		Collections.shuffle(list);



		Collections.sort(list, (l2,l1) -> Double.compare(ImgGeometry.angle2PI(orig, l1), ImgGeometry.angle2PI(orig, l2)));

		list.forEach(l->System.out.println(Arrays.toString(l)));
	}



	@Test
	public void testBoundingBoxesUnion1(){

		long[] min1 = new long[]{0,0};		long[] max1 = new long[]{2,2};
		long[] min2 = new long[]{1,1};		long[] max2 = new long[]{3,3};

		System.out.println("First = "+ImgGeometry.boundingBoxUnion(min1, max1, min2, max2));

	}

	@Test
	public void testBoundingBoxesUnion2(){

		long[] min1 = new long[]{0,0};		long[] max1 = new long[]{2,2};
		long[] min2 = new long[]{-1,-1};		long[] max2 = new long[]{3,3};

		System.out.println("Second = "+ImgGeometry.boundingBoxUnion(min1, max1, min2, max2));

	}


	@Test
	public void testBoundingBoxesUnion3(){

		long[] min1 = new long[]{0,0};		long[] max1 = new long[]{2,2};
		long[] min2 = new long[]{2,3};		long[] max2 = new long[]{4,4};

		System.out.println("Third = "+ImgGeometry.boundingBoxUnion(min1, max1, min2, max2));

	}

	@Test
	public void testBoundingBoxesJaccard(){

		long[] min1 = new long[]{0,0};		long[] max1 = new long[]{2,2};
		long[] min2 = new long[]{-1,-1};		long[] max2 = new long[]{3,3};

		System.out.println("Jaccard = "+ImgGeometry.boundingBoxJaccard(min1, max1, min2, max2));

	}
}
