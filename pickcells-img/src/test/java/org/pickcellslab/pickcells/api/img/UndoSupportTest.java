package org.pickcellslab.pickcells.api.img;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.pickcellslab.pickcells.api.img.editors.UndoSupport;

public class UndoSupportTest {

	
	@Test
	public void testLimit(){
		
		int limit = 8;
		
		UndoSupport<String> us = new UndoSupport<>(limit);
		for(int i = 0; i<10; i++)
			us.done("done "+i);
		System.out.println(us);
		Assert.assertTrue(us.size() == limit);
		
	}
	
	
	@Test
	public void testUndo(){
		
		int limit = 8;
		
		UndoSupport<String> us = new UndoSupport<>(limit);
		for(int i = 0; i<10; i++)
			us.done("done "+i);
		
		Assert.assertTrue(us.undo().get().equals("done "+9));
		Assert.assertTrue(us.undo().get().equals("done "+8));	
		System.out.println(us);
		
	}
	
	@Test
	public void testRedo(){
		
		int limit = 8;
		
		UndoSupport<String> us = new UndoSupport<>(limit);
		for(int i = 0; i<10; i++)
			us.done("done "+i);
		
		Assert.assertTrue(us.undo().get().equals("done "+9));
		Assert.assertTrue(us.undo().get().equals("done "+8));	
		
		
		Assert.assertTrue(us.redo().get().equals("done "+8));
		Assert.assertTrue(us.size() == 8);
		
		System.out.println(us);
	}
	
	
	@Test
	public void testDoneAfterUndo(){
		
		int limit = 8;
		
		UndoSupport<String> us = new UndoSupport<>(limit);
		for(int i = 0; i<10; i++)
			us.done("done "+i);
		
		Assert.assertTrue(us.undo().get().equals("done "+9));
		Assert.assertTrue(us.undo().get().equals("done "+8));	
		
		us.done("before last");
		us.done("done last");
		Assert.assertTrue(!us.redo().isPresent());
		Assert.assertTrue(us.size() == 8);
		
		System.out.println(us);
	}
	
}
