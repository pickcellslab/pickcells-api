/*******************************************************************************
 * Copyright (c) 2012-2013 Biomedical Image Group (BIG), EPFL, Switzerland.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 *     Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 ******************************************************************************/
package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2019 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Stores the parameters for the steerable templates. Based on the
 * <code>ParameterSet</code> package of Francois Aguet.
 * 
 * @version April 23, 2013
 * 
 * @author Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 * 
 * <hr>
 * NOTICE: This code was copied from a plugin originally implemented by François Aguet for ImageJ, and ported to Icy by Ricard Delgado-Gonzalo (ricard.delgado@gmail.com) 
 * and Zsuzsanna Püspöki (zsuzsanna.puspoki@epfl.ch). This plugin was based on the following publication:
 * <br><br>
 * M. Jacob, M. Unser, “Design of Steerable Filters for Feature Detection Using Canny-Like Criteria,”
 * IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 8, pp. 1007-1019
 * <hr>
 * 
 * 
 */
public class ParameterSet {

	// ----------------------------------------------------------------------------
	// PARAMETERS

	/** Identifier of the parameter set. */
	private int set_ = 0;
	/** Order of the feature template. */
	private int M_ = 0;
	/** Smoothness parameters used during the filter optimization. */
	private double mu_ = 0;
	/** Image weights. */
	private double[] weights_ = null;

	// ============================================================================
	// PUBLIC METHODS

	/**
	 * Creates a new Preview object.
	 * 
	 * @param M
	 *            Order of the filter.
	 * @param set
	 *            Set number in case there are multiple configurations for order
	 *            M.
	 * @param mu
	 *            Smoothness parameters used during the filter optimization.
	 * @param w
	 *            Weights vector.
	 */
	public ParameterSet(int M, int set, double mu, double[] w) {
		M_ = M;
		set_ = set;
		mu_ = mu;
		weights_ = new double[w.length];
		System.arraycopy(w, 0, weights_, 0, w.length);
	}

	// ----------------------------------------------------------------------------

	/**
	 * Creates a new Preview object.
	 * 
	 * @param M
	 *            Order of the filter.
	 * @param set
	 *            Set number in case there are multiple configurations for order
	 *            M.
	 * @param mu
	 *            Smoothness parameters used during the filter optimization.
	 * @param w
	 *            Weight element.
	 */
	public ParameterSet(int M, int set, double mu, double w) {
		M_ = M;
		set_ = set;
		mu_ = mu;
		weights_ = new double[1];
		weights_[0] = w;
	}

	// ----------------------------------------------------------------------------

	/** Loads a parameter set from an XML file. */
	public static ParameterSet[][] loadParameterSets(InputStream xmlStream) {

		ParameterSet[][] pSet = null;
		double[] weightList;
		double mu;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document dom = db.parse(xmlStream);
			Element docEle = dom.getDocumentElement();
			NodeList nl = docEle.getElementsByTagName("order");
			pSet = new ParameterSet[nl.getLength()][];
			for (int i = 0; i < nl.getLength(); i++) {
				NodeList set = ((Element) nl.item(i))
						.getElementsByTagName("set");
				pSet[i] = new ParameterSet[set.getLength()];

				for (int k = 0; k < set.getLength(); k++) {
					Element setE = (Element) set.item(k);
					mu = Double.parseDouble(setE.getAttribute("mu"));
					NodeList weights = ((Element) set.item(k))
							.getElementsByTagName("w");
					weightList = new double[weights.getLength()];
					for (int w = 0; w < weights.getLength(); w++) {
						weightList[w] = Double.parseDouble(((Element) weights
								.item(w)).getTextContent());
					}
					pSet[i][k] = new ParameterSet(i + 1, k + 1, mu, weightList);
				}
			}
			xmlStream.close();
		} catch (ParserConfigurationException pce) {
			System.err.println("" + pce);
		} catch (SAXException se) {
			System.err.println("" + se);
		} catch (IOException ioe) {
			System.err.println("" + ioe);
		}
		return pSet;
	}

	// ============================================================================
	// GETTERS

	/** Returns the template weights. */
	public double[] getWeights() {
		return weights_;
	}

	// ----------------------------------------------------------------------------

	/** Returns the order of the filter. */
	public double getOrder() {
		return M_;
	}

	// ----------------------------------------------------------------------------

	/** Returns the set number. */
	public double getSet() {
		return set_;
	}

	// ----------------------------------------------------------------------------

	/** Returns the smoothness parameter. */
	public double getMu() {
		return mu_;
	}
}
