package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import net.imglib2.IterableInterval;
import net.imglib2.converter.RealLUTConverter;
import net.imglib2.histogram.Histogram1d;
import net.imglib2.histogram.Real1dBinMapper;
import net.imglib2.type.numeric.RealType;

class RealLutChannelControlModel<T extends RealType<T>> extends RealLUTConverter<T> implements ChannelControlModel<T> {

	private final List<ChannelListener<T>> lstrs = new ArrayList<>();
	private final T type;
	private Histogram1d<T> histo;
	private String name;
	private boolean visible;
	private ImgLut<?> lut;
	
	public RealLutChannelControlModel(String name, IterableInterval<T> itrv) {
		this.name = name;
		this.type = itrv.firstElement().createVariable();
		this.setProbingInterval(itrv);
		this.setImgLut(this.createDefaultColorTable(type));		
	}
	
	
	@Override
	public void setChannelName(String newName) {
		name = newName;
		lstrs.forEach(l->l.nameHasChanged(this));
	}

	@Override
	public void setContrast(int minimum, int maximum) {
		this.setMin(minimum);
		this.setMax(maximum);
		lstrs.forEach(l->l.contrastChanged(this));
	}

	@Override
	public void setImgLut(ImgLut<?> table) {
		lut = table;
		this.setLUT(lut.table());
		lstrs.forEach(l->l.lutChanged(this));
	}

	@Override
	public void setVisible(boolean isVisible) {
		if(isVisible != visible){
			visible = isVisible;
			lstrs.forEach(l->l.visibilityChanged(this));
		}
	}
	
	

	@Override
	public void setProbingInterval(IterableInterval<T> itrv) {
		long b = System.currentTimeMillis();
		Real1dBinMapper<T> mapper =	new Real1dBinMapper<>(type.getMinValue(), type.getMaxValue(), (long)(type.getMaxValue()-type.getMinValue()), false);
		histo = new Histogram1d<>(itrv, mapper);
		System.out.println("RealLutChannelControlModel -> "+name+" creating Histogram took "+(System.currentTimeMillis()-b)+"ms");
		lstrs.forEach(l->l.histogramChanged(this));
	}
	

	@Override
	public void autoContrast() {
		long b = System.currentTimeMillis();
		T min = type.createVariable();
		System.out.println("RealLutChannelControlModel histogram bin count -> "+histo.getBinCount());
		for(int i = 0; i<histo.getBinCount(); i++){
			if(histo.frequency(i)>0){
				histo.getLowerBound(i, min);
				break;
			}
		}
		T max = type.createVariable();		
		histo.getUpperBound((int)histo.getBinCount()-1, max);
		for(int i = (int)histo.getBinCount()-1; i>0; i--){
			if(histo.frequency(i)>0){
				histo.getUpperBound(i, max);
				break;
			}
		}
		System.out.println("RealLutChannelControlModel -> "+name+" autocontrast took "+(System.currentTimeMillis()-b)+"ms");
		System.out.println("RealLutChannelControlModel autoContrast -> "+min+"; "+max);
		this.setContrast((int)min.getRealDouble(), (int)max.getRealDouble());
	}

	@Override
	public T getChannelType() {
		return type;
	}

	@Override
	public Histogram1d<T> getHistogram() {
		return histo;
	}

	@Override
	public ImgLut<?> getLut() {
		if(lut==null)
			setImgLut(getAvailableLuts()[0]);
		return lut;
	}

	@Override
	public int[] getCurrentContrast() {
		return new int[]{(int)min, (int)max};
	}

	@Override
	public String getChannelName() {
		return name;
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public double getGlobalMinIntensity() {
		return getChannelType().getMinValue();
	}

	@Override
	public double getGlobalMaxIntensity() {
		return getChannelType().getMaxValue();
	}

	@Override
	public ImgLut<?>[] getAvailableLuts() {
		int bits = getChannelType().getBitsPerPixel();
		if(bits==16)
			return Lut16.values();
		else
			return Lut8.values();
	}

	@Override
	public void addChannelListener(ChannelListener<T> l) {
		lstrs.add(l);
	}

	@Override
	public void removeChannelListener(ChannelListener<T> l) {
		lstrs.remove(l);
	}

	
	@Override
	public String toString(){
		return name+" ("+type.getBitsPerPixel()+"bits)";
	}


	
	private static int lutIndex = 0;
	
	private ImgLut<?> createDefaultColorTable(T type){
		lutIndex = ++lutIndex % Lut8.values().length;
		int bits = type.getBitsPerPixel();
		if(bits==16)
			return Lut16.values()[lutIndex];
		else
			return Lut8.values()[lutIndex];
	}
	
}
