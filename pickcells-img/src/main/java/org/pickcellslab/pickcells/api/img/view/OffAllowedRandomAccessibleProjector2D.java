package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.imglib2.FinalInterval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessible;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.converter.Converter;
import net.imglib2.display.projector.AbstractProjector2D;
import net.imglib2.type.numeric.NumericType;

public class OffAllowedRandomAccessibleProjector2D<A,B extends NumericType<B>> extends AbstractProjector2D {

	final protected Converter< ? super A, B > visibleConverter;

	final protected RandomAccessibleInterval< B > target;

	final protected RandomAccessible< A > source;

	final int numDimensions;

	private final int dimX;

	private final int dimY;

	protected final int X = 0;

	protected final int Y = 1;
	private boolean isVisible = true;
	private final Converter< ? super A, B > blackConverter = new Converter<A, B>(){

		@Override
		public void convert(A input, B output) {
			output.setZero();
		}
		
	};
	
	
	public OffAllowedRandomAccessibleProjector2D(int dimX, int dimY, RandomAccessible<A> source,
			RandomAccessibleInterval<B> target, Converter<? super A, B> converter) {
		super( source.numDimensions() );
		this.dimX = dimX;
		this.dimY = dimY;
		this.target = target;
		this.source = source;
		this.visibleConverter = converter;
		this.numDimensions = source.numDimensions();
	}

	
	public void setVisible(boolean isVisible){
		this.isVisible = isVisible;
	}
	
	@Override
	public void map(){
		
		Converter< ? super A, B > converter = isVisible ? visibleConverter : blackConverter;
		
		for ( int d = 2; d < position.length; ++d )
			min[ d ] = max[ d ] = position[ d ];

		min[ dimX ] = target.min( dimX );
		min[ dimY ] = target.min( dimY );
		max[ dimX ] = target.max( dimX );
		max[ dimY ] = target.max( dimY );
		final FinalInterval sourceInterval = new FinalInterval( min, max );

		final long cr = -target.dimension( dimX );

		final RandomAccess< B > targetRandomAccess = target.randomAccess( target );
		final RandomAccess< A > sourceRandomAccess = source.randomAccess( sourceInterval );

		final long width = target.dimension( dimX );
		final long height = target.dimension( dimY );

		sourceRandomAccess.setPosition( min );
		targetRandomAccess.setPosition( min[ dimX ], dimX );
		targetRandomAccess.setPosition( min[ dimY ], dimY );
		for ( long y = 0; y < height; ++y )
		{
			for ( long x = 0; x < width; ++x )
			{
				converter.convert( sourceRandomAccess.get(), targetRandomAccess.get() );
				sourceRandomAccess.fwd( dimX );
				targetRandomAccess.fwd( dimX );
			}
			sourceRandomAccess.move( cr, dimX );
			targetRandomAccess.move( cr, dimX );
			sourceRandomAccess.fwd( dimY );
			targetRandomAccess.fwd( dimY );
		}
	}

}
