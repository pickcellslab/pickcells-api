package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Function;
import java.util.logging.Level;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.gauss3.Gauss3;
import net.imglib2.exception.IncompatibleTypeException;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.view.Views;

public class GaussianFilter<T extends RealType<T>> implements ImgProcessing<T, FloatType> {


	private KernelRadiiUI dialog;
	private PreviewBag<T> input;
	private final ImgIO io;
	private final NotificationFactory notif;

	public GaussianFilter(ImgIO io, NotificationFactory notif) {
		this.io = io;
		this.notif = notif;
	}
	
	@Override
	public String name() {
		return "Gaussian Filter";
	}

	@Override
	public FloatType outputType(T inputType) {
		return new FloatType();
	}

	@Override
	public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<T> input) {
		dialog = new KernelRadiiUI(input.getInputInfo(), "Radius (in pixels)");
		this.input = input;
		return dialog;
	}

	@Override
	public void changeInput(PreviewBag<T> current) {
		this.input = current;
	}

	@Override
	public boolean requiresOriginal() {
		return false;
	}

	@Override
	public boolean requiresFlat() {
		return true;
	}

	@Override
	public RandomAccessibleInterval<FloatType> validate() {
		
		RandomAccessibleInterval<FloatType> filtered = null;
		if(input.getCurrentInputType() instanceof FloatType)
			filtered = (RandomAccessibleInterval<FloatType>) input.getInput(); 
		else
			filtered = io.createImg(input.getInput(), new FloatType());
		
		try {
		
			Gauss3.gauss(dialog.getRadii(), Views.extendMirrorSingle(input.getInput()), filtered );
		
		} catch (IncompatibleTypeException e) {
			notif.display("Unsupported Image", "Image type not supported", e, Level.SEVERE);
		}

		return filtered;
	}

	@Override
	public String description() {
		return "n-dimensional Gaussian filtering";
	}

	@Override
	public boolean repeatable() {
		return true;
	}

	@Override
	public DimensionalityEffect dimensionalityEffect() {
		return DimensionalityEffect.FLAT_TO_FLAT;
	}

	@Override
	public boolean requiresAnnotationChannel() {
		return false;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}



	@Override
	public Function<ImageBag<T>, RandomAccessibleInterval<FloatType>> toPipe() {
		final double[] radii = dialog.getRadiiNoTime();
		return (bag) -> {
			RandomAccessibleInterval<FloatType> filtered = null;
			if(bag.getCurrentInputType() instanceof FloatType)
				filtered = (RandomAccessibleInterval<FloatType>) bag.getInputFrame(0); 
			else
				filtered = io.createImg(bag.getInputFrame(0), new FloatType());
			
			try {
			
				Gauss3.gauss(radii, Views.extendMirrorSingle(bag.getInputFrame(0)), filtered );
			
			} catch (IncompatibleTypeException e) {
				notif.display("Unsupported Image", "Image type not supported", e, Level.SEVERE);
			}

			return filtered;			
		};
	}

}
