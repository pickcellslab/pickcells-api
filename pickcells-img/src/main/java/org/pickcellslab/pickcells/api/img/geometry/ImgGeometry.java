package org.pickcellslab.pickcells.api.img.geometry;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathUtils;
import org.apache.commons.math3.util.Pair;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.EllipsoidNeighborhood;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.RealType;

public final class ImgGeometry {



	public static double distance(long[] p1, long[] p2) {
		double sum = 0;
		for (int i = 0; i < p1.length; i++) {
			final double dp = p1[i] - p2[i];
			sum += dp * dp;
		}
		return FastMath.sqrt(sum);
	}


	/**
	 * Generates a list of contiguous positions (2D) along the perimeter of a circle with the given radius in clockwise order 
	 * @param radius
	 * @return The list of positions as an array of 2d long[];
	 */
	public static long[][] clockwiseKernel(final long radius){		

		final long[] center = new long[2];
		final List<long[]> kernel = new ArrayList<>();

		sampleCircle(center, radius, pos -> {
			for(int i = 0; i<kernel.size(); i++)
				if(Arrays.equals(kernel.get(i),pos))
					return;
			kernel.add(Arrays.copyOf(pos,pos.length));
		});

		//final List<long[]> distinct = kernel.stream().distinct().collect(Collectors.toList());
		kernel.sort((l1,l2)->sortClockwise(center,l1,l2));		
		return kernel.toArray(new long[kernel.size()][2]);		
	}


	/**
	 * @param A
	 * @param B
	 * @return The angle from 0 to 2 PI between the 2 given vectors.
	 */
	public static double angle2PI(long[] A, long[] B){		
		double a = Math.atan2(B[1],B[0]) - Math.atan2(A[1],A[0]);
		return  (a + MathUtils.TWO_PI) % MathUtils.TWO_PI;
	}



	/**
	 * Identifies wether A is higher or lower in clockwise rotation with respect to {0,-1}
	 * @param orig The center
	 * @param A The first location
	 * @param B The second location
	 * @return a negative or positive int as in {@link Comparable#compareTo(Object)}
	 */
	public static int sortClockwise(long[] orig, long[] A, long[] B){


		//  Fetch the atans
		final double aTanA = Math.atan2(A[1] - orig[1], A[0] - orig[0]);
		final double aTanB = Math.atan2(B[1] - orig[1], B[0] - orig[0]);

		//  Determine next point in Clockwise rotation
		if (aTanA > aTanB) return -1;
		else if (aTanB > aTanA) return 1;
		return 0;


	}


	/**
	 * Identifies wether A is higher or lower in clockwise rotation with respect to {0,-1}
	 * @param ref The center
	 * @param A The first location
	 * @param B The second location
	 * @return a negative or positive int as in {@link Comparable#compareTo(Object)}
	 */
	public static int sortClockwiseFromRef(long[] ref, long[] A, long[] B){

		//  Fetch the atans
		final double aTanA = Math.atan2(A[1] - ref[1], A[0] - ref[0]);
		final double aTanB = Math.atan2(B[1] - ref[1], B[0] - ref[0]);

		//  Determine next point in Clockwise rotation
		if (aTanA > aTanB) return -1;
		else if (aTanB > aTanA) return 1;
		return 0;


	}


	public static <T extends RealType<T> >void drawCircle(final long[] center, final long radius, RandomAccess<T> access, T value) {
		sampleCircle(center, radius, 
				pos -> {
					access.setPosition(pos);
					access.get().set(value);
				});
	}




	public static void sampleCircle(final long[] center, final long radius, Consumer<long[]> processor) {

		long d = (5 - radius * 4)/4;
		int x = 0;
		long y = radius;

		long[] pos = Arrays.copyOf(center, center.length);

		do {
			pos[0] = center[0] + x;		pos[1] = center[1] + y;		processor.accept(pos);
			pos[0] = center[0] + x;		pos[1] = center[1] - y;		processor.accept(pos);
			pos[0] = center[0] - x;		pos[1] = center[1] + y;		processor.accept(pos);
			pos[0] = center[0] - x;		pos[1] = center[1] - y;		processor.accept(pos);
			pos[0] = center[0] + y;		pos[1] = center[1] + x;		processor.accept(pos);
			pos[0] = center[0] + y;		pos[1] = center[1] - x;		processor.accept(pos);
			pos[0] = center[0] - y;		pos[1] = center[1] + x;		processor.accept(pos);
			pos[0] = center[0] - y;		pos[1] = center[1] - x;		processor.accept(pos);

			if (d < 0) {
				d += 2 * x + 1;
			} else {
				d += 2 * (x - y) + 1;
				y--;
			}
			x++;
		} while (x <= y);

	}

	
	
	
	
	public static void sampleDisc(final long[] center, final long r, Consumer<long[]> processor) {

		final long[] pos = new long[2];
		long r2 = r * r;
		long area = r2 << 2;
		long rr = r << 1;

		for (int i = 0; i < area; i++)
		{
		    long tx = (i % rr) - r;
		    long ty = (i / rr) - r;

		    if (tx * tx + ty * ty <= r2){
		    	pos[0] = center[0] + tx;
		    	pos[1] = center[1] + ty;
		    	processor.accept(pos);
		    }
		}

	}
	
	
	
	
	

	public static <E extends RealType<E>> void drawEllipse(RandomAccessibleInterval<E> view, long[] center, long[] radii, double[] orient, E value){

		final EllipsoidNeighborhood<E> ellipse = new EllipsoidNeighborhood<>(view, center, radii);

		final Function<long[],long[]> rotation = ImgDimensions.rotation3D(orient, center);

		long[] pos = new long[3];
		Cursor<E> c = ellipse.localizingCursor();
		RandomAccess<E> access = view.randomAccess();
		while(c.hasNext()){
			c.next();
			c.localize(pos);
			long[] rot = rotation.apply(pos);
			access.setPosition(rot);
			access.get().set(value);
		}		

	}


	public static <E extends RealType<E>> void drawEllipse(RandomAccessibleInterval<E> view, long[] center, long[] radii, E value){

		EllipsoidNeighborhood<E> ellipse = new EllipsoidNeighborhood<>(view, center, radii);

		Cursor<E> c = ellipse.localizingCursor();
		while(c.hasNext()){
			c.next();
			c.get().set(value);
		}		

	}



	public static <T extends Type<T>> void drawLine(RandomAccess<T> access, long[] o, long[] h, T value){

		//Create a vector
		double[] v = new double[o.length];
		double sumSqr = 0;
		for(int i = 0; i<o.length; i++){
			v[i] = o[i]-h[i];
			sumSqr+=Math.pow(v[i], 2);
		}
		//Normalize
		double d = (int) Math.sqrt(sumSqr);
		for(int i = 0; i<o.length; i++)
			v[i]/=d;

		long[] p = new long[o.length];

		for(long x = 0; x<Math.ceil(d); x++){
			for(int i = 0; i<o.length; i++){
				p[i] = (long) (h[i] + x*v[i]);
			}
			access.setPosition(p);				
			access.get().set(value);
		}

	}





	/**
	 * Provides to a {@link Consumer} the locations of the point of a line between a source and a target point.
	 * NB: The 2 provided points are excluded from the path. Also the long[] provided to the consumer is updated as the
	 * line is traversed, copy the array every time if you want to list the locations!! 
	 * @param s The source point
	 * @param t The target point
	 * @param consumer The consumer of locations
	 */
	public static <T extends Type<T>> void linePath(long[] s, long[] t, Consumer<long[]> consumer){

		//Create a vector
		double[] v = new double[s.length];
		double sumSqr = 0;
		for(int i = 0; i<s.length; i++){
			v[i] = t[i]-s[i];
			sumSqr+=Math.pow(v[i], 2);
		}
		//Normalize
		double d = (int) Math.sqrt(sumSqr);
		for(int i = 0; i<s.length; i++)
			v[i]/=d;

		long[] last = Arrays.copyOf(s, s.length);
		long[] p = new long[s.length];

		for(long x = 1; x<Math.ceil(d); x++){
			for(int i = 0; i<s.length; i++){
				p[i] = (long) (s[i] + x*v[i]);
			}
			if(!Arrays.equals(last, p))
				consumer.accept(p);
			for(int i = 0; i<s.length; i++)
				last[i] = p[i];
		}

	}




	/*
	 * check if polygon is convex
	 */
	public static boolean checkConvexity(List<long[]> polygon)
	{
		if (polygon.size() < 3) return false;

		long[] p;
		long[] v;
		long[] u;
		long res = 0;
		for (int i = 0; i < polygon.size() - 2; i++)
		{
			p = polygon.get(i);
			long[] tmp = polygon.get(i+1);
			v = new long[2];
			v[0] = tmp[0] - p[0];
			v[1] = tmp[1] - p[1];
			u = polygon.get(i+2);

			if (i == 0)
			{
				res = u[0] * v[1] - u[1] * v[0] + v[0] * p[1] - v[1] * p[0];
			}
			else
			{
				long newres = u[0] * v[1] - u[1] * v[0] + v[0] * p[1] - v[1] * p[0];
				if ( (newres > 0 && res < 0) || (newres < 0 && res > 0) )
					return false;
			}
		}
		return true;
	}




	public static boolean selfIntersecting(List<long[]> polygon){

		//TODO use Bentley-Ottman instead
		// See http://www.cs.yorku.ca/~aaw/TristanCarvelho/SegmentIntersectionAlgorithm.html


		List<Vector2D> vertices = polygon.stream().map(l->new Vector2D(l[0],l[1])).collect(Collectors.toList());


		for (int i = 0; i < vertices.size(); ++i)

		{

			if (i < vertices.size() - 1)

			{

				for (int h = i + 1; h < vertices.size(); ++h)

				{

					// Do two vertices lie on top of one another?

					if (vertices.get(i).equals(vertices.get(h)))

					{

						return true;

					}

				}

			}



			int j = (i + 1) % vertices.size();

			Vector2D iToj = vertices.get(j).subtract(vertices.get(i));

			Vector2D iTojNormal = new Vector2D(iToj.getY(), -iToj.getX());

			// i is the first vertex and j is the second

			int startK = (j + 1) % vertices.size();

			int endK = (i - 1 + vertices.size()) % vertices.size();

			endK += startK < endK ? 0 : startK + 1;

			int k = startK;

			Vector2D iTok = vertices.get(k).subtract(vertices.get(i));

			boolean onLeftSide = iTok.dotProduct(iTojNormal) >= 0;

			Vector2D prevK = vertices.get(k);

			++k;

			for (; k <= endK; ++k)

			{

				int modK = k % vertices.size();

				iTok = vertices.get(modK).subtract(vertices.get(i));

				if (onLeftSide != iTok.dotProduct(iTojNormal) >= 0)

				{

					Vector2D prevKtoK =vertices.get(modK).subtract(prevK);

					Vector2D prevKtoKNormal = new Vector2D(prevKtoK.getY(), -prevKtoK.getX());

					Vector2D sub1 = vertices.get(i).subtract(prevK);
					Vector2D sub2 = vertices.get(j).subtract(prevK);

					if (sub1.dotProduct(prevKtoKNormal) >= 0 != sub2.dotProduct(prevKtoKNormal) >= 0)

					{

						return true;

					}

				}

				onLeftSide = iTok.dotProduct(iTojNormal) > 0;

				prevK = vertices.get(modK);

			}

		}

		return false;

	}














	/*
	 * Modified from http://www.sunshine2k.de/coding/java/Polygon/Filling/FillPolygon.htm
	 * 
	 */
	public static void FillPolygon(List<long[]> polygon, BiConsumer<long[],long[]> lineDrawer)
	{
		if(polygon.size()<3)
			return;

		boolean remove = false;
		if(!Arrays.equals(polygon.get(0),polygon.get(polygon.size()-1))){
			polygon.add(polygon.get(0));
			remove = true;
		}	

		// create edges array from polygon vertice vector
		// make sure that first vertice of an edge is the smaller one
		Edge[] sortedEdges = createEdges(polygon);

		if(remove)
			polygon.remove(polygon.size()-1);





		// sort all edges by y coordinate, smallest one first, lousy bubblesort
		Edge tmp;

		for (int i = 0; i < sortedEdges.length - 1; i++)
			for (int j = 0; j < sortedEdges.length - 1; j++)
			{
				if (sortedEdges[j].p1[1] > sortedEdges[j+1].p1[1]) 
				{
					// swap both edges
					tmp = sortedEdges[j];
					sortedEdges[j] = sortedEdges[j+1];
					sortedEdges[j+1] = tmp;
				}  
			}

		// find biggest y-coord of all vertices
		int scanlineEnd = 0;
		for (int i = 0; i < sortedEdges.length; i++)
		{
			if (scanlineEnd < sortedEdges[i].p2[1])
				scanlineEnd = (int) sortedEdges[i].p2[1];
		}

		// --- DEBUG ---
		/*
        System.out.println("==============");
        for (int i = 0; i < sortedEdges.length; i++)
            System.out.println("X: " + sortedEdges[i].p1.x + " Y: " + sortedEdges[i].p1.y + 
                   " --- " + "X: " + sortedEdges[i].p2.x + " Y: " + sortedEdges[i].p2.y);
		 */


		// scanline starts at smallest y coordinate
		int scanline = (int) sortedEdges[0].p1[1];

		// this list holds all cutpoints from current scanline with the polygon
		ArrayList<Integer> list = new ArrayList<Integer>();


		// move scanline step by step down to biggest one
		for (scanline = (int) sortedEdges[0].p1[1]; scanline <= scanlineEnd; scanline++)
		{
			//System.out.println("ScanLine: " + scanline); // DEBUG

			list.clear();

			// loop all edges to see which are cut by the scanline
			for (int i = 0; i < sortedEdges.length; i++)
			{   

				// here the scanline intersects the smaller vertice
				if (scanline == sortedEdges[i].p1[1]) 
				{
					if (scanline == sortedEdges[i].p2[1])
					{
						// the current edge is horizontal, so we add both vertices
						sortedEdges[i].deactivate();
						list.add((int)sortedEdges[i].curX);
					}
					else
					{
						sortedEdges[i].activate();
						// we don't insert it in the list cause this vertice is also
						// the (bigger) vertice of another edge and already handled
					}
				}

				// here the scanline intersects the bigger vertice
				if (scanline == sortedEdges[i].p2[1])
				{
					sortedEdges[i].deactivate();
					list.add((int)sortedEdges[i].curX);
				}

				// here the scanline intersects the edge, so calc intersection point
				if (scanline > sortedEdges[i].p1[1] && scanline < sortedEdges[i].p2[1])
				{
					sortedEdges[i].update();
					list.add((int)sortedEdges[i].curX);
				}

			}

			// now we have to sort our list with our x-coordinates, ascendend
			int swaptmp;
			for (int i = 0; i < list.size(); i++)
				for (int j = 0; j < list.size() - 1; j++)
				{
					if (list.get(j) > list.get(j+1))
					{
						swaptmp = list.get(j);
						list.set(j, list.get(j+1));
						list.set(j+1, swaptmp);
					}

				}


			if (list.size() < 2 || list.size() % 2 != 0) 
			{
				// TODO System.out.println("This should never happen!");
				continue;
			}

			// so draw all line segments on current scanline
			for (int i = 0; i < list.size(); i+=2)
			{
				lineDrawer.accept(new long[]{list.get(i), scanline}, 
						new long[]{list.get(i+1), scanline});
			}

		}

	}









	/**
	 * Find the area of a non intersecting polygon. If the polygon self-intersects, then
	 * the returned value will be underestimated. 
	 * @param polygon
	 * @return The estimated area.
	 */
	public static double areaNonIntersecting(List<long[]> polygon){
		int i, j;
		double area = 0;

		for (i = 0; i < polygon.size(); i++) {
			j = (i + 1) % polygon.size();
			area += polygon.get(i)[0] * polygon.get(j)[1];
			area -= polygon.get(i)[1] * polygon.get(j)[0];
		}

		area /= 2.0;
		return (Math.abs(area));
	}


	/**
	 * @param contour The contour points of the polygon. Note the polygon should not self-intersect.
	 * @return The centroid and the Area of a polygon (+/- sign convention for clockwise or anti clockwise polygon respectively)
	 */
	public static Pair<Double,long[]> centroidArea(List<long[]> contour){

		if(contour.isEmpty())
			return new Pair<>(null,null);

		boolean remove = false;
		if(!Arrays.equals(contour.get(0), contour.get(contour.size()-1))){
			contour.add(contour.get(0));
			remove = true;
		}


		double area = 0;
		long[] c = new long[2];
		long x,y,xp1, yp1, xyp1,xp1y;

		for(int i = 0; i<contour.size()-1; i++){

			x = contour.get(i)[0];			y = contour.get(i)[1];
			xp1 = contour.get(i+1)[0];		yp1 = contour.get(i+1)[1];
			xyp1 = x*yp1; 					xp1y = xp1*y;

			area += xyp1 - xp1y;	
			c[0] += (x + xp1) * (xyp1 - xp1y);
			c[1] += (y + yp1) * (xyp1 - xp1y);
		}		


		area /= 2d;

		c[0] /= 6d*area;
		c[1] /= 6d*area;

		if(remove)
			contour.remove(contour.size()-1);

		//area = Math.abs(area);

		return new Pair<>(area,c);
	}





	public static Pair<Vector2D,Vector2D> fitEllipse(List<long[]> polyline){

		double[][] xy = new double[polyline.size()][2];

		for(int i = 0; i<polyline.size(); i++){
			xy[i][0] = polyline.get(i)[0];
			xy[i][1] = polyline.get(i)[1];       	
		}

		Covariance cov = new Covariance(xy);
		RealMatrix m = cov.getCovarianceMatrix();
		EigenDecomposition dec = new EigenDecomposition(m);

		RealVector v1 = dec.getEigenvector(0);
		RealVector v2 = dec.getEigenvector(1);
		Vector2D va = new Vector2D(v1.getEntry(0), v1.getEntry(1));
		Vector2D vb = new Vector2D(v2.getEntry(0), v2.getEntry(1));
		va = va.scalarMultiply(FastMath.sqrt(dec.getRealEigenvalue(0)));
		vb = vb.scalarMultiply(FastMath.sqrt(dec.getRealEigenvalue(1)));

		return new Pair<>(va,vb);
	}


	/**
	 * @param a Major semi axis length
	 * @param b Minor semi axis length
	 * @return Ramanujan estimation of the perimeter of the ellipse
	 */
	public static double perimeter(double a, double b){
		return Math.PI*(3*(a+b)- FastMath.sqrt((3*a+b)*(a+3*b)));
	}









	/*
	 * Create from the polygon vertices an array of edges.
	 * Note that the first vertice of an edge is always the one with the smaller y coordinate one of both
	 */
	private static Edge[] createEdges(List<long[]> polygon)
	{
		Edge[] sortedEdges = new Edge[polygon.size()-1];
		for (int i = 0; i < polygon.size() - 1; i++)
		{
			//if (polygon.elementAt(i).y == polygon.elementAt(i+1).y) continue;
			if (polygon.get(i)[1] < polygon.get(i+1)[1])
				sortedEdges[i] = new Edge(polygon.get(i), polygon.get(i+1));
			else
				sortedEdges[i] = new Edge(polygon.get(i+1), polygon.get(i));
		}
		return sortedEdges;
	}









	private static class Edge {

		public long[] p1;        // first vertice
		public long[] p2;        // second vertice
		float m;                // slope

		float curX;             // x-coord of intersection with scanline

		/*
		 * Create on edge out of two vertices
		 */
		public Edge(long[] a, long[] b)
		{
			p1 = Arrays.copyOf(a,a.length);
			p2 = Arrays.copyOf(b,b.length);

			// m = dy / dx
			m = (float)((float)(a[1] - b[1]) / (float)(a[0] - b[0]));
		}

		/*
		 * Called when scanline intersects the first vertice of this edge.
		 * That simply means that the intersection point is this vertice.
		 */
		public void activate()
		{
			curX = p1[0];
		}

		/*
		 * Update the intersection point from the scanline and this edge.
		 * Instead of explicitly calculate it we just increment with 1/m every time
		 * it is intersected by the scanline.
		 */
		public void update()
		{
			curX += (float)((float)1/(float)m);
		}

		/*
		 * Called when scanline intersects the second vertice, 
		 * so the intersection point is exactly this vertice and from now on 
		 * we are done with this edge
		 */
		public void deactivate()
		{
			curX = p2[0];
		}

	}






	/**
	 * @param p1 The polygon to be clipped
	 * @param p2 The clipper polygon
	 * @return The Clipped Polygon
	 */
	public static List<long[]> clipPolygon(List<long[]> p1, List<long[]> p2) {

		List<long[]> result = new ArrayList<>(p1);

		int len = p2.size();
		for (int i = 0; i < len; i++) {

			int len2 = result.size();
			List<long[]> input = result;
			result = new ArrayList<>(len2);

			long[] A = p2.get((i + len - 1) % len);
			long[] B = p2.get(i);

			for (int j = 0; j < len2; j++) {

				long[] P = input.get((j + len2 - 1) % len2);
				long[] Q = input.get(j);

				if (isInside(A, B, Q)) {
					if (!isInside(A, B, P))
						result.add(intersection(A, B, P, Q));
					result.add(Q);
				} else if (isInside(A, B, P))
					result.add(intersection(A, B, P, Q));
			}
		}

		return result;
	}

	private static boolean isInside(long[] a, long[] b, long[] c) {
		return (a[0] - c[0]) * (b[1] - c[1]) > (a[1] - c[1]) * (b[0] - c[0]);
	}

	private static long[] intersection(long[] a, long[] b, long[] p, long[] q) {
		long A1 = b[1] - a[1];
		long B1 = a[0] - b[0];
		long C1 = A1 * a[0] + B1 * a[1];

		long A2 = q[1] - p[1];
		long B2 = p[0] - q[0];
		long C2 = A2 * p[0] + B2 * p[1];

		long det = A1 * B2 - A2 * B1;
		long x = (B2 * C1 - B1 * C2) / det;
		long y = (A1 * C2 - A2 * C1) / det;

		return new long[]{x, y};
	}


	/**
	 * Computes the curvature at each point in the given chain code as described in 
	 * Shyamosree Pal et al, IEEE Explore, 2009 Estimation of discrete curvature based on chain-code pairing and digital straightness.
	 * @param cc
	 * @param k
	 * @return The curvature computed at every index in the given chain code
	 */
	public static List<Float> curvature(List<Integer> chainCode, int k) {


		final int s = chainCode.size();		
		if(s<k+1)
			throw new IllegalArgumentException("Chain code is too short "+s);

		final List<Integer> cc = new ArrayList<>(chainCode.subList(s-(k+1), s));
		//Collections.reverse(cc);
		cc.addAll(chainCode); 
		cc.addAll(chainCode.subList(0,k+1));

		//cc.forEach(System.out::println);

		List<Float> curv = new ArrayList<>(s);


		for(int i = k+1; i < s+k+1; i++){

			int pos = 0, neg = 0;
			float f = 0, fp1 = 0, fm1 = 0;
			float sum = 0;
			for(int j = -k/2; j < k/2; j++){

				f = Math.abs( cc.get(i+j) - cc.get(i-(j+1)) );
				fp1 = Math.abs( cc.get(i+(j+1)) - cc.get(i-(j+1)) );
				fm1 = Math.abs( cc.get(i+j) - cc.get(i-j) );

				float minF = Math.min(f, 8-f);
				float minFp1 = Math.min(fp1, 8-fp1);
				float minFm1 = Math.min(fm1, 8-fm1);
				minF = Math.min(minF, minFp1);
				minF = Math.min(minF, minFm1);
				sum += minF;

				// give a sign based on concave or convex (assuming clockwise here)
				int rot = FastMath.floorMod((cc.get(i+j+1) - cc.get(i+j)) , 8);
				
				//System.out.println(rot);
				
				if(rot!=0){
					if(rot<4 )
						neg++;
					else
						pos++;
				}

			}
			
			if(neg >= pos)
				sum = -sum;



			curv.add(sum/(float)k);
		}		


		return curv;
	}












	public static long boundingBoxUnion(long[] min1, long[] max1, long[] min2, long[] max2) {

		assert min1.length == max1.length : "min1 and max1 do not have the same length";
		assert min1.length == min2.length : "min1 and min2 do not have the same length";
		assert min2.length == max2.length : "min2 and max2 do not have the same length";

		long u = 1;

		for(int i = 0; i<min1.length; i++){
			long check = Math.min(max1[i], max2[i]) - Math.max(min1[i], min2[i]);
			if(check < 0)
				return 0;
			u *= check;
		}		
		return u;
	}

	public static long boundingBoxVolume(long[] min, long[] max) {
		assert min.length == max.length : "min and max do not have the same length";
		long vol = 1;
		for(int i = 0; i<min.length; i++){
			assert max[i] >= min[i] : "min "+i+" is > max!";
			vol *= max[i]-min[i];
		}
		return vol;
	}

	public static double boundingBoxJaccard(long[] min1, long[] max1, long[] min2, long[] max2) {
		double union = boundingBoxUnion(min1, max1, min2, max2);
		return union / (boundingBoxVolume(min1,max1) + boundingBoxVolume(min2,max2) - union); 
	}


}
