package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.imglib2.Cursor;
import net.imglib2.algorithm.region.localneighborhood.AbstractNeighborhood;
import net.imglib2.type.Type;

public class LabelledMorphology {

	public static <T extends Comparable<T> & Type<T>> NeighborhoodFunction<T> dilate(T label){
		return new NeighborhoodFunction<T>(){


			@Override
			public T apply(AbstractNeighborhood<T> n, T center) {

				if(center.compareTo(label)==0)
					return label;

				Cursor<T> c = n.cursor();			

				while(c.hasNext()){
					c.next();
					if(c.get().compareTo(label)==0)
						return label;
				}
				return center;
			}

		};
	}




	public static <T extends Comparable<T> & Type<T>> NeighborhoodFunction<T> erode(T label){
		return new NeighborhoodFunction<T>(){

			@Override
			public T apply(AbstractNeighborhood<T> n, T center) {

				if(center.compareTo(label)==0){

					Cursor<T> c = n.cursor();			

					while(c.hasNext()){
						c.next();
						if(c.get().compareTo(label)!=0)
							return c.get();
					}

					return label;
				}
				
				return center;
				

			}

		};
	}


}
