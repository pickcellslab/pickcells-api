package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.pickcellslab.pickcells.api.img.io.ImgIO;

import net.imglib2.Cursor;
import net.imglib2.IterableInterval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.RectangleNeighborhoodGPL;
import net.imglib2.img.Img;
import net.imglib2.outofbounds.OutOfBoundsConstantValueFactory;
import net.imglib2.outofbounds.OutOfBoundsFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public abstract class Outline {

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T extends RealType<T> & NativeType<T>> Img<T> run(ImgIO io, RandomAccessibleInterval<T> image){

		IterableInterval<T> source = Views.iterable(image);
		
		T v = source.firstElement().createVariable();
		v.setZero();

		RectangleNeighborhoodGPL<T> kernel = 
				new RectangleNeighborhoodGPL<T>(image,(OutOfBoundsFactory<T, RandomAccessibleInterval<T>>) new OutOfBoundsConstantValueFactory(v) );


		long[] span = new long[source.numDimensions()];
		Arrays.fill(span, 1);
		kernel.setSpan(span);
		
		long[] dim = new long[source.numDimensions()];
		source.dimensions(dim);
		
		Img<T> result = ImgDimensions.hardCopy(io, image, v);
		

		Cursor<T> sCursor = source.localizingCursor();
		Cursor<T> rCursor = source.cursor();
		while(sCursor.hasNext()){
			sCursor.fwd();
			rCursor.fwd();
			float value = sCursor.get().getRealFloat();
			if(0!=value){
				kernel.setPosition(sCursor);
				boolean isEdge = false;
				for(T t : kernel){
					if(t.getRealFloat()!=value){
						isEdge = true;
						break;
					}					
				}
				if(isEdge)
					rCursor.get().setReal(255f);
			}
		}
		return result;

	}
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T extends RealType<T>> void run(RandomAccessibleInterval<T> randomAccessibleInterval, T label, Consumer<long[]> consumer){

		T zero = label.createVariable();
		zero.setZero();

		RectangleNeighborhoodGPL<T> kernel = 
				new RectangleNeighborhoodGPL<T>(randomAccessibleInterval,(OutOfBoundsFactory<T, RandomAccessibleInterval<T>>) new OutOfBoundsConstantValueFactory(zero) );


		long[] span = new long[randomAccessibleInterval.numDimensions()];
		Arrays.fill(span, 1);
		kernel.setSpan(span);
		
		

		Cursor<T> sCursor = Views.iterable(randomAccessibleInterval).localizingCursor();
		while(sCursor.hasNext()){
			sCursor.fwd();			
			if(sCursor.get().compareTo(label) == 0){
				kernel.setPosition(sCursor);
				boolean isEdge = false;
				for(T t : kernel){
					if(t.compareTo(label) != 0){
						isEdge = true;
						break;
					}					
				}
				if(isEdge){
					long[] e = new long[span.length];
					sCursor.localize(e);
					consumer.accept(e);					
				}
			}
		}

	}
	
	
	
	
	
	
	
	public static <T extends RealType<T>> void run(RandomAccessibleInterval<T> source, Consumer<long[]> consumer, OutOfBoundsFactory<T, RandomAccessibleInterval<T>> fctry){

		final T zero = source.randomAccess().get().createVariable();

		final RectangleNeighborhoodGPL<T> kernel = 	new RectangleNeighborhoodGPL<T>(source, fctry);

		final long[] span = new long[source.numDimensions()];
		Arrays.fill(span, 1);
		kernel.setSpan(span);
		
		
		final long[] e = new long[span.length];
		final Cursor<T> sCursor = Views.iterable(source).cursor();
		while(sCursor.hasNext()){
			sCursor.fwd();			
			if(sCursor.get().compareTo(zero) != 0){
				kernel.setPosition(sCursor);
				boolean isEdge = false;
				for(T t : kernel){
					if(t.compareTo(sCursor.get()) != 0){
						isEdge = true;
						break;
					}					
				}
				if(isEdge){					
					sCursor.localize(e);
					consumer.accept(e);					
				}
			}
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	public static <T extends RealType<T>> List<long[]> run(List<long[]> points, RandomAccessibleInterval<T> source){

		List<long[]> edge = new ArrayList<>();
		
		T v = Views.iterable(source).firstElement().createVariable();
		v.setZero();

		@SuppressWarnings({ "unchecked", "rawtypes" })
		RectangleNeighborhoodGPL<T> kernel = 
				new RectangleNeighborhoodGPL<T>(
						source,(OutOfBoundsFactory<T, RandomAccessibleInterval<T>>) new OutOfBoundsConstantValueFactory(v) );

		long[] span = new long[source.numDimensions()];
		Arrays.fill(span, 1);
		kernel.setSpan(span);
		
		long[] dim = new long[source.numDimensions()];
		source.dimensions(dim);
		
		RandomAccess<T> access = source.randomAccess();
		for(long[] p : points){
			access.setPosition(p);
			float value = access.get().getRealFloat();
			if(0!=value){
				kernel.setPosition(p);
				boolean isEdge = false;
				for(T t : kernel){
					if(t.getRealFloat()!=value){
						isEdge = true;
						break;
					}					
				}
				if(isEdge){
					edge.add(p.clone());					
				}
			}
		}
		return edge;

	}
	
	
	
	


}
