/*******************************************************************************
 * Copyright (c) 2012-2013 Biomedical Image Group (BIG), EPFL, Switzerland.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 *     Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 ******************************************************************************/
package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2019 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Class that implements convolution with kernels. Based on the
 * <code>Convolver2D</code> package of Francois Aguet.
 * 
 * @version April 23, 2013
 * 
 * @author Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 * @author Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 * 
 * <hr>
 * NOTICE: This code was copied from a plugin originally implemented by François Aguet for ImageJ, and ported to Icy by Ricard Delgado-Gonzalo (ricard.delgado@gmail.com) 
 * and Zsuzsanna Püspöki (zsuzsanna.puspoki@epfl.ch). This plugin was based on the following publication:
 * <br><br>
 * M. Jacob, M. Unser, “Design of Steerable Filters for Feature Detection Using Canny-Like Criteria,”
 * IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 8, pp. 1007-1019
 * <hr>
 * 
 * 
 */
public class Convolver2D {

	// ============================================================================
	// PUBLIC METHODS

	/** Convolution with a symmetric kernel along x. */
	public static double[][] convolveEvenX(double[][] input, double[] kernel) {

		int nx = input.length;
		int ny = input[0].length;
		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * nx - 2;

		double[][] output = new double[nx][ny];

		for (int y = 0; y < ny; y++) {
			for (int x = 0; x < k_1; x++) {
				output[x][y] = kernel[0] * input[x][y];
				for (int i = 1; i <= x; i++) {
					output[x][y] += kernel[i]
							* (input[x - i][y] + input[x + i][y]);
				}
				for (int i = x + 1; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[i - x][y] + input[x + i][y]);
				}
			}
			for (int x = k_1; x <= nx - k; x++) {
				output[x][y] = kernel[0] * input[x][y];
				for (int i = 1; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[x - i][y] + input[x + i][y]);
				}
			}
			for (int x = nx - k_1; x < nx; x++) {
				output[x][y] = kernel[0] * input[x][y];
				for (int i = 1; i < nx - x; i++) {
					output[x][y] += kernel[i]
							* (input[x - i][y] + input[x + i][y]);
				}
				for (int i = nx - x; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[b - i - x][y] + input[x - i][y]);
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with a symmetric kernel along x, for an image defined as a
	 * 1-D array.
	 */
	public static double[] convolveEvenX(double[] input, double[] kernel,
			int nx, int ny) {

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * nx - 2;

		double[] output = new double[nx * ny];

		int idx = 0;
		for (int y = 0; y < ny; y++) {
			for (int x = 0; x < k_1; x++) {
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i <= x; i++) {
					output[idx] += kernel[i]
							* (input[idx - i] + input[idx + i]);
				}
				for (int i = x + 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[i - x + y * nx] + input[idx + i]);
				}
				idx++;
			}
			for (int x = k_1; x <= nx - k; x++) {
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[idx - i] + input[idx + i]);
				}
				idx++;
			}
			for (int x = nx - k_1; x < nx; x++) {
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i < nx - x; i++) {
					output[idx] += kernel[i]
							* (input[idx - i] + input[idx + i]);
				}
				for (int i = nx - x; i < k; i++) {
					output[idx] += kernel[i]
							* (input[b - i - x + y * nx] + input[idx - i]);
				}
				idx++;
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with a symmetric kernel along x, for an image defined as a
	 * 1-D array.
	 */
	public static float[] convolveEvenX(float[] input, float[] kernel, int nx,
			int ny) {

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * nx - 2;

		float[] output = new float[nx * ny];

		int idx = 0;
		for (int y = 0; y < ny; y++) {
			for (int x = 0; x < k_1; x++) {
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i <= x; i++) {
					output[idx] += kernel[i]
							* (input[idx - i] + input[idx + i]);
				}
				for (int i = x + 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[i - x + y * nx] + input[idx + i]);
				}
				idx++;
			}
			for (int x = k_1; x <= nx - k; x++) {
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[idx - i] + input[idx + i]);
				}
				idx++;
			}
			for (int x = nx - k_1; x < nx; x++) {
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i < nx - x; i++) {
					output[idx] += kernel[i]
							* (input[idx - i] + input[idx + i]);
				}
				for (int i = nx - x; i < k; i++) {
					output[idx] += kernel[i]
							* (input[b - i - x + y * nx] + input[idx - i]);
				}
				idx++;
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with a symmetric kernel along x, for an image defined as a
	 * 1-D array, in a region bounded by [x1..x2] and [y1..y2]. Usage:
	 * convolve{Even,Odd}X followed by convolve{Even,Odd}Y is mandatory.
	 */
	public static double[] convolveEvenX(double[] input, double[] kernel,
			int[] dims, int x1, int x2, int y1, int y2) {

		int nx = dims[0];

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * nx - 2;

		double[] output = new double[(x2 - x1 + 1) * (y2 - y1 + 1)];
		int idx;
		int odx = 0;

		if (x1 < k_1 && x2 > nx - k) { // 2 border conditions
			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x < k_1; x++) {
					idx = x + y * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i <= x; i++) {
						output[odx] += kernel[i]
								* (input[idx - i] + input[idx + i]);
					}
					for (int i = x + 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[i - x + y * nx] + input[idx + i]);
					}
					odx++;
				}
				for (int x = k_1; x < x2 - k_1; x++) {
					idx = x + y * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx - i] + input[idx + i]);
					}
					odx++;
				}
				for (int x = x2 - k_1; x <= x2; x++) {
					idx = x + y * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < nx - x; i++) {
						output[odx] += kernel[i]
								* (input[idx - i] + input[idx + i]);
					}
					for (int i = nx - x; i < k; i++) {
						output[odx] += kernel[i]
								* (input[b - i - x + y * nx] + input[idx - i]);
					}
					odx++;
				}
			}
		} else if (x1 < k_1 && x2 <= nx - k) { // left border conditions
			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x < k_1; x++) {
					idx = x + y * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i <= x; i++) {
						output[odx] += kernel[i]
								* (input[idx - i] + input[idx + i]);
					}
					for (int i = x + 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[i - x + y * nx] + input[idx + i]);
					}
					odx++;
				}
				for (int x = k_1; x <= x2; x++) {
					idx = x + y * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx - i] + input[idx + i]);
					}
					odx++;
				}
			}
		} else if (x1 >= k_1 && x2 > nx - k) { // right border conditions
			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x < x2 - k_1; x++) {
					idx = x + y * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx - i] + input[idx + i]);
					}
					odx++;
				}
				for (int x = x2 - k_1; x <= x2; x++) {
					idx = x + y * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < nx - x; i++) {
						output[odx] += kernel[i]
								* (input[idx - i] + input[idx + i]);
					}
					for (int i = nx - x; i < k; i++) {
						output[odx] += kernel[i]
								* (input[b - i - x + y * nx] + input[idx - i]);
					}
					odx++;
				}
			}
		} else { // no border conditions
			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x <= x2; x++) {
					idx = x + y * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx - i] + input[idx + i]);
					}
					odx++;
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/** Convolution with a symmetric kernel along y. */
	public static double[][] convolveEvenY(double[][] input, double[] kernel) {

		int nx = input.length;
		int ny = input[0].length;
		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * ny - 2;

		double[][] output = new double[nx][ny];

		for (int x = 0; x < nx; x++) {
			for (int y = 0; y < k_1; y++) {
				output[x][y] = kernel[0] * input[x][y];
				for (int i = 1; i <= y; i++) {
					output[x][y] += kernel[i]
							* (input[x][y - i] + input[x][y + i]);
				}
				for (int i = y + 1; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[x][i - y] + input[x][y + i]);
				}
			}
			for (int y = k_1; y <= ny - k; y++) {
				output[x][y] = kernel[0] * input[x][y];
				for (int i = 1; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[x][y - i] + input[x][y + i]);
				}
			}
			for (int y = ny - k_1; y < ny; y++) {
				output[x][y] = kernel[0] * input[x][y];
				for (int i = 1; i < ny - y; i++) {
					output[x][y] += kernel[i]
							* (input[x][y - i] + input[x][y + i]);
				}
				for (int i = ny - y; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[x][b - i - y] + input[x][y - i]);
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with a symmetric kernel along y, for an image defined as a
	 * 1-D array.
	 */
	public static double[] convolveEvenY(double[] input, double[] kernel,
			int nx, int ny) {

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * ny - 2;

		double[] output = new double[nx * ny];

		int idx, inx;
		for (int x = 0; x < nx; x++) {
			for (int y = 0; y < k_1; y++) {
				idx = x + y * nx;
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i <= y; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx - inx] + input[idx + inx]);
				}
				for (int i = y + 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[(i - y) * nx + x] + input[idx + i * nx]);
				}
			}
			for (int y = k_1; y <= ny - k; y++) {
				idx = x + y * nx;
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i < k; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx - inx] + input[idx + inx]);
				}
			}
			for (int y = ny - k_1; y < ny; y++) {
				idx = x + y * nx;
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i < ny - y; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx - inx] + input[idx + inx]);
				}
				for (int i = ny - y; i < k; i++) {
					output[idx] += kernel[i]
							* (input[(b - i - y) * nx + x] + input[idx - i * nx]);
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with a symmetric kernel along y, for an image defined as a
	 * 1-D array.
	 */
	public static float[] convolveEvenY(float[] input, float[] kernel, int nx,
			int ny) {

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * ny - 2;

		float[] output = new float[nx * ny];

		int idx, inx;
		for (int x = 0; x < nx; x++) {
			for (int y = 0; y < k_1; y++) {
				idx = x + y * nx;
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i <= y; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx - inx] + input[idx + inx]);
				}
				for (int i = y + 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[(i - y) * nx + x] + input[idx + i * nx]);
				}
			}
			for (int y = k_1; y <= ny - k; y++) {
				idx = x + y * nx;
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i < k; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx - inx] + input[idx + inx]);
				}
			}
			for (int y = ny - k_1; y < ny; y++) {
				idx = x + y * nx;
				output[idx] = kernel[0] * input[idx];
				for (int i = 1; i < ny - y; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx - inx] + input[idx + inx]);
				}
				for (int i = ny - y; i < k; i++) {
					output[idx] += kernel[i]
							* (input[(b - i - y) * nx + x] + input[idx - i * nx]);
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with a symmetric kernel along x, for an image defined as a
	 * 1-D array, in a region bounded by [x1..x2] and [y1..y2]. Usage:
	 * convolve{Even,Odd}X followed by convolve{Even,Odd}Y is mandatory.
	 */
	public static double[] convolveEvenY(double[] input, double[] kernel,
			int[] dims, int y1, int y2) {

		int nx = dims[0];
		int ny = dims[1];

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * ny - 2;
		double[] output = new double[nx * (y2 - y1 + 1)];

		int idx, inx;
		int odx = 0;

		if (y1 < k_1 && y2 > ny - k) { // 2 border conditions
			for (int x = 0; x < nx; x++) {
				for (int y = y1; y < k_1; y++) {
					idx = x + y * nx;
					odx = idx - y1 * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i <= y; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx - inx] + input[idx + inx]);
					}
					for (int i = y + 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[(i - y) * nx + x] + input[idx + i * nx]);
					}
				}
				for (int y = k_1; y < y2 - k_1; y++) {
					idx = x + y * nx;
					odx = idx - y1 * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < k; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx - inx] + input[idx + inx]);
					}
				}
				for (int y = y2 - k_1; y <= y2; y++) {
					idx = x + y * nx;
					odx = idx - y1 * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < ny - y; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx - inx] + input[idx + inx]);
					}
					for (int i = ny - y; i < k; i++) {
						output[odx] += kernel[i]
								* (input[(b - i - y) * nx + x] + input[idx - i
										* nx]);
					}
				}
			}
		} else if (y1 < k_1 && y2 <= ny - k) { // top border conditions
			for (int x = 0; x < nx; x++) {
				for (int y = y1; y < k_1; y++) {
					idx = x + y * nx;
					odx = idx - y1 * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i <= y; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx - inx] + input[idx + inx]);
					}
					for (int i = y + 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[(i - y) * nx + x] + input[idx + i * nx]);
					}
				}
				for (int y = k_1; y <= y2; y++) {
					idx = x + y * nx;
					odx = idx - y1 * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < k; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx - inx] + input[idx + inx]);
					}
				}
			}
		} else if (y1 >= k_1 && y2 > ny - k) { // bottom border conditions
			for (int x = 0; x < nx; x++) {
				for (int y = y1; y < ny - k_1; y++) {
					idx = x + y * nx;
					odx = idx - y1 * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < k; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx - inx] + input[idx + inx]);
					}
				}
				for (int y = ny - k_1; y <= y2; y++) {
					idx = x + y * nx;
					odx = idx - y1 * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < ny - y; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx - inx] + input[idx + inx]);
					}
					for (int i = ny - y; i < k; i++) {
						output[odx] += kernel[i]
								* (input[(b - i - y) * nx + x] + input[idx - i
										* nx]);
					}
				}
			}
		} else { // no border conditions
			for (int x = 0; x < nx; x++) {
				for (int y = y1; y <= y2; y++) {
					idx = x + y * nx;
					odx = idx - y1 * nx;
					output[odx] = kernel[0] * input[idx];
					for (int i = 1; i < k; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx - inx] + input[idx + inx]);
					}
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/** Convolution with an anti-symmetric kernel along x. */
	public static double[][] convolveOddX(double[][] input, double[] kernel) {

		int nx = input.length;
		int ny = input[0].length;
		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * nx - 2;

		double[][] output = new double[nx][ny];

		for (int y = 0; y < ny; y++) {
			for (int x = 0; x < k_1; x++) {
				output[x][y] = 0.0;
				for (int i = 1; i <= x; i++) {
					output[x][y] += kernel[i]
							* (input[x + i][y] - input[x - i][y]);
				}
				for (int i = x + 1; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[x + i][y] - input[i - x][y]);
				}
			}
			for (int x = k_1; x <= nx - k; x++) {
				output[x][y] = 0.0;
				for (int i = 1; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[x + i][y] - input[x - i][y]);
				}
			}
			for (int x = nx - k_1; x < nx; x++) {
				output[x][y] = 0.0;
				for (int i = 1; i < nx - x; i++) {
					output[x][y] += kernel[i]
							* (input[x + i][y] - input[x - i][y]);
				}
				for (int i = nx - x; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[b - i - x][y] - input[x - i][y]);
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with an anti-symmetric kernel along x, for an image defined
	 * as a 1-D array.
	 */
	public static double[] convolveOddX(double[] input, double[] kernel,
			int nx, int ny) {

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * nx - 2;

		double[] output = new double[nx * ny];

		int idx = 0;
		for (int y = 0; y < ny; y++) {
			for (int x = 0; x < k_1; x++) {
				output[idx] = 0.0;
				for (int i = 1; i <= x; i++) {
					output[idx] += kernel[i]
							* (input[idx + i] - input[idx - i]);
				}
				for (int i = x + 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[idx + i] - input[i - x + y * nx]);
				}
				idx++;
			}
			for (int x = k_1; x <= nx - k; x++) {
				output[idx] = 0.0;
				for (int i = 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[idx + i] - input[idx - i]);
				}
				idx++;
			}
			for (int x = nx - k_1; x < nx; x++) {
				output[idx] = 0.0;
				for (int i = 1; i < nx - x; i++) {
					output[idx] += kernel[i]
							* (input[idx + i] - input[idx - i]);
				}
				for (int i = nx - x; i < k; i++) {
					output[idx] += kernel[i]
							* (input[b - i - x + y * nx] - input[idx - i]);
				}
				idx++;
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with an anti-symmetric kernel along x, for an image defined
	 * as a 1-D array.
	 */
	public static float[] convolveOddX(float[] input, float[] kernel, int nx,
			int ny) {

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * nx - 2;

		float[] output = new float[nx * ny];

		int idx = 0;
		for (int y = 0; y < ny; y++) {
			for (int x = 0; x < k_1; x++) {
				output[idx] = 0.0f;
				for (int i = 1; i <= x; i++) {
					output[idx] += kernel[i]
							* (input[idx + i] - input[idx - i]);
				}
				for (int i = x + 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[idx + i] - input[i - x + y * nx]);
				}
				idx++;
			}
			for (int x = k_1; x <= nx - k; x++) {
				output[idx] = 0.0f;
				for (int i = 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[idx + i] - input[idx - i]);
				}
				idx++;
			}
			for (int x = nx - k_1; x < nx; x++) {
				output[idx] = 0.0f;
				for (int i = 1; i < nx - x; i++) {
					output[idx] += kernel[i]
							* (input[idx + i] - input[idx - i]);
				}
				for (int i = nx - x; i < k; i++) {
					output[idx] += kernel[i]
							* (input[b - i - x + y * nx] - input[idx - i]);
				}
				idx++;
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with an anti-symmetric kernel along x, for an image defined
	 * as a 1-D array, in a region bounded by [x1..x2] and [y1..y2]. Usage:
	 * convolve{Even,Odd}X followed by convolve{Even,Odd}Y is mandatory.
	 */
	public static double[] convolveOddX(double[] input, double[] kernel,
			int[] dims, int x1, int x2, int y1, int y2) {

		int nx = dims[0];

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * nx - 2;

		double[] output = new double[(x2 - x1 + 1) * (y2 - y1 + 1)];

		int idx;
		int odx = 0;

		if (x1 < k_1 && x2 > nx - k) { // 2 border conditions
			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x < k_1; x++) {
					idx = x + y * nx;
					output[odx] = 0.0;
					for (int i = 1; i <= x; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[idx - i]);
					}
					for (int i = x + 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[i - x + y * nx]);
					}
					odx++;
				}
				for (int x = k_1; x < x2 - k_1; x++) {
					idx = x + y * nx;
					output[odx] = 0.0;
					for (int i = 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[idx - i]);
					}
					odx++;
				}
				for (int x = x2 - k_1; x <= x2; x++) {
					idx = x + y * nx;
					output[odx] = 0.0;
					for (int i = 1; i < nx - x; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[idx - i]);
					}
					for (int i = nx - x; i < k; i++) {
						output[odx] += kernel[i]
								* (input[b - i - x + y * nx] - input[idx - i]);
					}
					odx++;
				}
			}
		} else if (x1 < k_1 && x2 <= nx - k) { // left border conditions
			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x < k_1; x++) {
					idx = x + y * nx;
					output[odx] = 0.0;
					for (int i = 1; i <= x; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[idx - i]);
					}
					for (int i = x + 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[i - x + y * nx]);
					}
					odx++;
				}
				for (int x = k_1; x <= x2; x++) {
					idx = x + y * nx;
					output[odx] = 0.0;
					for (int i = 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[idx - i]);
					}
					odx++;
				}
			}
		} else if (x1 >= k_1 && x2 > nx - k) { // right border conditions
			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x < x2 - k_1; x++) {
					idx = x + y * nx;
					output[odx] = 0.0;
					for (int i = 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[idx - i]);
					}
					odx++;
				}
				for (int x = x2 - k_1; x <= x2; x++) {
					idx = x + y * nx;
					output[odx] = 0.0;
					for (int i = 1; i < nx - x; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[idx - i]);
					}
					for (int i = nx - x; i < k; i++) {
						output[odx] += kernel[i]
								* (input[b - i - x + y * nx] - input[idx - i]);
					}
					odx++;
				}
			}
		} else { // no border conditions
			for (int y = y1; y <= y2; y++) {
				for (int x = x1; x <= x2; x++) {
					idx = x + y * nx;
					output[odx] = 0.0;
					for (int i = 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx + i] - input[idx - i]);
					}
					odx++;
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/** Convolution with an anti-symmetric kernel along y. */
	public static double[][] convolveOddY(double[][] input, double[] kernel) {

		int nx = input.length;
		int ny = input.length;
		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * ny - 2;

		double[][] output = new double[nx][ny];

		for (int x = 0; x < nx; x++) {
			for (int y = 0; y < k_1; y++) {
				output[x][y] = 0.0;
				for (int i = 1; i <= y; i++) {
					output[x][y] += kernel[i]
							* (input[x][y + i] - input[x][y - i]);
				}
				for (int i = y + 1; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[x][y + i] - input[x][i - y]);
				}
			}
			for (int y = k_1; y <= ny - k; y++) {
				output[x][y] = 0.0;
				for (int i = 1; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[x][y + i] - input[x][y - i]);
				}
			}
			for (int y = ny - k_1; y < ny; y++) {
				output[x][y] = 0.0;
				for (int i = 1; i < ny - y; i++) {
					output[x][y] += kernel[i]
							* (input[x][y + i] - input[x][y - i]);
				}
				for (int i = ny - y; i < k; i++) {
					output[x][y] += kernel[i]
							* (input[x][b - i - y] - input[x][y - i]);
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with an anti-symmetric kernel along y, for an image defined
	 * as a 1-D array.
	 */
	public static double[] convolveOddY(double[] input, double[] kernel,
			int nx, int ny) {

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * ny - 2;

		double[] output = new double[nx * ny];

		int idx, inx;
		for (int x = 0; x < nx; x++) {
			for (int y = 0; y < k_1; y++) {
				idx = x + y * nx;
				output[idx] = 0.0;
				for (int i = 1; i <= y; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx + inx] - input[idx - inx]);
				}
				for (int i = y + 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[idx + i * nx] - input[(i - y) * nx + x]);
				}
			}
			for (int y = k_1; y <= ny - k; y++) {
				idx = x + y * nx;
				output[idx] = 0.0;
				for (int i = 1; i < k; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx + inx] - input[idx - inx]);
				}
			}
			for (int y = ny - k_1; y < ny; y++) {
				idx = x + y * nx;
				output[idx] = 0.0;
				for (int i = 1; i < ny - y; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx + inx] - input[idx - inx]);
				}
				for (int i = ny - y; i < k; i++) {
					output[idx] += kernel[i]
							* (input[(b - i - y) * nx + x] - input[idx - i * nx]);
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with an anti-symmetric kernel along y, for an image defined
	 * as a 1-D array.
	 */
	public static float[] convolveOddY(float[] input, float[] kernel, int nx,
			int ny) {

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * ny - 2;

		float[] output = new float[nx * ny];

		int idx, inx;
		for (int x = 0; x < nx; x++) {
			for (int y = 0; y < k_1; y++) {
				idx = x + y * nx;
				output[idx] = 0.0f;
				for (int i = 1; i <= y; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx + inx] - input[idx - inx]);
				}
				for (int i = y + 1; i < k; i++) {
					output[idx] += kernel[i]
							* (input[idx + i * nx] - input[(i - y) * nx + x]);
				}
			}
			for (int y = k_1; y <= ny - k; y++) {
				idx = x + y * nx;
				output[idx] = 0.0f;
				for (int i = 1; i < k; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx + inx] - input[idx - inx]);
				}
			}
			for (int y = ny - k_1; y < ny; y++) {
				idx = x + y * nx;
				output[idx] = 0.0f;
				for (int i = 1; i < ny - y; i++) {
					inx = i * nx;
					output[idx] += kernel[i]
							* (input[idx + inx] - input[idx - inx]);
				}
				for (int i = ny - y; i < k; i++) {
					output[idx] += kernel[i]
							* (input[(b - i - y) * nx + x] - input[idx - i * nx]);
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Convolution with an anti-symmetric kernel along y, for an image defined
	 * as a 1-D array, in a region bounded by [x1..x2] and [y1..y2]. Usage:
	 * convolve{Even,Odd}X followed by convolve{Even,Odd}Y is mandatory.
	 */
	public static double[] convolveOddY(double[] input, double[] kernel,
			int[] dims, int y1, int y2) {

		int nx = dims[0];
		int ny = dims[1];

		int k = kernel.length;
		int k_1 = k - 1;
		int b = 2 * ny - 2;

		double[] output = new double[nx * (y2 - y1 + 1)];

		int idx, inx;
		int odx = 0;

		if (y1 < k_1 && y2 > ny - k) { // 2 border conditions
			for (int x = 0; x < nx; x++) {
				for (int y = y1; y < k_1; y++) {
					idx = x + y * nx;
					odx = x + (y - y1) * nx;
					output[odx] = 0.0;
					for (int i = 1; i <= y; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx + inx] - input[idx - inx]);
					}
					for (int i = y + 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx + i * nx] - input[(i - y) * nx + x]);
					}
				}
				for (int y = k_1; y <= ny - k; y++) {
					idx = x + y * nx;
					odx = x + (y - y1) * nx;
					output[odx] = 0.0;
					for (int i = 1; i < k; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx + inx] - input[idx - inx]);
					}
				}
				for (int y = ny - k_1; y <= y2; y++) {
					idx = x + y * nx;
					odx = x + (y - y1) * nx;
					output[odx] = 0.0;
					for (int i = 1; i < ny - y; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx + inx] - input[idx - inx]);
					}
					for (int i = ny - y; i < k; i++) {
						output[odx] += kernel[i]
								* (input[(b - i - y) * nx + x] - input[idx - i
										* nx]);
					}
				}
			}
		} else if (y1 < k_1 && y2 <= ny - k) { // left border conditions
			for (int x = 0; x < nx; x++) {
				for (int y = y1; y < k_1; y++) {
					idx = x + y * nx;
					odx = x + (y - y1) * nx;
					output[odx] = 0.0;
					for (int i = 1; i <= y; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx + inx] - input[idx - inx]);
					}
					for (int i = y + 1; i < k; i++) {
						output[odx] += kernel[i]
								* (input[idx + i * nx] - input[(i - y) * nx + x]);
					}
				}
				for (int y = k_1; y <= y2; y++) {
					idx = x + y * nx;
					odx = x + (y - y1) * nx;
					output[odx] = 0.0;
					for (int i = 1; i < k; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx + inx] - input[idx - inx]);
					}
				}
			}
		} else if (y1 >= k_1 && y2 > ny - k) { // right border conditions
			for (int x = 0; x < nx; x++) {
				for (int y = y1; y <= ny - k; y++) {
					idx = x + y * nx;
					odx = x + (y - y1) * nx;
					output[odx] = 0.0;
					for (int i = 1; i < k; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx + inx] - input[idx - inx]);
					}
				}
				for (int y = ny - k_1; y < y2; y++) {
					idx = x + y * nx;
					odx = x + (y - y1) * nx;
					output[odx] = 0.0;
					for (int i = 1; i < ny - y; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx + inx] - input[idx - inx]);
					}
					for (int i = ny - y; i < k; i++) {
						output[odx] += kernel[i]
								* (input[(b - i - y) * nx + x] - input[idx - i
										* nx]);
					}
				}
			}
		} else { // no border conditions
			for (int x = 0; x < nx; x++) {
				for (int y = y1; y <= y2; y++) {
					idx = x + y * nx;
					odx = x + (y - y1) * nx;
					output[odx] = 0.0;
					for (int i = 1; i < k; i++) {
						inx = i * nx;
						output[odx] += kernel[i]
								* (input[idx + inx] - input[idx - inx]);
					}
				}
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Moving sum filter, applied to both dimensions. Each dimension of the
	 * output is extended by 'length-1'.
	 */
	public static double[][] movingSum(double[][] input, int length) {

		int nx = input.length;
		int ny = input[0].length;

		double[][] output = new double[nx + length - 1][ny + length - 1];

		for (int y = 0; y < ny; y++) {
			output[0][y] = input[0][y];
			for (int x = 1; x < length; x++) {
				output[0][y] += input[x][y];
			}
			for (int x = 1; x < length; x++) {
				output[x][y] = output[x - 1][y] - input[length - x][y]
						+ input[x][y];
			}
			for (int x = length; x < nx; x++) {
				output[x][y] = output[x - 1][y] - input[x - length][y]
						+ input[x][y];
			}
			for (int x = nx; x < nx + length; x++) {
				output[x][y] = output[x - 1][y] - input[x - length][y]
						+ input[2 * nx - x - 2][y];
			}
		}

		for (int x = 0; x < nx + length - 1; x++) {
			output[x][0] = output[x][0];
			for (int y = 1; y < length; y++) {
				output[x][0] += output[x][y];
			}
			for (int y = 1; y < length; y++) {
				output[x][y] = output[x][y - 1] - output[x][length - y]
						+ output[x][y];
			}
			for (int y = length; y < ny; y++) {
				output[x][y] = output[x][y - 1] - output[x][y - length]
						+ output[x][y];
			}
			for (int y = ny; y < ny + length - y; y++) {
				output[x][y] = output[x][y - 1] - output[x][y - length]
						+ output[x][2 * ny - y - 2];
			}
		}

		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Moving sum filter, applied to the x-dimension. The x-dimension of the
	 * output is extended by 'length-1'.
	 */
	public static double[][] movingSumX(double[][] input, int length) {

		int nx = input.length;
		int ny = input[0].length;

		double[][] output = new double[nx + length - 1][ny + length - 1];

		for (int y = 0; y < ny; y++) {
			output[0][y] = input[0][y];
			for (int x = 1; x < length; x++) {
				output[0][y] += input[x][y];
			}
			for (int x = 1; x < length; x++) {
				output[x][y] = output[x - 1][y] - input[length - x][y]
						+ input[x][y];
			}
			for (int x = length; x < nx; x++) {
				output[x][y] = output[x - 1][y] - input[x - length][y]
						+ input[x][y];
			}
			for (int x = nx; x < nx + length; x++) {
				output[x][y] = output[x - 1][y] - input[x - length][y]
						+ input[2 * nx - x - 2][y];
			}
		}
		return output;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Moving sum filter, applied to the y-dimension. The y-dimension of the
	 * output is extended by 'length-1'.
	 */
	public static double[][] movingSumY(double[][] input, int length) {

		int nx = input.length;
		int ny = input[0].length;

		double[][] output = new double[nx + length - 1][ny + length - 1];

		for (int x = 0; x < nx + length - 1; x++) {
			output[x][0] = output[x][0];
			for (int y = 1; y < length; y++) {
				output[x][0] += output[x][y];
			}
			for (int y = 1; y < length; y++) {
				output[x][y] = output[x][y - 1] - output[x][length - y]
						+ output[x][y];
			}
			for (int y = length; y < ny; y++) {
				output[x][y] = output[x][y - 1] - output[x][y - length]
						+ output[x][y];
			}
			for (int y = ny; y < ny + length - y; y++) {
				output[x][y] = output[x][y - 1] - output[x][y - length]
						+ output[x][2 * ny - y - 2];
			}
		}

		return output;
	}
}
