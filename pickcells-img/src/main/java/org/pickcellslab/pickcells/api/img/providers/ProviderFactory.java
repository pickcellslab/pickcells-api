package org.pickcellslab.pickcells.api.img.providers;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;


/**
 * A Factory producing {@link ImageProvider} objects concurrently
 * @author Guillaume Blin
 *
 */
public interface ProviderFactory {


	/**
	 * Adds a {@link SegmentationImageProvider} in the production line
	 * @param labels
	 */
	public <T extends NativeType<T> & RealType<T>> void addToProduction(LabelsImage labels);
	/**
	 * Adds a {@link ChannelImageProvider} in the production line
	 * @param result
	 */
	public <T extends RealType<T>> void addToProduction(Image img);


	/**
	 * Start the production of the {@link ImageProvider} previously added in the production line.
	 * This method may be called multiple times if the caller needs to reload ImageProviders or change the list of providers
	 * to produce between different calls, see {@link  #clearProduction()}
	 * @param timeout
	 * @param unit
	 * @throws InterruptedException
	 */
	public void produceAndWait(long timeout, TimeUnit unit) throws InterruptedException;


	/**
	 * Removes any planned or already produces ImageProviders found in this Factory.
	 */
	public void clearProduction();

	/**
	 * Return the {@link SegmentationImageProvider} associated with the specified
	 * {@link SegmentationResult} or {@code null} if this provider was never
	 * added to the production line or if the production was not started
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public <T extends RealType<T>> SegmentationImageProvider<T> get(LabelsImage labels) throws InterruptedException, ExecutionException, NotInProductionException;

	/**
	 * Return a new instance of {@link SegmentationImageProvider} created from the provided
	 * {@link LabelsImage} and {@link RandomAccessibleInterval}. This method allows the caller to obtain 
	 * a SegmentationImageProvider without reading the image from file, however it is the caller's
	 * responsability to make sure that the LabelsImage does describe the given RandomAccessibleInterval
	 */
	public <T extends RealType<T>> SegmentationImageProvider<T> createFrom(LabelsImage labels, RandomAccessibleInterval<T> img);


	/**
	 * Return the {@link ChannelImageProvider} associated with the specified
	 * {@link Image} or {@code null} if this provider was never
	 * added to the production line or if the production was not started
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public <T extends RealType<T>> ChannelImageProvider<T> get(Image img) throws InterruptedException, ExecutionException;



	/**
	 * Return a new instance of {@link ChannelImageProvider} created from the provided
	 * {@link Image} and {@link RandomAccessibleInterval}. This method allows the caller to obtain 
	 * a ChannelImageProvider without reading the image from file, however it is the caller's
	 * responsability to make sure that the Image does describe the given RandomAccessibleInterval
	 */
	public <T extends RealType<T>> ChannelImageProvider<T> createFrom(Image image, RandomAccessibleInterval<T> img);




}
