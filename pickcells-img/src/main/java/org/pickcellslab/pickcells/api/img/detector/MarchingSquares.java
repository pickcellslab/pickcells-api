package org.pickcellslab.pickcells.api.img.detector;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A simple implementation of the marching squares algorithm that can identify
 * perimeters in an supplied byte array. The array of data over which this
 * instances of this class operate is not cloned by this class's constructor
 * (for obvious efficiency reasons) and should therefore not be modified while
 * the object is in use. It is expected that the data elements supplied to the
 * algorithm have already been thresholded. The algorithm only distinguishes
 * between zero and non-zero values.
 * 
 * @author Tom Gibara
 * 
 */

public class MarchingSquares {

	// fields
	
	private final byte[] data;

	private final int width;

	private final int height;

	// constructors
	
	/**
	 * Creates a new object that can locate perimeter paths in the supplied
	 * data. The length of the supplied data array must exceed width * height,
	 * with the data elements in row major order and the top-left-hand data
	 * element at index zero.
	 * 
	 * @param width
	 *            the width of the data matrix
	 * @param height
	 *            the width of the data matrix
	 * @param data
	 *            the data elements
	 */

	public MarchingSquares(int width, int height, byte[] data) {
		this.width = width;
		this.height = height;
		this.data = data;
	}

	// accessors
	
	/**
	 * @return the width of the data matrix over which this object is operating
	 */
	
	public int getWidth() {
		return width;
	}

	/**
	 * @return the width of the data matrix over which this object is operating
	 */
	
	public int getHeight() {
		return height;
	}

	/**
	 * @return the data matrix over which this object is operating
	 */
	
	public byte[] getData() {
		return data;
	}
	
	// methods
	
	
	
	/**
	 * Finds the perimeter between a set of zero and non-zero values which
	 * begins at the specified data element. If no initial point is known,
	 * consider using the convenience method supplied. The paths returned by
	 * this method are always closed.
	 * 
	 * @param initialX
	 *            the column of the data matrix at which to start tracing the
	 *            perimeter
	 * @param initialY
	 *            the row of the data matrix at which to start tracing the
	 *            perimeter
	 * 
	 * @return a closed, anti-clockwise path that is a perimeter of between a
	 *         set of zero and non-zero values in the data.
	 * @throws IllegalArgumentException
	 *             if there is no perimeter at the specified initial point.
	 */
	
	// TODO could be made more efficient by accumulating value during movement
	public Path identifyPerimeter(int initialX, int initialY) {
		if (initialX < 0) initialX = 0;
		if (initialX > width) initialX = width;
		if (initialY < 0) initialY = 0;
		if (initialY > height) initialY = height;

		int initialValue = value(initialX, initialY);
		if (initialValue == 0 || initialValue == 15)
			throw new IllegalArgumentException(String.format("Supplied initial coordinates (%d, %d) do not lie on a perimeter.", initialX, initialY));

		ArrayList<Direction> directions = new ArrayList<Direction>();

		int x = initialX;
		int y = initialY;
		Direction previous = null;

		do {
			final Direction direction;
			switch (value(x, y)) {
				case  1: direction = Direction.N; break;
				case  2: direction = Direction.E; break;
				case  3: direction = Direction.E; break;
				case  4: direction = Direction.W; break;
				case  5: direction = Direction.N; break;
				case  6: direction = previous == Direction.N ? Direction.W : Direction.E; break;
				case  7: direction = Direction.E; break;
				case  8: direction = Direction.S; break;
				case  9: direction = previous == Direction.E ? Direction.N : Direction.S; break;
				case 10: direction = Direction.S; break;
				case 11: direction = Direction.S; break;
				case 12: direction = Direction.W; break;
				case 13: direction = Direction.N; break;
				case 14: direction = Direction.W; break;
				default: throw new IllegalStateException();
			}
			directions.add(direction);
			x += direction.screenX;
			y += direction.screenY; // accomodate change of basis
			previous = direction;
		} while (x != initialX || y != initialY);

		return new Path(initialX, -initialY, directions);
	}

	/**
	 * A convenience method that locates at least one perimeter in the data with
	 * which this object was constructed. If there is no perimeter (ie. if all
	 * elements of the supplied array are identically zero) then null is
	 * returned.
	 * 
	 * @return a perimeter path obtained from the data, or null
	 */

	public Path identifyPerimeter() {
		int size = width * height;
		for (int i = 0; i < size; i++) {
			if (data[i] != 0) {
				return identifyPerimeter(i % width, i / width);
			}
		}
		return null;
	}

	// private utility methods
	
	private int value(int x, int y) {
		int sum = 0;
		if (isSet(x, y)) sum |= 1;
		if (isSet(x + 1, y)) sum |= 2;
		if (isSet(x, y + 1)) sum |= 4;
		if (isSet(x + 1, y + 1)) sum |= 8;
		return sum;
	}

	private boolean isSet(int x, int y) {
		return x <= 0 || x > width || y <= 0 || y > height ?
			false :
			data[(y - 1) * width + (x - 1)] != 0;
	}
	
	
	
	
	/**
	 * A direction in the plane. As a convenience, directions provide unit vector
	 * components (manhattan metric) for both the conventional plane and screen
	 * coordinates (y axis reversed).
	 * 
	 * @author Tom Gibara
	 * 
	 */

	enum Direction {

		// statics

		E(1, 0), NE(1, 1),

		N(0, 1), NW(-1, 1),

		W(-1, 0), SW(-1, -1),

		S(0, -1), SE(1, -1);

		// fields

		/**
		 * The horizontal distance moved in this direction within the plane.
		 */

		public final int planeX;

		/**
		 * The vertical distance moved in this direction within the plane.
		 */

		public final int planeY;

		/**
		 * The horizontal distance moved in this direction in screen coordinates.
		 */

		public final int screenX;

		/**
		 * The vertical distance moved in this direction in screen coordinates.
		 */

		public final int screenY;

		/**
		 * The euclidean length of this direction's vectors.
		 */

		public final double length;

		// constructor

		private Direction(int x, int y) {
			planeX = x;
			planeY = y;
			screenX = x;
			screenY = -y;
			length = x != 0 && y != 0 ? Math.sqrt(2.0) / 2.0 : 1.0;
		}

	}

	
	
	
	
	/**
	 * Combines a sequence of directions into a path that is rooted at some point in
	 * the plane. No restrictions are placed on paths; they may be zero length,
	 * open/closed, self-intersecting. Path objects are immutable.
	 * 
	 * @author Tom Gibara
	 * 
	 */

	static class Path {

		// statics

		private static final double ADJ_LEN = Math.sqrt(2.0) / 2.0 - 1;

		// fields

		private final Direction[] directions;

		private final List<Direction> directionList;

		private final double length;

		private final int originX;

		private final int originY;

		private final int terminalX;

		private final int terminalY;

		// constructors

		private Path(Path that, int deltaX, int deltaY) {
			this.directions = that.directions;
			this.directionList = that.directionList;
			this.length = that.length;
			this.originX = that.originX + deltaX;
			this.originY = that.originY + deltaY;
			this.terminalX = that.terminalX + deltaX;
			this.terminalY = that.terminalY + deltaY;
		}

		/**
		 * Constructs a path which starts at the specified point in the plane. The
		 * array may be zero length.
		 * 
		 * @param startX
		 *            the x coordinate of the path's origin in the plane
		 * @param startY
		 *            the y coordinate of the path's origin in the plane
		 * @param directions
		 *            an array of directions, never null
		 */

		public Path(int startX, int startY, Direction[] directions) {
			this.originX = startX;
			this.originY = startY;
			this.directions = directions.clone();
			this.directionList = Collections.unmodifiableList(Arrays
					.asList(directions));

			int endX = startX;
			int endY = startY;
			int diagonals = 0;
			for (Direction direction : directions) {
				endX += direction.screenX;
				endY += direction.screenY;
				if (direction.screenX != 0 && direction.screenY != 0) {
					diagonals++;
				}
			}

			this.terminalX = endX;
			this.terminalY = endY;

			this.length = directions.length + diagonals * ADJ_LEN;
		}

		/**
		 * Convenience constructor that converts the supplied direction list into an
		 * array which is then passed to another constructor.
		 * 
		 * @param startX
		 *            the x coordinate of the path's origin in the plane
		 * @param startY
		 *            the y coordinate of the path's origin in the plane
		 * @param directions
		 *            a list of the directions in the path
		 */

		public Path(int startX, int startY, List<Direction> directions) {
			this(startX, startY, directions
					.toArray(new Direction[directions.size()]));
		}

		// accessors

		/**
		 * @return an immutable list of the directions that compose this path, never
		 *         null
		 */

		public List<Direction> getDirections() {
			return directionList;
		}

		/**
		 * @return the x coordinate in the plane at which the path begins
		 */

		public int getOriginX() {
			return originX;
		}

		/**
		 * @return the y coordinate in the plane at which the path begins
		 */

		public int getOriginY() {
			return originY;
		}

		/**
		 * @return the x coordinate in the plane at which the path ends
		 */

		public int getTerminalX() {
			return terminalX;
		}

		/**
		 * @return the y coordinate in the plane at which the path ends
		 */

		public int getTerminalY() {
			return terminalY;
		}

		/**
		 * @return the length of the path using the standard euclidean metric
		 */

		public double getLength() {
			return length;
		}

		/**
		 * @return true if and only if the path's point of origin is the same as
		 *         that of its point of termination
		 */

		public boolean isClosed() {
			return originX == terminalX && originY == terminalY;
		}

		// methods

		/**
		 * Creates a new path by translating this path in the plane.
		 * @param deltaX the change in the path's x coordinate
		 * @param deltaY the change in the path's y coordinate
		 * @return a new path whose origin has been translated
		 */
		
		public Path translate(int deltaX, int deltaY) {
			return new Path(this, deltaX, deltaY);
		}

		// TODO add rotate, mirror and reverse methods

		// object methods

		/**
		 * Two paths are equal if they have the same origin and the same directions.
		 */
		
		@Override
		public boolean equals(Object obj) {
			if (obj == this)
				return true;
			if (!(obj instanceof Path))
				return false;
			Path that = (Path) obj;

			if (this.originX != that.originX)
				return false;
			if (this.originY != that.originY)
				return false;
			if (this.terminalX != that.terminalX)
				return false; // optimization
			if (this.terminalY != that.terminalY)
				return false; // optimization
			if (!Arrays.equals(this.directions, that.directions))
				return false;

			return true;
		}

		@Override
		public int hashCode() {
			return originX ^ 7 * originY ^ directions.hashCode();
		}

		@Override
		public String toString() {
			return "X: " + originX + ", Y: " + originY + " "
					+ Arrays.toString(directions);
		}

	}

	
	

}
