package org.pickcellslab.pickcells.api.img.io;

import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.type.numeric.real.FloatType;

public abstract class HelpIO {
	
	/**
	 * Helper method to obtain an appropriate {@link Data} instance for a specific bit depth (2^bitdepth). Usually obtained
	 * from an {@link Image} or {@link LabelsImage}
	 * @param bitdepth the exposent of the bit depth
	 * @return A {@link Data} to work with the actual {@link Img}
	 */
	@SuppressWarnings("unchecked")
	public static <T extends RealType<T> & NativeType<T>> T type(int bitdepth) {
		T type = null;
		switch(bitdepth){
		case 1: type = (T) new BitType(); break;
		case 2: type = (T) new UnsignedByteType(); break;
		case 3: type = (T) new UnsignedShortType(); break;
		case 4: type = (T) new FloatType(); break;
		}
		return type;
	}
}
