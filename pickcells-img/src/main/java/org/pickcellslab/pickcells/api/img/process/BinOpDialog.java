package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.text.NumberFormat;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
class BinOpDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private boolean wasCancelled = true;
	private int itr = 1;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			BinOpDialog dialog = new BinOpDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	BinOpDialog() {


		JLabel lblKernelType = new JLabel("Kernel Type");

		JComboBox comboBox = new JComboBox<>();

		JLabel lblIterations = new JLabel("Iterations");

		NumberFormat f = NumberFormat.getNumberInstance();
		f.setParseIntegerOnly(true);
		JFormattedTextField itrField = new JFormattedTextField(f);
		itrField.setText("1");



		JButton okButton = new JButton("OK");
		okButton.addActionListener(l->{
			try{
				itr = Integer.parseInt(itrField.getText());
				if(itr < 1){
					JOptionPane.showMessageDialog(this, "The minimum number of iterations is 1");
					return;
				}
			}
			catch(NumberFormatException e){
				JOptionPane.showMessageDialog(this, "Please enter a valid integer for the number of iterations");
				return;
			}
			wasCancelled = false;
			dispose();
		});


		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(l->dispose());



		//Layout
		//----------------------------------------------------------------------------------------

		setBounds(100, 100, 244, 157);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);



		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblKernelType)
								.addComponent(lblIterations))
								.addGap(18)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
										.addComponent(itrField)
										.addComponent(comboBox, 0, 122, Short.MAX_VALUE))
										.addContainerGap(217, Short.MAX_VALUE))
				);
		gl_contentPanel.setVerticalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblKernelType)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGap(18)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblIterations)
										.addComponent(itrField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addContainerGap(158, Short.MAX_VALUE))
				);
		contentPanel.setLayout(gl_contentPanel);



		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonPane.add(okButton);
		buttonPane.add(cancelButton);

		getContentPane().add(buttonPane, BorderLayout.SOUTH);


	}
	
	
	public boolean wasCancelled(){
		return wasCancelled;
	}
	
	public int iterations(){
		return itr;
	}
	
	
	
	
}
