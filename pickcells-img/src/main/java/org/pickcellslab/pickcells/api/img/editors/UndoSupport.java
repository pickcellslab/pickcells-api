package org.pickcellslab.pickcells.api.img.editors;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Optional;

/**
 * A very basic and generic Undo Support.
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of objects done
 */
public class UndoSupport<T> implements Iterable<T> {

	private final LinkedList<T> queue = new LinkedList<>();
	private final int limit ;
	private int cursor = -1;

	public UndoSupport(int length) {
		this.limit = length - 1;
	}

	public void done(T done){
		queue.add(++cursor, done);
		if(cursor>limit){
			queue.removeFirst();
			cursor--;
		}
		else if(cursor < queue.size()-1)
			while(queue.size() != cursor+1)
				queue.pollLast();
	}

	public Optional<T> redo(){
		if(cursor+1 == queue.size() || cursor + 1 == limit || cursor == -1)
			return Optional.empty();
		else
			return Optional.of(queue.get(++cursor));			
	}


	public Optional<T> undo(){
		if(cursor == -1)
			return Optional.empty();
		else
			return Optional.of(queue.get(cursor--));
	}



	public void clear(){
		queue.clear();
		cursor = -1;
	}
	
	public int size(){
		return queue.size();
	}


	@Override
	public String toString(){

		String s = "Undo Queue : \n";
		for(int i = 0; i<queue.size(); i++){
			s+=queue.get(i);
			if(i == cursor)
				s+=" <-- ";
			s+="\n";
		}
		return s;

	}

	@Override
	public Iterator<T> iterator() {
		return queue.iterator();
	}

}
