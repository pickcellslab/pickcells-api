package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.swing.JOptionPane;

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.Wizard;
import org.pickcellslab.foundationj.ui.wizard.WizardListener.Outcome;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.ImgsCheckerBuilder;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.view.ImageViewer;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.display.ColorTable;
import net.imglib2.img.Img;
import net.imglib2.img.ImgView;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.util.Util;
import net.imglib2.view.Views;

/**
 * A Builder of {'link ProcessingPipeline}
 * 
 * @author Guillaume Blin
 *
 * @param <T> The {@link Type} of the input image
 * @param <R> The {@link Type} of the result image
 */
public class PipelineBuilder<T extends NativeType<T> & RealType<T>, R extends NativeType<R> & RealType<R>>{


	@SuppressWarnings("rawtypes")
	final LinkedList<List<ImgProcessing>> steps;
	final List<String> descriptions;





	/**
	 * true if the processing pipeline leads to an image with only one channel
	 */
	protected boolean isFlattened = false;
	protected boolean requiresOverlay = false;
	protected boolean wasCropped = false;


	/**
	 * Image preview display
	 */
	protected ImageViewer<T> view;





	public PipelineBuilder(){	
		steps = new LinkedList<>();
		descriptions = new ArrayList<>();
	}





	/**
	 * Adds a step to the processing pipeline. Note if the step is optional, the caller can include a "mock" ImgProcessing
	 * @param step
	 * @return a new Builder with the new step added.
	 */
	public <N extends RealType<N> & NativeType<N>> PipelineBuilder<T,N> step(ImgProcessing<R,N> step, String description){

		Objects.requireNonNull(step, "Feeding the builder with a null step is forbiden");

		if(step.dimensionalityEffect()==DimensionalityEffect.CHANNELS_TO_FLAT || step.dimensionalityEffect()==DimensionalityEffect.FLAT_TO_FLAT){
			isFlattened = true;
		}

		if(step.requiresAnnotationChannel()){
			requiresOverlay = true;
		}
		this.steps.add(Collections.singletonList(step));
		this.descriptions.add(description);
		
		return (PipelineBuilder) this;
	}





	/**
	 * Adds a step to the processing pipeline. The user will have the choice among all the provided ImgProcessing for this step
	 * and may repeat them if the ImgProcessing are repeatable.
	 * Note if the step is optional, the caller can include a "mock" ImgProcessing named skip for instance
	 * @param steps
	 * @return a new Builder with the new steps added.
	 */
	public <N extends RealType<N> & NativeType<N>> PipelineBuilder<T,N> steps(List<ImgProcessing<R,N>> steps, String description){

		Objects.requireNonNull(steps, "Feeding the builder with null steps is forbiden");

		if(steps.isEmpty())
			throw new IllegalArgumentException("The provided list of steps is empty");

		//Check that all the ImgProcessing in the list are doing the same transformation type
		DimensionalityEffect t = steps.get(0).dimensionalityEffect();
		for(ImgProcessing<R,N> ip : steps){
			if(t != ip.dimensionalityEffect())
				throw new IllegalArgumentException("All the ImgProcessing in the provided list must create the same type of DimensionalityEffect");
			if(t==DimensionalityEffect.CHANNELS_TO_FLAT){
				isFlattened = true;
			}
			if(ip.requiresAnnotationChannel()){
				requiresOverlay = true;
			}
		}
		
		this.steps.add((List)steps);
		this.descriptions.add(description);

		return (PipelineBuilder) this;
	}














	/**
	 * Builds a {@link ProcessingPipeline} configured with the options previously included via this Builder.
	 * @param checkerBuilder An {@link ImgsCheckerBuilder} which will be used to perform any checks required on the list of images chosen by the user.
	 * @return An {@link Optional} holding a user built {@link ProcessingPipeline} if the user completed the built process, or an empty Optional 
	 * if the user cancelled the building process.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Optional<ProcessingPipeline<T,R>> buildForUserChosenFiles(
			UITheme theme,
			NotificationFactory notifier,
			ImgIO io,
			ImgsCheckerBuilder checkerBuilder){




		// Choose images to be processed
		ImgsChooser chooser = io.createChooserBuilder().build();
		chooser.setModal(true);
		chooser.setVisible(true);



		// Ensure at least one has been chosen
		final ImgFileList list = chooser.getChosenList();

		if(list == null) return Optional.empty();


		// Check Consistency (defined by caller)			
		final ImgsChecker checker = checkerBuilder.build(list);


		// Get the image selected for preview
		final int[] prevCoords = chooser.getSelectedImage();
		System.out.println("Prevcoords = "+Arrays.toString(prevCoords));
		if(prevCoords[1] == -1)
			prevCoords[1] = 0;

		final int[] order = ImgsChecker.convert(checker.value(ImgsChecker.ordering, list, prevCoords[0], prevCoords[1]));
		final double[] cal = checker.value(ImgsChecker.calibration, list, prevCoords[0], prevCoords[1]);
		final long[] dims = checker.value(ImgsChecker.dimensions, list, prevCoords[0], prevCoords[1]);
		final String[] units = checker.value(ImgsChecker.units, list, prevCoords[0], prevCoords[1]);

		final MinimalImageInfo prevImgInfo = new MinimalImageInfo(order, dims, cal, units);

		// Load the image
		final Img<T> previewedImage = list.load(prevCoords[0], prevCoords[1]);


		final Pair<ProcessingPipelineNode, ProcessingStep<T, RandomAccessibleInterval<R>>> pipeline = 
				this.showGuiAndGetPipeline(theme, notifier, io, previewedImage, prevImgInfo, order);


		if(pipeline != null){

			ProcessingPipelineNode<?,R> node = pipeline.getKey();

			
			
			// Create TimeManager
			ProcessingRunner<T,R, FileInput<T>> tm = new FileInputManager<>();

			//Create the ImgWriteManagerFactory
			ImgWriteManagerFactory<R> fctry = null;
			if(isFlattened)
				fctry = new SingleChannelImgWriteManagerFactory<>();
			else 
				fctry = new MultiChannelImgWriteManagerFactory<>();

			// Create the PreviewedImageHandler
			InputsHandler<T,R, FileInput<T>> handler = new PreviewedFileHandler(list, prevCoords, prevImgInfo, previewedImage, node.getProcessedImageInfo(), node.getProcessedImage(), wasCropped);

			
			// Now return the ProcessingPipeline
			return Optional.of(new DefaultProcessingPipeline<>(theme, notifier, pipeline.getSecond(), node.getProcessedImageType(), tm, fctry, handler));
			
		}
		else
			return Optional.empty();

	}








	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Pair<ProcessingPipelineNode, ProcessingStep<T, RandomAccessibleInterval<R>>> showGuiAndGetPipeline(
			UITheme theme,
			NotificationFactory notifier,
			ImgIO io,
			Img<T> previewedImage,
			MinimalImageInfo prevImgInfo,
			int[] order){


		// Add channels to the images for both result and overlay if required
		int toAdd = 0;
		if(isFlattened)
			toAdd++;
		if(requiresOverlay)
			toAdd++;

		//final int[] order = info.

		// Check if we need to crop the image	

		long xCrop = 4098, yCrop = 4098;
		if( order[Image.z]!=-1 || order[Image.t]!=-1 ){
			xCrop = 800;	yCrop = 800;
		}
		boolean xyLarge = prevImgInfo.dimension(Image.x)>xCrop && prevImgInfo.dimension(Image.y)>yCrop;		
		boolean zLarge = prevImgInfo.dimension(Image.z)>50;
		boolean tLarge = prevImgInfo.dimension(Image.t)>5;



		//Init dims info
		MinimalImageInfo prevIntervalInfo = null;


		Img<T> previewInterval;
		if(xyLarge || zLarge || tLarge){// Crop Required

			

			//TODO Use imageviewer with overlay to allow the user to choose the cropped area



			long[] sizes = new long[previewedImage.numDimensions()];
			sizes[order[Image.x]] = xCrop/2;	sizes[order[Image.y]] = yCrop/2;
			if(order[Image.c] != -1) sizes[order[Image.c]] = previewedImage.dimension(order[Image.c]);	
			if(order[Image.z] != -1) sizes[order[Image.z]] = 50;	
			if(order[Image.t] != -1) sizes[order[Image.t]] = 2;	
			
			
			JOptionPane.showMessageDialog(null, "A cropped version of the first selected image will be used for preview."
					+ "\nThis will consist of the center region of the original image."
					+ "\nDimensions of the cropped image (same order as the original image) : " + Arrays.toString(sizes));
			

			System.out.println("Sizes" + Arrays.toString(sizes));

			T type = Views.iterable(previewedImage).firstElement().createVariable();
			previewInterval = ImgDimensions.hardCopy(io, ImgDimensions.createCenteredInterval(previewedImage, sizes), type);

			long[] cropDims = new long[previewInterval.numDimensions()];
			previewInterval.dimensions(cropDims);
			prevIntervalInfo = prevImgInfo.crop(cropDims);
			wasCropped = true;

		}else{
			previewInterval = previewedImage;
			prevIntervalInfo = prevImgInfo;
		}	



		//Increase by one channel the image if the user may choose from a processing which may flatten the channels
		System.out.println("Is Flattened ? --> "+isFlattened);
		System.out.println("Requires Overlay ? --> "+requiresOverlay);
		System.out.println("Number of slices to add --> "+toAdd);

		RandomAccessibleInterval<T> copy = previewInterval;
		if(order[Image.c]!=-1){
			if(toAdd != 0)
				copy = ImgDimensions.increase(io, previewInterval, order[Image.c], toAdd); 
		}
		else { // The original image is not multichannel

			T type = Views.iterable(copy).firstElement().createVariable();
			List<RandomAccessibleInterval<T>> stack = new ArrayList<>();
			stack.add(copy);
			if(isFlattened)
				stack.add(io.createImg(copy, type));
			if(requiresOverlay)
				stack.add(io.createImg(copy, type));
			RandomAccessibleInterval<T> multiChannel = Views.stack(stack);
			copy = ImgView.wrap(multiChannel, Util.getArrayOrCellImgFactory(multiChannel, type));

			order[Image.c] = copy.numDimensions()-1;
		}



		ColorTable[] tables = ImgDimensions.createDefaultColorTables(Views.iterable(copy), order[Image.c]);			

		view = new ImageViewer<>(copy, tables, order);




		String[] channelNames = new String[(int) prevIntervalInfo.dimension(Image.c)];
		for(int i = 0; i<channelNames.length; i++)
			channelNames[i] = "Channel "+i;

		String[] names = Arrays.copyOf(channelNames,view.numChannels());
		if(isFlattened)
			names[channelNames.length] = "Preview";
		if(requiresOverlay)
			names[channelNames.length+1] = "Annotations";




		// Now filter steps based on compatibility with image dimensions
		Iterator<List<ImgProcessing>> it = steps.iterator();
		while(it.hasNext()){
			if(!it.next().get(0).condition(prevImgInfo))
				it.remove();
		}


		Wizard w = 
				new Wizard<>(
						new ProcessingPipelineNode<>(
								null,
								0, io, theme, notifier,
								new PreviewState<>(view, channelNames),
								view,
								new PreviewBag<>(prevIntervalInfo, previewInterval, prevIntervalInfo, previewInterval, null, null),
								new LinkedList<>(),
								steps,
								descriptions
								));

		w.addWizardListener((s,o)->{
			if(o == Outcome.CANCELLED)
				steps.forEach(steps->steps.forEach(step->step.stop()));
		});

		Optional<ProcessingStep<T, RandomAccessibleInterval<R>>> pipeline = Wizard.showWizardDialog(w, view.createPanelWithChannelController(names));		

		if(pipeline.isPresent())
			return new Pair<>((ProcessingPipelineNode) w.currentNode(), pipeline.get());
		else
			return null;

	}



}
