package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Dimension;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeListener;

import org.pickcellslab.foundationj.services.simplecharts.DisplayedRangeListener;
import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogram;
import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogramFactory;
import org.pickcellslab.foundationj.services.simplecharts.SimpleSeries;

import com.alee.extended.button.WebSwitch;
import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;

import net.imglib2.histogram.Histogram1d;
import net.imglib2.type.numeric.RealType;

@SuppressWarnings("serial")
public class ChannelControlUI<T extends RealType<T>> extends JPanel implements ChannelListener<T>, DisplayedRangeListener {


	private final RangeSlider slider;
	private final JComboBox<ImgLut<?>> luts;
	private final WebSwitch visibility;
	private final SimpleHistogram H;
	private final ChannelControl<T> model;
	private final ChangeListener contrastControl;
	

	public ChannelControlUI(ChannelControl<T> model, SimpleHistogramFactory histoFctry) {

		//Null check
		assert model != null : "Model is null";

		//Register model
		this.model = model;
		model.addChannelListener(this);

		//Create Chart
		H = histoFctry.createChart();
		H.getModel().addSeries(new HistogramSeries(model.getHistogram(), model.getChannelName()));
		H.setAxisFormat(0, NumberFormat.getIntegerInstance());
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumIntegerDigits(2);
		H.setAxisFormat(1, nf);

		H.showLegend(false);
		H.setAxisLabel(0, "Intensity");
		H.setSeriesColor(0, model.getLut().dominante());

		
		H.addDisplayedRangeListener(this);
		
		// Visibility
		visibility = new WebSwitch(model.isVisible());
		visibility.addActionListener(e->{
			//System.out.println("Visibility Action received");
			if(model.isVisible()!=visibility.isSelected())
				model.setVisible(visibility.isSelected());
		});

		//Create Lut choice		
		luts = new JComboBox<>(model.getAvailableLuts());
		luts.setSelectedItem(model.getLut());
		//luts.setRenderer(new ImgLutRenderer()); 
		luts.addActionListener((e)-> {
			//System.out.println("Luts Action received");
			if(model.getLut() != luts.getSelectedItem()){
				model.setImgLut((ImgLut<?>) luts.getSelectedItem());				
				H.setSeriesColor(0, model.getLut().dominante());
			}
		});

		//Create Contrast Controller
		slider = new RangeSlider((int)model.getGlobalMinIntensity(), (int)model.getGlobalMaxIntensity());
		final int[] initContrast = model.getCurrentContrast();
		slider.setValue(initContrast[0]);	slider.setUpperValue(initContrast[1]);
		H.setDisplayedAxisRange(0, slider.getValue(), slider.getUpperValue());
		contrastControl = l->{
			//System.out.println("Contrast Action received");
			if(slider.getValue()== slider.getUpperValue()){
				if(slider.getUpperValue()<slider.getMaximum()){
					slider.setUpperValue(slider.getValue()+1);
				}
				else
					slider.setValue(slider.getValue()-1);
				return;
			}

			int[] modelContrast = model.getCurrentContrast();
			if(modelContrast[0]!= slider.getValue() || modelContrast[1]!=slider.getUpperValue()){
				model.setContrast(slider.getValue(), slider.getUpperValue());
				H.setDisplayedAxisRange(0, slider.getValue(), slider.getUpperValue());
			}			
		};
		slider.addChangeListener(contrastControl);
		slider.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

		//Layout Components
		this.setLayout(new VerticalFlowLayout());
		H.getScene().setPreferredSize(new Dimension(250,250));
		this.add(H.getScene());
		GroupPanel gp = new GroupPanel(10, visibility, new JLabel("LUT: "), luts);
		gp.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		this.add(gp);
		this.add(slider);

	}

	public ChannelControl<T> getModel(){
		return model;
	}

	
	
	public void addDisplayedRangeListener(DisplayedRangeListener l) {
		H.addDisplayedRangeListener(l);
	}


	
	public void removeDisplayedRangeListener(DisplayedRangeListener l) {
		H.removeDisplayedRangeListener(l);
	}


	@Override
	public void rangeChanged(SimpleHistogram h) {
		slider.setValue((int) h.getDisplayedMinimumValue());
		slider.setUpperValue((int) h.getDisplayedMaximumValue());
	}


	@Override
	public void lutChanged(ChannelControl<T> source) {
		luts.setSelectedItem(source.getLut());
	}

	@Override
	public void visibilityChanged(ChannelControl<T> source) {
		visibility.setSelected(source.isVisible());
	}

	@Override
	public void histogramChanged(ChannelControl<T> source) {
		//Update histogram
		if(H.getModel().getSeries().size()>0)
			H.getModel().removeSeries(H.getModel().getSeries().get(0).id());
		H.getModel().addSeries(new HistogramSeries(source.getHistogram(), source.getChannelName()));
		//update slider
		slider.getModel().setMaximum((int) model.getGlobalMaxIntensity());
		slider.getModel().setMinimum((int) model.getGlobalMinIntensity());
		int[] modelContrast = model.getCurrentContrast();
		slider.setValue(modelContrast[0]);
		slider.setUpperValue(modelContrast[1]);
		//H.setDisplayedAxisRange(0, slider.getValue(), slider.getUpperValue());

	}

	@Override
	public void contrastChanged(ChannelControl<T> source) {
		contrastControl.stateChanged(null);
	}


	@Override
	public void nameHasChanged(ChannelControl<T> source) {/*we don't care*/}



	private class HistogramSeries implements SimpleSeries<double[]>{

		private Histogram1d<T> h;
		private final T type;
		private final String name;

		public HistogramSeries(Histogram1d<T> h, String name) {
			this.h = h;
			this.name = name;
			type = model.getChannelType();//.createVariable();
		}

		@Override
		public String id() {
			return name;
		}

		@Override
		public void addObservation(double[] value) {/*Unsupported*/}

		@Override
		public int numValues() {
			//System.out.println("Histogram series : bins = "+h.getBinCount());
			return (int)h.getBinCount();
		}

		@Override
		public double[] getValue(int i) {
			h.getLowerBound(i, type);
			final double l = type.getRealDouble();
			h.getUpperBound(i, type);
			final double u = type.getRealDouble();
			double[] result = new double[]{l, u, h.relativeFrequency(i, true)};//frequency(i)};
			//System.out.println("Histogram series : value = "+Arrays.toString(result));
			return result;
		}

	}



}
