package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;

class FileInputManager<I,O extends Type<O>> implements ProcessingRunner<I,O, FileInput<I>>{

	@Override
	public ExtendedImageInfo<O> run(ProcessingStep<I, RandomAccessibleInterval<O>> pipeline, ImgIO io,  FileInput<I> input, O type,
			File outputFolder, String outputName, ImgWriteManagerFactory<O> fctry) throws IOException {
		
		final MinimalImageInfo info = input.getInfo();
		final ImgWriteManager<O> wm = fctry.createManager(io, info, type, outputFolder, outputName);			
		for(long t=0; t<info.dimension(Image.t); t++){
			wm.writeFrame(pipeline.apply(input.getImg(), info, t), t);
		}
		wm.close();

		return new ExtendedImageInfo<>(info, outputFolder, outputName, wm.getExt(), type);
		
	}

			

}
