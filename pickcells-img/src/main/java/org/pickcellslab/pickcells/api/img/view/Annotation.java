package org.pickcellslab.pickcells.api.img.view;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyArrayDimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyDimension;

/**
 * 
 * Base interface for annotations drawn over an image
 * 
 * @author Guillaume Blin
 *
 */
public interface Annotation  {

	/** 
	 * A String describing what this annotation represents. For example {@link DataItem#declaredType} 
	 * if this annotation represents a {@link DataItem}
	 * @return A String describing what this annotation represents. 
	 */
	public String representedType();	
	
	public Stream<AKey<?>> properties();
	
	public <E> E getProperty(AKey<E> k);
	
	public AnnotationStatus status();
	
	
	public static <E> Dimension<Annotation, E> createDimension(AKey<E> k, E prototype){
		if(prototype.getClass().isArray()) {
			return new AnnotationArrayDimension<>(k, Array.getLength(prototype), k.name);
		}
		else
			return new AnnotationDimension<>(k, k.name);		
	}



	public static <E> List<Dimension<Annotation, ?>> createDecomposedDimension(AKey<E> k, E prototype){
		assert k != null : "key is null";
		assert prototype != null : "prototype is null";
		final List<Dimension<Annotation, ?>> list = new ArrayList<>();
		if(prototype.getClass().isArray()){
			final int length = Array.getLength(prototype);
			for(int i = 0; i<length; i++){
				list.add(new AnnotationDimension(k, i, k.name+" "+i));
			}
		}
		else{
			list.add(createDimension(k, prototype));
		}
		return list;			
	}
	
	
}
