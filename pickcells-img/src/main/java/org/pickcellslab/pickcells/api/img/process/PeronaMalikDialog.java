package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;

@SuppressWarnings("serial")
class PeronaMalikDialog extends ValidityTogglePanel {



	private double delta, kappa;
	private int iterations;


	/**
	 * Creates the dialog with the given default values
	 * @param d The integration constant for the numerical integration scheme. Typically less than 1
	 * @param k The constant for the diffusion function that sets its gradient threshold
	 * @param itr The number of times the function is applied to the image
	 */
	PeronaMalikDialog(double d, double k, int itr) {

		delta = d;
		kappa = k;
		iterations = itr;
		
		//Controlers
		JLabel lblDelta = new JLabel("Delta");
		lblDelta.setToolTipText("The constant for the numerical integration scheme. Typically less than 1");
		
		SpinnerNumberModel dModel = new SpinnerNumberModel(delta, 0, 5, 0.1);
		dModel.addChangeListener(l->setDelta(dModel.getNumber().doubleValue()));
		JSpinner dField = new JSpinner(dModel);


		JLabel lblKappa = new JLabel("Kappa");
		lblKappa.setToolTipText("Controls the rate of the diffusion \n "
				+ "Soft threshold between what is attributed to noise and what is attributed to edges");

		SpinnerNumberModel kModel = new SpinnerNumberModel(kappa, 1, 150, 5);
		dModel.addChangeListener(l->setKappa(kModel.getNumber().doubleValue()));
		JSpinner kField = new JSpinner(kModel);


		JLabel lblIterations = new JLabel("Iterations");
		lblIterations.setToolTipText("The number of times the function is applied to the image");

		SpinnerNumberModel iModel = new SpinnerNumberModel(itr, 1, 50, 1);
		dModel.addChangeListener(l->setItr(iModel.getNumber().intValue()));
		JSpinner iField = new JSpinner(iModel);


		

		//Layout
		//-----------------------------------------------------------------------

		setBounds(100, 100, 195, 156);
		setBorder(new EmptyBorder(5, 5, 5, 5));

		GroupLayout gl_contentPanel = new GroupLayout(this);
		gl_contentPanel.setHorizontalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addComponent(lblDelta)
										.addGap(19))
										.addComponent(lblKappa)
										.addComponent(lblIterations))
										.addGap(34)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
												.addComponent(iField, GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
												.addComponent(dField, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
												.addComponent(kField, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
												.addContainerGap())
				);
		gl_contentPanel.setVerticalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addGap(17)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblDelta)
								.addComponent(dField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
										.addComponent(kField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblKappa))
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
												.addComponent(lblIterations)
												.addComponent(iField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
												.addContainerGap(35, Short.MAX_VALUE))
				);
		setLayout(gl_contentPanel);
		
	}
	
	
	private void setItr(int intValue) {
		iterations = intValue;
	}


	private void setKappa(double doubleValue) {
		kappa = doubleValue;
	}


	private void setDelta(double doubleValue) {
		delta = doubleValue;
	}


	public double delta(){
		return delta;
	}
	
	public double kappa(){
		return kappa;
	}
	
	public int iterations(){
		return iterations;
	}



	@Override
	public boolean validity() {
		return true;
	}
	
}
