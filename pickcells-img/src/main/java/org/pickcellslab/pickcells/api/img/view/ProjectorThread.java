package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.concurrent.RejectedExecutionException;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.imglib2.display.projector.Projector;
import net.imglib2.ui.PainterThread;

public class ProjectorThread extends Thread {

	
	private final Projector projector;
	private boolean pleaseRemap;
	private final PainterThread pt;
	private final List<ChangeListener> lstrs;
	private final ChangeEvent evt;

	ProjectorThread(Projector projector, PainterThread pt, List<ChangeListener> listeners, ChangeEvent evt){
		this.projector = projector;
		this.pt = pt;
		this.lstrs = listeners;
		this.evt = evt;
	}

	
	@Override
	public void run()
	{
		while ( !isInterrupted() ){
			final boolean b;
			synchronized ( this ){
				b = pleaseRemap;
				pleaseRemap = false;
			}
			if ( b )
				try{
					//long before = System.currentTimeMillis();
					projector.map();
					//System.out.println("Mapping for "+getName()+" took "+(System.currentTimeMillis()-before+"ms"));
					//lstrs.forEach(l->l.stateChanged(evt));
					//pt.requestRepaint();
					
				}
				catch ( final RejectedExecutionException e ){
					e.printStackTrace();
				}
			synchronized ( this ){
				try{
					if ( !pleaseRemap )
						wait();
				}
				catch ( final InterruptedException e ){
					e.printStackTrace();
					break;
				}
			}
		}
	}

	
	public void requestRemap(){
		synchronized ( this ){
			pleaseRemap = true;
			notify();
		}
	}
	
	public boolean isComplete() {
		return !pleaseRemap;
	}
	
}
