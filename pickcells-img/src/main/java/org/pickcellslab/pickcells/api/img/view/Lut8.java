package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.util.Random;

import net.imglib2.display.ColorTable8;

public enum Lut8 implements ImgLut<byte[]>{

	Grays{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 0 ].length; i++ ){
				gray[ 0 ][ i ] = ( byte ) i;
				gray[ 1 ][ i ] = ( byte ) i;
				gray[ 2 ][ i ] = ( byte ) i;					
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.black;
		}
		
		
	},
	Red{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 0 ].length; i++ )
				gray[ 0 ][ i ] = ( byte ) i;
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.RED;
		}
	},
	Green{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ )
				gray[ 1 ][ i ] = ( byte ) i;
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.GREEN;
		}
	},
	Blue{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 2 ].length; i++ )
				gray[ 2 ][ i ] = ( byte ) i;
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.BLUE;
		}
	},
	Cyan{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ ){
				gray[ 1 ][ i ] = ( byte ) i;
				gray[ 2 ][ i ] = ( byte ) i;
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.CYAN;
		}
	},
	Yellow{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ ){
				gray[ 0 ][ i ] = ( byte ) i;
				gray[ 1 ][ i ] = ( byte ) i;
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.YELLOW;
		}
	},
	Magenta{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ ){
				gray[ 0 ][ i ] = ( byte ) i;
				gray[ 2 ][ i ] = ( byte ) i;
			}
			return gray;
		}	

		@Override
		public Color dominante(){
			return Color.MAGENTA;
		}
		
	},	
	RANDOM{
		@Override
		public byte[][] data() {
			Random r = new Random();
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for ( int i = 1; i < gray[ 1 ].length; i++ ){
				gray[ 0 ][ i ] = ( byte ) (r.nextInt(254)+1);
				gray[ 1 ][ i ] = ( byte ) (r.nextInt(254)+1);
				gray[ 2 ][ i ] = ( byte ) (r.nextInt(254)+1);
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.BLACK;
		}
	},	

	Colors_16{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];

			byte[] c = new byte[]{0,0,0};			
			for(int i = 0; i<16; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,(byte) 171,(byte) 171};
			for(int i = 16; i<32; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,1,(byte) 224};
			for(int i = 32; i<48; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,110,(byte) 254};
			for(int i = 48; i<64; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,(byte) 171,(byte) 254};
			for(int i = 64; i<80; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,(byte) 224,(byte) 254};
			for(int i = 80; i<96; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,(byte) 254,1};
			for(int i = 96; i<112; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{(byte) 190,(byte) 255,0};
			for(int i = 112; i<128; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{(byte) 255,(byte) 224,0};
			for(int i = 128; i<160; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{(byte) 255,(byte) 141,0};
			for(int i = 160; i<176; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{(byte) 250,94,0};
			for(int i = 176; i<192; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{(byte) 245,0,0};
			for(int i = 192; i<208; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{(byte) 245,0,(byte) 135};
			for(int i = 208; i<223; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{(byte) 222,0,(byte) 222};
			for(int i = 223; i<240; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{(byte) 255,(byte) 255,(byte) 255};
			for(int i = 240; i<256; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			return gray;
		}

		@Override
		public Color dominante(){
			return Color.BLACK;
		}
		
	},



	SPECTRUM{

		public byte[][] data(){
			final byte[][] gray = new byte[ 3 ][ 256 ];
			for (int i = 0; i < 256; i++) {
				final int c = Color.HSBtoRGB(i / 255f, 1f, 1f);
				gray[0][i] =  (byte) ((c)&0xFF);
				gray[1][i]  = (byte) ((c>>8)&0xFF);
				gray[2][i] =  (byte) ((c>>16)&0xFF);
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.ORANGE;
		}

	},



	FIRE{



		public byte[][] data(){


			final int[] r = { 0, 0, 1, 25, 49, 73, 98, 122, 146, 162, 173, 184, 195, 207,
					217, 229, 240, 252, 255, 255, 255, 255, 255, 255, 255, 255,
					255, 255, 255, 255, 255, 255 };
			final int[] g = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 35, 57, 79, 101,
					117, 133, 147, 161, 175, 190, 205, 219, 234, 248, 255, 255,
					255, 255 };
			final int[] b = { 0, 61, 96, 130, 165, 192, 220, 227, 210, 181, 151, 122, 93,
					64, 35, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 35, 98, 160, 223,
					255 };

			byte[][] map = new byte[3][r.length];

			// cast elements
			for (int i = 0; i < r.length; i++) {
				map[0][i] = (byte) r[i];
				map[1][i] = (byte) g[i];
				map[2][i] = (byte) b[i];
			}

			return map;
		}
		
		@Override
		public Color dominante(){
			return Color.BLUE;
		}
	},




	HILO{
		@Override
		public byte[][] data() {
			final byte[][] gray = new byte[ 3 ][ 256 ];
			gray[2][0] = ( byte ) 255;	gray[0][gray[ 0 ].length-1] = ( byte ) 255;
			for ( int i = 1; i < gray[ 0 ].length-1; i++ ){
				gray[ 0 ][ i ] = ( byte ) i;
				gray[ 1 ][ i ] = ( byte ) i;
				gray[ 2 ][ i ] = ( byte ) i;
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.BLACK;
		}
	};



	public  ColorTable8 table(){
		return new ColorTable8(data());			
	}








}
