package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogramFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.view.DisplayPositionListener.DisplayPosition;

import com.alee.extended.panel.GroupPanel;
import com.alee.extended.panel.GroupingType;
import com.alee.extended.panel.WebCollapsiblePane;
import com.alee.laf.label.WebLabel;
import com.alee.managers.tooltip.TooltipManager;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.realtransform.AffineGet;
import net.imglib2.realtransform.AffineTransform2D;
import net.imglib2.type.numeric.RealType;
import net.imglib2.ui.AffineTransformType2D;
import net.imglib2.ui.PainterThread;
import net.imglib2.ui.PainterThread.Paintable;
import net.imglib2.ui.TransformListener;

@SuppressWarnings("serial")
class ImageViewer2 extends JPanel implements Paintable, TransformListener<AffineTransform2D>, ChangeListener, ImageDisplay{

	private final DynamicInteractiveDisplayCanvasComponent< AffineTransform2D > canvas;
	private final DefaultMouseDisplayEventModel mouseModel;
	private final KeyStrokeModel ksModel;

	private final List<Overlay2DTransformHandler<?>> ovlHandlers = new ArrayList<>();
	private final List<Annotation2DTransformHandler> annotationMgrs = new ArrayList<>();

	private AffineTransform2D viewerTransform;
	private AffineTransformType2D transformType;

	private JSlider sliderZ;
	private JSlider sliderT;



	// Display Listener
	private final List<DisplayPositionListener> dpl = new ArrayList<>();
	private final List<DisplayContentListener> dcl = new ArrayList<>();


	// UI
	private GroupPanel slidersPanel;
	private WebCollapsiblePane channelPane;
	private ImageDisplayControlsPanel channelsUI;

	private final PainterThread painterThread = new PainterThread(this);

	private final SimpleHistogramFactory hFctry;
	private final UITheme theme;
	private final NotificationFactory notifier;

	/**
	 * @param interval The Interval to be displayed
	 * @param tables The Lookup tables to render each channel
	 * @throws IllegalArgumentException if the length of tables is smaller than the number of channels
	 */
	ImageViewer2(SimpleHistogramFactory hFctry, UITheme theme, NotificationFactory notifier){

		//Setup environment
		//this.setDoubleBuffered(true);

		//Canvas with dynamic transform handler
		canvas = new DynamicInteractiveDisplayCanvasComponent< AffineTransform2D >( 500, 500, DynamicTransformEventHandler2D.factory());
		canvas.setDoubleBuffered(true);

		//KeyStrokeModel -> NB must be first as called in constructor of DefaultMouseDisplayEventModel
		ksModel = new DefaultKeyStrokeModel(this);

		//Mouse model
		mouseModel = new DefaultMouseDisplayEventModel(this, canvas.getTransformEventHandler());


		// View Transform
		transformType =  AffineTransformType2D.instance;
		viewerTransform = transformType.createTransform();    


		// Fctry for histogram
		this.hFctry = hFctry;
		
		this.theme = theme;
		this.notifier = notifier;
		
		
		// ---------------------------------------------------------------------------------------------------------------------------------
		// Setting up listeners

		canvas.addTransformListener( this );


		






		// Key Binding to unzoom and recenter the image
		canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "Recenter");
		canvas.getActionMap().put("Recenter", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {	
				if(ovlHandlers.size()!=0){
					MinimalImageInfo info = ovlHandlers.get(0).getMinimalInfo();
					((DynamicTransformEventHandler2D) canvas.getTransformEventHandler()).centerImage(info.dimension(Image.x), info.dimension(Image.y));
				}
			}			
		});



		//Layout components

		setLayout(new BorderLayout(0, 0));

		add(canvas, BorderLayout.CENTER);

		slidersPanel = new GroupPanel(false);
		add(slidersPanel, BorderLayout.SOUTH);


		painterThread.start();

	}



	@Override
	public UITheme getIconTheme() {
		return theme;
	};
	
	
	
	@Override
	public NotificationFactory getNotificationFactory() {
		return notifier;
	}
	
	

	@Override
	public int numImages() {
		return ovlHandlers.size();
	}






	public <T extends RealType<T>> void addImage(MinimalImageInfo info, RandomAccessibleInterval<T> img){

		// Create the OverlayHandler
		final Overlay2DTransformHandler<T> handler = new Overlay2DTransformHandler<>(img, info, painterThread);

		// ------------------------------------------------------------------------------------------------------------------------------------
		//Adjust contrast
		for(int i = 0; i<info.dimension(Image.c); i++){			
			handler.getChannel(i).autoContrast();
			handler.getChannel(i).setVisible(true);	
		}

		if(sliderZ==null)
			this.buildZSlider(info);
		if(sliderT==null)
			this.buildTSlider(info);

		checkControlsUI();
		channelsUI.addHandler(handler);


		ovlHandlers.add(handler);
		if(ovlHandlers.size()==1){
			canvas.addMouseListener(mouseModel);
			canvas.addMouseMotionListener(mouseModel);
		}

		// Add renderers to canvas
		for(int c = 0; c<info.dimension(Image.c); c++)
			canvas.addOverlayRenderer(handler.getBlender(c));

		if(ovlHandlers.size()==1){
			((DynamicTransformEventHandler2D) canvas.getTransformEventHandler()).centerImage(info.dimension(Image.x), info.dimension(Image.y));
			if(sliderZ!=null)
				sliderZ.setValue((int)info.dimension(Image.z)/2);
		}
		else{
			//handler.setAlpha(alpha);
			this.repaint();
		}

		handler.addChangeListener(this);
		dcl.forEach(l->l.imageAdded(this));

	}






	@Override
	public void removeImage(int index) {
		Overlay2DTransformHandler<?> h = ovlHandlers.remove(index);
		for(int c = 0; c<h.getMinimalInfo().dimension(Image.c); c++)
			canvas.removeOverlayRenderer(h.getBlender(c));		
		channelsUI.removeHandler(h);
		if(ovlHandlers.size()==0){
			canvas.removeMouseListener(mouseModel);
			canvas.removeMouseMotionListener(mouseModel);
		}
		dcl.forEach(l->l.imageRemoved(this, index));
	}









	@Override
	public int numAnnotationLayers() {
		return annotationMgrs.size();
	}









	@Override
	public void addAnnotationLayer(String name, AnnotationManager mgr) {

		final AnnotationAppearanceControlManager apps = new DefaultAnnotationAppearanceControlManager(mgr, hFctry);
		final Annotation2DTransformHandler annotationHandler = new Annotation2DTransformHandler(apps, painterThread);	

		annotationHandler.addChangeListener(this);

		this.addDisplayPositionListener(mgr);

		checkControlsUI();
		channelsUI.addAnnotationControl(apps);

		canvas.addAnnotationRenderer(annotationHandler.getRenderer());
		annotationMgrs.add(annotationHandler);

		dcl.forEach(l->l.annotationAdded(this));
		mgr.positionHasChanged(this, DisplayPosition.FRAME);

	}









	@Override
	public void removeAnnotationLayer(int index) {
		final Annotation2DTransformHandler annotationHandler = annotationMgrs.remove(index);
		channelsUI.removeAnnotationControl(annotationHandler.getAppearanceControl()); 
		canvas.removeOverlayRenderer(annotationHandler.getRenderer());
		this.removeDisplayPositionListener(annotationHandler.getManager());
		this.repaint();
		dcl.forEach(l->l.annotationRemoved(this, index));
	}





	@Override
	public AnnotationManager getAnnotationManager(int index) {
		return annotationMgrs.get(index).getManager();
	}







	@Override
	public JComponent getImageView() {
		return canvas;
	}




	@Override
	public KeyStrokeModel getKeyStrokeModel() {
		return ksModel;
	}










	private void checkControlsUI() {

		if(channelPane == null){
			channelsUI = new ImageDisplayControlsPanel(hFctry);
			channelPane = new WebCollapsiblePane("Image Controllers", channelsUI);
			channelPane.setTitlePanePostion ( SwingConstants.LEFT );
			channelPane.setTitleAlignment(WebLabel.CENTER);
			this.add(channelPane,BorderLayout.EAST);
		}		

	}







	private void buildZSlider(MinimalImageInfo info){

		if(info.dimension(Image.z)==1)
			return;

		//Slider for adjusting the plane
		sliderZ = new JSlider();
		sliderZ.setMinimum(0);
		sliderZ.setMaximum((int) info.dimension(Image.z)-1);

		sliderZ.addChangeListener(l->{

			SwingUtilities.invokeLater(()->{
				TooltipManager.setTooltip(sliderZ, "Current Z Slice = "+ sliderZ.getValue());
				ovlHandlers.forEach(h->h.setPosition(Image.z, sliderZ.getValue()));
			});
			new ArrayList<>(dpl).forEach(dl->dl.positionHasChanged(this, DisplayPosition.SLICE));

		});


		// Key Binding to navigate slices and time frames
		canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("UP"), "UP");
		canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("DOWN"), "DOWN");

		canvas.getActionMap().put("UP", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				sliderZ.setValue(sliderZ.getValue()+1);
			}			
		});

		canvas.getActionMap().put("DOWN", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				sliderZ.setValue(sliderZ.getValue()-1);
			}			
		});

		JLabel lblZ = new JLabel("  Z  ");
		JLabel lblNewLabel = new JLabel("     ");
		GroupPanel zPanel = new GroupPanel(GroupingType.fillMiddle, lblZ, sliderZ, lblNewLabel);
		slidersPanel.add(zPanel);

	}




	private void buildTSlider(MinimalImageInfo info){


		if(info.dimension(Image.t)==1)
			return;

		//Slider for adjusting time frames
		sliderT = new JSlider();
		sliderT.setMinimum(0);
		sliderT.setMaximum((int) info.dimension(Image.t)-1);


		sliderT.addChangeListener(l->{			
			SwingUtilities.invokeLater(()->{
				TooltipManager.setTooltip(sliderT, "Current Z Slice = "+ sliderT.getValue());
				ovlHandlers.forEach(h->h.setPosition(Image.t, sliderT.getValue()));
			});
			new ArrayList<>(dpl).forEach(dl->dl.positionHasChanged(this, DisplayPosition.FRAME));
		});


		// Key Binding to navigate slices and time frames
		canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("RIGHT"), "RIGHT");
		canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("LEFT"), "LEFT");


		canvas.getActionMap().put("RIGHT", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				sliderT.setValue(sliderT.getValue()+1);
			}			
		});

		canvas.getActionMap().put("LEFT", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {
				sliderT.setValue(sliderT.getValue()-1);
			}			
		});


		JLabel lblT = new JLabel("  T  ");
		JLabel lblNewLabel = new JLabel("     ");
		GroupPanel tPanel = new GroupPanel(GroupingType.fillMiddle, lblT, sliderT, lblNewLabel);
		slidersPanel.add(tPanel);

	}







	//Internal display events methods
	//--------------------------------------------------------------------------------------------------------------------

	@Override
	public void paint(){		
		ovlHandlers.forEach(h->h.paint(viewerTransform));
		annotationMgrs.forEach(h->h.paint(viewerTransform));
		canvas.repaint();
	}





	@Override
	public void transformChanged(AffineTransform2D transform) {
		transformType.set( viewerTransform, transform );   
		painterThread.requestRepaint();

	}



	//Called by handlers when their state changes
	@Override
	public void stateChanged(ChangeEvent e) {		
		painterThread.requestRepaint();
	}




	//Listener registration
	//--------------------------------------------------------------------------------------------------------------------


	/**
	 * Registers an {@link EventListener} on the canvas displaying the image
	 * @param lst The EventListener to register
	 */
	public void addViewListener(EventListener lst){
		canvas.addHandler(lst);
	}



	/**
	 * Regmove the {@link EventListener} from the canvas displaying the image
	 * @param lst The EventListener to remove
	 */
	public void removeViewListener(EventListener lst){
		canvas.removeHandler(lst);
	}






	@Override
	public AffineGet getCurrentTransform() {
		return viewerTransform;
	}



	public void addDisplayPositionListener(DisplayPositionListener l){
		if(l!=null)
			this.dpl.add(l);
	}

	public void removeDisplayPositionListener(DisplayPositionListener l){
		this.dpl.remove(l);
	}




	// Access to the display
	//-------------------------------------------------------------------------------------------------------------------


	/**
	 * @return The JComponent displaying the image
	 */
	public JComponent getImageDisplay(){
		return canvas;
	}













	@Override
	public <T extends RealType<T>> ChannelControl<T> getChannel(int imgIndex, int channel) {
		return (ChannelControl)this.ovlHandlers.get(imgIndex).getChannel(channel);
	}





	@Override
	public int currentZSlice() {
		if(sliderZ == null)
			return 0;
		return sliderZ.getValue();
	}





	@Override
	public int currentTimeFrame() {
		if(sliderT == null)
			return 0;
		return sliderT.getValue();
	}





	@Override
	public void setCurrentZSlice(int z) {
		if(sliderZ == null)
			sliderZ.setValue(z);
	}





	@Override
	public void setcurrentTimeFrame(int t) {
		if(sliderT == null)
			sliderT.setValue(t);
	}


	@Override
	public <T extends RealType<T>> ImageDisplayModel<T> getImageModel(int index) {
		return (ImageDisplayModel<T>) ovlHandlers.get(index);
	}




	@Override
	public MouseDisplayEventModel getMouseModel() {
		return mouseModel;
	}



	@Override
	public void setTextOverlay(String text) {
		if(text==null)	return;
		canvas.setText(text);
		canvas.repaint();
	}



	@Override
	public void refreshDisplay() {
		painterThread.requestRepaint();
	}





	@Override
	public JComponent getView() {
		return this;
	}

	public void setImageCursor(Cursor c){
		canvas.setCursor(c);
	}



	@Override
	public void addDisplayContentListener(DisplayContentListener l) {
		dcl.add(l);
	}





	@Override
	public void removeDisplayContentListener(DisplayContentListener l) {
		dcl.remove(l);
	}




	@Override
	public void addTransformListener(TransformListener<? super AffineGet> l) {
		canvas.addTransformListener((TransformListener)l);
	}




	@Override
	public void removeTransformListener(TransformListener<? super AffineGet> l) {
		canvas.removeTransformListener((TransformListener)l);
	}





}
