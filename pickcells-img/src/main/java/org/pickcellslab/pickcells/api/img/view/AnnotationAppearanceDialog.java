package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.Objects;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.ui.utils.CardPanel;

import com.alee.laf.colorchooser.WebColorChooser;

@SuppressWarnings("serial")
public class AnnotationAppearanceDialog extends JDialog{

	

	private final JComboBox<String> series;
	private final CardPanel<String, WebColorChooser> card;


	
	public AnnotationAppearanceDialog(AnnotationAppearance app) {
		
		Objects.requireNonNull(app, "app is null");
		
		// Build the dialog
		series = new JComboBox<>();
		app.registeredSeries().forEach(s->series.addItem(s));
				
		
		card = new CardPanel<>((s)-> new WebColorChooser(app.getColor(s)));
		card.show((String) series.getSelectedItem());
		
		
		series.addActionListener(l->{
			card.show((String) series.getSelectedItem());
		});
		
		
		JPanel okCancelPanel = new JPanel();
		
		JButton okBtn = new JButton("Apply");
		okBtn.addActionListener(l->{
			app.setColor(card.currentKey(), card.current().getColor());
		});
		
		JButton cancelBtn = new JButton("Done");
		cancelBtn.addActionListener(l->{
			this.dispose();
		});
		
		okCancelPanel.add(okBtn);
		okCancelPanel.add(cancelBtn);
		
		
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(series,BorderLayout.NORTH);				
		getContentPane().add(card, BorderLayout.CENTER);
		getContentPane().add(okCancelPanel, BorderLayout.SOUTH);
		
	}
	
	
	
}
