package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgWriter;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

class MultiChannelImgWriteManager<T extends NativeType<T> & RealType<T>> implements ImgWriteManager<T>{

	final MinimalImageInfo frameInfo;
	final ImgWriter<T> w;
	final String ext;

	MultiChannelImgWriteManager(ImgIO io, MinimalImageInfo info, T type, File outputFolder, String outputName) throws IOException{
		frameInfo = info.removeDimension(Image.t);
		ext = io.standard(info.imageDimensions(), type.getBitsPerPixel());
		w = io.createWriter(info, type, outputFolder.getPath()+File.separator+outputName+ext);
		
	}

	@Override
	public void writeFrame(RandomAccessibleInterval<T> frame, long t) throws IOException {
		// Decompose channels
		for(int c = 0; c<frameInfo.dimension(Image.c); c++)
			w.writeChannelFrame(Views.hyperSlice(frame, frameInfo.index(Image.c), c), c, (int) t);
	}

	@Override
	public void close() {
		w.close();
	}

	@Override
	public String getExt() {
		return ext;
	}

}
