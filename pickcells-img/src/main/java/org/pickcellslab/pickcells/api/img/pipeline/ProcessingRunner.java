package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;

import org.pickcellslab.pickcells.api.img.io.ImgIO;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;

/**
 * A {@link ProcessingRunner} takes care of running a pipeline on a given {@link ProcessingInput} as well as writing
 * the result to file as the process is performed. 
 * <br><b>NB: </b> Saving is performed alongside the image processing procedure to allow larger than memory images to be handled
 * @author Guillaume Blin
 *
 * @param <I> The type of the input {@link RandomAccessibleInterval} accepted by the pipeline
 * @param <O> The type of the result {@link RandomAccessibleInterval} generated by the pipeline
 * @param <P> The type of {@link ProcessingInput} accepted by this {@link ProcessingRunner}
 */
public interface ProcessingRunner<I,O extends Type<O>, P extends ProcessingInput<I>>{
	/**
	 * Runs the pipeline on the given input
	 * @param pipeline The {@link ProcessingStep} to be run
	 * @param io An {@link ImgIO} instance used for saving the result image to disk
	 * @param input The input
	 * @param type The {@link Type} of the result {@link RandomAccessibleInterval} generated by the pipeline
	 * @param outputFolder The directory where the result should be saved 
	 * @param outputName The name of the result image
	 * @param fctry An {@link ImgWriteManagerFactory} responsible for creating {@link ImgWriteManager}
	 * @return Information on the result of the process in the form of an {@link ExtendedImageInfo}
	 * @throws IOException
	 */
	ExtendedImageInfo<O> run(ProcessingStep<I, RandomAccessibleInterval<O>> pipeline, ImgIO io, P input, O type, File outputFolder, String outputName, ImgWriteManagerFactory<O> fctry) throws IOException;
}
