package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.swing.JTextArea;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.converter.Converter;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;
import net.imglib2.view.composite.CompositeIntervalView;
import net.imglib2.view.composite.GenericComposite;


public class MultiThresholding<T extends NativeType<T> & RealType<T>> implements ImgProcessing<T,BitType> {



	private ImageViewState<?> state;
	private PreviewBag<T> input;
	private final List<Double> min = new ArrayList<>();

	private final ImgIO io;

	public MultiThresholding(ImgIO io) {
		this.io = io;
	}


	@Override
	public String name() {
		return "Thresholding";
	}



	@Override
	public String description() {
		return "Combines the thresholds set manualy on each channel into one binary image";
	}

	@Override
	public boolean repeatable() {
		return false;
	}




	@SuppressWarnings("serial")
	@Override
	public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<T> input) {

		this.state = state;
		this.input = input;



		return new ValidityTogglePanel(){

			{			
				setLayout(new BorderLayout());
				JTextArea text = new JTextArea();
				text.setEditable(false);
				text.setLineWrap(true);
				text.setText("\nUse the Brightness and Contrast dialog to adjust the threshold for each channel.\n\n"
						+ "Tick or untick channels which should be taken into account or not during the thresholding operation.");
				add(text, BorderLayout.CENTER);
			}

			@Override
			public boolean validity() {
				return true;
			}

		};
	}


	@Override
	public void changeInput(PreviewBag<T> current) {
		this.input = current;			
	}



	@Override
	public RandomAccessibleInterval<BitType> validate() {

		min.clear();

		for(int c = 0; c<state.channelNames().length; c++)
			if(state.isVisible(c))
				min.add(state.getCurrentMinIntensity(c));
			else
				min.add(state.getType().getMaxValue());


		return run(io, input.getInput(), min, input.getInputInfo().index(Image.c));

	}



	private static <V extends RealType<V>> RandomAccessibleInterval<BitType> run(ImgIO io, RandomAccessibleInterval<V> input, List<Double> min, int cDim){

		if(cDim!=-1){
			CompositeIntervalView<V, ? extends GenericComposite<V>> source = Views.collapse(Views.permute(input, cDim, input.numDimensions()-1));
			Img<BitType> target = io.createImg(source,new BitType());

			double[] minT = new double[min.size()];
			for(int i = 0; i<min.size(); i++)
				minT[i] = min.get(i);
			Operations.convert(source, target, new MT_Converter<>(minT));
			return target;
		}
		else{
			Img<BitType> target = io.createImg(input,new BitType());
			Operations.convert(input, target, new Thresholder<>(min.get(0), Double.MAX_VALUE));
			return target;
		}

	}




	@Override
	public Function<ImageBag<T>, RandomAccessibleInterval<BitType>> toPipe() {
		return (bag) -> MultiThresholding.run(io, bag.getInputFrame(0), min, bag.getInputFrameInfo().index(Image.c));
	}


	@Override
	public DimensionalityEffect dimensionalityEffect() {
		return DimensionalityEffect.CHANNELS_TO_FLAT;
	}








	private static class MT_Converter<C extends GenericComposite<T>, T extends RealType<T>> implements Converter<C,BitType>{

		final double[] min;

		MT_Converter(double[] minT){
			min = minT;
		}

		@Override
		public void convert(C input, BitType output) {
			boolean above = false;
			for(int i = 0; i<min.length; i++)
				if(input.get(i).getRealDouble()>min[i]){
					above = true; break;
				}
			if(above)
				output.setOne();
			else output.setZero();
		}

	}



	@Override
	public boolean requiresAnnotationChannel() {
		return false;
	}



	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}



	@Override
	public boolean requiresOriginal() {
		return false;
	}



	@Override
	public boolean requiresFlat() {
		return false;
	}



	@Override
	public BitType outputType(T inputType) {
		return new BitType();
	}






}
