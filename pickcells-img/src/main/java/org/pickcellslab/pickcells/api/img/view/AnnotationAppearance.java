package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import net.imglib2.display.ColorTable;
import net.imglib2.type.numeric.ARGBType;

/**
 * This {@link ColorTable} performs a mapping between a gray value in the view (unique for each object id) and a {@link Color} for representation
 * by a renderer.
 * 
 * @author Guillaume Blin
 *
 */
public class  AnnotationAppearance implements ColorTable {

	private final static int[] cols = new int[]{
			ARGBType.rgba(0, 0, 0, 0), 			
			ARGBType.rgba(Color.LIGHT_GRAY.getRed(), Color.LIGHT_GRAY.getGreen(), Color.LIGHT_GRAY.getBlue(), 255),						
			ARGBType.rgba(Color.YELLOW.getRed(), Color.YELLOW.getGreen(), 0, 255),
			ARGBType.rgba(255, 0, 0, 255),
			ARGBType.rgba(Color.BLUE.getRed(), Color.BLUE.getGreen(), Color.BLUE.getBlue(), 255),
			ARGBType.rgba(Color.ORANGE.getRed(), Color.ORANGE.getGreen(), Color.ORANGE.getBlue(), 255),
			ARGBType.rgba(Color.WHITE.getRed(), Color.WHITE.getGreen(), Color.WHITE.getBlue(), 255),			
			ARGBType.rgba(Color.GRAY.getRed(), Color.GRAY.getGreen(), Color.GRAY.getBlue(), 255),
			ARGBType.rgba(Color.CYAN.getRed(), Color.CYAN.getGreen(), Color.CYAN.getBlue(), 255),			
			ARGBType.rgba(Color.MAGENTA.getRed(), Color.MAGENTA.getGreen(), Color.MAGENTA.getBlue(), 255),
			ARGBType.rgba(Color.PINK.getRed(), Color.PINK.getGreen(), Color.PINK.getBlue(), 255),
			ARGBType.rgba(Color.GREEN.getRed(), Color.GREEN.getGreen(), Color.GREEN.getBlue(), 255) 
	};

	
	private final List<AnnotationAppearanceListener> lstrs = new ArrayList<>();
	private final List<Integer> colors = new CopyOnWriteArrayList<>();
	private final Map<String, Integer> ids = new ConcurrentHashMap<>();


	public AnnotationAppearance() {
		colors.add(cols[0]);
	}

	
	/**
	 * @return A List of all the ids that have been registered in this {@link AnnotationAppearance}
	 */
	public List<String> registeredSeries(){
		return new ArrayList<>(ids.keySet());
	}
	
	
	/**
	 * @param s The id of interest
	 * @return The actual integer value found in voxels of the view
	 */
	public synchronized Integer getAppearance(String s) {
		Integer i = ids.get(s);		
		if(i == null){		
			i = colors.size();
			ids.put(s,i);
			colors.add(getDefault(i));
		}			
		return i;
	}




	private int getDefault(int newID) {
		return cols[newID%cols.length];
	}


	/**
	 * Sets the specified {@link Color} for the given id
	 * @param id A registered id, if unknown, nothing will happen
	 * @param c The new Color
	 */
	public void setColor(String id, Color c){
		Integer v = ids.get(id);
		if(v!=null && c!=null){
			colors.set(v, c.getRGB());
			lstrs.forEach(l->l.appearanceChanged(id, c));
		}
	}
	
	
	public void createSeriesSilent(String id, int rgb){		
		Integer imgValue =  getAppearance(id);
		colors.set(imgValue, rgb);
	}
	
	
	
	/**
	 * @param seriesID
	 * @return The currently assigned Color for the given seriesID.
	 */
	public Color getColor(String seriesID) {
		int i = colors.get(ids.get(seriesID));
		return new Color(ARGBType.red(i), ARGBType.green(i), ARGBType.blue(i));
	}
	


	/**
	 * Clears all registered ids and colors.
	 */
	public void reset(){
		colors.clear();
		colors.add(cols[0]);
		ids.clear();
	}




	@Override
	public int lookupARGB(double min, double max, double value) {
		int index = (int) value;
		if(value>=colors.size())
			return cols[0];
		return colors.get(index);
	}

	@Override
	public int getComponentCount() {
		return colors.size();
	}

	@Override
	public int getLength() {
		return 1;
	}

	@Override
	public int get(int comp, int bin) {		
		// System.out.println("get() "+colors.get(comp));
		return colors.get(comp);
	}

	@Override
	public int getResampled(int comp, int bins, int bin) {
		final int newBin = getLength() * bin / bins ;
		return get( comp, newBin );
	}


	public void addListener(AnnotationAppearanceListener l){
		lstrs.add(l);
	}

	public void removeListener(AnnotationAppearanceListener l){
		lstrs.remove(l);
	}

}
