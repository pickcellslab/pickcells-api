package org.pickcellslab.pickcells.api.img.io;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.Scope;
import org.pickcellslab.foundationj.annotations.ScopePolicy;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.Dimensions;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImg;
import net.imglib2.img.cell.CellImg;
import net.imglib2.type.NativeType;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.RealType;
import net.imglib2.util.Util;

/**
 * This interface is designed to be injected into constructors of classes consuming the
 * pickcells io api. It provides functions such as opening and saving images from/to file,
 * as well as factory method to obtain builders to configure helper dialogs or image information
 * checkers.  
 * 
 * @author Guillaume Blin
 *
 */
@Core
@Scope(name="IMAGE-RESOURCES", parent=Scope.APPLICATION, policy=ScopePolicy.SINGLETON)
public interface ImgIO {

	
	
	/**
	 * @param type
	 * @return An int representing the given Type (i.e same as the one stored in SegmentationResult)
	 */
	public <T extends NativeType<T>> int pxCode(T type);
	
	/**
	 * @return The extension of the preferred file format this {@link ImgIO} uses
	 */
	public <T extends NativeType<T>> String standard(RandomAccessibleInterval<T> img);
	
	/**
	 * @return The extension of the preferred file format this {@link ImgIO} uses given dimensions and bit depth
	 */
	public String standard(long[] dims, int bitDepth);
	
	/**
	 * Opens the specified Image
	 * @param img The {@link Image} to open
	 * @return The {@link Img} corresponding to the provided Image
	 * @throws IOException 
	 */
	public <T extends RealType<T> & NativeType<T>> Img<T> open(Image img) throws IOException;
	
	/**
	 * Opens the specified Image
	 * @param img The {@link LabelsImage} to open
	 * @return The {@link Img} corresponding to the provided LabelsImage
	 * @throws IOException 
	 */
	public <T extends RealType<T> & NativeType<T>> Img<T> open(LabelsImage result) throws IOException;
	
	/**
	 * @param path The full path of the file to open
	 * @return An {@link Img} opened as a {@link CellImg}
	 * @throws IOException
	 */
	public <T extends RealType<T> & NativeType<T>> Img<T> openAsCellImg(String path, T type) throws IOException;

	/**
	 * @param path The full path of the file to open
	 * @return An {@link Img} opened as a {@link ArrayImg}
	 * @throws IOException
	 */
	public <T extends RealType<T> & NativeType<T>> Img<T> openInMemory(String path, T type) throws IOException;
	
	/**
	 * Saves the provided {@link RandomAccessibleInterval} to the provided path.
	 * @param info The Image data object holding the image info to populate the metadata on the file. Note this may be null,
	 * in this case the no additional information than the size of the image will be stored. Note that the default order
	 * of the dimensions is XYCZT.
	 * @param data The actual image
	 * @param path The full path of the file to create or overwrite (The extension will define the file format)
	 */
	public < T extends NativeType< T > > void save(Image info, RandomAccessibleInterval<T> data, String path) throws IOException;
	
	/**
	 * @param info The LabelsImage data object holding the image info to store in the header of the file if possible
	 * @param data The actual image
	 * @param path The full path of the file to create or overwrite (The extension will define the file format)
	 * @throws IOException 
	 */
	public < T extends NativeType< T > > void save(LabelsImage info, RandomAccessibleInterval<T> data, String path) throws IOException;
		
	
	/**
	 * Saves the given RandomAccibleInterval to file
	 * @param info a {@link MinimalImageInfo} for the given data
	 * @param data 
	 * @param dest
	 * @throws IOException
	 */
	public < T extends NativeType< T > > void save(MinimalImageInfo info, RandomAccessibleInterval<T> data, String dest) throws IOException;
	
	/**
	 * Copies and optionally convert image files to a folder destination. The output files will start
	 * with the given name pattern with a number suffix from 0 to images.numImages().
	 * The extension of the output is the one given by {@link #standard()}
	 * @param images An {@link ImgFileList} to process
	 * @param destination The destination folder
	 * @param namePattern The convention to name the copied images (null permitted in which case original names will be used)
	 * @throws IOException
	 */
	public void importImages(ImgFileList images, String destination, String namePattern) throws IOException;	
	
	
	
	
	/**
	 * Creates an {@link ImgFileList} from a List of {@link File}s.
	 * @param files The list of image files on the disk
	 * @return An {@link ImgFileList} handling the given List of files.
	 * @throws IOException If filesare not supported
	 */
	public ImgFileList imgFileListFrom(List<File> files) throws IOException;
	
	
	
	/**
	 * @return A Builder to configure an {@link ImgsChooser} dialog
	 */
	public ImgsChooserBuilder createChooserBuilder();
	
	/**
	 * @return A Builder to configure an {@link ImgsChecker}
	 */
	public ImgsCheckerBuilder createCheckerBuilder();

	
	/**
	 * @return An empty ImgFileList
	 */
	public ImgFileList emptyList();
	
	
	/**
	 * @param file Full path to the image file
	 * @return An {@link ImgWriter} dedicated to edit an existing ImageFile.
	 * @throws IOException
	 */
	public <T extends NativeType<T> & RealType<T>> ImgWriter<T> createWriter(String file) throws IOException;
	
	
	/**
	 * Creates an {@link ImgWriter} dedicated to write an image to a new file. NB: if the file already exists, it will be replaced.
	 * To obtain an {@link ImgWriter} dedicated to edit an existing image file, use {@link #createWriter(String)}
	 * @param info A {@link MinimalImageInfo} to populate metadata.
	 * @param type The {@link Type} of the image to create on disk
	 * @param file Full path to the file to create
	 * @return The dedicated ImgWriter.
	 * @throws IOException
	 */
	public <T extends NativeType<T> & RealType<T>> ImgWriter<T> createWriter(MinimalImageInfo info, T type, String file) throws IOException;
	
	/**
	 * It is advised to use this method call instead of {@link Util#getArrayOrCellImgFactory} to create a novel image.
	 * The {@link ImgIO} module will make sure to create an image that fits into memory.
	 * 
	 * @param dim The {@link Dimensions} of the desired Img.
	 * @param type The {@link Type} of the desired {@link Img}
	 * @return An {@link Img} with the given dimensions and type.
	 * @throws IOException
	 */
	@Deprecated
	public <T extends NativeType<T> & RealType<T>> Img<T> createImg(Dimensions dim, T type);


	/**
	 * Returns a thumbnail for the given {@link Image} for the given (xy or xyz) min and max bounding box and at the gievn time frame position
	 * @param img
	 * @param min
	 * @param max
	 * @param timeFrame
	 * @return
	 * @throws IOException
	 */
	public ImageIcon thumbnail(Image img, long[] min, long[] max, int timeFrame) throws IOException;

		
}
