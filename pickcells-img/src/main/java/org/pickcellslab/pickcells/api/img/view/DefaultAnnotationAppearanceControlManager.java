package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogramFactory;

public class DefaultAnnotationAppearanceControlManager implements AnnotationAppearanceControlManager{

	private final List<AppearanceControlChangeListener> lstrs = new ArrayList<>();
	private final AnnotationAppearanceControl[] controls;
	private final AnnotationManager mgr;
	
	private int active = 0;
	
	public DefaultAnnotationAppearanceControlManager(AnnotationManager mgr, SimpleHistogramFactory hFctry) {
		
		this.mgr = mgr;
		
		controls = new AnnotationAppearanceControl[2];
		controls[0] = new AnnotationStateAppearanceControl(mgr);
		controls[1] = new AnnotationHeatMapAppearanceControl(mgr, hFctry);
				
	}


	@Override
	public AnnotationManager getAnnotationManager() {
		return mgr;
	}
	
	@Override
	public int numModels() {
		return controls.length;
	}

	@Override
	public AnnotationAppearanceControl getControl(int i) {
		return controls[i];
	}

	@Override
	public void setActiveControl(int i) {
		if(i==active)	return;
		if(active>=controls.length)	throw new IndexOutOfBoundsException(i+" >= number of controls in this manager ("+controls.length+")");
		active = i;
		lstrs.forEach(l->l.appearanceModelChanged(this));
	}

	@Override
	public AnnotationAppearanceControl getActiveControl() {
		return controls[active];
	}

	@Override
	public void addAppearanceControlChangeListener(AppearanceControlChangeListener l) {
		lstrs.add(l);
	}

	@Override
	public void removeAppearanceModelChangeListener(AppearanceControlChangeListener l) {
		lstrs.remove(l);
	}


}
