package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.type.Type;

public final class ExtendedImageInfo<T extends Type<T>> {

	private final MinimalImageInfo info;
	private final Image image;
	private final  File outFolder;
	private final String name;
	private final String ext;
	private final T type;
	
	public ExtendedImageInfo(MinimalImageInfo info, File outFolder, String name, String ext, T type) {
		this(info, outFolder, name, ext, type, null);
	}

	public ExtendedImageInfo(MinimalImageInfo info, File outFolder, String name, String ext, T type, Image image) {
		this.info = info;
		this.outFolder = outFolder;
		this.name = name;
		this.ext = ext;
		this.type = type;
		this.image = image;
	}

	
	/**
	 * @return The {@link Image} which was processed if this {@link ExtendedImageInfo} was obtained from a {@link ProcessingPipeline} which processed database
	 * images.
	 */
	public Image getAssociatedImage(){
		return image;
	}
	
	
	public MinimalImageInfo getInfo() {
		return info;
	}

	public File getOutFolder() {
		return outFolder;
	}

	public String getName() {
		return name;
	}

	public String getExt() {
		return ext;
	}

	public T getType() {
		return type;
	}
	
	public String getFullPath(){
		return outFolder+File.separator+name+ext;
	};
	
	
	
}
