package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.util.Random;

import net.imglib2.display.ColorTable16;

public enum Lut16 implements ImgLut<short[]>{

	Grays{
		@Override
		public short[][] data() {
			final short[][] gray = new short[ 3 ][ 65536 ];
			for ( int i = 0; i < gray[ 0 ].length; i++ ){
				gray[ 0 ][ i ] = ( short ) i;
				gray[ 1 ][ i ] = ( short ) i;
				gray[ 2 ][ i ] = ( short ) i;					
			}
			return gray;
		}

		@Override
		public Color dominante(){
			return Color.black;
		}
	},
	Red{
		@Override
		public short[][] data() {
			final short[][] gray = new short[ 3 ][ 65536 ];
			for ( int i = 0; i < gray[ 0 ].length; i++ )
				gray[ 0 ][ i ] = ( short ) i;
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.red;
		}
	},
	Green{
		@Override
		public short[][] data() {
			final short[][] gray = new short[ 3 ][ 65536 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ )
				gray[ 1 ][ i ] = ( short ) i;
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.green;
		}
	},
	Blue{
		@Override
		public short[][] data() {
			final short[][] gray = new short[ 3 ][ 65536 ];
			for ( int i = 0; i < gray[ 2 ].length; i++ )
				gray[ 2 ][ i ] = ( short ) i;
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.blue;
		}
	},
	Cyan{
		@Override
		public short[][] data() {
			final short[][] gray = new short[ 3 ][ 65536 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ ){
				gray[ 1 ][ i ] = ( short ) i;
				gray[ 2 ][ i ] = ( short ) i;
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.cyan;
		}
	},
	Yellow{
		@Override
		public short[][] data() {
			final short[][] gray = new short[ 3 ][ 65536 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ ){
				gray[ 0 ][ i ] = ( short ) i;
				gray[ 1 ][ i ] = ( short ) i;
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.yellow;
		}
	},
	Magenta{
		@Override
		public short[][] data() {
			final short[][] gray = new short[ 3 ][ 65536 ];
			for ( int i = 0; i < gray[ 1 ].length; i++ ){
				gray[ 0 ][ i ] = ( short ) i;
				gray[ 2 ][ i ] = ( short ) i;
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.magenta;
		}
	},
	RANDOM{
		@Override
		public short[][] data() {
			Random r = new Random();
			final short[][] gray = new short[ 3 ][ 65536 ];
			for ( int i = 1; i < gray[ 1 ].length; i++ ){
				gray[ 0 ][ i ] = ( short ) (r.nextInt(32767) + 1);
				gray[ 1 ][ i ] = ( short ) (r.nextInt(32767) + 1);
				gray[ 2 ][ i ] = ( short ) (r.nextInt(32767) + 1);
			}
			return gray;
		}
		
		@Override
		public Color dominante(){
			return Color.ORANGE;
		}
	},

	Colors_16{
		@Override
		public short[][] data() {
			final short[][] gray = new short[ 3 ][ 65536 ];

			byte[] c = new byte[]{0,0,0};			
			for(int i = 0; i<4112; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,(byte)171,(byte)171};
			for(int i = 4113; i<8224; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,1,(byte) 224};
			for(int i = 8225; i<12336; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,110,(byte)254};
			for(int i = 12337; i<16448; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1, (byte)171,(byte) 254};
			for(int i = 16449; i<20560; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1,(byte) 224,(byte) 254};
			for(int i = 20561; i<24672; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{1, (byte)254,1};
			for(int i = 24673; i<28784; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{ (byte)190, (byte)255,0};
			for(int i = 28785; i<32896; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{ (byte)255, (byte)224,0};
			for(int i = 32897; i<41120; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{ (byte)255, (byte)141,0};
			for(int i = 41121; i<45232; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{ (byte)250,94,0};
			for(int i = 45233; i<49344; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{ (byte)245,0,0};
			for(int i = 49345; i<53456; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{(byte) 245,0, (byte)135};
			for(int i = 53457; i<57311; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{ (byte)222,0, (byte)222};
			for(int i = 57312; i<61680; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			c = new byte[]{ (byte)255,(byte) 255,(byte) 255};
			for(int i = 61680; i<65536; i++){
				gray[0][i] = c[0];
				gray[1][i] = c[1];
				gray[2][i] = c[2];
			}

			return gray;
		}			


		@Override
		public Color dominante(){
			return Color.black;
		}

	},



	SPECTRUM{

		public short[][] data(){
			final short[][] gray = new short[ 3 ][ 65536 ];
			for (int i = 0; i < 65536; i++) {
				final int c = Color.HSBtoRGB(i / 65536f, 1f, 1f);
				gray[0][i] =  (byte) ((c)&0xFF);
				gray[1][i]  = (byte) ((c>>8)&0xFF);
				gray[2][i] =  (byte) ((c>>16)&0xFF);
			}
			return gray;
		}

		
		@Override
		public Color dominante(){
			return Color.blue;
		}
		
	},


	FIRE{



		public short[][] data(){

			final int[] r = { 0, 0, 1, 25, 49, 73, 98, 122, 146, 162, 173, 184, 195, 207,
					217, 229, 240, 252, 255, 255, 255, 255, 255, 255, 255, 255,
					255, 255, 255, 255, 255, 255 };
			final int[] g = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 35, 57, 79, 101,
					117, 133, 147, 161, 175, 190, 205, 219, 234, 248, 255, 255,
					255, 255 };				
			final int[] b = { 0, 61, 96, 130, 165, 192, 220, 227, 210, 181, 151, 122, 93,
					64, 35, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 35, 98, 160, 223,
					255 };

			short[][] map = new short[ 3 ][ 65536 ];

			// cast elements
			int j;
			for (int i = 0; i < 65536; i++) {
				j = i/2048;
				map[0][i] = (short) (r[j]*257);
				map[1][i] = (short) (g[j]*257);
				map[2][i] = (short) (b[j]*257);
			}

			return map;
		}

		@Override
		public Color dominante(){
			return Color.orange;
		}

	},


	HILO{
		@Override
		public short[][] data() {
			final short[][] gray = new short[ 3 ][ 65536 ];
			gray[2][0] = ( short ) 65535;	gray[0][gray[ 0 ].length-1] = ( short ) 65535;
			for ( int i = 1; i < gray[ 0 ].length-1; i++ ){
				gray[ 0 ][ i ] = ( short ) i;
				gray[ 1 ][ i ] = ( short ) i;
				gray[ 2 ][ i ] = ( short ) i;
			}
			return gray;
		}


		@Override
		public Color dominante(){
			return Color.black;
		}
		
		
	};




	public  ColorTable16 table(){
		return new ColorTable16(data());			
	}


	public static void main(String[] args) {
		System.out.println(Lut16.SPECTRUM.data()[0][65535] & 0xffff);
		System.out.println(Lut16.SPECTRUM.data()[1][65535] & 0xffff);
		System.out.println(Lut16.SPECTRUM.data()[2][65535] & 0xffff);
	}



}
