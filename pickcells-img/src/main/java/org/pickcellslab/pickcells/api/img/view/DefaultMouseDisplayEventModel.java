package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.type.numeric.RealType;

public class DefaultMouseDisplayEventModel implements MouseDisplayEventModel, MouseListener, MouseMotionListener, DisplayContentListener {




	private final NumberFormat format = NumberFormat.getNumberInstance();


	private final ImageDisplay display;

	private SelectionMode mode = SelectionMode.NONE;

	private AnnotationManager selectableManager;

	private int selectableIndex = -1;

	private final MouseDisplayEventListener selectionListener = new SelectionListener();

	private final List<MouseDisplayEventListener> mouseLstrs = new ArrayList<>();

	private final List<Boolean> tooltipsEnabled = new ArrayList<>();



	private MouseDisplayEventListener currentControl;


	private DynamicTransformEventHandler<?> handler;





	DefaultMouseDisplayEventModel(ImageDisplay display, DynamicTransformEventHandler<?> handler) {
		this.display = display;
		display.addDisplayContentListener(this);
		format.setMaximumFractionDigits(2);
		this.handler = handler;
		display.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("shift I"), "Inverse", new InverseAction());
		display.getKeyStrokeModel().addKeyBinding(KeyStroke.getKeyStroke("shift C"), "Clear", new ClearAction());
	}




	@Override
	public void addListener(MouseDisplayEventListener l) {
		mouseLstrs.add(l);
	}

	@Override
	public void removeListener(MouseDisplayEventListener l) {
		if(l==currentControl)
			currentControl = null;
		System.out.println("Listener removed : "+mouseLstrs.remove(l));
	}

	@Override
	public void setVoxelTooltipsEnabled(int imgIndex, boolean enabled) {
		tooltipsEnabled.set(imgIndex, enabled);
	}

	@Override
	public void setRotationEnabled(boolean enabled) {
		handler.setAllowRotation(enabled);
	}



	@Override
	public MouseDisplayEventListener setControl(MouseDisplayEventListener l) {

		MouseDisplayEventListener prev = currentControl;
		if(prev!=null)
			removeListener(prev);

		this.currentControl = l;
		if(l!=null)
			addListener(l);

		return prev;
	}




	@Override
	public void mouseClicked(MouseEvent e) {
		final EnhancedEvent evt = new EnhancedEvent(e);
		new ArrayList<>(mouseLstrs).forEach(l->l.mouseClicked(evt));

	}





	@Override
	public void mousePressed(MouseEvent e) {
		final EnhancedEvent evt = new EnhancedEvent(e);
		mouseLstrs.forEach(l->l.mousePressed(evt));
	}


	@Override
	public void mouseReleased(MouseEvent e) {
		final EnhancedEvent evt = new EnhancedEvent(e);
		mouseLstrs.forEach(l->l.mouseReleased(evt));
	}


	@Override
	public void mouseEntered(MouseEvent e) {
		display.setTextOverlay(this.getText(e));
		final EnhancedEvent evt = new EnhancedEvent(e);
		mouseLstrs.forEach(l->l.mouseEntered(evt));
	}


	@Override
	public void mouseExited(MouseEvent e) {
		display.setTextOverlay("");
		final EnhancedEvent evt = new EnhancedEvent(e);
		mouseLstrs.forEach(l->l.mouseExited(evt));
	}


	@Override
	public void mouseDragged(MouseEvent e) {
		final EnhancedEvent evt = new EnhancedEvent(e);
		mouseLstrs.forEach(l->l.mouseDragged(evt));
	}


	@Override
	public void mouseMoved(MouseEvent e) {
		display.setTextOverlay(this.getText(e));
	}





	@Override
	public void imageAdded(ImageDisplay source) {
		tooltipsEnabled.add(true);
	}


	@Override
	public void imageRemoved(ImageDisplay source, int index) {
		tooltipsEnabled.remove(index);
	}


	@Override
	public void annotationAdded(ImageDisplay source) {
		if(selectableManager==null){
			selectableIndex = 0;
			selectableManager=source.getAnnotationManager(selectableIndex);
			addListener(selectionListener);
			System.out.println("Selection Listener added !");
		}
	}


	@Override
	public void annotationRemoved(ImageDisplay source, int index) {
		if(source.numAnnotationLayers()==0){
			removeListener(selectionListener);
			selectableIndex = -1;
			selectableManager = null;			
		}else{
			selectableIndex = 0;
			selectableManager=source.getAnnotationManager(selectableIndex);
			addListener(selectionListener);
			System.out.println("Selection Listener added !");
		}

	}


	@Override
	public void setSelectableAnnotationLayer(int i, boolean clearPrevious) {
		if(clearPrevious && selectableManager!=null)
			selectableManager.clearSelection();
	}




	@Override
	public void setSelectionMode(SelectionMode mode) {
		if(this.mode!=mode && selectableManager!=null)
			selectableManager.clearSelection();
		this.mode = mode;
	}


	
	private <T extends RealType<T>> String getText(MouseEvent e) {

		//First get coordinates:
		final double[] imgCoords = getImageCoordinates(e.getX(), e.getY());		
		String info = "Image Coordinates: "+(int)imgCoords[0]+", "+(int)imgCoords[1];
		if(display.getImageModel(0).getMinimalInfo().dimension(Image.z)>1)
			info+= ", "+(int)imgCoords[2];

		if(display.getImageModel(0).getMinimalInfo().dimension(Image.t)>1)
			info+=", "+display.currentTimeFrame();

		info+="\n";

		//TODO calibrated coordinates
		// Now for each image get the current intensity at the click location
		for(int i = 0; i<display.numImages(); i++){		
			if(!tooltipsEnabled.get(i)) continue;
			final ImageDisplayModel<T> model = display.getImageModel(i);
			final MinimalImageInfo mii = model.getMinimalInfo();
			final long[] pos = getImageLocation(imgCoords, i);
			for(int c = 0; c<mii.dimension(Image.c); c++){
				if(mii.dimension(Image.c)>1)
					pos[mii.index(Image.c)] = c;
				info+=display.getChannel(i, c).getChannelName()+" Intensity: "+format.format(model.getValueAt(pos).getRealDouble())+"\n";
			}
		}
		// Now get Annotations at the click location
		// NB by definition Annotations layers are not multichannel and localization is done within frame so either 2d or 3d.


		for(int i = 0; i<display.numAnnotationLayers(); i++){	
			final long[] pos = this.getAnnotationLocation(imgCoords, i);
			final Annotation annotation = display.getAnnotationManager(i).getAnnotationAt(pos);
			if(annotation!=null)
				info+="Annotation: "+annotation.toString()+" ("+annotation.status()+")"+"\n";
		}

		return info;
	}
	
	
	


	private <T extends RealType<T>> String getToolTipText(MouseEvent e) {

		//First get coordinates:
		final double[] imgCoords = getImageCoordinates(e.getX(), e.getY());		
		String info = "<HTML><b>Image Coordinates: </b>"+(int)imgCoords[0]+", "+(int)imgCoords[1];
		if(display.getImageModel(0).getMinimalInfo().dimension(Image.z)>1)
			info+= ", "+(int)imgCoords[2];

		if(display.getImageModel(0).getMinimalInfo().dimension(Image.t)>1)
			info+=", "+display.currentTimeFrame();

		info+="<br>";

		//TODO calibrated coordinates
		// Now for each image get the current intensity at the click location
		for(int i = 0; i<display.numImages(); i++){		
			if(!tooltipsEnabled.get(i)) continue;
			final ImageDisplayModel<T> model = display.getImageModel(i);
			final MinimalImageInfo mii = model.getMinimalInfo();
			final long[] pos = getImageLocation(imgCoords, i);
			for(int c = 0; c<mii.dimension(Image.c); c++){
				if(mii.dimension(Image.c)>1)
					pos[mii.index(Image.c)] = c;
				info+="<b>"+display.getChannel(i, c).getChannelName()+" Intensity: </b>"+format.format(model.getValueAt(pos).getRealDouble())+"<br>";
			}
		}
		// Now get Annotations at the click location
		// NB by definition Annotations layers are not multichannel and localization is done within frame so either 2d or 3d.


		for(int i = 0; i<display.numAnnotationLayers(); i++){	
			final long[] pos = this.getAnnotationLocation(imgCoords, i);
			final Annotation annotation = display.getAnnotationManager(i).getAnnotationAt(pos);
			if(annotation!=null)
				info+="<b> Annotation: </b>"+annotation.toString()+" ("+annotation.status()+")"+"<br>";
		}


		info+="</HTML>";
		return info;
	}


	private double[] getImageCoordinates(int x, int y){

		double[] imgCoords = new double[4];

		display.getCurrentTransform().applyInverse(imgCoords, new double[]{x, y, display.currentZSlice(), display.currentTimeFrame()});

		if(display.getImageModel(0).getMinimalInfo().dimension(Image.z)>1)
			if(display.getCurrentTransform().numTargetDimensions() == 2) 
				imgCoords[2] = display.currentZSlice();			

		if(display.getImageModel(0).getMinimalInfo().dimension(Image.t)>1)
			imgCoords[3] = display.currentTimeFrame();

		return imgCoords;

	}


	private long[] getImageLocation(double[] imgCoords, int i){
		final ImageDisplayModel<?> model = display.getImageModel(i);
		final MinimalImageInfo mii = model.getMinimalInfo();
		final long[] pos = model.currentlyDisplayedLocation();
		pos[mii.index(Image.x)] = (long) imgCoords[0];		pos[mii.index(Image.y)] = (long) imgCoords[1];
		if(mii.dimension(Image.z)>1)	pos[mii.index(Image.z)] = display.currentZSlice();
		if(mii.dimension(Image.t)>1)	pos[mii.index(Image.t)] = display.currentTimeFrame();
		return pos;
	}



	private long[] getAnnotationLocation(double[] imgCoords, int i){
		final MinimalImageInfo mii = display.getAnnotationManager(i).annotatedImageInfo();
		final long[] pos = new long[mii.numDimensions()];
		pos[mii.index(Image.x)] = (long) imgCoords[0];		pos[mii.index(Image.y)] = (long) imgCoords[1];
		if(mii.dimension(Image.z)>1)	pos[mii.index(Image.z)] = display.currentZSlice();
		if(mii.dimension(Image.t)>1)	pos[mii.index(Image.t)] = display.currentTimeFrame();
		return pos;
	}



	private final class EnhancedEvent implements MouseDisplayEvent{

		private final MouseEvent evt;
		private final double[] imgCoords;

		public EnhancedEvent(MouseEvent evt) {
			this.evt = evt;
			this.imgCoords = getImageCoordinates(evt.getX(), evt.getY());
		}


		@Override
		public MouseEvent getMouseEvent() {
			return evt;
		}

		@Override
		public ImageDisplay getSource() {
			return display;
		}

		@Override
		public int getImageX(int imgIndex) {
			return (int) imgCoords[0];
		}

		@Override
		public int getAnnotationX(int antIndex) {
			return (int) imgCoords[0];
		}

		@Override
		public int getImageY(int imgIndex) {
			return (int) imgCoords[1];
		}

		@Override
		public int getAnnotationY(int antIndex) {
			return (int) imgCoords[1];
		}

		@Override
		public int getImageZ(int imgIndex) {
			return (int) imgCoords[2];
		}

		@Override
		public int getAnnotationZ(int antIndex) {
			return (int) imgCoords[2];
		}

		@Override
		public int getImageT(int imgIndex) {
			return (int) imgCoords[3];
		}

		@Override
		public int getAnnotationT(int antIndex) {
			return (int) imgCoords[3];
		}


		@Override
		public long[] getImageEventLocation(int imgIndex) {
			return getImageLocation(imgCoords, imgIndex);
		}


		@Override
		public long[] getAnnotationEventLocation(int antIndex) {
			return getAnnotationLocation(imgCoords, antIndex);
		}

	}



	private class SelectionListener extends MouseDisplayEventListener{

		private Annotation previous;

		@Override 
		public void mouseClicked(MouseDisplayEvent e) {

			if(mode==SelectionMode.NONE) return;


			if(SwingUtilities.isLeftMouseButton(e.getMouseEvent()) && !e.getMouseEvent().isControlDown()){

				Annotation selected = selectableManager.getAnnotationAt(e.getAnnotationEventLocation(selectableIndex));

				if(selected == null){
					selectableManager.clearSelection();
					return;
				}

				if(selected.equals(previous)){
					selectableManager.toggleSelected(previous);
					return;
				}
				if(previous!=null && previous.status() == AnnotationStatus.SELECTED)
					selectableManager.toggleSelected(previous);
				previous = selected;
				selectableManager.toggleSelected(previous);
				System.out.println("Selection created for "+previous);
			}
			else if(mode == SelectionMode.MULTIPLE && SwingUtilities.isLeftMouseButton(e.getMouseEvent()) && e.getMouseEvent().isControlDown()){
				Annotation selected = selectableManager.getAnnotationAt(e.getAnnotationEventLocation(selectableIndex));
				if(selected != null){
					selectableManager.toggleSelected(selected);
				}
			}
		}
	}



	private class InverseAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(selectableManager!=null)
				selectableManager.inverseSelection();
		}

	}

	private class ClearAction extends AbstractAction{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(selectableManager!=null)
				selectableManager.clearSelection();
		}

	}



}
