package org.pickcellslab.pickcells.api.img.view;

import java.util.Objects;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyArrayDimension;

public class AnnotationArrayDimension<E> implements Dimension<Annotation,E> {

	private final AKey<E> key;
	private final int index;
	private final int length;
	private final String description;


	public AnnotationArrayDimension(AKey<E> k, int length, String description){

		Objects.requireNonNull(k,"the provided AKey cannot be null");
		if(!k.type().isArray())
			throw new IllegalArgumentException("The provided AKey does not point to an array value");		
		if(length<0)
			throw new IllegalArgumentException("Negative length provided");
		
		this.key = k;
		this.index = -1;
		this.length = length;

		if(null == description)
			description = "Not Available";
		this.description = description;
	}


	@Override
	public int index() {
		return index;
	}

	@Override
	public String name() {
		return key.name;
	}



	@Override
	public dType dataType() {
		dType t = key.dType();
		return t;
	}


	@Override
	public String toString() {
		return name();
	}


	@Override
	public boolean equals(Object o){
		if(o instanceof KeyArrayDimension)
			return key == ((AnnotationArrayDimension<?>)o).key && index == ((Dimension<?,?>)o).index();
		else return false;
	}


	@Override
	public int hashCode(){
		return key.hashCode() + Integer.hashCode(index);
	}




	@Override
	public E apply(Annotation i) {
		if(i==null)	return null;
		return i.getProperty(key);
	}





	@Override
	public Class<E> getReturnType() {
		return key.type();
	}



	@Override
	public String info() {
		return description;
	}



	@Override
	public int length() {
		return length;
	}




}
