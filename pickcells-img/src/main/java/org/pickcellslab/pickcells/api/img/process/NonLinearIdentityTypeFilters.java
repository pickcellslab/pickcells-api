package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessingPackage;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;

import net.imglib2.Cursor;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.neighborhood.Neighborhood;
import net.imglib2.outofbounds.OutOfBoundsConstantValueFactory;
import net.imglib2.outofbounds.OutOfBoundsFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;


public class NonLinearIdentityTypeFilters<T extends Comparable<T> & NativeType<T> & RealType<T>> implements ImgProcessingPackage<T,T>{


	@SuppressWarnings("unchecked")
	private final ImgProcessing<T,T>[] ps = new ImgProcessing[]{new Median()};

	private final ImgIO io;
	
	public NonLinearIdentityTypeFilters(ImgIO io) {
		this.io = io;
	}
	

	@Override
	public ImgProcessing<T, T>[] processors() {
		return ps;
	}


	public class Median implements ImgProcessing<T,T>{

		private PreviewBag<T> input;
		private int radius = 1;
		@SuppressWarnings("serial")
		private ValidityTogglePanel dialog 
		= new ValidityTogglePanel(){

			{
				add(new JLabel("Radius "));
				SpinnerNumberModel model = new SpinnerNumberModel(1,1,20,1);
				add(new JSpinner(model));
				model.addChangeListener(l->setRadius(model.getNumber().intValue()));
			}

			@Override
			public boolean validity() {
				return true;
			}

		};
		
		
		private void setRadius(int r){
			r = radius;
		}


		@Override
		public String name() {
			return "Median filter";
		}

	
		@Override
		public String description() {
			return "Median filtering of the input image, the kernel is a sphere with a radius defined by the user";
		}

		@Override
		public boolean repeatable() {
			return true;
		}

		@Override
		public DimensionalityEffect dimensionalityEffect() {
			return DimensionalityEffect.CHANNELS_TO_CHANNELS;
		}
		
		
		@Override
		public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<T> input) {
			this.input = input;
			return dialog;
		}
		
		
		@Override
		public void changeInput(PreviewBag<T> current) {
			this.input = current;			
		}


		@Override
		public RandomAccessibleInterval<T> validate() {
			
			long[] radii = new long[input.getInput().numDimensions()];
			Arrays.fill(radii, radius);
			MinimalImageInfo info = input.getInputInfo();
			if(info.dimension(Image.c)!=1)
				radii[info.index(Image.c)]=0;
			if(info.dimension(Image.t)!=1)
				radii[info.index(Image.t)]=0;

			OutOfBoundsFactory<T,RandomAccessibleInterval<T>> strategy = 
					new OutOfBoundsConstantValueFactory<T,RandomAccessibleInterval<T>>(input.getInput().randomAccess().get().createVariable());
			HyperEllipseFactory<T> factory = new HyperEllipseFactory<>(radii, strategy);
			RandomAccessibleInterval<T> result = Operations.run(io, input.getInput(), factory, median());
			
			return result;
		}


		

		@Override
		public Function<ImageBag<T>, RandomAccessibleInterval<T>> toPipe() {
			final long r = radius;
			return (bag)->{
				final RandomAccessibleInterval<T> input = bag.getInputFrame(0);
				final long[] radii = new long[input.numDimensions()];
				Arrays.fill(radii, r);
				final MinimalImageInfo info = bag.getInputFrameInfo();
				if(info.dimension(Image.c)!=1)
					radii[info.index(Image.c)]=0;

				final OutOfBoundsFactory<T,RandomAccessibleInterval<T>> strategy = 
						new OutOfBoundsConstantValueFactory<T,RandomAccessibleInterval<T>>(input.randomAccess().get().createVariable());
				final HyperEllipseFactory<T> factory = new HyperEllipseFactory<>(radii, strategy);
				return Operations.run(io, input, factory, median());
			};
		}


		


		@Override
		public boolean requiresAnnotationChannel() {
			return false;
		}


		@Override
		public void stop() {
			// TODO Auto-generated method stub
			
		}


		@Override
		public boolean requiresOriginal() {
			return false;
		}


		@Override
		public boolean requiresFlat() {
			return false;
		}


		@Override
		public T outputType(T inputType) {
			return inputType;
		}


		

	}


	public static <T extends Comparable<T> & NativeType<T>> NeighborhoodFunction<T> median(){
		return (n,center)->{	

			List<T> list = new ArrayList<>((int) n.size());
			Cursor<T> c = n.cursor();
			while(c.hasNext()){
				c.next();	
				list.add(c.get().copy());
			}
			//System.out.println(c.get());
			Collections.sort(list);

			return list.get((int) n.size()/2);
		};
	}



	public static <T extends Comparable<T> & NativeType<T>> Function<Neighborhood<T>,T> medianFunction(){
		return (n)->{	

			List<T> list = new ArrayList<>((int) n.size());
			Cursor<T> c = n.cursor();
			while(c.hasNext()){
				c.next();	
				list.add(c.get().copy());
			}
			//System.out.println(c.get());
			Collections.sort(list);

			return list.get((int) n.size()/2);
		};
	}



}
