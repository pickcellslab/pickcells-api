package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class ImgLutRenderer implements ListCellRenderer<ImgLut<?>> {

	
	//private final Map<ImgLut<?>, Icon> map = new HashMap<>();
	
	@Override
	public Component getListCellRendererComponent(JList<? extends ImgLut<?>> list, ImgLut<?> value, int index,
			boolean isSelected, boolean cellHasFocus) {
		
		//Icon icon = map.get(value);
		//if(icon == null){
		//	icon = value.createIcon((list.getWidth()-10, list.getHeight()-10);
		//}
		JLabel label = new JLabel(value.createIcon(list.getWidth()-10, list.getHeight()-10));
		label.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		
		return label;
	}

}
