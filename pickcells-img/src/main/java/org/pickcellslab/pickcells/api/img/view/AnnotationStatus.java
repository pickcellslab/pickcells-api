package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;

import net.imglib2.type.numeric.ARGBType;

public interface AnnotationStatus {

	public int preferredARGB();
	
	
	public static AnnotationStatus SELECTED = new AnnotationStatus(){
		@Override
		public String toString(){
			return "SELECTED";
		}

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(Color.YELLOW.getRed(), Color.YELLOW.getGreen(), Color.YELLOW.getBlue(), 255);
		}
	};
	
	public static AnnotationStatus DESELECTED = new AnnotationStatus(){
		@Override
		public String toString(){
			return "DESELECTED";
		}

		@Override
		public int preferredARGB() {
			return ARGBType.rgba(Color.LIGHT_GRAY.getRed(), Color.LIGHT_GRAY.getGreen(), Color.LIGHT_GRAY.getBlue(), 150);
		}
	};

}
