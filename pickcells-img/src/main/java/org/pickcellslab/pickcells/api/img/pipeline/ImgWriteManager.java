package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;

/**
 * Writes image frames to disk
 * @author Guillaume Blin
 *
 * @param <T> The {@link Type} of the image to be written.
 */
public interface ImgWriteManager<T extends Type<T>>{
	
	/**
	 * Writes the given frame to disk
	 * @param frame The frame to be written
	 * @param t The index of the frame to be written
	 * @throws IOException
	 */
	void writeFrame(RandomAccessibleInterval<T> frame, long t) throws IOException;
	
	/**
	 * Closes the resources
	 */
	void close();
	
	/**
	 * @return The extension of the file written by this {@link ImgWriteManager}
	 */
	String getExt();
}
