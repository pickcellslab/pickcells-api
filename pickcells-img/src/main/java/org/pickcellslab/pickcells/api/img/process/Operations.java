package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.pickcellslab.pickcells.api.img.io.ImgIO;

import net.imglib2.Cursor;
import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessible;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.neighborhood.Neighborhood;
import net.imglib2.algorithm.neighborhood.Shape;
import net.imglib2.algorithm.region.localneighborhood.AbstractNeighborhood;
import net.imglib2.converter.Converter;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.util.Intervals;
import net.imglib2.view.ExtendedRandomAccessibleInterval;
import net.imglib2.view.Views;

public abstract class Operations {


	public static < F extends RandomAccessibleInterval<T>,T extends NativeType<T> & RealType<T> >  RandomAccessibleInterval< T > run( 
			ImgIO io,
			final F in, 
			final NeighborhoodFactory<T> shape,
			final NeighborhoodFunction<T> op
			)
	{



		final int numThreads = Runtime.getRuntime().availableProcessors();
		final ExecutorService service = Executors.newFixedThreadPool( numThreads );


		//create copy of the source
		T t = in.randomAccess().get().createVariable();
		final RandomAccessibleInterval< T > out = io.createImg( in, t );

		final Interval full = Intervals.expand( in, 0);
		final int n = full.numDimensions();
		final int splitd = n-1;


		final int numTasks = numThreads <= 1 ? 1 : ( int ) Math.min( full.dimension( splitd ), numThreads * 20 );
		final long dsize = full.dimension( splitd ) / numTasks;
		final long[] min = new long[ n ];
		final long[] max = new long[ n ];
		full.min( min );
		full.max( max );


		final ArrayList< Future< Void > > futures = new ArrayList< Future< Void > >();

		for ( int taskNum = 0; taskNum < numTasks; ++taskNum )
		{
			min[ splitd ] = full.min( splitd ) + taskNum * dsize;
			max[ splitd ] = ( taskNum == numTasks - 1 ) ? full.max( splitd ) : min[ splitd ] + dsize - 1;
			final Cursor<T> center = Views.interval( in, new FinalInterval( min, max )).cursor();
			final RandomAccess<T> output = Views.interval( out, new FinalInterval( min, max ) ).randomAccess();
			final AbstractNeighborhood<T> k = shape.create(in);
			//System.out.println("Min --> "+Arrays.toString(min));
			//System.out.println("Max --> "+Arrays.toString(max));
			
			final Callable< Void > r = new Callable< Void >()
			{
				@Override
				public Void call()
				{
					
					while ( center.hasNext() )
					{						
						center.next();
						k.setPosition(center);						
						T result = op.apply(k, center.get());
						output.setPosition(center);
						output.get().set(result);
					}
					return null;	
				}
			};
			futures.add( service.submit( r ) );
		}
		for ( final Future< Void > f : futures )
		{
			try
			{
				f.get();
			}
			catch ( final InterruptedException e )
			{
				e.printStackTrace();
			}
			catch ( final ExecutionException e )
			{
				e.printStackTrace();
			}
		}

		service.shutdown();
		

		return out;
	}












	public static < T extends NativeType< T > & RealType<T> >  void inPlace( 
			ImgIO io,
			final  RandomAccessibleInterval< T > img, 
			final Shape shape,
			final Function<Neighborhood< T >,T> op
			)
	{

		final int numThreads = Runtime.getRuntime().availableProcessors();
		final ExecutorService service = Executors.newFixedThreadPool( numThreads );

		//create copy of the source
		final ExtendedRandomAccessibleInterval< T ,RandomAccessibleInterval< T >> copy = Views.extendMirrorSingle(ImgDimensions.copy(io, img));

		final Interval full = Intervals.expand( img, -1 );
		final int n = img.numDimensions();
		final int splitd = n - 1;


		final int numTasks = numThreads <= 1 ? 1 : ( int ) Math.min( full.dimension( splitd ), numThreads * 20 );
		final long dsize = full.dimension( splitd ) / numTasks;
		final long[] min = new long[ n ];
		final long[] max = new long[ n ];
		full.min( min );
		full.max( max );


		final ArrayList< Future< Void > > futures = new ArrayList< Future< Void > >();

		for ( int taskNum = 0; taskNum < numTasks; ++taskNum )
		{
			min[ splitd ] = full.min( splitd ) + taskNum * dsize;
			max[ splitd ] = ( taskNum == numTasks - 1 ) ? full.max( splitd ) : min[ splitd ] + dsize - 1;
			final RandomAccessibleInterval< T > source = Views.interval( img, new FinalInterval( min, max ) );
			final RandomAccessibleInterval< T > output = Views.interval( copy, new FinalInterval( min, max ) );

			final Callable< Void > r = new Callable< Void >()
			{
				@Override
				public Void call()
				{
					final Cursor< T > center = Views.flatIterable( source ).cursor();
					for ( final Neighborhood< T > neighborhood : shape.neighborhoods( output ) )
					{
						center.fwd();
						T result = op.apply(neighborhood);
						center.get().set(result);
					}
					return null;	
				}
			};
			futures.add( service.submit( r ) );
		}
		for ( final Future< Void > f : futures )
		{
			try
			{
				f.get();
			}
			catch ( final InterruptedException e )
			{
				e.printStackTrace();
			}
			catch ( final ExecutionException e )
			{
				e.printStackTrace();
			}
		}

		service.shutdown();

	}




	public static < I,O extends NativeType<O> & RealType<O>>  RandomAccessibleInterval< O > convert( 
			ImgIO io,
			final RandomAccessibleInterval< I > img, 
			final Converter<I,O> op,
			O type
			)
	{

		final int numThreads = Runtime.getRuntime().availableProcessors();
		final ExecutorService service = Executors.newFixedThreadPool( numThreads );

		final Interval full = Intervals.expand( img, -1 );
		final int n = img.numDimensions();
		final int splitd = n - 1;


		final int numTasks = numThreads <= 1 ? 1 : ( int ) Math.min( full.dimension( splitd ), numThreads * 20 );
		final long dsize = full.dimension( splitd ) / numTasks;
		final long[] min = new long[ n ];
		final long[] max = new long[ n ];
		full.min( min );
		full.max( max );


		//The new image to return 
		//Image to be returned
		Img<O> copy =  io.createImg(img, type);


		final ArrayList< Future< Void > > futures = new ArrayList< Future< Void > >();

		for ( int taskNum = 0; taskNum < numTasks; ++taskNum )
		{
			min[ splitd ] = full.min( splitd ) + taskNum * dsize;
			max[ splitd ] = ( taskNum == numTasks - 1 ) ? full.max( splitd ) : min[ splitd ] + dsize - 1;
			final RandomAccessibleInterval< I > source = Views.interval( img, new FinalInterval( min, max ) );
			final RandomAccessibleInterval< O > target = Views.interval( copy, new FinalInterval( min, max ) );

			final Callable< Void > r = new Callable< Void >(){
				@Override
				public Void call()	{
					final Cursor< I > c = Views.flatIterable( source ).cursor();
					final Cursor< O > d = Views.flatIterable( target ).cursor();
					while( c.hasNext() )
					{
						c.next();
						d.next();
						op.convert(c.get(),d.get());
					}
					return null;	
				}
			};
			futures.add( service.submit( r ) );

		}
		for ( final Future< Void > f : futures )
		{
			try
			{
				f.get();
			}
			catch ( final InterruptedException e )
			{
				e.printStackTrace();
			}
			catch ( final ExecutionException e )
			{
				e.printStackTrace();
			}
		}

		service.shutdown();

		return copy;


	}



	public static < I,O extends NativeType<O> >  void convert( 
			final RandomAccessibleInterval< I > input, 
			final RandomAccessible<O> output,
			final Converter<I,O> op
			)
	{

		final int numThreads = Runtime.getRuntime().availableProcessors();
		final ExecutorService service = Executors.newFixedThreadPool( numThreads );

		final Interval full = Intervals.expand( input, -1 );
		final int n = input.numDimensions();
		final int splitd = n - 1;


		final int numTasks = numThreads <= 1 ? 1 : ( int ) Math.min( full.dimension( splitd ), numThreads * 20 );
		final long dsize = full.dimension( splitd ) / numTasks;
		final long[] min = new long[ n ];
		final long[] max = new long[ n ];
		full.min( min );
		full.max( max );




		final ArrayList< Future< Void > > futures = new ArrayList< Future< Void > >();

		for ( int taskNum = 0; taskNum < numTasks; ++taskNum )
		{
			min[ splitd ] = full.min( splitd ) + taskNum * dsize;
			max[ splitd ] = ( taskNum == numTasks - 1 ) ? full.max( splitd ) : min[ splitd ] + dsize - 1;
			final RandomAccessibleInterval< I > source = Views.interval( input, new FinalInterval( min, max ) );
			final RandomAccessibleInterval< O > target = Views.interval( output, new FinalInterval( min, max ) );

			final Callable< Void > r = new Callable< Void >(){
				@Override
				public Void call()	{
					final Cursor< I > c = Views.flatIterable( source ).cursor();
					final Cursor< O > d = Views.flatIterable( target ).cursor();
					while( c.hasNext() )
					{
						c.next();
						d.next();
						op.convert(c.get(),d.get());
					}
					return null;	
				}
			};
			futures.add( service.submit( r ) );

		}
		for ( final Future< Void > f : futures )
		{
			try
			{
				f.get();
			}
			catch ( final InterruptedException e )
			{
				e.printStackTrace();
			}
			catch ( final ExecutionException e )
			{
				e.printStackTrace();
			}
		}

		service.shutdown();



	}





	public static < T extends NativeType< T > >  void modifyInPlace( 
			final RandomAccessibleInterval< T > img, 
			final Modifier<T> op
			)
	{

		final int numThreads = Runtime.getRuntime().availableProcessors();
		final ExecutorService service = Executors.newFixedThreadPool( numThreads );

		final Interval full = Intervals.expand( img, -1 );
		final int n = img.numDimensions();
		final int splitd = n - 1;


		final int numTasks = numThreads <= 1 ? 1 : ( int ) Math.min( full.dimension( splitd ), numThreads * 20 );
		final long dsize = full.dimension( splitd ) / numTasks;
		final long[] min = new long[ n ];
		final long[] max = new long[ n ];
		full.min( min );
		full.max( max );


		final ArrayList< Future< Void > > futures = new ArrayList< Future< Void > >();

		for ( int taskNum = 0; taskNum < numTasks; ++taskNum )
		{
			min[ splitd ] = full.min( splitd ) + taskNum * dsize;
			max[ splitd ] = ( taskNum == numTasks - 1 ) ? full.max( splitd ) : min[ splitd ] + dsize - 1;
			final RandomAccessibleInterval< T > source = Views.interval( img, new FinalInterval( min, max ) );

			final Callable< Void > r = new Callable< Void >()
			{
				@Override
				public Void call()
				{
					final Cursor< T > c = Views.flatIterable( source ).cursor();
					while( c.hasNext() )
					{
						c.next();
						op.modify(c.get());
					}
					return null;	
				}
			};
			futures.add( service.submit( r ) );
		}
		for ( final Future< Void > f : futures )
		{
			try
			{
				f.get();
			}
			catch ( final InterruptedException e )
			{
				e.printStackTrace();
			}
			catch ( final ExecutionException e )
			{
				e.printStackTrace();
			}
		}

		service.shutdown();

	}



}
