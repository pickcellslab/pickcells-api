package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import net.imglib2.converter.Converter;
import net.imglib2.type.numeric.ARGBType;

public class AnnotationToStatusConverter implements Converter<Annotation,ARGBType> {

	private final static int[] cols = new int[]{		
			ARGBType.rgba(Color.LIGHT_GRAY.getRed(), Color.LIGHT_GRAY.getGreen(), Color.LIGHT_GRAY.getBlue(), 255),						
			ARGBType.rgba(Color.YELLOW.getRed(), Color.YELLOW.getGreen(), 0, 255),
			ARGBType.rgba(255, 0, 0, 255),
			ARGBType.rgba(Color.BLUE.getRed(), Color.BLUE.getGreen(), Color.BLUE.getBlue(), 255),
			ARGBType.rgba(Color.ORANGE.getRed(), Color.ORANGE.getGreen(), Color.ORANGE.getBlue(), 255),
			ARGBType.rgba(Color.WHITE.getRed(), Color.WHITE.getGreen(), Color.WHITE.getBlue(), 255),			
			ARGBType.rgba(Color.GRAY.getRed(), Color.GRAY.getGreen(), Color.GRAY.getBlue(), 255),
			ARGBType.rgba(Color.CYAN.getRed(), Color.CYAN.getGreen(), Color.CYAN.getBlue(), 255),			
			ARGBType.rgba(Color.MAGENTA.getRed(), Color.MAGENTA.getGreen(), Color.MAGENTA.getBlue(), 255),
			ARGBType.rgba(Color.PINK.getRed(), Color.PINK.getGreen(), Color.PINK.getBlue(), 255),
			ARGBType.rgba(Color.GREEN.getRed(), Color.GREEN.getGreen(), Color.GREEN.getBlue(), 255) 
	};

	private final Map<AnnotationStatus, Integer> apps;


	public AnnotationToStatusConverter(AnnotationManager mgr) {
		apps = new HashMap<>();
		mgr.possibleAnnotationStates().forEach(a->{
			apps.put(a, a.preferredARGB());
		});
	}

	@Override
	public void convert(Annotation input, ARGBType output) {
		Integer value = apps.get(input.status());
		if(value==null){
			value = (apps.size())%cols.length;
			apps.put(input.status(), cols[value]);
		}
		output.set(value);		
	}

	

}
