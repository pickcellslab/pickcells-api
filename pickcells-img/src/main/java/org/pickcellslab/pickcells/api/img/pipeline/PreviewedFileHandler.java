package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.function.Function;

import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;

class PreviewedFileHandler<I,O extends Type<O>> implements InputsHandler<I, O, FileInput<I>> {

	private final ImgFileList list;
	private final int[] prevCoords;
	private MinimalImageInfo prevImgInfo;
	private RandomAccessibleInterval<I> prevImg;
	private MinimalImageInfo prevResultInfo;
	private RandomAccessibleInterval<O> prevResult;
	private final boolean wasCropped;


	public PreviewedFileHandler(ImgFileList list, int[] prevCoords, MinimalImageInfo prevImgInfo, RandomAccessibleInterval<I> prevImg, MinimalImageInfo prevResultInfo, RandomAccessibleInterval<O> prevResult, boolean wasCropped){
		this.list = list;
		this.prevCoords = prevCoords;
		this.prevImgInfo = prevImgInfo;
		this.prevImg = prevImg;
		this.prevResultInfo = prevResultInfo;
		this.prevResult = prevResult;
		this.wasCropped = wasCropped;
	}



	@Override
	public <E extends Type<E>> InputsHandler<I, E, FileInput<I>> addStep(Function<RandomAccessibleInterval<O>, RandomAccessibleInterval<E>> step) {
		if(!wasCropped)// Add step only if we need to compute the pipeline for the original image
			return new PreviewedFileHandler<>(list, prevCoords, null, null, prevResultInfo, step.apply(prevResult), wasCropped);
		else
			return new PreviewedFileHandler<>(list, prevCoords, prevImgInfo, prevImg, null, null, wasCropped);
	}

	
	
	


	@Override
	public String nameOfPreview() {
		return list.name(prevCoords[0], prevCoords[1]);
	}
	
	


	@SuppressWarnings("unchecked")
	@Override
	public ExtendedImageInfo<O> getPreviewedResult(ProcessingStep<I, RandomAccessibleInterval<O>> pipeline, ImgIO io,
			Function<String, String> namingConvention, ImgWriteManagerFactory<O> fctry,
			ProcessingRunner<I, O, FileInput<I>> runner, O outType) throws IOException {

		final File outFolder = new File(list.path(prevCoords[0]));
		String name = list.name(prevCoords[0], prevCoords[1]);


		name = formatName(name);


		ExtendedImageInfo<O> result;
		if(wasCropped){
			System.out.println("Running on preview image");			
			FileInput<I> input = new FileInput<>(prevImg, prevImgInfo, name, outFolder);			
			result = runner.run(pipeline, io, input, outType, outFolder, namingConvention.apply(name), fctry);
		}
		else{//save prevResult
			final String resultName = namingConvention.apply(name);
			final String ext = io.standard((RandomAccessibleInterval)prevResult);
			io.save(prevResultInfo, (RandomAccessibleInterval)prevResult, outFolder.getPath()+File.separator+resultName+ext);
			result = new ExtendedImageInfo<>(prevResultInfo, outFolder, name, ext, outType);
		}
		prevImg=null;	prevImgInfo = null;		prevResult=null;		prevResultInfo=null;	
		return result;		
	}



	private String formatName(String name) {
		
		String n = name;
		
		//Remove file extension
		int index = name.lastIndexOf('.');
		if(index!=-1)
			n = name.substring(0, index);

		//Replace illegal characters in the name
		n = n.replace("\\", "_");
		n = n.replace("/", "_");
		n = n.replace(" ", "_");

		return n;
		
	}



	@Override
	public Iterator<FileInput<I>> iterator() {
		return new Iterator<FileInput<I>>(){

			private File folder = new File(list.path(0));
			private int i=0, j=-1;

			@Override
			public boolean hasNext() {
				if(++j<list.numImages(i))
					if(i == prevCoords[0] && j == prevCoords[1])
						return hasNext();
					else
						return true;
				if(++i<list.numDataSets()){
					j=0;	folder = new File(list.path(i));
					if(i == prevCoords[0] && j == prevCoords[1])
						return hasNext();
					else
						return true;
				}					
				return false;
			}

			@Override
			public FileInput<I> next() {
				String name = list.name(i, j);
				name = formatName(name);				
				MinimalImageInfo info = list.getMinimalInfo(i, j);
				return new FileInput(list.load(i, j), info, name, folder);
			}

		};
	}




}
