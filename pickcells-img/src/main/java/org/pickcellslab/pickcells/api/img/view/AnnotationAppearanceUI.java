package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;

import org.pickcellslab.foundationj.ui.utils.CardPanel;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;

@SuppressWarnings("serial")
public class AnnotationAppearanceUI extends JComponent{

	private final AnnotationAppearanceControlManager manager;


	public AnnotationAppearanceUI(AnnotationAppearanceControlManager mgr) {

		this.manager = mgr;

		final JComboBox<AnnotationAppearanceControl> modelsBox = new JComboBox<>();
		for(int m = 0; m<mgr.numModels(); m++)
			modelsBox.addItem(mgr.getControl(m));

		final CardPanel<AnnotationAppearanceControl, JComponent> cards = new CardPanel<>(model->model.getUI());

		cards.show(modelsBox.getItemAt(0));

		modelsBox.addActionListener(l->{
			if(mgr.getActiveControl()!=modelsBox.getItemAt(modelsBox.getSelectedIndex())){
				System.out.println("AnnotationAppearanceUI : modelsBox changed");
				mgr.setActiveControl(modelsBox.getSelectedIndex());
				System.out.println("AnnotationAppearanceUI : controls changed in mgr");
			}
		});
		modelsBox.addActionListener(l->cards.show((AnnotationAppearanceControl) modelsBox.getSelectedItem()));

		mgr.addAppearanceControlChangeListener(l->modelsBox.setSelectedItem(mgr.getActiveControl()));


		this.setLayout(new VerticalFlowLayout(10));
		this.add(new GroupPanel(10, new JLabel("Appearance Mode:"), modelsBox));
		this.add(cards);

	}


	public AnnotationAppearanceControlManager getModelManager(){
		return manager;
	};

}
