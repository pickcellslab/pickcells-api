package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessingPackage;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;
import org.pickcellslab.pickcells.api.img.pipeline.Skip;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.AbstractNeighborhood;
import net.imglib2.converter.Converter;
import net.imglib2.img.Img;
import net.imglib2.outofbounds.OutOfBoundsFactory;
import net.imglib2.outofbounds.OutOfBoundsMirrorFactory;
import net.imglib2.outofbounds.OutOfBoundsMirrorFactory.Boundary;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;


/**
 * An {@link ImgProcessingPackage} for binary morphology operation.
 * TODO migrate to T.Boudier separable kernel algorithms when released
 * 
 * @author Guillaume Blin
 *
 */
public class BinaryMorphology implements ImgProcessingPackage<BitType, BitType> {


	private final ImgIO io;
	@SuppressWarnings("rawtypes")
	private final ImgProcessing[] data;



	public BinaryMorphology(ImgIO io) {
		this.io = io;

		data = new ImgProcessing[] {
				new MorphProcessing("Open", openingValidate(), opening(), "Erode and then Dilate"),
				new MorphProcessing("Close", closingValidate(), closing(), "Dilate and then Erode"),
				new MorphProcessing("Dilate", dilationValidate(), dilation(), "Dilation"),
				new MorphProcessing("Erode", erosionValidate(), erosion(), "Erosion"),
				new Invert(),
				new Skip<>(io, new BitType(), DimensionalityEffect.FLAT_TO_FLAT),
		};

	}



	@SuppressWarnings("unchecked")
	@Override
	public ImgProcessing<BitType, BitType>[] processors() {
		return data;
	}




	private BiFunction<Integer, ImageBag<BitType>, RandomAccessibleInterval<BitType>> opening(){
		return (count, bag) ->{


			RandomAccessibleInterval<BitType> input = bag.getInputFrame(0);
			long[] radii = new long[input.numDimensions()];
			Arrays.fill(radii, count);

			OutOfBoundsFactory<BitType,RandomAccessibleInterval<BitType>> strategy = new OutOfBoundsMirrorFactory<BitType,RandomAccessibleInterval<BitType>>(Boundary.SINGLE);
			HyperEllipseFactory<BitType> factory = new HyperEllipseFactory<>(radii, strategy);
			RandomAccessibleInterval<BitType> r = Operations.run(io, input, factory, erode());
			r =  Operations.run(BinaryMorphology.this.io, r, factory, dilate());

			return r;
		};
	}


	private BiFunction<Integer, PreviewBag<BitType>, RandomAccessibleInterval<BitType>> openingValidate (){
		return (count, bag) ->{
			RandomAccessibleInterval<BitType> input = bag.getInput();
			long[] radii = new long[input.numDimensions()];
			Arrays.fill(radii, count);

			if(bag.getInputInfo().dimension(Image.t)>1)
				radii[bag.getInputInfo().index(Image.t)]=0;


			OutOfBoundsFactory<BitType,RandomAccessibleInterval<BitType>> strategy = new OutOfBoundsMirrorFactory<BitType,RandomAccessibleInterval<BitType>>(Boundary.SINGLE);
			HyperEllipseFactory<BitType> factory = new HyperEllipseFactory<>(radii, strategy);
			RandomAccessibleInterval<BitType> r = Operations.run(io, input, factory, erode());
			r =  Operations.run(io, r, factory, dilate());

			return r;
		};
	}



	private final BiFunction<Integer, ImageBag<BitType>, RandomAccessibleInterval<BitType>> closing(){
		return (count, bag) ->{
			RandomAccessibleInterval<BitType> input = bag.getInputFrame(0);
			long[] radii = new long[input.numDimensions()];
			Arrays.fill(radii, count);

			OutOfBoundsFactory<BitType,RandomAccessibleInterval<BitType>> strategy = new OutOfBoundsMirrorFactory<BitType,RandomAccessibleInterval<BitType>>(Boundary.SINGLE);
			HyperEllipseFactory<BitType> factory = new HyperEllipseFactory<>(radii, strategy);
			RandomAccessibleInterval<BitType> r = Operations.run(io, input, factory, dilate());
			r =  Operations.run(io, r, factory, erode());

			return r;
		};
	}

	private BiFunction<Integer, PreviewBag<BitType>, RandomAccessibleInterval<BitType>> closingValidate(){
		return (count, bag) ->{

			RandomAccessibleInterval<BitType> input = bag.getInput();
			long[] radii = new long[input.numDimensions()];
			Arrays.fill(radii, count);

			if(bag.getInputInfo().dimension(Image.t)>1)
				radii[bag.getInputInfo().index(Image.t)]=0;

			OutOfBoundsFactory<BitType,RandomAccessibleInterval<BitType>> strategy = new OutOfBoundsMirrorFactory<BitType,RandomAccessibleInterval<BitType>>(Boundary.SINGLE);
			HyperEllipseFactory<BitType> factory = new HyperEllipseFactory<>(radii, strategy);
			RandomAccessibleInterval<BitType> r = Operations.run(io, input, factory, dilate());
			r =  Operations.run(io, r, factory, erode());

			return r;
		};
	}


	private BiFunction<Integer, ImageBag<BitType>, RandomAccessibleInterval<BitType>> dilation(){
		return (count, bag) ->{

			RandomAccessibleInterval<BitType> source = bag.getInputFrame(0);
			long[] radii = new long[source.numDimensions()];
			Arrays.fill(radii, count);

			OutOfBoundsFactory<BitType,RandomAccessibleInterval<BitType>> strategy = new OutOfBoundsMirrorFactory<BitType,RandomAccessibleInterval<BitType>>(Boundary.SINGLE);
			HyperEllipseFactory<BitType> factory = new HyperEllipseFactory<>(radii, strategy);
			RandomAccessibleInterval<BitType> r = Operations.run(io, source, factory, dilate());

			return r;
		};
	}

	private BiFunction<Integer, PreviewBag<BitType>, RandomAccessibleInterval<BitType>> dilationValidate(){
		return (count, bag) ->{

			RandomAccessibleInterval<BitType> source = bag.getInput();
			long[] radii = new long[source.numDimensions()];
			Arrays.fill(radii, count);
			if(bag.getInputInfo().dimension(Image.t)>1)
				radii[bag.getInputInfo().index(Image.t)]=0;

			OutOfBoundsFactory<BitType,RandomAccessibleInterval<BitType>> strategy = new OutOfBoundsMirrorFactory<BitType,RandomAccessibleInterval<BitType>>(Boundary.SINGLE);
			HyperEllipseFactory<BitType> factory = new HyperEllipseFactory<>(radii, strategy);
			RandomAccessibleInterval<BitType> r = Operations.run(io, source, factory, dilate());

			return r;
		};
	}


	private BiFunction<Integer, ImageBag<BitType>, RandomAccessibleInterval<BitType>> erosion(){
		return (count, bag) ->{

			RandomAccessibleInterval<BitType> source = bag.getInputFrame(0);
			long[] radii = new long[source.numDimensions()];
			Arrays.fill(radii, count);

			OutOfBoundsFactory<BitType,RandomAccessibleInterval<BitType>> strategy = new OutOfBoundsMirrorFactory<BitType,RandomAccessibleInterval<BitType>>(Boundary.SINGLE);
			HyperEllipseFactory<BitType> factory = new HyperEllipseFactory<>(radii, strategy);
			RandomAccessibleInterval<BitType> r = Operations.run(io, source, factory, erode());

			return r;
		};
	}

	private BiFunction<Integer, PreviewBag<BitType>, RandomAccessibleInterval<BitType>> erosionValidate(){
		return (count, bag) ->{

			RandomAccessibleInterval<BitType> source = bag.getInput();
			long[] radii = new long[source.numDimensions()];
			Arrays.fill(radii, count);

			if(bag.getInputInfo().dimension(Image.t)>1)
				radii[bag.getInputInfo().index(Image.t)]=0;

			OutOfBoundsFactory<BitType,RandomAccessibleInterval<BitType>> strategy = new OutOfBoundsMirrorFactory<BitType,RandomAccessibleInterval<BitType>>(Boundary.SINGLE);
			HyperEllipseFactory<BitType> factory = new HyperEllipseFactory<>(radii, strategy);
			RandomAccessibleInterval<BitType> r = Operations.run(io, source, factory, erode());

			return r;
		};
	}




	public class Invert implements ImgProcessing<BitType,BitType>{


		private PreviewBag<BitType> input;



		@Override
		public String description() {
			return "Returns the negative of the image";
		}


		@Override
		public String name() {
			return "Invert";
		}

		@SuppressWarnings("serial")
		@Override
		public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<BitType> input) {

			this.input = input;

			return new ValidityTogglePanel(){


				@Override
				public boolean validity() {
					return true;
				}

			};
		}


		@Override
		public void changeInput(PreviewBag<BitType> current) {
			this.input = current;			
		}


		@Override
		public boolean repeatable() {
			return false;
		}

		@Override
		public DimensionalityEffect dimensionalityEffect() {
			return DimensionalityEffect.FLAT_TO_FLAT;
		}




		@Override
		public boolean requiresAnnotationChannel() {
			return false;
		}


		@Override
		public RandomAccessibleInterval<BitType> validate() {


			Img<BitType> target = ImgDimensions.copy(io, input.getInput());
			RandomAccess<BitType> a = input.getInput().randomAccess();	
			Cursor<BitType> c = Views.iterable(target).localizingCursor();

			while(c.hasNext()){
				c.next();
				a.setPosition(c);
				if(a.get().get()){
					c.get().setZero();
				}
				else{
					c.get().setOne();
				}
			}

			return target;
		}



		@Override
		public Function<ImageBag<BitType>, RandomAccessibleInterval<BitType>> toPipe() {

			return (bag) ->{
				RandomAccessibleInterval<BitType> input = bag.getInputFrame(0);
				Img<BitType> target = ImgDimensions.copy(io, input);
				RandomAccess<BitType> a = input.randomAccess();			
				Cursor<BitType> c = Views.iterable(target).localizingCursor();

				while(c.hasNext()){
					c.next();
					a.setPosition(c);
					if(a.get().get())
						c.get().setZero();
					else
						c.get().setOne();
				}

				return target;
			};
		}


		@Override
		public void stop() {
			// TODO Auto-generated method stub

		}


		@Override
		public boolean requiresOriginal() {
			return false;
		}


		@Override
		public boolean requiresFlat() {
			return false;
		}


		@Override
		public BitType outputType(BitType inputType) {
			return inputType;
		}



	}









	private class MorphProcessing implements ImgProcessing<BitType,BitType>{


		private final BiFunction<Integer, PreviewBag<BitType>, RandomAccessibleInterval<BitType>> vOp;
		private final BiFunction<Integer, ImageBag<BitType>, RandomAccessibleInterval<BitType>> pOp;
		private final String name;		
		private final String description;
		private int count = 1;

		private PreviewBag<BitType> input;

		@SuppressWarnings("serial")
		private ValidityTogglePanel dialog =
		new ValidityTogglePanel(){

			{
				add(new JLabel("Radius "));
				SpinnerNumberModel model = new SpinnerNumberModel(1,1,20,1);
				add(new JSpinner(model));
				model.addChangeListener(l->setRadius(model.getNumber().intValue()));
			}

			@Override
			public boolean validity() {
				return true;
			}

			private void setRadius(int intValue) {
				count = intValue;
			}

		};



		private MorphProcessing(String name, BiFunction<Integer, PreviewBag<BitType>, RandomAccessibleInterval<BitType>> vOp, BiFunction<Integer, ImageBag<BitType>, RandomAccessibleInterval<BitType>> pOp, String description){
			this.name = name;
			this.description = description();
			this.pOp = pOp;
			this.vOp = vOp;
		}


		@Override
		public String name() {
			return name;
		}


		@Override
		public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<BitType> input){
			this.input = input;
			return dialog;
		}


		@Override
		public void changeInput(PreviewBag<BitType> current) {
			this.input = current;			
		}


		@Override
		public String description() {
			return description;
		}

		@Override
		public boolean repeatable() {
			return true;
		}


		@Override
		public Function<ImageBag<BitType>, RandomAccessibleInterval<BitType>> toPipe() {
			return (bag) -> pOp.apply(count, bag);
		}


		@Override
		public DimensionalityEffect dimensionalityEffect() {
			return DimensionalityEffect.FLAT_TO_FLAT;
		}


		@Override
		public RandomAccessibleInterval<BitType> validate() {
			return vOp.apply(count, input);
		}


		@Override
		public boolean requiresAnnotationChannel() {
			return false;
		}


		@Override
		public void stop() {
			//TODO
		}


		@Override
		public boolean requiresOriginal() {
			return false;
		}


		@Override
		public boolean requiresFlat() {
			return false;
		}


		@Override
		public BitType outputType(BitType inputType) {
			return inputType;
		}

	}












	public static NeighborhoodFunction<BitType> dilate(){
		return new NeighborhoodFunction<BitType>(){

			BitType t = new BitType(true);
			BitType f = new BitType(false);

			@Override
			public BitType apply(AbstractNeighborhood<BitType> n, BitType center) {

				if(center.get())
					return t;

				Cursor<BitType> c = (Cursor<BitType>) n.cursor().copy();			

				while(c.hasNext()){
					c.next();
					if(c.get().get())
						return t;
				}
				return f;
			}

		};
	}













	public static NeighborhoodFunction<BitType> erode(){
		return new NeighborhoodFunction<BitType>(){

			@Override
			public BitType apply(AbstractNeighborhood<BitType> t, BitType center) {

				if(!center.get())
					return center;

				Cursor<BitType> c = t.cursor();			
				while(c.hasNext()){
					c.next();
					if(!c.get().get())
						return c.get();
				}
				return center;
			}

		};
	}










	public static <T extends NativeType<T> & RealType<T>> Modifier<T> invertInPlace(T min, T max){
		return t -> t.setReal(max.getRealDouble()- (t.getRealDouble() - min.getRealDouble()));
	}




	public static <T extends NativeType<T> & RealType<T>> Converter<T,T> invertConverter(T min, T max){
		return new Converter<T,T>(){

			@Override
			public void convert(T input, T output) {
				output.setReal(max.getRealDouble()- (input.getRealDouble() - min.getRealDouble()));				
			}

		};
	}



}
