package org.pickcellslab.pickcells.api.img.io;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.function.Predicate;

import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.img.Img;
import net.imglib2.type.NativeType;

/**
 * An ImgFileList handles and provides information about groups of image files. 
 * 
 * @author Guillaume Blin
 *
 */
public interface ImgFileList {

	/**
	 * @return The number of images in this ImgFileList
	 */
	public int numImages();
		
	/**
	 * @param i The dataset index in this ImgFileList
	 * @return The number of Images in the specified dataset
	 */
	public int numImages(int i);	
	
	/**
	 * @return The number of datasets (images groups) in this ImgFileList
	 */
	public int numDataSets();
	
	/**
	 * @param dataset The index of the desired dataset
	 * @return The path for the specified dataset index. Note that this might be the
	 * path to a folder containing the image files or to an image database.
	 */
	public String path(int dataset);
	
	/**
	 * @param dataset The index of the desired dataset.
	 * @return The name of the desired dataset
	 */
	public String name(int dataset);
	
	/**
	 * @param dataset The index of the dataset in this ImgFileList
	 * @param image The index of the image in the specified dataset
	 * @return The name of the desired image (including extension)
	 */
	public String name(int dataset, int image);
	
	/**
	 * @param dataset The name of the dataset in this ImgFileList
	 * @param image The index of the image in the specified dataset
	 * @return The name of the desired image
	 */
	public String name(String dataset, int image);
	
	/**
	 * Loads a specific image
	 * @param dataset The index of the dataset in this ImgFileList
	 * @param image The index of the image in the specified dataset
	 * @return The Img corresponding to the specified coordinates in this ImgFileList
	 */
	public <T extends NativeType<T>> Img<T> load(int dataset, int image);
	
	/**
	 * Loads a specific image
	 * @param dataset The name of the dataset in this ImgFileList
	 * @param image The index of the image in the specified dataset
	 * @return The Img corresponding to the specified coordinates in this ImgFileList
	 */
	public <T extends NativeType<T>> Img<T> load(String dataset, int image);

	
	public <T extends NativeType<T>> T getType(int dataset, int image);
	
	/**
	 * Resample this ImgFileList to create a new ImgFileList. Note that the indices will not be conserved
	 * only the natural ordering will be, in other words, indices will start from 0 and augment up to
	 * the new {@link #numImages()}.
	 * @param dataset The dataset to sample
	 * @param images A {@link Predicate} indicating which image to sample
	 * @return An ImgFileList containing the defined Image subset
	 */
	public ImgFileList subList(int dataset, Predicate<Integer> images);
	
	
	/**
	 * Merges this ImgFileList with another. If datasets in the 2 list have equal names, then these datasets will be
	 * merged.
	 * @param other The other ImgFileList (will remain unmodified)
	 */
	public void merge(ImgFileList other);
	
	
	
	/**
	 * Create a {@link MinimalImageInfo} object for the desired image
	 * @param i dataset coordinate
	 * @param j Image coordinate in dataset i
	 * @return The desired {@link MinimalImageInfo} object.
	 */
	public MinimalImageInfo getMinimalInfo(int i, int j);
	
	
	
	/**
	 * Creates an {@link ImgWriter} pointing a a specific image file from this ImgFileList which can be used to overwrite 
	 * planes of the image file on the disk
	 * @param i dataset coordinate
	 * @param j Image coordinate in dataset i
	 * @return An {@lin ImgWriter} for the desired Image
	 * @throws IOException 
	 */
	public <T extends NativeType<T>> ImgWriter<T> createWriterHandle(int i, int j) throws IOException; 
	
	
	
}
