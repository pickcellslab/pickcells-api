package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Takes care of various mouse related events such as tooltips to be shown over images when the user clicks on it
 * or rotations and scaling of the image. Can relay information to {@link MouseDisplayEventListener}
 * 
 * @author Guillaume Blin
 *
 */
public interface MouseDisplayEventModel {

	public enum SelectionMode{
		NONE, SINGLE, MULTIPLE
	}
	
	
	/**
	 * A convenience method to add a {@link MouseDisplayEventListener} while removing a listener which was previously set
	 * via this method
	 * @param l The {@link MouseDisplayEventListener} to set, null accepted in which case the method simply removes the previous listener.
	 * @return The {@link MouseDisplayEventListener} previously set via this method or null if none were previously set.
	 */
	public MouseDisplayEventListener setControl(MouseDisplayEventListener l);
	
	public void addListener(MouseDisplayEventListener l);
	
	public void removeListener(MouseDisplayEventListener l);
	
	public void setVoxelTooltipsEnabled(int imgIndex, boolean enabled);

	public void setRotationEnabled(boolean enabled);
	
	public void setSelectableAnnotationLayer(int i, boolean clearPrevious);
	
	public void setSelectionMode(SelectionMode mode);
	
}
