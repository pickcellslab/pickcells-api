package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Function;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.pde.PeronaMalikAnisotropicDiffusion;
import net.imglib2.converter.RealFloatConverter;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImgFactory;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.view.Views;

public class AnisotropicDiffusion<T extends RealType<T>> implements ImgProcessing<T,FloatType>{





	private final double d = 0.02, k = 30;
	private final int itr = 3;
	private PeronaMalikDialog dialog;
	private PreviewBag<T> input;

	private final ImgIO io;
	
	public AnisotropicDiffusion(ImgIO io) {
		this.io = io;
	}
	
	
	



	@Override
	public String description() {
		return "<HTML>"
				+ "This algorithm implements the so-called anisotropic diffusion scheme of Perona & Malik, 1990,"
				+ "<br>with imglib. For details on the anisotropic diffusion principles, see "
				+ " <br><a href=\"http://en.wikipedia.org/wiki/Anisotropic_diffusion\">http://en.wikipedia.org/wiki/Anisotropic_diffusion</a>" 
				+ "<br>and the original paper:"
				+ " <pre>"
				+"\nPerona and Malik." 
				+"\nScale-Space and Edge Detection Using Anisotropic Diffusion. "
				+"\nIEEE Transactions on Pattern Analysis and Machine Intelligence (1990) vol. 12 pp. 629-639"
				+" </pre></HTML";

	}



	@Override
	public String name() {
		return "Anisotropic Diffusion";
	}

	@Override
	public boolean repeatable() {
		return false;
	}

	@Override
	public DimensionalityEffect dimensionalityEffect() {
		return DimensionalityEffect.FLAT_TO_FLAT;
	}



	@Override
	public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<T> input) {

		this.input = input;

		if(dialog == null){
			dialog = new PeronaMalikDialog(d,k,itr);
		}
		return dialog;
	}

	@Override
	public void changeInput(PreviewBag<T> current) {
		this.input = current;			
	}


	@Override
	public RandomAccessibleInterval<FloatType> validate() {


		Img<FloatType> result = ImgDimensions.hardCopy(io, input.getInput(), new RealFloatConverter<T>(), new FloatType());
		if(input.getInputInfo().dimension(Image.t)==1)
			PeronaMalikAnisotropicDiffusion.inFloatInPlace(result, dialog.delta(), dialog.kappa());
		else{			
			for(int t = 0; t<input.getInputInfo().dimension(Image.t); t++){
				RandomAccessibleInterval<FloatType> frame = Views.hyperSlice(result, input.getInputInfo().index(Image.t), t);
				PeronaMalikAnisotropicDiffusion.inFloatInPlace(frame, new ArrayImgFactory<>(), dialog.delta(), dialog.kappa());
			}
		}


		return result;

	}






	private Function<ImageBag<T>, RandomAccessibleInterval<FloatType>> toPipe(double d, double k){


		return (bag)->{			
			Img<FloatType> result = ImgDimensions.hardCopy(io, bag.getInputFrame(0), new RealFloatConverter<T>(), new FloatType());
			PeronaMalikAnisotropicDiffusion.inFloatInPlace(result, d, k);
			return result;
		};		

	}


	@Override
	public Function<ImageBag<T>, RandomAccessibleInterval<FloatType>> toPipe() {
		return toPipe(dialog.delta(), dialog.kappa());
	}

	@Override
	public boolean requiresAnnotationChannel() {
		return false;
	}



	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}



	@Override
	public boolean requiresOriginal() {
		return false;
	}



	@Override
	public boolean requiresFlat() {
		return false;
	}



	@Override
	public FloatType outputType(T inputType) {
		return new FloatType();
	}









}
