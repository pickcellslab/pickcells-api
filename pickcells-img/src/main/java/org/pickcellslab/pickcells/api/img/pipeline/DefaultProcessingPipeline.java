package org.pickcellslab.pickcells.api.img.pipeline;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.ProgressFactory;
import org.pickcellslab.foundationj.services.ProgressPanel;
import org.pickcellslab.foundationj.services.TaskProgress;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;


/**
 * 
 * The default implementation of {@link ProcessingPipeline}: Runs the pipeline on each input image sequentially
 * and displays a {@link ProgressPanel} to enable the user to follow the progress and abort computation if desired. 
 *
 * @author Guillaume Blin
 *
 * @param <T> The type of the input image
 * @param <R> The type of the result image
 * @param <P> The type of {@link ProcessingInput} required for 
 */
public class DefaultProcessingPipeline 

<T extends NativeType<T> & RealType<T>,
R extends NativeType<R> & RealType<R>,
P extends ProcessingInput<T>>

implements ProcessingPipeline<T,R> {



	//private final Logger log = LoggerFactory.getLogger(DefaultProcessingPipeline.class);

	private final UITheme theme;
	private final NotificationFactory notif;
	
	private final ProcessingStep<T, RandomAccessibleInterval<R>> pipeline;
	private final R outputType;
	private final ImgWriteManagerFactory<R> fctry;
	private final ProcessingRunner<T,R,P> runner;
	private final InputsHandler<T,R,P> prevHandler;



	public DefaultProcessingPipeline(UITheme theme, NotificationFactory notif, ProcessingStep<T, RandomAccessibleInterval<R>> pipeline,	R outputType, ProcessingRunner<T,R,P> tm, ImgWriteManagerFactory<R> fctry, InputsHandler<T,R,P> prevHandler) {
		this.notif = notif;
		this.theme = theme;
		this.pipeline = pipeline;
		this.outputType = outputType;
		this.fctry = fctry;
		this.runner = tm;
		this.prevHandler = prevHandler;
	}






	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <E extends NativeType<E> & RealType<E>> ProcessingPipeline<T,E> appendStep(Function<RandomAccessibleInterval<R>,RandomAccessibleInterval<E>> step, E outputType){
		return new DefaultProcessingPipeline<T,E,P>(theme, notif, pipeline.andThen(step), outputType, (ProcessingRunner)runner, (ImgWriteManagerFactory)fctry, prevHandler.addStep(step));
	}


	//TODO 
	//public void toXML(File file){}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ProgressPanel<ExtendedImageInfo<R>> runPipeline(ImgIO io, Function<String,String> namingConvention){

		// Get a progress factory
		final ProgressFactory<?, ExtendedImageInfo<R>> pFctry = notif.newProgressFactory();
		
		// Create a list of all tasks
		final List<TaskProgress<ExtendedImageInfo<R>>> list = new ArrayList<>();
		
		
		// Create a task for the previewed image
		final TaskProgress<ExtendedImageInfo<R>> progress = pFctry.newTaskProgress(prevHandler.nameOfPreview(), 
				() -> prevHandler.getPreviewedResult(pipeline, io, namingConvention, fctry, runner, outputType));
				
		// Add the task to the list
		list.add(progress);
		
		
		// Now add all inputs as new tasks
		Iterator<P> inputs = prevHandler.iterator();
		while(inputs.hasNext()){
			
			P input = inputs.next();
			
			final TaskProgress<ExtendedImageInfo<R>> prog = pFctry.newTaskProgress(input.getName(), 
					() -> runner.run(pipeline, io, input, outputType, input.getFolder(), namingConvention.apply(input.getName()), fctry));
			
			// Add the task to the list
			list.add(prog);
			
		}
		
		return pFctry.progressPanel(theme, (List)list);
		
	}



}
