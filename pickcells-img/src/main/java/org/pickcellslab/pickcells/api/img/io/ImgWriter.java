package org.pickcellslab.pickcells.api.img.io;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;

/**
 * Created by an {@link ImgIO}, an {@link ImgWriter} allows to write an Image to disk sequentially, 1 channel
 * and 1 frame at a time. This is useful to save the result of some operations on an image, especially long time lapses
 * which do not fit in RAM. 
 * 
 * @author Guillaume Blin
 *
 * @param <T> The {@link Type} of the image to be created
 */
public interface ImgWriter<T> {

	
	
	/**
	 * @return The number of z slices in the image to be written
	 */
	public int sliceNumber();
	
	/**
	 * @return The number of channels in the image to be written
	 */
	public int channelNumber();
	
	/**
	 * @return The number of frames in the image to be written
	 */
	public int frameNumber();		
		
	/**
	 * @return a blank {@link RandomAccessibleInterval} corresponding to an hyperslice of the image to write for 1 channel and 1 time point
	 */
	public RandomAccessibleInterval<T> createNewChannelFrame(); 
	
	/**
	 * Writes the given {@link RandomAccessibleInterval} to file at the given location
	 * @param channelFrame the hyperslice to write to file (same dimensionality as {@link #createNewChannelFrame()})
	 * @param channel The channel coordinate of the hyperslice
	 * @param frame The time coordinate of the hyperslice.
	 * @throws IOException
	 */
	public void writeChannelFrame(RandomAccessibleInterval<T> channelFrame, int channel, int frame) throws IOException;
	
	/**
	 * Writes the given {@link RandomAccessibleInterval} to file at the given location
	 * @param plane the hyperslice to write to file
	 * @param slice The z coordinate of the hyperslice
	 * @param channel The channel coordinate of the hyperslice
	 * @param frame The time coordinate of the hyperslice.
	 * @throws IOException
	 */
	public void writePlane(RandomAccessibleInterval<T> plane, int slice, int channel, int frame) throws IOException;
	
		
	/**
	 * Closes this ImgWriter and releases resources.
	 */
	public void close();
}
