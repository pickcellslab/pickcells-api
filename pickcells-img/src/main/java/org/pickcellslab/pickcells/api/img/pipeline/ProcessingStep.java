package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.function.Function;

import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.RandomAccessibleInterval;

/**
 * A step in a {@link ProcessingPipeline}
 * 
 * @author Guillaume Blin
 *
 * @param <I> The type of the input {@link RandomAccessibleInterval}
 * @param <O> the type of the output
 */
@FunctionalInterface
public interface ProcessingStep<I,O> {

	/**
	 * Applies this step on a specific frame of the input image and returns a result
	 * @param input The input image
	 * @param info The associated information about the image
	 * @param frame The specific frame on which this step should be applied
	 * @return The result
	 */
	public O apply(RandomAccessibleInterval<I> input, MinimalImageInfo info, long frame);
	
	/**
	 * Appends a {@link Function} to the output of this {@link ProcessingStep}
	 * @param after The {@link Function} to be applied after this step
	 * @return The modified {@link ProcessingStep}
	 */
	default <V> ProcessingStep<I, V> andThen(Function<O,V> after) {
        Objects.requireNonNull(after);        
        return (input, info, frame) -> after.apply(apply(input, info, frame));
    }
	
}
