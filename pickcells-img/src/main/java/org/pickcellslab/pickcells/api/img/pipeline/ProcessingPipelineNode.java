package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.foundationj.ui.wizard.WizardLeaf;
import org.pickcellslab.foundationj.ui.wizard.WizardNode;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.view.ImageViewer;
import org.pickcellslab.pickcells.api.img.view.ImgProcessingRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.laf.filechooser.WebFileChooser;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;
import com.alee.managers.tooltip.TooltipManager;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;




@SuppressWarnings({ "rawtypes", "serial" })
public final class ProcessingPipelineNode<T extends NativeType<T> & RealType<T>, V extends NativeType<V> & RealType<V>> extends WizardLeaf<ProcessingStep<T, RandomAccessibleInterval<V>>> {

	private static final Logger log = LoggerFactory.getLogger(ProcessingPipelineNode.class);

	//Wizard fields validity handling
	private boolean isValid = false;
	/**
	 * Number of steps this node has added to the pipeline
	 */
	private int added; 


	final List<List<ImgProcessing>> steps;
	final List<String> descriptions;
	final int index;

	final LinkedList<ImgProcessing> pipeline;


	//Images references, display, original and current result
	final ImageViewer<T> view;
	final PreviewState<T> state;

	private boolean requiresOrigin;
	private boolean requiresFirstFlat;

	private final PreviewBag<V> original; 

	private PreviewBag current;

	private final ImgIO io;
	private final UITheme theme;
	private final NotificationFactory notifier;






	@SuppressWarnings("unchecked")
	ProcessingPipelineNode(
			WizardNode parent,			
			int nodeDepth,
			ImgIO io,
			UITheme theme,
			NotificationFactory notifier,
			PreviewState<T> state,
			ImageViewer<T> view,
			PreviewBag<V> originalImg,
			LinkedList<ImgProcessing> choices,
			List<List<ImgProcessing>> steps,
			List<String> descriptions
			) {



		super(parent);

		this.io = io;
		this.theme = theme;
		this.notifier = notifier;
		this.index = nodeDepth;
		this.original = originalImg;
		this.view = view;
		this.state = state;
		this.pipeline = choices;
		this.steps = steps;
		this.descriptions = descriptions;





		// Create current as a copy of original
		this.current = original.createWith(originalImg.getInputInfo(), ImgDimensions.copy(io, originalImg.getInput()));




		this.setLayout(new BorderLayout());

		// Add a panel on the left for methods 
		JPanel panel = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));





		// ====================================   Panel Describing available method for this node  =======================================





		JPanel methodPanel = new JPanel();
		methodPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(184, 207, 229), 2), descriptions.get(nodeDepth)));
		panel.add(methodPanel, BorderLayout.NORTH);

		JLabel lblAvailableMethods = new JLabel("Available Methods");



		JComboBox<ImgProcessing<?,?>> methodBox = new JComboBox<>();
		methodBox.setRenderer(new ImgProcessingRenderer());

		for(ImgProcessing<?,?> p :steps.get(nodeDepth))
			methodBox.addItem(p); 



		JLabel lblSaveTheCurrent = new JLabel("Save the current preview");

		JButton btnSave = new JButton(theme.icon(IconID.Files.SAVE, 16));



		//   ==================================== Panel for applying, reseting and visualising chosen methods  ==============================
		JPanel choicesPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) choicesPanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel.add(choicesPanel, BorderLayout.SOUTH);

		JButton btnAdd = new JButton("Use ");
		TooltipManager.setTooltip(btnAdd, "Add the currently selected method to the pipeline, This will run the method and display the result");
		choicesPanel.add(btnAdd);		



		JButton btnReset = new JButton("Reset");
		btnReset.setEnabled(false);
		TooltipManager.setTooltip(btnReset, "Resets this step");
		choicesPanel.add(btnReset);

		JLabel lblCurrentChoices = new JLabel("     Current Choices");
		lblCurrentChoices.setHorizontalAlignment(SwingConstants.LEFT);
		choicesPanel.add(lblCurrentChoices);

		JComboBox<ImgProcessing<?,?>> choicesBox = new JComboBox<>();
		choicesBox.setPrototypeDisplayValue(methodBox.getItemAt(0));
		choicesBox.setRenderer(new ImgProcessingRenderer());
		choicesPanel.add(choicesBox);






		//   ==================================== Panel with a CardLayout to display current method options  ==============================


		JPanel cardPanel = new JPanel();
		cardPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(184, 207, 229), 2), "Method Options"));
		panel.add(cardPanel, BorderLayout.CENTER);
		CardLayout card = new CardLayout();
		cardPanel.setLayout(card);

		final Map<String,ValidityTogglePanel> optionsMap = new HashMap<>();

		for(int i = 0; i<methodBox.getItemCount(); i++){
			ValidityTogglePanel options = ((ImgProcessing) methodBox.getItemAt(i)).inputDialog(state, (PreviewBag)current);
			options.addValidityListener(l->btnAdd.setEnabled(l.validity()));
			String key = ""+i;
			optionsMap.put(key, options);
			cardPanel.add(new JScrollPane(options), ""+i);
		}
		card.show(cardPanel, ""+0);

		if(MouseListener.class.isAssignableFrom(methodBox.getSelectedItem().getClass()))
			state.setMouseListener((MouseListener) methodBox.getSelectedItem());
		else
			state.setMouseListener(null);













		// ===========================================    Action Events   ==================================================



		// When the method change -> display the correct options
		methodBox.addActionListener(l->{	
			card.show(cardPanel, ""+methodBox.getSelectedIndex());
			btnAdd.setEnabled(optionsMap.get(""+methodBox.getSelectedIndex()).validity());
			if(MouseListener.class.isAssignableFrom(methodBox.getSelectedItem().getClass()))
				state.setMouseListener((MouseListener) methodBox.getSelectedItem());
			else
				state.setMouseListener(null);
		});


		btnAdd.addActionListener(l->{

			btnReset.setEnabled(true);
			btnAdd.setEnabled(false);
			final ImgProcessing process = (ImgProcessing)methodBox.getSelectedItem();
			Thread t = new Thread(()->{
				apply(view, (PreviewBag)current, process);
			});				

			notifier.waitAnimation(t,"Running ... ");
			if(process.repeatable())
				btnAdd.setEnabled(true);

			choicesBox.addItem(process);

		});


		btnReset.addActionListener(l->{
			reset();
			btnReset.setEnabled(false);
			btnAdd.setEnabled(true);
			choicesBox.removeAllItems();
		});





		btnSave.addActionListener(l->{
			File f = WebFileChooser.showSaveDialog();
			if(f == null)
				return;

			RandomAccessibleInterval<T> interval = state.getPreviewImage();
			try {


				io.save(view.getMinimalInfo().removeDimension(Image.c), interval, f.getAbsolutePath()+io.standard(interval));
			} catch (Exception e) {
				notifier.display("Error", "An exception has occured while saving the image", e, Level.WARNING);
				return ;
			}

			WebNotification wn = NotificationManager.showNotification("Image saved!");
			wn.setDisplayTime(1000);

		});










		// ===========================================   Layouts    ==========================================================



		GroupLayout gl_methodPanel = new GroupLayout(methodPanel);
		gl_methodPanel.setHorizontalGroup(
				gl_methodPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_methodPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_methodPanel.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(Alignment.LEADING, gl_methodPanel.createSequentialGroup()
										.addComponent(lblAvailableMethods)
										.addGap(18)
										.addComponent(methodBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_methodPanel.createSequentialGroup()
										.addComponent(lblSaveTheCurrent)
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(btnSave)))
						.addContainerGap(264, Short.MAX_VALUE))
				);
		gl_methodPanel.setVerticalGroup(
				gl_methodPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_methodPanel.createSequentialGroup()
						.addGroup(gl_methodPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_methodPanel.createSequentialGroup()
										.addGap(7)
										.addComponent(lblAvailableMethods))
								.addGroup(gl_methodPanel.createSequentialGroup()
										.addGap(1)
										.addComponent(methodBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_methodPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_methodPanel.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(btnSave))
								.addGroup(gl_methodPanel.createSequentialGroup()
										.addGap(11)
										.addComponent(lblSaveTheCurrent)))
						.addContainerGap(39, Short.MAX_VALUE))
				);
		methodPanel.setLayout(gl_methodPanel);



	}

	@Override
	public WizardNode getPreviousStep(){
		reset();
		if(MouseListener.class.isAssignableFrom(pipeline.getLast().getClass()))
			state.setMouseListener((MouseListener) pipeline.getLast());
		else
			state.setMouseListener(null);		
		return parent;
	}




	private void reset() {

		if(added == 0) return;

		ImgProcessing ip = null;
		for(int i = 0; i<added; i++)
			ip = pipeline.removeLast();

		added = 0;


		System.out.println("Transformation -> "+ip.dimensionalityEffect());

		switch(ip.dimensionalityEffect()){

		case CHANNELS_TO_CHANNELS: ImgDimensions.copyReal(original.getInput(), state.getChannels());
		break;
		case CHANNELS_TO_FLAT: Views.iterable(state.getPreviewImage()).forEach(px->px.setZero());
		break;
		case FLAT_TO_FLAT: 
			Function<Double, Number> f = ImgDimensions.copyRealAndContrastWithOutput(original.getInput(), state.getPreviewImage());
			state.setVoxelMeasureConverter(f, state.getPreviewIndex());
			state.autoContrast(state.getPreviewIndex());
			break;
		default:
			break;

		}

		view.refresh();

		current = original.createWith(original.getInputInfo(), ImgDimensions.copy(io, original.getInput()));
		for(ImgProcessing<?,?> p :steps.get(this.index)){
			p.changeInput((PreviewBag)current);
		}

		isValid = false;

		this.fireNextValidityChanged();

	}








	@SuppressWarnings("unchecked")
	private <I extends RealType<I>,O extends RealType<O>> void apply(ImageViewer<T> view, PreviewBag<I> input, ImgProcessing<I,O> p) {



		//Apply the processing step on the input (current state of the image)
		RandomAccessibleInterval<O> result = p.validate();

		MinimalImageInfo resultInfo = p.dimensionalityEffect().createAlteration(current.getInputInfo()); 
		current = new PreviewBag<>(resultInfo, result, current.getOriginalInfo(), current.getOriginal(), resultInfo, result);

		// Now update the preview if required

		RandomAccessibleInterval<T> target = null;

		switch(p.dimensionalityEffect()){// TODO hide logic into the enum (use a new interface)
		case CHANNELS_TO_CHANNELS: target = null;
		break;
		case CHANNELS_TO_FLAT: target = state.getPreviewImage() ;
		break;
		case FLAT_TO_FLAT: target = state.getPreviewImage();
		break;
		default:
			break;

		}

		if(target!=null){
			Function<Double, Number> f = ImgDimensions.copyRealAndContrastWithOutput(result, target);
			state.setVoxelMeasureConverter(f, state.getPreviewIndex());
			state.autoContrast(state.getPreviewIndex());
		}
		else	
			view.refresh();

		if(p.repeatable())
			for(ImgProcessing<?,?> ip :steps.get(this.index)){
				ip.changeInput((PreviewBag)current);
			}


		//add to the list of chosen steps
		pipeline.add(p);
		added++;
		System.out.println("Added ->"+p.description());

		//fire event
		isValid = true;

		this.fireNextValidityChanged();			



	}












	@Override
	public boolean validity() {
		return isValid;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Optional<ProcessingStep<T, RandomAccessibleInterval<V>>> getConstruct() {


		for(ImgProcessing ip : pipeline){
			if(ip.requiresFlat())
				requiresFirstFlat = true;
			if(ip.requiresOriginal())
				requiresOrigin = true;
		}

		ProcessingStep pl = createFirst(pipeline.removeFirst());
		while(pipeline.size()>1){
			ImgProcessing ip = pipeline.removeFirst();
			pl = pl.andThen(createStep(ip));
			System.out.println("After init ->"+ip.description());
		}
		pl = pl.andThen(createLast(pipeline.remove()));

		return Optional.ofNullable(pl);
	}





	@SuppressWarnings("unchecked")
	@Override
	protected WizardNode getNextStep() {
		ProcessingPipelineNode next = 
				new ProcessingPipelineNode(
						this,
						index+1,
						io,
						theme,
						notifier,
						state,
						view,
						current,
						pipeline,
						steps,
						descriptions
						);

		return next;
	}





	@Override
	public int numberOfForks() {
		if(index==steps.size()-1)
			return 0;
		else return 1;
	}




	private <I extends Type<I>, O extends Type<O>> ProcessingStep<I, ImageBag<O>> createFirst(ImgProcessing<I,O> ip){


		final String name = ip.name();
		final Function<ImageBag<I>, RandomAccessibleInterval<O>> f = ip.toPipe();
		final DimensionalityEffect dEffect = ip.dimensionalityEffect();		
		final boolean originIsFlat = dEffect == DimensionalityEffect.FLAT_TO_FLAT;
		final boolean keepFlat = requiresFirstFlat && dEffect == DimensionalityEffect.CHANNELS_TO_FLAT;

		// System.out.println("Keep flat for "+ip.name()+" -> "+keepFlat);


		return (img, info, frame)->{

			long before = System.currentTimeMillis();

			// Use the original PreviewBag to infer image infos 
			final ImageBag<I> firstBag = new ImageBag<>(info, img, null, null, null, null, frame);
			System.out.println("-------------------------------------");
			System.out.println("ProcessingPipeline: firstInfo \n"+info);

			final RandomAccessibleInterval<O> out = f.apply(firstBag);

			log.debug("Processing time for "+name+": "+(System.currentTimeMillis()-before)+"ms");
			final MinimalImageInfo newInfo = dEffect.createAlteration(info).removeDimension(Image.t);
			// IMPORTANT remove time dimension from newInfo as we force a frame by frame scheme --> thus returned image is one frame only
			System.out.println("-------------------------------------");
			System.out.println("ProcessingPipeline: newInfo \n"+newInfo);
			
			if(originIsFlat){
				if(requiresOrigin)
					return new ImageBag<>(newInfo, out, info, img, info, img, frame);
				else
					return new ImageBag<>(newInfo, out, null, null, info, img, frame);
			}
			else if(keepFlat){					
				if(requiresOrigin)
					return new ImageBag<>(newInfo, out, info, img, newInfo, out, frame);
				else
					return new ImageBag<>(newInfo, out, null, null, newInfo, out, frame);
			}
			else{
				if(requiresOrigin)
					return new ImageBag<>(newInfo, out, info, img, null, null, frame);
				else
					return new ImageBag<>(newInfo, out, null, null, null, null, frame);
			}
		};


	}




	@SuppressWarnings("unchecked")
	private <I extends Type<I>, O extends Type<O>> Function<ImageBag<I>, ImageBag<O>> createStep(ImgProcessing<I,O> ip){

		final String desc = ip.description();
		final DimensionalityEffect dEffect = ip.dimensionalityEffect();
		final Function<ImageBag<I>, RandomAccessibleInterval<O>> f = ip.toPipe();
		final boolean keepFlat = requiresFirstFlat && dEffect == DimensionalityEffect.CHANNELS_TO_FLAT;

		System.out.println("Keep flat for "+ip.name()+" -> "+keepFlat);

		return (bag) -> {

			System.out.println("-------------------------------------");
			System.out.println("ProcessingPipeline - Step : "+ip.name());
			
			// Get the output -> input for next step
			final RandomAccessibleInterval<O> newInput = f.apply(bag);
			
			System.out.println("ProcessingPipeline - Step : "+ip.name()+" done");
			
			// Record the dimensionality change
			final MinimalImageInfo newInfo = dEffect.createAlteration(bag.getInputInfo());
			
			if(keepFlat)
				return new ImageBag(newInfo, newInput, bag.getOriginalInfo(), bag.getOriginal(), newInfo, newInput, bag.getCurrentFrame());
			else
				return bag.createWith(newInfo, newInput, bag.getCurrentFrame());
		};

	}



	private <I extends Type<I>, O extends Type<O>> Function<ImageBag<I>, RandomAccessibleInterval<O>> createLast(ImgProcessing<I,O> ip){

		final String desc = ip.description();
		final Function<ImageBag<I>, RandomAccessibleInterval<O>> f = ip.toPipe();

		return (bag) -> f.apply(bag);

	}




	public RandomAccessibleInterval<V> getProcessedImage() {
		if(current == null)
			return null;
		return current.getInput();
	}


	public MinimalImageInfo getProcessedImageInfo() {
		if(current == null)
			return null;
		return current.getInputInfo();
	}

	public V getProcessedImageType() {
		if(current == null)
			return null;
		return (V) current.getCurrentInputType();
	}




}
