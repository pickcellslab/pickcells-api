package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JComponent;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.realtransform.AffineGet;
import net.imglib2.type.numeric.RealType;
import net.imglib2.ui.TransformListener;

public interface ImageDisplay {
	
	public UITheme getIconTheme();
	
	public NotificationFactory getNotificationFactory();

	public JComponent getView();
	
	public JComponent getImageView();
	
	public <T extends RealType<T>> void addImage(MinimalImageInfo info, RandomAccessibleInterval<T> img);
	
	public int numImages();
	
	public void removeImage(int index);
	
	public void addAnnotationLayer(String name, AnnotationManager mgr);
	
	public int numAnnotationLayers();
		
	public void removeAnnotationLayer(int index);
	
	public <T extends RealType<T>> ChannelControl<T> getChannel(int imgIndex, int channel);
	
		
	public int currentZSlice();
	
	public int currentTimeFrame();
	
	public void setCurrentZSlice(int z);
	
	public void setcurrentTimeFrame(int t);	

	public <T extends AffineGet> T getCurrentTransform();
	
	
	
	public void refreshDisplay();
	
	public void setTextOverlay(String text);
	
	
	
	public KeyStrokeModel getKeyStrokeModel();
	
	public MouseDisplayEventModel getMouseModel();
		
	public <T extends RealType<T>> ImageDisplayModel<T> getImageModel(int index);
	
	public AnnotationManager getAnnotationManager(int index);
	
	
	
	
	public void addDisplayPositionListener(DisplayPositionListener l);

	public void removeDisplayPositionListener(DisplayPositionListener l);
	
	
	
	public void addTransformListener(TransformListener<? super AffineGet> l);

	public void removeTransformListener(TransformListener<? super AffineGet> l);

	
	public void addDisplayContentListener(DisplayContentListener l);
	
	public void removeDisplayContentListener(DisplayContentListener l);
	
		
}
