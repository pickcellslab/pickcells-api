package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.function.Function;

import javax.swing.JTextArea;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;
import org.pickcellslab.pickcells.api.img.view.Lut16;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;


public class Thresholding<T extends NativeType<T> & RealType<T>> implements ImgProcessing<T,BitType> {

	private double min, max;

	private ImageViewState<?> state;
	private PreviewBag<T> input;
	
	private final ImgIO io;
	
	public Thresholding(ImgIO io) {
		this.io = io;
	}
	

	@Override
	public String name() {
		return "Thresholding";
	}



	@Override
	public String description() {
		return "Set a threshold to binarize an image";
	}

	@Override
	public boolean repeatable() {
		return false;
	}




	




	
	@Override
	public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<T> input) {
		
		this.state = state;
		this.input = input;
		
		state.setLut(state.getPreviewIndex(), Lut16.HILO.name());

		return new ValidityTogglePanel(){

			{			
				setLayout(new BorderLayout());
				JTextArea text = new JTextArea();
				text.setEditable(false);
				text.setLineWrap(true);
				text.setText("\nUse the Brightness and Contrast dialog to adjust the threshold (on the preview)."
						+ "\n\n Using the 'HILO' LUT to visualize your thresholds (low and high threshold).");
				add(text, BorderLayout.CENTER);
			}

			@Override
			public boolean validity() {
				return true;
			}

		};
	}



	@Override
	public void changeInput(PreviewBag<T> current) {
		this.input = current;
	}

	
	
	


	@Override
	public RandomAccessibleInterval<BitType> validate() {

	
		min = state.getCurrentMinIntensity(state.getPreviewIndex());
		max = state.getCurrentMaxIntensity(state.getPreviewIndex());

		return run(io, input.getInput(), min, max);
	}



	private static <V extends RealType<V>> RandomAccessibleInterval<BitType> run(ImgIO io, RandomAccessibleInterval<V> input, double min, double max){

		
			Img<BitType> target = io.createImg(input,new BitType());
			Operations.convert(input, target, new Thresholder<>(min, max));
			return target;
		

	}




	@Override
	public Function<ImageBag<T>, RandomAccessibleInterval<BitType>> toPipe() {

		final double tMin = min;
		final double tMax = max;
		
		
		return (bag)->{
			return Thresholding.run(io, bag.getInputFrame(0), tMin, tMax);
		};

	
	}


	@Override
	public DimensionalityEffect dimensionalityEffect() {
		return DimensionalityEffect.FLAT_TO_FLAT;
	}









	@Override
	public boolean requiresAnnotationChannel() {
		return false;
	}



	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}






	@Override
	public boolean requiresOriginal() {
		return false;
	}



	@Override
	public boolean requiresFlat() {
		return false;
	}



	@Override
	public BitType outputType(T inputType) {
		return new BitType();
	}






}

