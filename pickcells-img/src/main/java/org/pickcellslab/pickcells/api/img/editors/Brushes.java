package org.pickcellslab.pickcells.api.img.editors;

import java.awt.Color;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Iterator;
import java.util.Objects;

import javax.swing.ImageIcon;

import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.img.process.HyperEllipseFactory;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.AbstractNeighborhood;
import net.imglib2.outofbounds.OutOfBoundsConstantValueFactory;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public final class Brushes {

	private Brushes(){/* Not Instantiable */}





	/**
	 * Creates an ellipsoid shaped {@link Brush} which will apply the given {@code viewColor} in {@code interval} only if 
	 * at the same location in {@code sensed}, the voxel equals {@code label}. 
	 * This brush can be useful to edit specific objects in a segmentation result for example in combination with {@link PaintInstance}
	 * @param theme the UITheme required to obtain the brush icon
	 * @param sensed The {@link RandomAccessibleInterval} to check if this brush is allowed to be applied
	 * @param view The {@link RandomAccessibleInterval} where the brush is going to be applied
	 * @param dims The radii of the ellipsoid
	 * @param sensedColor The label in {@code sensed} which is writable.
	 * @param viewColor The color to be applied by this brush in the view
	 * @return A dedicated {@link Brush}
	 * @throws IllegalArgumentException if dims length != interval numDimensions(). 
	 */
	public static <P extends RealType<P>, D extends RealType<D>> Brush selectiveEllipse(
			UITheme theme,
			RandomAccessibleInterval<D> sensed,
			RandomAccessibleInterval<P> view, long[] dims, final D sensedColor, final P viewColor){		

		Objects.requireNonNull(sensed);
		Objects.requireNonNull(view);
		Objects.requireNonNull(dims);
		Objects.requireNonNull(viewColor);

		final HyperEllipseFactory<P> fctry = new  HyperEllipseFactory<>(dims, new OutOfBoundsConstantValueFactory<>(viewColor.createVariable()) );
		
		
		final AbstractNeighborhood<P> n = fctry.create(view);
		final RandomAccess<D> dAccess = Views.extendZero(sensed).randomAccess();
		final RandomAccess<P> pAccess = Views.extendZero(view).randomAccess();
		final P color = viewColor.createVariable();
		color.set(viewColor);
		final D label = sensedColor.createVariable();
		label.set(sensedColor);


		return new NeighborhoodBrush<P>(n){

			@Override
			public boolean computeAndSet(long[] pos) {
				dAccess.setPosition(pos);
				D data = dAccess.get();
				if(data.equals(label)){
					pAccess.setPosition(dAccess);
					pAccess.get().set(color);
					return true;
				}
				else
					return false;
			}

		
			@Override
			public ImageIcon cursorIcon() {
				return UITheme.toImageIcon(theme.icon(IconID.Misc.CIRCLE, 16, Color.YELLOW));
			}


			@Override
			public boolean revert(long[] pos) {
				dAccess.setPosition(pos);
				D data = dAccess.get();
				if(data.equals(label)){
					pAccess.setPosition(dAccess);
					pAccess.get().setZero();
					return true;
				}
				else
					return false;
			}


			@Override
			public Brush createWithDims(long[] newDims) {
				return Brushes.selectiveEllipse(theme, sensed, view, newDims, sensedColor, viewColor);
			}

		};
	}










	private abstract static class NeighborhoodBrush<T> implements Brush{

		private final AbstractNeighborhood<T> n;
		private final long[] min, max;
		private long[] center;

		NeighborhoodBrush(AbstractNeighborhood<T> n) {
			this.n = n;
			this.min = new long[n.numDimensions()];
			this.max = new long[n.numDimensions()];
			this.center = new long[n.numDimensions()];
			updateBounds();
		}

		
		
		@Override
		public void setPosition(long[] pos) {
			n.setPosition(pos);
			center = pos;
			updateBounds();
		}

		private void updateBounds() {
			n.min(min);
			n.max(max);
		}

		@Override
		public long[] minBound() {
			return min;
		}

		@Override
		public long[] maxBound() {
			return max;
		}

		@Override
		public long[] currentPosition() {
			return center;
		}

		
		@Override
		public Iterator<long[]> iterator() {
			
			final Cursor<T> c = n.localizingCursor();
			final long[] pos = new long[c.numDimensions()];
			
			return new Iterator<long[]>(){
				
				@Override
				public boolean hasNext() {
					return c.hasNext();
				}

				@Override
				public long[] next() {
					c.next();
					c.localize(pos);
					return pos;
				}
				
			};
		}



		@Override
		public int numDimensions() {
			return n.numDimensions();
		}

	}





}
