package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Function;

import org.pickcellslab.foundationj.services.ProgressPanel;
import org.pickcellslab.foundationj.services.TaskResultConsumer;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.RealType;

/**
 * Contains the logic to process an input image to create an output image and save it on the disk.
 * {@link ProcessingPipeline} can be generated via a {@link PipelineBuilder}.
 *
 * @param <T> The {@link Type} of the input {@link RandomAccessibleInterval}
 * @param <R> The {@link Type} of the output {@link RandomAccessibleInterval}
 */
public interface ProcessingPipeline<T extends NativeType<T> & RealType<T>, R extends NativeType<R> & RealType<R>> {

		
	/**
	 * Adds an additional step at the end of the current {@link ProcessingPipeline}
	 * @param step A {@link Function} which takes the output of the current pipeline as an input and returns a result image. 
	 * @param outputType The output {@link Type} of the step
	 * @return A new {@link ProcessingPipeline} with the given step appended to the current ones.
	 */
	public <E extends NativeType<E> & RealType<E>> ProcessingPipeline<T,E> appendStep(Function<RandomAccessibleInterval<R>,RandomAccessibleInterval<E>> step, E outputType);
	
	/**
	 * Runs this pipeline. For each image this pipeline is configured to process, the passed {@link Consumer} will be given an {@link ExtendedImageInfo} providing information
	 * about the output of the pipeline.
	 * @param io The {@link ImgIO} to use for I/O operations
	 * @param namingConvention A {@link Function} which takes the name of the input image as an input and return the name of the processed image
	 * @return
	 * @throws IOException If an exception occur while reading / saving images
	 * @throws IllegalStateException if this method has already been called.
	 */
	public ProgressPanel<ExtendedImageInfo<R>> runPipeline(ImgIO io, Function<String,String> namingConvention);
}
