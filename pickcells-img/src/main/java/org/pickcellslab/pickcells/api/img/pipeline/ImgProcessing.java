package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.event.MouseListener;
import java.nio.channels.Pipe;
import java.util.function.Function;

import javax.swing.text.html.ImageView;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.foundationj.ui.wizard.Wizard;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.view.ImageViewer;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.RealType;


/**
 * ImgProcessing objects are (GUI) factories for an image processing step (in the form of a {@link Function}). They are meant to be inserted in a 
 * {@link Pipeline} customised by the user from within a {@link Wizard}.
 * As such, {@link ImgProcessing} objects are used in a 2 phases process:
 * <ul> 
 * 		<li> First, the ImgProcessing provides a GUI (controllers only) for the user to adjust parameters specific to the ImgProcessing function.
 * 			 see {@link #inputDialog(ImageViewState, ImageBag)}
 * 		<li> Then when the parameters have been validated by the user, the ImgProcessing provides a {@link Function} which can perform the image treatment with the
 * 			exact same parameters as the ones validated by the user in the first phase.
 * </ul>
 * <ul><b> NB: </b>
 *		 <li> In order to ensure that very large images (notably long timelapse) which do not fit in RAM can be processed as well, the Function is
 *			  expected to return a {@link RandomAccessibleInterval} which corresponds to one frame only of the input image. In order to process the entire
 *			  image, several call to the {@link Function#apply(Object)} method will be performed. The Function will nevertheless have access to all the frames
 *			  of the input image using {@link ImageBag#getInputFrame()} (in case the time dimension enters the calculation of the output for the current frame).
 * 		 <li> If your ImgProcessing also implements {@link MouseListener}, your object will receive events from mouse interactions of the user with
 * 			  the preview image. 
 * </ul>
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of the source {@link Type}
 * @param <R> The type of the result {@link Type}
 */
public interface ImgProcessing<T extends Type<T>,R extends Type<R>> {

	/**
	 * @return A short name for this {@link ImgProcessing}
	 */
	public String name();
	
	public R outputType(T inputType);
	
	/**
	 * 
	 * @param state An {@link ImageViewState} providing info about an {@link ImageView} configured to display previews of this {@link ImgProcessing}.
	 * @param input A {@link PreviewBag} containing the current input for this ImgProcessing
	 * 
	 * @return A {@link ValidityTogglePanel} containing the controllers for the user to enter the inputs for the method. NB:
	 * do not insert the view in your panel, the view will be displayed independently of the returned panel.
	 */
	
	public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<T> input);
	
	
	/**
	 * Reinject the {@link ImageBag} on which the ImgProcessing should work on. Replaces the input from previous call
	 * to {@link #inputDialog(ImageViewState, RandomAccessibleInterval)}
	 * @param current
	 */
	public void changeInput(PreviewBag<T> current);
	
	
	
	/**
	 * @return true if the original multichannel image is required by this processing step, false otherwise.
	 */
	public boolean requiresOriginal();
	
	
	/**
	 * @return true if the output of the transformation from a multichannel to a single channel image is required by this processing step, false otherwise.
	 */
	public boolean requiresFlat();
	
	
	/**
	 * 
	 * Here validate means that the user whishes to add this ImgProcessing to his Pipeline. The implementation should create a result from the input
	 * provided via a previous call to {@link #inputDialog(ImageViewState, RandomAccessibleInterval)} and create a valid representation of this 
	 * result in the view
	 * 
	 * @param preview The actual data displayed in an {@link ImageViewer} which should be updated by this method. The dimensionality of the preview
	 * depends on the returned object of {@link #dimensionalityEffect()}.
	 * 
	 * @return The {@link RandomAccessibleInterval} the {@link Pipe} created by this {@link ImgProcessing} would return given the input
	 * provided in a previous call to {@link #inputDialog(ImageViewState, RandomAccessibleInterval)}
	 */
	public RandomAccessibleInterval<R> validate();
	
		
	/**
	 * @return The description of the operation performed by this ImgProcessing
	 */
	public String description();
	
	/**
	 * @return {@code true} if it would make sense to repeat this {@link ImgProcessing} several time on an image, {@code false} otherwise
	 */
	public boolean repeatable();
	
	
	/**
	 * @return The {@link DimensionalityEffect} of this {@link ImgProcessing}
	 */
	public DimensionalityEffect dimensionalityEffect();
	
	
	/**
	 * Determines whether or not this ImgProcessing requires an additional channel in order to write some annotations for the user to see.
	 * NB: This allows to ensure that an overlay channel is present at runtime in the view. However, this additional channel will not be provided
	 * to the potentially created Pipe which means that if the operation performed by the Pipe requires duplication of the image for some reason, the
	 * Pipe will be in charge of doing so.
	 * @return {@code true} if this {@link ImgProcessing} requires an overlay channel on which to create annotations and {@code false} otherwise
	 */
	public boolean requiresAnnotationChannel();
	
	
	
	/**
	 * Notifies this ImgProcessing that it should stop any ongoing task it is currently performing. Generally called when a Pipeline UI is cancelled
	 */
	public void stop();
	
	
	/**
	 * @return A {@link Function} which takes an {@link ImageBag} as input and outputs a {@link RandomAccessibleInterval} which corresponds to one time point
	 * of the dataset.
	 * NB: In order to ensure that very large images (notably long timelapse) which do not fit in RAM can be processed as well, the Function is
	 *	  expected to return a {@link RandomAccessibleInterval} which corresponds to one frame only of the input image. In order to process the entire
	 *	  image, several call to the {@link Function#apply(Object)} method will be performed. The Function will nevertheless have access to all the frames
	 *	  of the input image using {@link ImageBag#getInputFrame()} (in case the time dimension enters the calculation of the output for the current frame).
	 */
	public Function<ImageBag<T>,RandomAccessibleInterval<R>> toPipe();

	

	/**
	 * @return true if this {@link ImgProcessing} can be used on an image with the given {@link MinimalImageInfo} and false otherwise 
	 */
	public default boolean condition(MinimalImageInfo info){return true;};
	
	
		
	
}
