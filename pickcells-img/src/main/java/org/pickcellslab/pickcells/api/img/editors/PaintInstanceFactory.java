package org.pickcellslab.pickcells.api.img.editors;

import org.pickcellslab.pickcells.api.img.providers.OutOfLabelException;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public interface PaintInstanceFactory<P,D> {
	
	public long[] defaultBrushDimensions();
	
	public void setDefaultBrushDimensions(long[] dims);
	
	public PaintInstance<P,D> createEraserFor(PaintableAnnotation<P,D> a) throws IllegalArgumentException;
	
	public PaintInstance<P,D> createExpanderFor(PaintableAnnotation<P,D> a) throws IllegalArgumentException;
	
	public PaintInstance<P,D> createSplitterFor(PaintableAnnotation<P,D> a) throws IllegalArgumentException, OutOfLabelException;
	
	public PaintInstance<P,D> createAnnotationPainter(PaintInstanceConsumer<P,D> consumer, int time) throws OutOfLabelException;

	
	
	
}
