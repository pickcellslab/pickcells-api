package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.AbstractNeighborhood;
import net.imglib2.algorithm.region.localneighborhood.EllipseNeighborhood;
import net.imglib2.algorithm.region.localneighborhood.EllipsoidNeighborhood;
import net.imglib2.outofbounds.OutOfBoundsFactory;

public class HyperEllipseFactory<T> implements NeighborhoodFactory<T>{

	private final long[] radii;
	private OutOfBoundsFactory<T, RandomAccessibleInterval<T>> outOfBounds;

	public HyperEllipseFactory(long[] radii, OutOfBoundsFactory<T,RandomAccessibleInterval<T>> outOfBounds){

		Objects.requireNonNull(radii, "The provided radii array is null, this is forbbiden");
		Objects.requireNonNull(outOfBounds, "The provided Out of  bounds strategy is null, this is forbbiden");

		this.radii = radii;
		this.outOfBounds = outOfBounds;
	}

	@Override
	public AbstractNeighborhood<T> create(RandomAccessibleInterval<T> source) {
		if(source.numDimensions()!=radii.length)
			throw new IllegalArgumentException("The source image has different dimensions than the kernels this factory can create");
		if(radii.length < 3)			
			return new EllipseNeighborhood<>(source, new long[radii.length],radii, outOfBounds);
		else
			return new EllipsoidNeighborhood<>(source, new long[radii.length],radii, outOfBounds);
	}




}
