package org.pickcellslab.pickcells.api.img.process;

import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;

@SuppressWarnings("serial")
public class KernelRadiiUI extends ValidityTogglePanel{
	
	private final MinimalImageInfo info;
	private final double[] k;
	
	
	public KernelRadiiUI(MinimalImageInfo info, String hint) {
	
		if(info.dimension(Image.c)>1)
			throw new IllegalArgumentException("Multi Channel Image Unsupported");
		
		this.info = info;
		k = info.imageCalibration();
				
				
		setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
		
		this.setLayout(new VerticalFlowLayout());
		
		final JLabel xLbl = new JLabel("X "+hint);
		final SpinnerNumberModel xModel = new SpinnerNumberModel(k[info.index(Image.x)], 0.001, 50, 0.1);
		final JSpinner xSpin = new JSpinner(xModel);
		xModel.addChangeListener(l->k[info.index(Image.x)]=xModel.getNumber().doubleValue());
		add(new GroupPanel(10, xLbl, xSpin));
	
		final JLabel yLbl = new JLabel("Y "+hint);
		final SpinnerNumberModel yModel = new SpinnerNumberModel(k[info.index(Image.y)], 0.001, 50, 0.1);
		final JSpinner ySpin = new JSpinner(yModel);
		yModel.addChangeListener(l->k[info.index(Image.y)]=yModel.getNumber().doubleValue());
		add(new GroupPanel(10, yLbl, ySpin));
		
		if(info.dimension(Image.z)>1){
			JLabel zLbl = new JLabel("Z "+hint);
			SpinnerNumberModel zModel = new SpinnerNumberModel(k[info.index(Image.z)], 0.001, 50, 0.1);
			JSpinner zSpin = new JSpinner(zModel);
			zModel.addChangeListener(l->k[info.index(Image.z)]=zModel.getNumber().doubleValue());
			add(new GroupPanel(10, zLbl, zSpin));
			
		}
		
		if(info.dimension(Image.t)>1)
			k[info.index(Image.t)] = 0;
		
		
	}

	
	/**
	 * @return The radii in a double array of length 3 if the input image is Z-Stack, of length 2 otherwise (even if image has time or several channels). 
	 */
	public double[] getRadii(){
		return Arrays.copyOf(k, k.length);
	}
	
	public double[] getRadiiNoTime() {
		if(info.dimension(Image.t)==1)
			return Arrays.copyOf(k, k.length);
		
		MinimalImageInfo noT = info.removeDimension(Image.t);
		double[] r = new double[k.length-1];
		r[noT.index(Image.x)] = k[info.index(Image.x)];
		r[noT.index(Image.y)] = k[info.index(Image.y)];
		if(noT.dimension(Image.z)>1)
			r[noT.index(Image.z)] = k[info.index(Image.z)];
		
		return r;		
	}
	
	
	
	@Override
	public boolean validity() {
		return true;
	}


	

}
