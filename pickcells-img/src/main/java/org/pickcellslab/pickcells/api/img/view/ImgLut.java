package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBufferInt;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import net.imglib2.display.ArrayColorTable;

public interface ImgLut<T> {	
	public ArrayColorTable<T> table();
	public T[] data();
	public String name();
	public Color dominante();
	public default Icon createIcon(int w, int h){

		if(w<0)	w = 24;
		if(h<0)	h = 24;

		final ArrayColorTable<T> table = table();
		final int[] matrix = new int[w * h];
		final int bins = table.getLength()/w;
		//System.out.println("bins =" +bins);
		for(int x = 0; x<w; x++){			
			int rgb = table.argb(x+bins);
			for(int y = 0; y<h; y++){
				matrix[x*y+x]=rgb;				
			}
		}

		final DataBufferInt buffer = new DataBufferInt(matrix, matrix.length);

		int[] bandMasks = {0xFF0000, 0xFF00, 0xFF, 0xFF000000}; // ARGB (yes, ARGB, as the masks are R, G, B, A always) order
		WritableRaster raster = Raster.createPackedRaster(buffer, w, h, w, bandMasks, null);

		//System.out.println("raster: " + raster);

		final ColorModel cm = ColorModel.getRGBdefault();
		final BufferedImage image = new BufferedImage(cm, raster, cm.isAlphaPremultiplied(), null);



		//final BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		//img.getData().createBandedRaster(dataType, w, h, bands, location)
		return new ImageIcon(image);//.getScaledInstance(w,h,Image.SCALE_FAST));
	}
}
