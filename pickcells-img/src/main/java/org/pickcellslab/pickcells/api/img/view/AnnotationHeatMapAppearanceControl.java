package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.math3.util.FastMath;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogramFactory;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;

import net.imglib2.converter.Converter;
import net.imglib2.display.ColorTable;
import net.imglib2.histogram.Histogram1d;
import net.imglib2.histogram.Real1dBinMapper;
import net.imglib2.type.numeric.ARGBType;
import net.imglib2.type.numeric.real.DoubleType;

public class AnnotationHeatMapAppearanceControl implements AnnotationAppearanceControl, ChannelListener<DoubleType> {


	private final AnnotationManager mgr;
	private final List<ChangeListener> lstrs = new ArrayList<>();

	private final AnnotationHeatMapUI ui;
	private final MultiConverter converter;

	private final ChangeEvent e = new ChangeEvent(this);
	
	private final SimpleHistogramFactory hFctry;

	
	public AnnotationHeatMapAppearanceControl(AnnotationManager mgr, SimpleHistogramFactory hFctry) {

		this.mgr = mgr;
		this.hFctry = hFctry;

		final Map<String, List<Dimension<Annotation, ?>>> dims = mgr.possibleAnnotationDimensions(d->d.dataType()==dType.NUMERIC, true);
		final Map<String, AnnotationChannelControl> controls = new HashMap<>();

		for(String type : dims.keySet()){
			controls.put(type, new AnnotationChannelControl(type, mgr, (Function<Annotation, Number>) dims.get(type).get(0)));
			controls.get(type).addChannelListener(this);
		}

		this.converter = new MultiConverter(controls);
		this.ui = new AnnotationHeatMapUI((Map)dims, controls);

	}
	
	
	

	@Override
	public AnnotationManager getManager() {
		return mgr;
	}

	@Override
	public JComponent getUI() {
		return ui;
	}

	@Override
	public Converter<Annotation, ARGBType> getConverter() {
		return converter;
	}

	@Override
	public void addChangeListener(ChangeListener l) {
		lstrs.add(l);
	}

	@Override
	public void removeChangeListener(ChangeListener l) {
		lstrs.remove(l);
	}


	public String toString(){
		return "Property HeatMap";
	}









	@Override
	public void lutChanged(ChannelControl<DoubleType> source) {
		lstrs.forEach(l->l.stateChanged(e));
	}

	@Override
	public void visibilityChanged(ChannelControl<DoubleType> source) {
		lstrs.forEach(l->l.stateChanged(e));
	}

	@Override
	public void histogramChanged(ChannelControl<DoubleType> source) {
		lstrs.forEach(l->l.stateChanged(e));
	}

	@Override
	public void contrastChanged(ChannelControl<DoubleType> source) {
		lstrs.forEach(l->l.stateChanged(e));
	}

	@Override
	public void nameHasChanged(ChannelControl<DoubleType> source) {
		lstrs.forEach(l->l.stateChanged(e));
	}






	@SuppressWarnings("serial")
	private class AnnotationHeatMapUI extends JPanel{


		@SuppressWarnings("unchecked")
		public AnnotationHeatMapUI(Map<String, List<Dimension<? super Annotation, Number>>> dims, Map<String, AnnotationChannelControl> controls) {

			this.setLayout(new VerticalFlowLayout(10));

			for(String type : dims.keySet()){

				final JComboBox<Dimension<? super Annotation, Number>> keyBox = new JComboBox<>();
				dims.get(type).forEach(d->keyBox.addItem(d));

				keyBox.addActionListener(l->controls.get(type).setMappedProperty((Function<? super Annotation, Number>) keyBox.getSelectedItem()));

				final ChannelControlUI<DoubleType> ui = new ChannelControlUI<>(controls.get(type), hFctry);

				GroupPanel panel = new GroupPanel(10, false,
						new GroupPanel(10, new JLabel("Property : "), keyBox),
						ui);

				panel.setBorder(BorderFactory.createTitledBorder(type));

				this.add(panel);
			}

		}


	}



	private class MultiConverter implements Converter<Annotation,ARGBType>{

		private final Map<String, AnnotationChannelControl> map;

		public MultiConverter(Map<String, AnnotationChannelControl> map) {
			this.map = map;
		}


		@Override
		public void convert(Annotation input, ARGBType output) {
			map.get(input.representedType()).convert(input, output);
		}

	}








	private class AnnotationChannelControl implements Converter<Annotation,ARGBType>, ChannelControl<DoubleType>, Iterable<DoubleType>{

		private final List<ChannelListener<DoubleType>> clstrs = new ArrayList<>();

		private final AnnotationManager mgr;

		private final String type;


		private Histogram1d<DoubleType> histo;

		private String name = "";
		private boolean visible = true;

		private ImgLut<?> lut = Lut16.FIRE;
		private ColorTable table = Lut16.FIRE.table();

		private double min, max;

		private Function<? super Annotation, Number> propertyGetter;






		public AnnotationChannelControl(String type, AnnotationManager mgr, Function<? super Annotation, Number> dimension) {
			this.type = type;
			this.mgr = mgr;
			this.propertyGetter =  dimension; 
			buildHistogram();	
		}


		public void setMappedProperty(Function<? super Annotation, Number> propertyGetter) {
			this.propertyGetter = propertyGetter;
			buildHistogram();
			//clstrs.forEach(l->l.lutChanged(this));
		}


		@Override
		public void convert(Annotation i, ARGBType o) {
			if(i==null){
				o.setZero();
				return;
			}
			final Number a = propertyGetter.apply(i);
			if(a==null){
				o.setZero();
				return;
			}
			final int argb = table.lookupARGB( min, max, a.doubleValue());
			o.set( argb );
		}



		@Override 
		public void setChannelName(String newName) {
			name = newName;
			clstrs.forEach(l->l.nameHasChanged(this));
		}

		@Override
		public void setContrast(int minimum, int maximum) {
			min = minimum;
			max = maximum;
			clstrs.forEach(l->l.contrastChanged(this));
		}

		@Override
		public void setImgLut(ImgLut<?> lut) {
			this.lut = lut;
			table = lut.table();
			clstrs.forEach(l->l.lutChanged(this));
		}

		@Override
		public void setVisible(boolean isVisible) {
			if(isVisible != visible){
				visible = isVisible;
				clstrs.forEach(l->l.visibilityChanged(this));
			}
		}




		public void buildHistogram() {
			long b = System.currentTimeMillis();
			//get the range
			min = Double.MAX_VALUE;	max = -Double.MAX_VALUE;
			for(DoubleType t : this){
				if(!Double.isNaN(t.get())){
					min = FastMath.min(min, t.get());
					max = FastMath.max(max, t.get());
				}
			}
			if(min>max){
				min = 0; max = 1;
			}
			final Real1dBinMapper<DoubleType> mapper =	new Real1dBinMapper<>(min, max, 100, false);
			histo = new Histogram1d<>(this, mapper);
			System.out.println("AnnotationChannelControl histo min max -> "+min+"; "+max);
			System.out.println("AnnotationChannelControl -> "+name+" creating Histogram took "+(System.currentTimeMillis()-b)+"ms");
			clstrs.forEach(l->l.histogramChanged(this));
		}


		@Override
		public void autoContrast() {
			final long b = System.currentTimeMillis();
			final DoubleType min = new DoubleType();
			System.out.println("AnnotationChannelControl histogram bin count -> "+histo.getBinCount());
			for(int i = 0; i<histo.getBinCount(); i++){
				if(histo.frequency(i)>0){
					histo.getLowerBound(i, min);
					break;
				}
			}
			final DoubleType max = new DoubleType();		
			histo.getUpperBound((int)histo.getBinCount()-1, max);
			for(int i = (int)histo.getBinCount()-1; i>0; i--){
				if(histo.frequency(i)>0){
					histo.getUpperBound(i, max);
					break;
				}
			}
			System.out.println("AnnotationChannelControl -> "+name+" autocontrast took "+(System.currentTimeMillis()-b)+"ms");
			System.out.println("AnnotationChannelControl autoContrast -> "+min+"; "+max);
			this.setContrast((int)min.getRealDouble(), (int)max.getRealDouble());
		}

		@Override
		public DoubleType getChannelType() {
			return new DoubleType();
		}

		@Override
		public Histogram1d<DoubleType> getHistogram() {
			return histo;
		}

		@Override
		public ImgLut<?> getLut() {
			if(lut==null)
				setImgLut(getAvailableLuts()[0]);
			return lut;
		}

		@Override
		public int[] getCurrentContrast() {
			return new int[]{(int)min, (int)max};
		}

		@Override
		public String getChannelName() {
			return name;
		}

		@Override
		public boolean isVisible() {
			return visible;
		}

		@Override
		public double getGlobalMinIntensity() {
			return min-(max-min)/3;
		}

		@Override
		public double getGlobalMaxIntensity() {
			return max+(max-min)/3;
		}

		@Override
		public ImgLut<?>[] getAvailableLuts() {
			return Lut16.values();
		}

		@Override
		public void addChannelListener(ChannelListener<DoubleType> l) {
			clstrs.add(l);
		}

		@Override
		public void removeChannelListener(ChannelListener<DoubleType> l) {
			clstrs.remove(l);
		}


		@Override
		public String toString(){
			return name;
		}



		@Override
		public Iterator<DoubleType> iterator() {
			final DoubleType t = new DoubleType();
			final Iterator<? extends Annotation> it = mgr.stream(type).limit(200).iterator();
			return new Iterator<DoubleType>(){

				@Override
				public boolean hasNext() {
					return it.hasNext();
				}

				@Override
				public DoubleType next() {
					final Annotation a = it.next();
					Number n = propertyGetter.apply(a);
					if(n==null)
						t.set(Double.NaN);
					else
						t.set(n.doubleValue());
					return t;
				}

			};
		}



	}



}
