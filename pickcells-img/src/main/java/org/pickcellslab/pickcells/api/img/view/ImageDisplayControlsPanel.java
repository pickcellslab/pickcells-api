package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ViewportLayout;
import javax.swing.table.DefaultTableModel;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.threads.ModeratedConsumerThread;
import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogramFactory;
import org.pickcellslab.pickcells.api.datamodel.types.Image;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.WebCollapsiblePane;
import com.alee.laf.table.WebTable;

@SuppressWarnings("serial")
public class ImageDisplayControlsPanel extends JScrollPane{

	//private final WebAccordion accordion = new WebAccordion();
	private final JPanel accordion = new JPanel();
	private final Map<ChannelControlModel<?>, WebCollapsiblePane> channelMapping = new HashMap<>(); //We need this map as names of ui may change over time
	private final Map<AnnotationAppearanceControlManager, WebCollapsiblePane> annotationMapping = new HashMap<>();

	private final SimpleHistogramFactory hFctry;
	
	private WebCollapsiblePane annotationInfoPane;
	private InfoPanel info;

	public ImageDisplayControlsPanel(SimpleHistogramFactory hFctry) {
		this.getViewport().setLayout(new ConstrainedViewPortLayout());		
		this.hFctry = hFctry;
		accordion.setLayout(new VerticalFlowLayout());
		this.setViewportView(accordion);
	}



	public void addHandler(Overlay2DTransformHandler<?> h) {
		for(int channel = 0; channel<h.getMinimalInfo().dimension(Image.c); channel++){			
			ChannelControlModel<?> model = h.getChannel(channel);
			ChannelControlUI<?> ui = new ChannelControlUI<>(model, hFctry);
			WebCollapsiblePane pane = new WebCollapsiblePane(model.toString(), ui);		
			pane.setExpanded(false);
			accordion.add(pane);
			channelMapping.put(model, pane);
		}
	}

	public void removeHandler(Overlay2DTransformHandler<?> h) {
		for(int channel = 0; channel<h.getMinimalInfo().dimension(Image.c); channel++){			
			ChannelControlModel<?> model = h.getChannel(channel);
			int index = -1;
			for (int i = 0; i < accordion.getComponentCount(); i++) {
				if (accordion.getComponent(i) == channelMapping.get(model)){
					index = i;
					break;
				}
			}
			accordion.remove(index);
			channelMapping.remove(model);
			accordion.revalidate();
		}
	}



	public void addAnnotationControl(AnnotationAppearanceControlManager apps) {

		if(annotationMapping.isEmpty()){
			info = new InfoPanel();
			annotationInfoPane = new WebCollapsiblePane("Last Selected Annotation", info);	
			accordion.add(annotationInfoPane);
		}
		info.registerManager(apps.getAnnotationManager());

		final WebCollapsiblePane pane = new WebCollapsiblePane("Annotation States Appearances", new AnnotationAppearanceUI(apps));
		pane.setExpanded(false);
		accordion.add(pane);
		annotationMapping.put(apps, pane);
	}


	public void removeAnnotationControl(AnnotationAppearanceControlManager apps) {
		annotationMapping.remove(apps);
		info.deregisterManager(apps.getAnnotationManager());
		if(annotationMapping.isEmpty()){
			int index = -1;
			for (int i = 0; i < accordion.getComponentCount(); i++) {
				if (accordion.getComponent(i) == annotationInfoPane){
					index = i;
					break;
				}
			}
			accordion.remove(index);
		}
	}





	private class InfoPanel extends JPanel implements AnnotationStatusListener {

		final DefaultTableModel model;
		final ModeratedConsumerThread<Annotation> mt;

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public InfoPanel() {	

			setBackground(Color.white);
			setOpaque(true);
			setLayout(new VerticalFlowLayout());
			final JLabel idLabel = new JLabel();
			idLabel.setFont(new Font("SansSerif", Font.BOLD, 18));
			add(idLabel);

			//TODO add ScrollPane so column titles are visible
			final WebTable table = new WebTable ();
			model = ((DefaultTableModel)table.getModel());
			table.setEditable(false);
			table.setVisibleRowCount (10);
			add(table);

			model.addColumn("Property");
			model.addColumn("Value");
			mt = new ModeratedConsumerThread<>((annotation)->{
				if(annotation.status() == AnnotationStatus.SELECTED){
					idLabel.setText(annotation.toString());
					while(model.getRowCount()!=0){
						model.removeRow(0);
					};
					annotation.properties()
					.sorted((k1,k2)->k1.name.compareTo(k2.name))
					.forEachOrdered(k->model.addRow(new Object[]{k.name, ((AKey)k).asString(annotation.getProperty(k))}));
				}
			});
			mt.start();
		}


		void registerManager(AnnotationManager mgr){
			mgr.addAnnotationStatusListener(this);
		}

		void deregisterManager(AnnotationManager mgr){
			mgr.removeAnnotationStatusListener(this);
		}


		@Override
		public void statusChanged(Annotation annotation) {
			mt.requestRerun(annotation);
		}



	}








	public class ConstrainedViewPortLayout extends ViewportLayout {

		@Override
		public Dimension preferredLayoutSize(Container parent) {

			Dimension preferredViewSize = super.preferredLayoutSize(parent);

			Container viewportContainer = parent.getParent();
			if (viewportContainer != null) {
				Dimension parentSize = viewportContainer.getSize();
				preferredViewSize.height = parentSize.height;
			}

			return preferredViewSize;
		}
	}



}
