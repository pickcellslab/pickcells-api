package org.pickcellslab.pickcells.api.img.providers;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.Image.ImageBuilder;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.RawFile;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.view.Lut8;

/**
 * A utility class useful to create Image or LabelsImage (data model objects) to use as inputs for creating ProviderFactories 
 * without an access to 'real' database objects
 *
 */
public final class ProviderFactoryUtils {

	private ProviderFactoryUtils(){/**/}




	/**
	 * Builds an {@link LabelsImage} instance for a given file (image representing a segmentation output) 
	 * @param image {@link Image} instance to set as {@link LabelsImage#origin()}
	 * @param checker An {@link ImgChecker} used to read the image file properties 
	 * @param list The {@link ImgFileList} pointing to the file for which to create the Image
	 * @param i Dataset coordinate of the desired file in the ImgFileList
	 * @param j Index coordinate of the desired file in the ImgFileList
	 * @return The {@link Image} instance with all attributes matching the desired image file.
	 */
	public static LabelsImage createLabels(Image image, ImgsChecker checker, ImgFileList list, int i, int j){

		// 1- get the dimensions of the image, including ordering and calibration
		int[] order = checker.value(ImgsChecker.ordering, list, i, j);
		order = ImgsChecker.convert(order);

		// Make sure only 1 channel!
		if(order[Image.c] !=-1)
			throw new IllegalArgumentException("The given Image has more than 1 channel and thus cannot be a segmentation result");


		// 2- initialise the builder
		int bitDepth = checker.value(ImgsChecker.bitDepth, list, i, j);
		
		return new SegmentationResult(image, list.path(i), list.name(i,j), "Provider generated",  bitDepth, "Created from an ImgFileList programatically");
	}



	/**
	 * Builds an {@link Image} instance for a given image file 
	 * @param io {@link ImgIO} instance used to configure the image
	 * @param checker An {@link ImgChecker} used to read the image file properties 
	 * @param list The {@link ImgFileList} pointing to the file for which to create the Image
	 * @param i Dataset coordinate of the desired file in the ImgFileList
	 * @param j Index coordinate of the desired file in the ImgFileList
	 * @return The {@link Image} instance with all attributes matching the desired image file.
	 */
	public static Image createImage(ImgIO io, ImgsChecker checker, ImgFileList list, int i, int j){
		// ************************************** Create an Image origin *********************************

		// 1- get the dimensions of the image, including ordering and calibration
		long[] dims = checker.value(ImgsChecker.dimensions, list, i, j);
		int[] order = checker.value(ImgsChecker.ordering, list, i, j);
		double[] cal = checker.value(ImgsChecker.calibration, list, i, j); 

		int x = index(order,Image.x);
		int y = index(order,Image.y);
		int c = index(order,Image.c);
		int z = index(order,Image.z);
		int t = index(order,Image.t);


		// 2- initialise the builder
		int bitDepth = checker.value(ImgsChecker.bitDepth, list, i, j);
		ImageBuilder builder = new ImageBuilder(
				list.name(i, j),
				dims[x], dims[y], 
				bitDepth)
				.setLocation(list.path(i));

		System.out.println("Image created for path : "+list.path(i)+File.separator+list.name(i, j));
		

		// 3- assign a RawFile
		RawFile rf = new RawFile(
				list.path(i),
				checker.value(ImgsChecker.extension, list, i, j),
				list.path(i)+File.separator+list.name(i, j),dims);
		builder.setRawFile(rf);


		// 4- Define the channels
		long cNum = c == -1 ? 1 : dims[c];
		for(int n = 0; n<cNum; n++)
			builder.setChannel(n, "Channel"+n, Lut8.values()[n].name());

		// 5- Set the channel dimension position if more than one channel
		if(c!=-1)
			builder.setChannelDimension(order[Image.c]);

		// 6- Define units and calibration in X and Y
		builder.setUnits(cal[x], cal[y], checker.value(ImgsChecker.sUnit, list, i, j));


		// 7- Define Z if image is 3D
		if(z!=-1)
			builder.setZ(dims[z], cal[z], z);					

		// 8- Define Time if image is time lapse
		if(t!=-1)
			builder.setT(dims[t],  cal[t], t, checker.value(ImgsChecker.tUnit, list, i, j));

		return  builder.build();
	}




	private static int index(int[] order, int i){
		for(int j = 0; j<order.length; j++)
			if(order[j]==i)
				return j;
		return -1;
	}


}
