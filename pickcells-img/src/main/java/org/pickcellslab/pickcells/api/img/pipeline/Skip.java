package org.pickcellslab.pickcells.api.img.pipeline;

import java.util.Objects;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Function;

import javax.swing.JLabel;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;

/**
 * 
 * An {@link ImgProcessing} Node which allows the user to skip the step
 * 
 * @author Guillaume Blin
 *
 * @param <T>
 */
public class Skip<T extends RealType<T>, O extends NativeType<O> & RealType<O>> implements ImgProcessing<T,O>{

	private PreviewBag<T> input;
	private final O outType;
	private final DimensionalityEffect tr;
	private final ImgIO io;

	public Skip(ImgIO io, O outType, DimensionalityEffect tr) {
		Objects.requireNonNull(io, "io is null");
		this.io = io;
		this.outType = outType;
		this.tr = tr;
	}


	@Override
	public String description() {
		return "Skips the step";
	}




	@Override
	public String name() {
		return "Skip";
	}


	@SuppressWarnings("serial")
	@Override
	public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state,	PreviewBag<T> input) {

		this.input = input;

		return new ValidityTogglePanel(){

			{
				add(new JLabel("Skip the Step"));
			}

			@Override
			public boolean validity() {
				return true;
			}

		};
	}


	@Override
	public void changeInput(PreviewBag<T> current) {
		this.input = current;			
	}


	@SuppressWarnings("unchecked")
	@Override
	public RandomAccessibleInterval<O> validate() {
		if(input.getCurrentInputType().getClass() == outType.getClass())
			return (RandomAccessibleInterval<O>) input.getInput();
		else
			return ImgDimensions.hardCopy(io, input.getInput(), outType);
	}




	@Override
	public boolean repeatable() {
		return false;
	}


	@Override
	public DimensionalityEffect dimensionalityEffect() {
		return tr;
	}





	@Override
	public Function<ImageBag<T>, RandomAccessibleInterval<O>> toPipe() {
		
		return (bag)->{
			if(bag.getCurrentInputType().getClass() == outType.getClass())
				return (RandomAccessibleInterval<O>) bag.getInputFrame(0);
			else
				return ImgDimensions.hardCopy(io, bag.getInputFrame(0), outType);
		};
		
	}




	@Override
	public boolean requiresAnnotationChannel() {
		return false;
	}


	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}


	@Override
	public boolean requiresOriginal() {
		return false;
	}


	@Override
	public boolean requiresFlat() {
		return false;
	}


	@Override
	public O outputType(T inputType) {
		return outType;
	}






}
