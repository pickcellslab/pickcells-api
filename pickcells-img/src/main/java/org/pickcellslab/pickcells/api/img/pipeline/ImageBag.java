package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;
import net.imglib2.view.Views;

/**
 * An {@link ImageBag} is an immutable class which provides access to the original image provided as input to the
 * {@link ProcessingPipeline} and to the intermediate images produced by upstream steps of the {@link ProcessingPipeline}.
 *
 * @param <I> The {@link Type} of the input image
 */
public final class ImageBag<I extends Type<I>> {

	private final MinimalImageInfo iInfo;
	private final MinimalImageInfo oInfo;
	private final MinimalImageInfo fInfo;
	private final long currentFrame;
	
	private final RandomAccessibleInterval<I> input;
	@SuppressWarnings("rawtypes")
	private final RandomAccessibleInterval original;
	@SuppressWarnings("rawtypes")
	private final RandomAccessibleInterval flat;
	
	<R1 extends Type<R1>, R2 extends Type<R2>> ImageBag(
			MinimalImageInfo iInfo, 
			RandomAccessibleInterval<I> input,
			MinimalImageInfo oInfo,
			RandomAccessibleInterval<R1> original,
			MinimalImageInfo fInfo,
			RandomAccessibleInterval<R2> flat,
			long currentFrame) {
		
		this.iInfo = iInfo; 	this.input = input;
		this.oInfo = oInfo;		this.original = original;
		this.fInfo = fInfo;		this.flat = flat;
		this.currentFrame = currentFrame;
		long[] dims = new long[input.numDimensions()];
		input.dimensions(dims);
		System.out.println("ImageBag : input dimensions : "+Arrays.toString(dims));
	}
	
	
	@SuppressWarnings("unchecked")
	public <T extends Type<T>> ImageBag<T> createWith(MinimalImageInfo iInfo, RandomAccessibleInterval<T> input, long timePos){
		assert input != null : "Input is null";
		return new ImageBag<>(iInfo, input, oInfo, original, fInfo, flat, timePos);
	}
	
	
	
		
	
	/**
	 * @param relIndex The frame index relative to the current time frame
	 * @return The t + relIndex frame in the input image. By 'input' we mean the result image created by the previous step in the 
	 * {@link ProcessingPipeline}
	 */
	public RandomAccessibleInterval<I> getInputFrame(long relIndex){
		System.out.println("ImageBag : Current Frame = "+ currentFrame);
		//if(currentFrame+relIndex<0 || currentFrame+relIndex>iInfo.dimension(Image.t))
		//	return null;
		return iInfo.dimension(Image.t)==1 ? input : Views.hyperSlice(input, iInfo.index(Image.t), currentFrame+relIndex); 
	}

	/**
	 * @param relIndex The frame index relative to the current time frame
	 * @return The t+relIndex frame in the original image
	 */
	@SuppressWarnings("unchecked")
	public <R extends Type<R>> RandomAccessibleInterval<R> getOriginalFrame(long relIndex) {
		//if(currentFrame+relIndex<0 || currentFrame+relIndex>oInfo.dimension(Image.t))
		//	return null;
		return oInfo.dimension(Image.t)==1 ? original : Views.hyperSlice(original, oInfo.index(Image.t), currentFrame+relIndex); 
	}


	/**
	 * @param relIndex The frame index relative to the current time frame
	 * @return The t + relIndex frame in the image produced by the first step which produced a single channel image.
	 * Note that this single channel image may have been modified in place by subsequent steps.
	 * @see DimensionalityEffect
	 */
	@SuppressWarnings("unchecked")
	public <R extends Type<R>> RandomAccessibleInterval<R> getFirstFlat(long relIndex) {
		//if(currentFrame+relIndex<0 || currentFrame+relIndex>fInfo.dimension(Image.t))
		//	return null;
		return fInfo.dimension(Image.t)==1 ? flat : Views.hyperSlice(flat, fInfo.index(Image.t), currentFrame+relIndex); 
	}


	/**
	 * @return An instance of the {@link Type} of the input image. By 'input' we mean the result image created by the previous step in the 
	 * {@link ProcessingPipeline}
	 */
	public I getCurrentInputType() {
		return Views.iterable(input).firstElement();
	}
	
	/**
	 * @return The number of frames in the input image. By 'input' we mean the result image created by the previous step in the 
	 * {@link ProcessingPipeline}
	 */
	public long numInputFrames() {
		return iInfo.dimension(Image.t);
	};
	
	
	/**
	 * @return A {@link MinimalImageInfo} for a single frame of the input image. By 'input' we mean the result image created by the previous step in the 
	 * {@link ProcessingPipeline}
	 */
	public MinimalImageInfo getInputFrameInfo(){
		return iInfo.removeDimension(Image.t);
	}
	
	/**
	 * @return A {@link MinimalImageInfo} for a single frame of the original image. By 'original', we mean the image provided as input at the start of
	 * the {@link ProcessingPipeline}.
	 */
	public MinimalImageInfo getOriginalFrameInfo(){
		return oInfo.removeDimension(Image.t);
	}
	
	/**
	 * @return The number of frames in the original image. By 'original', we mean the image provided as input at the start of
	 * the {@link ProcessingPipeline}.
	 */
	public long numOriginalFrames() {
		return oInfo.dimension(Image.t);
	};
	
	/**
	 * @return A {@link MinimalImageInfo} for a single frame of the first flat image. By 'flat', we mean the image created by the first step which produced
	 * a single channel image in the {@link ProcessingPipeline}. Note that this image may have been modified in place by subsequent steps.
	 * @see DimensionalityEffect
	 */
	public MinimalImageInfo getFlatFrameInfo(){
		return fInfo.removeDimension(Image.t);
	}
	
	/**
	 * @return The number of frames in the first flat image. By 'flat', we mean the image created by the first step which produced
	 * a single channel image in the {@link ProcessingPipeline}.
	 * @see DimensionalityEffect
	 */
	public long numFlatFrames() {
		return fInfo.dimension(Image.t);
	};
	
	
	
	/**
	 * @return A {@link MinimalImageInfo} for the input image. By 'input' we mean the result image created by the previous step in the 
	 * {@link ProcessingPipeline}
	 */
	MinimalImageInfo getInputInfo() {
		return iInfo;
	}
	
	/**
	 * @return The input image. By 'input' we mean the result image created by the previous step in the 
	 * {@link ProcessingPipeline}
	 */
	RandomAccessibleInterval<I> getInput(){
		return input;
	}
	
	/**
	 * @return A {@link MinimalImageInfo} for the original image. By 'original', we mean the image provided as input at the start of
	 * the {@link ProcessingPipeline}.
	 */
	MinimalImageInfo getOriginalInfo() {
		return oInfo;
	}
	
	/**
	 * @return The original image. By 'original', we mean the image provided as input at the start of
	 * the {@link ProcessingPipeline}.
	 */
	@SuppressWarnings("unchecked")
	<R extends Type<R>> RandomAccessibleInterval<R> getOriginal(){
		return original;
	}

	/**
	 * @return A {@link MinimalImageInfo} for the first flat image. By 'flat', we mean the image created by the first step which produced
	 * a single channel image in the {@link ProcessingPipeline}. Note that this image may have been modified in place by subsequent steps.
	 * @see DimensionalityEffect
	 */
	MinimalImageInfo getFirstFlatInfo() {
		return fInfo;
	}
	
	/**
	 * @return The first flat image. By 'flat', we mean the image created by the first step which produced
	 * a single channel image in the {@link ProcessingPipeline}. Note that this image may have been modified in place by subsequent steps.
	 * @see DimensionalityEffect
	 */
	@SuppressWarnings("unchecked")
	<R extends Type<R>> RandomAccessibleInterval<R> getFirstFlat(){
		return flat;
	}

	
	/**
	 * @return The current frame index
	 */
	long getCurrentFrame(){
		return currentFrame;
	}

	
	
	
	
	
}
