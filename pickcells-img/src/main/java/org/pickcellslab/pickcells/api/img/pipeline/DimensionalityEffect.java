package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.process.DimensionalityAlteration;

import net.imglib2.RandomAccessibleInterval;

/**
 * 
 * Defines the dimensionality changes that an {@link ImgProcessing} may operate on an input {@link RandomAccessibleInterval}
 * 
 * @author Guillaume Blin
 *
 */
public enum DimensionalityEffect implements DimensionalityAlteration{
	//TODO Add other transformations such as Z flattening, CFlat_CFlat, etc..., make tests and throw exception in Builder
	
	/**
	 * No change applied to the dimensions of the image.
	 * Supports any type of dimensionality
	 */
	CHANNELS_TO_CHANNELS{

		@Override
		public MinimalImageInfo createAlteration(MinimalImageInfo input) {
			// TODO Auto-generated method stub
			return null;
		}
		
	}, 
	
	/**
	 * No change applied to the dimensions of the image, works on the result directly.
	 * NB: Expects a single channel image.
	 */
	FLAT_TO_FLAT{

		@Override
		public MinimalImageInfo createAlteration(MinimalImageInfo input) {
			return input;
		}
		
	}, 
	
	/**
	 * Removes the channel dimension
	 */
	CHANNELS_TO_FLAT{

		@Override
		public MinimalImageInfo createAlteration(MinimalImageInfo input) {
			return input.removeDimension(Image.c);
		}
		
	}
}
