package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.concurrent.atomic.AtomicBoolean;

import net.imglib2.Interval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.converter.Converter;
import net.imglib2.type.numeric.NumericType;
import net.imglib2.view.Views;

public class AnnotationProjector2D<O extends NumericType<O>> {



	private final AnnotationManager mgr;	
	private Converter<Annotation,O> converter;

	private final RandomAccessibleInterval<O> target;
	private final O outType;

	private AtomicBoolean stopMapping = new AtomicBoolean(false);


	public AnnotationProjector2D(AnnotationManager mgr, RandomAccessibleInterval<O> target, Converter<Annotation,O> converter, O outType) {		
		this.mgr = mgr;
		this.converter = converter;
		this.target = Views.interval(Views.extendZero(target), target);
		//System.out.println(target);
		this.outType = outType;
	}


	public void setConverter(Converter<Annotation,O> converter){
		this.converter = converter;
	}


	
	public void map(Interval itrv) {

		if(itrv == null)
			return;


		final O zero = outType.createVariable();

		final RandomAccess<O> access = target.randomAccess(itrv);
		mgr.produceCurrentPlaneIteration(
				stopMapping,
				(pos,annotation)->{
					access.setPosition(pos);
					if(annotation==null)
						access.get().set(zero);
					else{
						converter.convert(annotation, outType);
						access.get().set(outType);
					}
				},
				itrv
				);

		//System.out.println("AnnotationProjector2D : Mapping done");
		stopMapping.set(false);
	}



	public Interval defaultInterval() {
		return target;
	}




	







}
