package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.colorchooser.ColorSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.pickcellslab.foundationj.ui.utils.CardPanel;

import com.alee.extended.button.WebSwitch;
import com.alee.laf.colorchooser.WebColorChooser;

import net.imglib2.converter.Converter;
import net.imglib2.type.numeric.ARGBType;

public class AnnotationStateAppearanceControl implements AnnotationAppearanceControl {


	private final List<ChangeListener> lstrs = new ArrayList<>();
	private final ChangeEvent evt = new ChangeEvent(this);

	private final static int[] cols = new int[]{		
			ARGBType.rgba(Color.LIGHT_GRAY.getRed(), Color.LIGHT_GRAY.getGreen(), Color.LIGHT_GRAY.getBlue(), 255),						
			ARGBType.rgba(Color.YELLOW.getRed(), Color.YELLOW.getGreen(), 0, 255),
			ARGBType.rgba(255, 0, 0, 255),
			ARGBType.rgba(Color.BLUE.getRed(), Color.BLUE.getGreen(), Color.BLUE.getBlue(), 255),
			ARGBType.rgba(Color.ORANGE.getRed(), Color.ORANGE.getGreen(), Color.ORANGE.getBlue(), 255),
			ARGBType.rgba(Color.WHITE.getRed(), Color.WHITE.getGreen(), Color.WHITE.getBlue(), 255),			
			ARGBType.rgba(Color.GRAY.getRed(), Color.GRAY.getGreen(), Color.GRAY.getBlue(), 255),
			ARGBType.rgba(Color.CYAN.getRed(), Color.CYAN.getGreen(), Color.CYAN.getBlue(), 255),			
			ARGBType.rgba(Color.MAGENTA.getRed(), Color.MAGENTA.getGreen(), Color.MAGENTA.getBlue(), 255),
			ARGBType.rgba(Color.PINK.getRed(), Color.PINK.getGreen(), Color.PINK.getBlue(), 255),
			ARGBType.rgba(Color.GREEN.getRed(), Color.GREEN.getGreen(), Color.GREEN.getBlue(), 255) 
	};

	private final Map<AnnotationStatus, Integer> apps = new HashMap<>();
	
	private final AnnotationManager mgr;	
	
	private final AnnotationStatusAppearanceUI ui;


	public AnnotationStateAppearanceControl(AnnotationManager mgr) {
		this.mgr = mgr;
		this.ui = new AnnotationStatusAppearanceUI(mgr);
	}
	
	
	@Override
	public AnnotationManager getManager() {
		return mgr;
	}
	

	@Override
	public JComponent getUI() {
		return ui;
	}

	@Override
	public Converter<Annotation, ARGBType> getConverter() {
		return (input,output)->	output.set(apps.get(input.status()));
	}




	public void addChangeListener(ChangeListener l){
		lstrs.add(l);
	}


	public void removeChangeListener(ChangeListener l){
		lstrs.remove(l);
	}

	
	@Override 
	public String toString(){
		return "Annotation State";
	}



	@SuppressWarnings("serial")
	private class AnnotationStatusAppearanceUI extends JPanel{



		private final JComboBox<AnnotationStatus> series;
		private final CardPanel<AnnotationStatus, WebColorChooser> card;



		private AnnotationStatusAppearanceUI(AnnotationManager mgr) {



			// Build the dialog
			series = new JComboBox<>();
			for(AnnotationStatus as : mgr.possibleAnnotationStates()){
				series.addItem(as);
				apps.put(as, cols[(apps.size())%cols.length]);
			}

			//app.registeredSeries().forEach(s->series.addItem(s));


			card = new CardPanel<>((s)-> {
								
				final WebColorChooser wc = new WebColorChooser(new Color(apps.get(s)));
				final ColorSelectionModel model = wc.getSelectionModel();
				model.addChangeListener(l->{
						apps.put(s, model.getSelectedColor().getRGB());
						lstrs.forEach(delegate->delegate.stateChanged(evt));
				});
				return wc;
				
			});
			card.show((AnnotationStatus) series.getSelectedItem());


			series.addActionListener(l->{
				card.show((AnnotationStatus) series.getSelectedItem());
			});


			final WebSwitch visibility = new WebSwitch();
			visibility.addActionListener(l->{
				mgr.toggleAnnotationsVisible();
			});
			
			setLayout(new BorderLayout());
			add(series,BorderLayout.NORTH);				
			add(card, BorderLayout.CENTER);
			add(visibility, BorderLayout.SOUTH);

		}



	}



}
