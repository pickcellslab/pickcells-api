/*******************************************************************************
 * Copyright (c) 2012-2013 Biomedical Image Group (BIG), EPFL, Switzerland.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 *     Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 ******************************************************************************/
package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Class that implements convolution with kernels. Based on the
 * <code>Interpolator</code> package of Francois Aguet.
 * 
 * @version April 22, 2013
 * 
 * @author Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 * @author Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 * 
 * <hr>
 * NOTICE: This code was copied from a plugin originally implemented by François Aguet for ImageJ, and ported to Icy by Ricard Delgado-Gonzalo (ricard.delgado@gmail.com) 
 * and Zsuzsanna Püspöki (zsuzsanna.puspoki@epfl.ch). This plugin was based on the following publication:
 * <br><br>
 * M. Jacob, M. Unser, “Design of Steerable Filters for Feature Detection Using Canny-Like Criteria,”
 * IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 8, pp. 1007-1019
 * <hr> 
 * 
 */
public class SplineInterpolator {

	/** Rasterized image. */
	private double[] input_ = null;
	private int imageWidth_ = 0;
	private int imageHeight_ = 0;

	/** Spline coefficients. */
	private double[] c_ = null;
	/** Auxiliary magnification factor for the spline coefficients. */
	private double a_ = 0;
	/** Auxiliary magnification factor for the spline coefficients. */
	private double c0_ = 0;

	/** Interpolation mode. */
	private String interpolationMode_ = null;

	// ============================================================================
	// PUBLIC METHODS

	/**
	 * Constructor.
	 * 
	 * @param input
	 *            1D array storing the rasterized image.
	 * @param nx
	 *            Width of the image.
	 * @param ny
	 *            Height of the image.
	 * @param mode
	 *            Interpolation mode. Options: "linear", "quadratic", "cubic".
	 */
	public SplineInterpolator(double[] input, int nx, int ny, String mode) {
		this.input_ = input;
		this.imageWidth_ = nx;
		this.imageHeight_ = ny;
		this.interpolationMode_ = mode;
		if (mode.equals("linear")) {
			c_ = input;
		} else if (mode.equals("quadratic")) {
			c0_ = 8.0;
			a_ = -3.0 + 2.0 * Math.sqrt(2.0);
			computeCoefficients();
		} else if (mode.equals("cubic")) {
			c0_ = 6.0;
			a_ = -2.0 + Math.sqrt(3.0);
			computeCoefficients();
		}

	}

	// ----------------------------------------------------------------------------

	/** Return the value interpolated at (x,y). */
	public double getValue(double x, double y) {
		int xi = (int) x;
		int yi = (int) y;
		int x0, x1, y0, y1;

		if (interpolationMode_.equals("linear")) {
			double dx = x - xi;
			double dy = y - yi;
			if (x < 0) {
				dx = -dx;
				x1 = mirror(xi - 1, imageWidth_);
			} else {
				x1 = mirror(xi + 1, imageWidth_);
			}
			if (y < 0) {
				dy = -dy;
				y1 = mirror(yi - 1, imageHeight_);
			} else {
				y1 = mirror(yi + 1, imageHeight_);
			}
			x0 = mirror(xi, imageWidth_);
			y0 = mirror(yi, imageHeight_);
			return (1.0 - dy)
					* (dx * input_[x1 + y0 * imageWidth_] + (1.0 - dx)
							* input_[x0 + y0 * imageWidth_])
					+ dy
					* (dx * input_[x1 + y1 * imageWidth_] + (1.0 - dx)
							* input_[x0 + y1 * imageWidth_]);
		} else {
			double dx = x - xi;
			double dy = y - yi;
			double[] wx, wy;
			int x2, x3, y2, y3;

			if (x < 0) {
				xi = xi - 1;
				dx = 1.0 + dx;
			}
			if (y < 0) {
				yi = yi - 1;
				dy = 1.0 + dy;
			}

			if (interpolationMode_.equals("quadratic")) {
				wx = getQuadraticSpline(dx);
				wy = getQuadraticSpline(dy);
			} else { // if (mode.equals("cubic")) {
				wx = getCubicSpline(dx);
				wy = getCubicSpline(dy);
			}

			x0 = xi - 1;
			x1 = xi;
			x2 = xi + 1;
			x3 = xi + 2;
			x0 = mirror(x0, imageWidth_);
			x1 = mirror(x1, imageWidth_);
			x2 = mirror(x2, imageWidth_);
			x3 = mirror(x3, imageWidth_);

			y0 = yi - 1;
			y1 = yi;
			y2 = yi + 1;
			y3 = yi + 2;
			y0 = mirror(y0, imageHeight_);
			y1 = mirror(y1, imageHeight_);
			y2 = mirror(y2, imageHeight_);
			y3 = mirror(y3, imageHeight_);
			y0 *= imageWidth_;
			y1 *= imageWidth_;
			y2 *= imageWidth_;
			y3 *= imageWidth_;

			double v = wx[0]
					* (wy[0] * c_[x0 + y0] + wy[1] * c_[x0 + y1] + wy[2]
							* c_[x0 + y2] + wy[3] * c_[x0 + y3])
					+ wx[1]
					* (wy[0] * c_[x1 + y0] + wy[1] * c_[x1 + y1] + wy[2]
							* c_[x1 + y2] + wy[3] * c_[x1 + y3])
					+ wx[2]
					* (wy[0] * c_[x2 + y0] + wy[1] * c_[x2 + y1] + wy[2]
							* c_[x2 + y2] + wy[3] * c_[x2 + y3])
					+ wx[3]
					* (wy[0] * c_[x3 + y0] + wy[1] * c_[x3 + y1] + wy[2]
							* c_[x3 + y2] + wy[3] * c_[x3 + y3]);
			return v;
		}

	}

	// ============================================================================
	// PRIVATE METHODS

	/** Computes the B-spline coefficients. */
	private void computeCoefficients() {
		// Horizontal
		double[] cp = getCausalInitHorizontal(input_, a_);
		int i;
		for (int y = 0; y < imageHeight_; y++) {
			for (int x = 1; x < imageWidth_; x++) { // causal
				i = x + y * imageWidth_;
				cp[i] = input_[i] + a_ * cp[i - 1];
			}
		}
		c_ = getAntiCausalInitHorizontal(cp, a_); // cn
		for (int y = 0; y < imageHeight_; y++) {
			for (int x = imageWidth_ - 2; x >= 0; x--) { // anticausal
				i = x + y * imageWidth_;
				c_[i] = a_ * (c_[i + 1] - cp[i]);
			}
		}
		// Vertical
		cp = getCausalInitVertical(c_, a_);
		for (int x = 0; x < imageWidth_; x++) {
			for (int y = 1; y < imageHeight_; y++) {
				i = x + y * imageWidth_;
				cp[i] = c_[i] + a_ * cp[i - imageWidth_];
			}
		}
		c_ = getAntiCausalInitVertical(cp, a_);
		for (int x = 0; x < imageWidth_; x++) {
			for (int y = imageHeight_ - 2; y >= 0; y--) {
				i = x + y * imageWidth_;
				c_[i] = a_ * (c_[i + imageWidth_] - cp[i]);
			}
		}
		// constant
		double c02 = c0_ * c0_;
		for (i = 0; i < imageHeight_ * imageHeight_; i++) {
			c_[i] = c02 * c_[i];
		}
	}

	// ----------------------------------------------------------------------------

	/** Computes the mirror boundary conditions. */
	private int mirror(int x, int nx) {
		if (x >= 0 && x < nx) {
			return x;
		} else if (x < 0) {
			return -x;
		} else {
			return 2 * nx - 2 - x;
		}
	}

	// ----------------------------------------------------------------------------

	/**
	 * Computes the quadratic spline basis function at a position t.
	 * 
	 * @param t
	 *            argument between 0 and 1.
	 * @return 4 sampled values of the cubic B-spline (B3[t+1], B3[t], B3[t-1],
	 *         B3[t-2]).
	 */
	private static double[] getQuadraticSpline(double t) {
		if (t < 0.0 || t > 1.0) {
			throw new ArrayStoreException(
					"Argument t for quadratic B-spline outside of expected range [0, 1]: "
							+ t);
		}
		double v[] = new double[4];
		if (t <= 0.5) {
			v[0] = (t - 0.5) * (t - 0.5) / 2.0;
			v[1] = 0.75 - t * t;
			v[2] = 1.0 - v[1] - v[0]; // (t+0.5)*(t+0.5)/2.0;
			v[3] = 0.0;
		} else {
			v[0] = 0.0;
			v[1] = (t - 1.5) * (t - 1.5) / 2.0;
			v[3] = (t - 0.5) * (t - 0.5) / 2.0;
			v[2] = 1.0 - v[3] - v[1];
		}
		return v;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Computes the cubic spline basis function at a position t.
	 * 
	 * @param t
	 *            argument between 0 and 1.
	 * @return 4 sampled values of the cubic B-spline (B3[t+1], B3[t], B3[t-1],
	 *         B3[t-2]).
	 */
	private static double[] getCubicSpline(double t) {
		if (t < 0.0 || t > 1.0) {
			throw new ArrayStoreException(
					"Argument t for cubic B-spline outside of expected range [0, 1]: "
							+ t);
		}
		double v[] = new double[4];
		double t1 = 1.0 - t;
		double t2 = t * t;
		v[0] = (t1 * t1 * t1) / 6.0;
		v[1] = (2.0 / 3.0) + 0.5 * t2 * (t - 2);
		v[3] = (t2 * t) / 6.0;
		v[2] = 1.0 - v[3] - v[1] - v[0];
		return v;
	}

	// ----------------------------------------------------------------------------

	private double[] getAntiCausalInitVertical(double[] s, double a) {
		double[] cn = new double[imageWidth_ * imageHeight_];
		int idx = (imageHeight_ - 1) * imageWidth_;
		double d = a * a - 1.0;
		for (int x = 0; x < imageWidth_; x++) {
			cn[x + idx] = a * (s[x + idx] + a * s[x + idx - imageWidth_]) / d;
		}
		return cn;
	}

	// ----------------------------------------------------------------------------

	private double[] getAntiCausalInitHorizontal(double[] s, double a) {
		double[] cn = new double[imageWidth_ * imageHeight_];
		double d = a * a - 1.0;
		for (int y = 0; y < imageHeight_; y++) {
			cn[imageWidth_ - 1 + y * imageWidth_] = a
					* (s[imageWidth_ - 1 + y * imageWidth_] + a
							* s[imageWidth_ - 2 + y * imageWidth_]) / d;
		}
		return cn;
	}

	// ----------------------------------------------------------------------------

	private double[] getCausalInitVertical(double[] s, double z) {
		double[] cp = new double[imageWidth_ * imageHeight_];
		double zd, sum, den, za;
		for (int x = 0; x < imageWidth_; x++) {
			zd = Math.pow(z, imageHeight_ - 1);
			sum = s[x] + s[x + (imageHeight_ - 1) * imageWidth_] * zd;
			den = zd * zd;
			zd = den / z;
			za = z;
			for (int y = 1; y < imageHeight_ - 1; y++) {
				sum += s[x + y * imageWidth_] * (za + zd);
				za *= z;
				zd /= z;
			}
			sum /= (1 - den);
			cp[x] = sum;
		}
		return cp;
	}

	// ----------------------------------------------------------------------------

	private double[] getCausalInitHorizontal(double[] s, double z) {
		double[] cp = new double[imageWidth_ * imageHeight_];
		double zd, sum, den, za;
		for (int y = 0; y < imageHeight_; y++) {
			zd = Math.pow(z, imageWidth_ - 1);
			sum = s[y * imageWidth_] + s[imageWidth_ - 1 + y * imageWidth_]
					* zd;
			den = zd * zd;
			zd = den / z;
			za = z;
			for (int x = 1; x < imageWidth_ - 1; x++) {
				sum += s[x + y * imageWidth_] * (za + zd);
				za *= z;
				zd /= z;
			}
			sum /= (1 - den);
			cp[y * imageWidth_] = sum;
		}
		return cp;
	}
}
