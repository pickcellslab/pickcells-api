package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.ImageIcon;

import org.apache.commons.math3.util.FastMath;
import org.pickcellslab.pickcells.api.img.editors.Brush;
import org.pickcellslab.pickcells.api.img.editors.PaintInstance;
import org.pickcellslab.pickcells.api.img.editors.PaintInstanceListener;
import org.pickcellslab.pickcells.api.img.editors.PaintableAnnotation;
import org.pickcellslab.pickcells.api.img.editors.UndoSupport;

public class DefaultPaintInstance<P,D> implements PaintInstance<P,D> {

	private final List<PaintInstanceListener> lstrs = new ArrayList<>();

	private final UndoSupport<long[]> us = new UndoSupport<>(5000);

	private final long[] min, max;

	private Brush brush;

	private final PaintableAnnotation<P,D> annotation;

	private int undoSteps = 1;

	public DefaultPaintInstance(Brush brush, PaintableAnnotation<P,D> annotation) {
		assert brush!=null : "brush is null";
		assert annotation!=null : "annotation is null";
		this.brush = brush;
		this.annotation = annotation;
		min = new long[brush.minBound().length];
		Arrays.fill(min, Long.MAX_VALUE);
		max = new long[brush.maxBound().length];
		Arrays.fill(max, Long.MIN_VALUE);
		for(int d = 0; d<brush.numDimensions(); d++)
			undoSteps *= brush.maxBound()[d]-brush.minBound()[d];
		System.out.println("DefaultPaintInstances : UndoSteps = "+undoSteps);
	}




	@Override
	public PaintableAnnotation<P,D> getAnnotation() {
		return annotation;
	}




	@Override
	public void draw(long[] coords) {		

		final AtomicBoolean change = new AtomicBoolean(false);
		brush.setPosition(coords);	
		brush.forEach(pos->{
			if(brush.computeAndSet(pos)){
				change.set(true);	
				us.done(Arrays.copyOf(pos, pos.length));
			}
		});
		if(change.get()){			
			updateMin(min, brush.minBound());
			updateMax(max, brush.maxBound());
			lstrs.forEach(l->l.paintJobPerformed(this, brush.minBound(), brush.maxBound()));
		}

	}


	@Override
	public void undo() {
		for(int i = 0; i<undoSteps; i++)
			us.undo().ifPresent(coords->brush.revert(coords));
		lstrs.forEach(l->l.paintJobPerformed(this, min, max));
	}



	@Override
	public void redo() {
		for(int i = 0; i<undoSteps; i++)
			us.redo().ifPresent(coords->{		
				brush.computeAndSet(coords);
				updateMin(min, coords);
				updateMax(max, coords);		
			});
		lstrs.forEach(l->l.paintJobPerformed(this, min, max));
	}


	@Override
	public void clear() {
		us.forEach(l->brush.revert(l));
		us.clear();
	}




	@Override
	public void cancel() {
		clear();
		new ArrayList<>(lstrs).forEach(l->l.cancelled(this));
	}




	@Override
	public long[] min() {
		return min;
	}

	@Override
	public long[] max() {
		return max;
	}

	private void updateMin(long[] current, long[] novel){
		for(int i = 0; i<current.length; i++)
			current[i] = FastMath.min(current[i], novel[i]);
	}




	private void updateMax(long[] current, long[] novel){
		for(int i = 0; i<current.length; i++)
			current[i] = FastMath.max(current[i], novel[i]);
	}




	@Override
	public void setBrush(Brush brush) {
		Objects.requireNonNull(brush, "The brush cannot be null");
		this.brush = brush;
		for(int d = 0; d<brush.numDimensions(); d++)
			undoSteps *= brush.maxBound()[d]-brush.minBound()[d];
	}



	@Override
	public Brush getBrush() {
		return brush;
	}




	@Override
	public ImageIcon getBrushIcon() {
		return brush.cursorIcon();
	}

	@Override
	public int getBrushSizeX(){
		int x = (int) (brush.maxBound()[0] - brush.minBound()[0]);
		return x;
	}
	@Override
	public int getBrushSizeY(){
		int y = (int) (brush.maxBound()[1] - brush.minBound()[1]);
		return y;
	}



	@Override
	public void addPaintInstanceListener(PaintInstanceListener l) {
		lstrs.add(l);
	}

	@Override
	public void removePaintInstanceListener(PaintInstanceListener l) {
		lstrs.remove(l);
	}

}
