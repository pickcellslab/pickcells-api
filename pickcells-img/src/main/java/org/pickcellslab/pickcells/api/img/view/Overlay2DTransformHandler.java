package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import com.alee.utils.SwingUtils;

import net.imglib2.IterableInterval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.converter.Converter;
import net.imglib2.converter.TypeIdentity;
import net.imglib2.display.screenimage.awt.ARGBScreenImage;
import net.imglib2.realtransform.AffineTransform2D;
import net.imglib2.type.numeric.ARGBType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.ui.AffineTransformType2D;
import net.imglib2.ui.OverlayRenderer;
import net.imglib2.ui.PainterThread;
import net.imglib2.ui.Renderer;
import net.imglib2.ui.util.Defaults;
import net.imglib2.ui.util.InterpolatingSource;
import net.imglib2.view.Views;

class Overlay2DTransformHandler<T extends RealType<T>> implements ImageDisplayModel<T>, ChannelListener<T> {

	private final RandomAccess<T> access;
	private final RandomAccessibleInterval<T> source;

	private final MinimalImageInfo info;
	private final MinimalImageInfo channelInfo;
	private final long[] currentLocation;


	private final List<ProjectorThread> projThreads = new ArrayList<>();
	private final List<Renderer<AffineTransform2D>> renderers = new ArrayList<>();
	private final List<ChannelControlModel<T>> models = new ArrayList<>();
	private final List<OverlayRenderer> blenders = new ArrayList<>();

	private final List<OffAllowedRandomAccessibleProjector2D<ARGBType,T>> projectors = new ArrayList<>();

	private final List<ChangeListener> lstrs = new ArrayList<>();
	private final ChangeEvent evt = new ChangeEvent(this);

	private final PainterThread pt;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	Overlay2DTransformHandler(final RandomAccessibleInterval<T> img, final MinimalImageInfo info, final PainterThread painterThread) {

		this.source = img;
		this.access = Views.extendZero(source).randomAccess();
		this.info = info;
		this.channelInfo = info.removeDimension(Image.c);
		currentLocation = new long[info.numDimensions()];

		this.pt = painterThread;

		System.out.println("is flat ->"+Views.iterable(img).iterationOrder().getClass().getName());

		RandomAccessibleInterval<T> channel = img;
		for(int c = 0; c<info.dimension(Image.c); c++){

			if(info.dimension(Image.c)>1)
				channel = Views.hyperSlice(img, info.index(Image.c), c);

			final ChannelControlModel<T> model = new RealLutChannelControlModel<>("Channel "+c, makeProbingInterval(channelInfo, channel, c));
			model.addChannelListener(this);
			models.add(model);


			final ARGBScreenImage screenImage = new ARGBScreenImage((int)info.dimension(Image.x), (int)info.dimension(Image.y));

			projectors.add(	new OffAllowedRandomAccessibleProjector2D((int)info.index(Image.x), (int)info.index(Image.y), channel, screenImage, (Converter) model));

			final InterpolatingSource<ARGBType, AffineTransform2D> interpolatingSource = 
					new InterpolatingSource<ARGBType,AffineTransform2D>(
							Views.extendValue(screenImage, new ARGBType()), new AffineTransform2D(), new TypeIdentity<ARGBType>() );

			final ProjectorThread projThread = new ProjectorThread(projectors.get(c), painterThread, lstrs, evt);		projThread.start();
			projThreads.add(projThread);

			final BlendedBufferedImageOverlayRenderer blender = new BlendedBufferedImageOverlayRenderer();
			blenders.add(blender);
			renderers.add(Defaults.rendererFactory(AffineTransformType2D.instance, interpolatingSource ).create(blender, painterThread));
		}


	}


	@Override
	public MinimalImageInfo getMinimalInfo(){
		return info;
	}

	@Override
	public RandomAccessibleInterval<T> getSource(){
		return source;
	}



	ChannelControlModel<T> getChannel(int c){
		return models.get(c);
	}


	public OverlayRenderer getBlender(int c) {
		return blenders.get(c);
	}




	void setPosition(int dim, int position) {
		currentLocation[channelInfo.index(dim)] = position;
		for(int c = 0; c<models.size(); c++){
			projectors.get(c).setPosition(position, channelInfo.index(dim));			
		}	
		refresh();
	}



	void refresh() {
	//	System.out.println("Overlay2DTransformHandler painting with transform refreshing");
		for(int c = 0; c<models.size(); c++){
			projThreads.get(c).requestRemap();
		}
		lstrs.forEach(l->l.stateChanged(evt));
	}


	void paint(AffineTransform2D viewerTransform) {		
	//	System.out.println("Overlay2DTransformHandler painting with transform "+viewerTransform);

		synchronized(pt) {

			int count = 0;
			while(count!=models.size()) {
				count=0;
				for(int c = 0; c<models.size(); c++) {
					if(projThreads.get(c).isComplete())
						count++;
				}
			}
			
			for(int c = 0; c<models.size(); c++){
				renderers.get(c).paint(viewerTransform);
			}

		}
	}



	private IterableInterval<T> makeProbingInterval(MinimalImageInfo info, RandomAccessibleInterval<T> interval, int channel){

		RandomAccessibleInterval<T> itrv = interval;
		MinimalImageInfo newInfo = info;

		if(newInfo.dimension(Image.t)>1){
			itrv = Views.hyperSlice(itrv, newInfo.index(Image.t), newInfo.dimension(Image.t)/2);
			newInfo = newInfo.removeDimension(Image.t);
		}
		if(newInfo.dimension(Image.z)>1 && newInfo.dimension(Image.x)*newInfo.dimension(Image.y)*newInfo.dimension(Image.z)>1000000){
			itrv = Views.hyperSlice(itrv, newInfo.index(Image.z), newInfo.dimension(Image.z)/2);
			newInfo = newInfo.removeDimension(Image.z);
		}

		//Check size of plane:
		if(newInfo.dimension(Image.x)>2048 || newInfo.dimension(Image.y)>2048){
			long[] min = new long[itrv.numDimensions()]; long[]max = new long[min.length];
			max[newInfo.index(Image.x)] = Math.min(2048, newInfo.dimension(Image.x));
			max[newInfo.index(Image.y)] = Math.min(2048, newInfo.dimension(Image.y));
			Views.interval(itrv, min, max);
			// System.out.println("Overlay2DTransformHandler -> Plane reduced");
		}

		// System.out.println("Overlay2DTransformHandler -> itrv dims : "+itrv.numDimensions());

		return Views.iterable(itrv);
	}



	//----------------------------------------------------------------------------
	//Listen for visibility changes of channels


	@Override
	public void visibilityChanged(ChannelControl<T> source) {
		int c = models.indexOf(source);
		projectors.get(c).setVisible(source.isVisible()); 		
		SwingUtils.invokeLater(()->projectors.get(c).map());
		lstrs.forEach(l->l.stateChanged(evt));
	}


	@Override
	public void contrastChanged(ChannelControl<T> source) {
		int c = models.indexOf(source);
		projThreads.get(c).requestRemap();
		lstrs.forEach(l->l.stateChanged(evt));
	}

	@Override
	public void lutChanged(ChannelControl<T> source) {
		int c = models.indexOf(source);
		projThreads.get(c).requestRemap();
		lstrs.forEach(l->l.stateChanged(evt));
	}

	@Override
	public void histogramChanged(ChannelControl<T> source) {}


	@Override
	public void nameHasChanged(ChannelControl<T> source) {}



	void addChangeListener(ChangeListener l){
		lstrs.add(l);
	}

	void removeChangeListener(ChangeListener l){
		lstrs.remove(l);
	}



	@Override
	public long[] currentlyDisplayedLocation() {
		return currentLocation;
	}


	@Override
	public T getValueAt(long[] loc) {
		access.setPosition(loc);
		return access.get();
	}






}
