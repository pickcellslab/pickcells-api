package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.imglib2.ui.InteractiveDisplayCanvasComponent;
import net.imglib2.ui.OverlayRenderer;

@SuppressWarnings("serial")
public class DynamicInteractiveDisplayCanvasComponent<A> extends InteractiveDisplayCanvasComponent<A> {

	private final List<BlendedBufferedImageOverlayRenderer> annotationRenderers = new CopyOnWriteArrayList<>();
	
	private String text = "";
	private Font font = new Font("SansSerif", Font.BOLD, 12);



	public DynamicInteractiveDisplayCanvasComponent(int width, int height,	DynamicTransformEventHandlerFactory<A> transformEventHandlerFactory) {
		super(width, height, transformEventHandlerFactory);

		addComponentListener( new ComponentAdapter()
		{
			@Override
			public void componentResized( final ComponentEvent e )
			{
				final int w = getWidth();
				final int h = getHeight();
				//if ( handler != null )
				//	handler.setCanvasSize( w, h, true );
				for ( final OverlayRenderer or : annotationRenderers )
					or.setCanvasSize( w, h );
				// enableEvents( AWTEvent.MOUSE_MOTION_EVENT_MASK );
			}
		} );

	}


	@Override
	public DynamicTransformEventHandler< A > getTransformEventHandler(){
		return (DynamicTransformEventHandler<A>) handler;
	}



	public void setText(String text){
		this.text = text;
	}



	@Override
	public synchronized void paintComponent( final Graphics g )	{


		( ( Graphics2D ) g ).setRenderingHint( RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR );
		( ( Graphics2D ) g ).setRenderingHint( RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED );
		( ( Graphics2D ) g ).setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF );
		( ( Graphics2D ) g ).setRenderingHint( RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED );
		( ( Graphics2D ) g ).setRenderingHint( RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED );

		//g.setColor(Color.BLACK);
		//g.fillRect(0, 0, getWidth(), getHeight());

		BufferedImage abi = null;
		
		for ( int i = 0; i < overlayRenderers.size(); i++ )
			abi = ((BlendedBufferedImageOverlayRenderer)overlayRenderers.get(i)).blend( g, abi);

		for ( int i = 0; i < annotationRenderers.size(); i++ )
			abi = annotationRenderers.get(i).blend(g, abi);

		g.drawImage( abi, 0, 0, getWidth(), getHeight(), null );
				
		g.setColor(Color.white);
		g.setFont(font);
		int x = 10; int y = 10;
		for (String line : text.split("\n"))
			g.drawString(line, x, y += g.getFontMetrics().getHeight());


	}


	@Override
	public void addOverlayRenderer( final OverlayRenderer renderer ){
		overlayRenderers.add( renderer );
		renderer.setCanvasSize( getWidth(), getHeight() );
	}

	public void addAnnotationRenderer(BlendedBufferedImageOverlayRenderer renderer){
		//overlayRenderers.add( renderer );
		annotationRenderers.add(renderer);
		renderer.setCanvasSize( getWidth(), getHeight() );
	}




}
