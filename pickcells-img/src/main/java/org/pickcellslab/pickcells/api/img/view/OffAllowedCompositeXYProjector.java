package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;

import net.imglib2.IterableInterval;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.converter.Converter;
import net.imglib2.display.projector.composite.CompositeXYProjector;
import net.imglib2.type.numeric.ARGBType;

public class OffAllowedCompositeXYProjector<A> extends CompositeXYProjector<A> {

	
	private final Converter<A, ARGBType> blackConv = new Converter<A, ARGBType>(){

		@Override
		public void convert(A input, ARGBType output) {
			output.setZero();
		}
		
	};
	
	
	public OffAllowedCompositeXYProjector(RandomAccessibleInterval<A> source, IterableInterval<ARGBType> target,
			ArrayList<Converter<A, ARGBType>> converters, int dimIndex) {
		super(source, target, converters, dimIndex);
	}
	
	
	@Override
	protected int updateCurrentArrays()	{
		
		int currentSize = 0;
		for ( int i = 0; i < this.currentConverters.length; i++ )
			if ( isComposite(i) )
				++currentSize;

		if ( currentSize == 0 )	{
			// this is the where we return a converter which returns only black ARGBType
			//currentPositions[ 0 ] = position[ dimIndex ];
			currentConverters[ 0 ] = blackConv;
			return 1;
		}
		else
			return super.updateCurrentArrays();
		
	}

}
