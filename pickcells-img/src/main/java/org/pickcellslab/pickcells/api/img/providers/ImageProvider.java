package org.pickcellslab.pickcells.api.img.providers;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.imglib2.RandomAccessibleInterval;


/**
 * Provides access to specific images
 * @author Guillaume Blin
 
 * @param <T> The type of object returned by the {@link RandomAccessibleInterval} provided by
 * this ImageProvider
 */
public interface ImageProvider<T> {
	
	
	
	/**
	 * @return The {@link RandomAccessibleInterval} holding the actual data.
	 */
	public RandomAccessibleInterval<? extends T> provide();
	
	
}
