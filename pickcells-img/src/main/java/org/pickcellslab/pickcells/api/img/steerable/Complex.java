/*******************************************************************************
 * Copyright (c) 2012-2013 Biomedical Image Group (BIG), EPFL, Switzerland.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 *     Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 ******************************************************************************/
package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2019 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Class that encapsulates several manipulation of complex numbers. Based on the
 * <code>ijmath</code> package of Francois Aguet.
 * 
 * @version April 23, 2013
 * 
 * @author Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 * 
 * <hr>
 * NOTICE: This code was copied from a plugin originally implemented by François Aguet for ImageJ, and ported to Icy by Ricard Delgado-Gonzalo (ricard.delgado@gmail.com) 
 * and Zsuzsanna Püspöki (zsuzsanna.puspoki@epfl.ch). This plugin was based on the following publication:
 * <br><br>
 * M. Jacob, M. Unser, “Design of Steerable Filters for Feature Detection Using Canny-Like Criteria,”
 * IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 8, pp. 1007-1019
 * <hr>
 * 
 */
public class Complex {

	// ============================================================================
	// PUBLIC METHODS

	/** Creates a an array of two elements representing a complex number. */
	public static double[] complex(double a, double b) {
		double[] c = { a, b };
		return c;
	}

	// ----------------------------------------------------------------------------

	/** Adds two complex numbers. */
	public static double[] add(double[] c1, double[] c2) {
		double[] c = { c1[0] + c2[0], c1[1] + c2[1] };
		return c;
	}

	// ----------------------------------------------------------------------------

	/** Subtracts two complex numbers. */
	public static double[] sub(double[] c1, double[] c2) {
		double[] c = { c1[0] - c2[0], c1[1] - c2[1] };
		return c;
	}

	// ----------------------------------------------------------------------------

	/** Computes the modulus of a complex number. */
	public static double modulus(double[] c) {
		return Math.sqrt(c[0] * c[0] + c[1] * c[1]);
	}

	// ----------------------------------------------------------------------------

	/** Multiplies a complex number by a scalar. */
	public static double[] mul(double a, double[] c) {
		double[] s = { a * c[0], a * c[1] };
		return s;
	}

	// ----------------------------------------------------------------------------

	/** Multiplies two complex numbers. */
	public static double[] mul(double[] c1, double[] c2) {
		double[] c = { c1[0] * c2[0] - c1[1] * c2[1],
				c1[0] * c2[1] + c2[0] * c1[1] };
		return c;
	}

	// ----------------------------------------------------------------------------

	/** Divides two complex numbers. */
	public static double[] div(double[] c1, double[] c2) {
		double d = c2[0] * c2[0] + c2[1] * c2[1];
		double[] c = { (c1[0] * c2[0] + c1[1] * c2[1]) / d,
				(c2[0] * c1[1] - c1[0] * c2[1]) / d };
		return c;
	}

	// ----------------------------------------------------------------------------

	/** Computes the square root of a complex number. */
	public static double[] sqrt(double[] c) {
		double t = modulus(c);
		double d = Math.sqrt(2);
		double[] s = { Math.sqrt(t + c[0]) / d,
				csign(c[1]) * Math.sqrt(t - c[0]) / d };
		return s;
	}

	// ============================================================================
	// PRIVATE METHODS

	/** Returns the sign of a real number. */
	private static double csign(double x) {
		if (x >= 0.0) {
			return 1.0;
		} else {
			return -1.0;
		}
	}
}
