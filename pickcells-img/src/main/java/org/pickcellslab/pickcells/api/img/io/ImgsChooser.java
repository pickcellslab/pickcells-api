package org.pickcellslab.pickcells.api.img.io;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * A dialog for the user to choose images from files
 * 
 * @author Guillaume Blin
 *
 */
public interface ImgsChooser {

	/**
	 * Sets whether or not the dialog should be modal
	 * @param isModal
	 */
	public void setModal(boolean isModal);
	
	/**
	 * Sets the visibility of the dialog
	 * @param isVisible
	 */
	public void setVisible(boolean isVisible);
	
	/**
	 * @return {@code true} if the dialog was cancelled, {@code false} otherwise.
	 */
	public boolean wasCancelled();

	/**
	 * @return The chosen ImgFileList. Note: will be null if the dialog was cancelled
	 */
	public ImgFileList getChosenList();

	/**
	 * @return A int[] holding the coordinates in {@link ImgFileList} of the image selected by the user in the {@link ImgChooser} dialog.
	 * This can be useful to ask the user to select one image for preview for example.
	 */
	public int[] getSelectedImage();
	


}
