package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

public class DefaultKeyStrokeModel implements KeyStrokeModel {

	private final ImageDisplay display;


	public DefaultKeyStrokeModel(ImageDisplay display) {
		assert display!=null : "display is null";
		this.display = display;
	}
	
	
	@Override
	public void addKeyBinding(KeyStroke ks, String id, Action action){
		display.getView().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(ks, id);
		display.getView().getActionMap().put(id, action);
	}

	
	@Override
	public void rebind(KeyStroke ks, String id){
		display.getView().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(ks, id);
	}

	
	@Override
	public void removeKeyBinding(KeyStroke ks){
		display.getView().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).remove(ks);
	}

	
	
	
	
}
