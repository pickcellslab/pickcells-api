package org.pickcellslab.pickcells.api.img.detector;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.process.HyperCubeFactory;
import org.pickcellslab.pickcells.api.img.process.NonLinearIdentityTypeFilters;
import org.pickcellslab.pickcells.api.img.process.Operations;

import net.imglib2.Cursor;
import net.imglib2.IterableInterval;
import net.imglib2.Point;
import net.imglib2.RandomAccessible;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.dog.DifferenceOfGaussian;
import net.imglib2.algorithm.gauss3.Gauss3;
import net.imglib2.algorithm.localextrema.LocalExtrema;
import net.imglib2.algorithm.localextrema.LocalExtrema.LocalNeighborhoodCheck;
import net.imglib2.exception.IncompatibleTypeException;
import net.imglib2.outofbounds.OutOfBoundsConstantValueFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.ComplexType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.util.Intervals;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

public abstract class DogDetect {


	/**
	 * Uses the Difference of Gaussian method to identify spots in the picture.	 * 
	 * @param plane The source image (do not use a view) 
	 * @param median Apply median filter before performing dog detection
	 * @param minima if true then minima will be searched for if false maxima will be returned
	 * @param radius (The radius in pixel coordinates of the spots)
	 * @param sigma (The estimated sigma of the image)
	 * @param threshold (The threshold below which spots should be disregarded)
	 * @param calibration Sizes of voxels
	 * @return A List of {@link Point} corresponding to the location of the spots 
	 */

	public static <T extends ComplexType<T> & NativeType<T> & RealType<T>>ArrayList<Point> detect(
			RandomAccessibleInterval<T> plane,
			boolean median, 
			boolean minima,
			double radius,
			double sigma,
			double threshold,
			double[] calibration,
			ImgIO io
			){







		int numThreads = Runtime.getRuntime().availableProcessors();


		//Apply a median filter first
		RandomAccessible<T> extended = null;
		T t = plane.randomAccess().get().createVariable();
		if(median){			
			long[] span = new long[plane.numDimensions()];
			Arrays.fill(span, 1);
			HyperCubeFactory<T> n = new HyperCubeFactory<>(
					span,
					new OutOfBoundsConstantValueFactory<T,RandomAccessibleInterval<T>>(t));
			final RandomAccessibleInterval<T> m = Operations.run(
					io,
					plane,
					n,
					NonLinearIdentityTypeFilters.median());
			extended = Views.extendMirrorSingle(m);
		}
		else
			extended = Views.extendMirrorSingle(plane);



		final double sigma1 = radius / Math.sqrt( plane.numDimensions() ) * 0.9;
		final double sigma2 = radius / Math.sqrt( plane.numDimensions() ) * 1.1;
		final double[][] sigmas = DifferenceOfGaussian.computeSigmas( sigma, 2, calibration, sigma1, sigma2 );





		final FloatType type = new FloatType();
		RandomAccessibleInterval<FloatType> dog =  io.createImg(plane, type);
		RandomAccessibleInterval<FloatType> dog2 = io.createImg(plane, type);


		try
		{
			//System.out.println("Starting Gauss3 with sigma : "+ Arrays.toString(sigmas[ 1 ]));
			Gauss3.gauss( sigmas[ 1 ], extended, dog2 );
			//System.out.println("Starting Gauss3 with sigma : "+ Arrays.toString(sigmas[ 0 ]));
			Gauss3.gauss( sigmas[ 0 ], extended, dog );

			//System.out.println("Gauss complete!");
		}
		catch ( final IncompatibleTypeException e )
		{
			e.printStackTrace();
		}


		final IterableInterval<FloatType> dogIterable = Views.iterable( dog );
		final IterableInterval<FloatType> tmpIterable = Views.iterable( dog2 );
		final Cursor<FloatType> dogCursor = dogIterable.cursor();
		final Cursor<FloatType> tmpCursor = tmpIterable.cursor();
		while ( dogCursor.hasNext() )
			dogCursor.next().sub( tmpCursor.next() );

		dog2 = null;

		//Identify peaks
		//System.out.println("Finding Objects...!");

		final FloatType val = new FloatType();
		val.setReal(threshold);
		LocalNeighborhoodCheck< Point, FloatType > localNeighborhoodCheck = null;
		if(minima)				
			localNeighborhoodCheck = new LocalExtrema.MinimumCheck< FloatType >( val );
		else
			localNeighborhoodCheck = new LocalExtrema.MaximumCheck< FloatType >( val );

		final IntervalView<FloatType> dogWithBorder = Views.interval( Views.extendMirrorSingle( dog ), Intervals.expand( dog, 1 ) );
		final ExecutorService service = Executors.newFixedThreadPool( numThreads );
		final ArrayList< Point > peaks = LocalExtrema.findLocalExtrema( dogWithBorder, localNeighborhoodCheck, service );		
		service.shutdown();

		return peaks;
	}




}







