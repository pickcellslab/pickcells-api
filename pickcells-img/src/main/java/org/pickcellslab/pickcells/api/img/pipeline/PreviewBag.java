package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;
import net.imglib2.view.Views;

public final class PreviewBag<I extends Type<I>> {

	private final MinimalImageInfo iInfo;
	private final MinimalImageInfo oInfo;
	private final MinimalImageInfo fInfo;
	
	private final RandomAccessibleInterval<I> input;
	@SuppressWarnings("rawtypes")
	private final RandomAccessibleInterval original;
	@SuppressWarnings("rawtypes")
	private final RandomAccessibleInterval flat;
	
	<R1 extends Type<R1>, R2 extends Type<R2>> PreviewBag(
			MinimalImageInfo iInfo, 
			RandomAccessibleInterval<I> input,
			MinimalImageInfo oInfo,
			RandomAccessibleInterval<R1> original,
			MinimalImageInfo fInfo,
			RandomAccessibleInterval<R2> flat) {
		
		this.iInfo = iInfo; 	this.input = input;
		this.oInfo = oInfo;		this.original = original;
		this.fInfo = fInfo;		this.flat = flat;
	}
	
	
	@SuppressWarnings("unchecked")
	public <T extends Type<T>> PreviewBag<T> createWith(MinimalImageInfo iInfo, RandomAccessibleInterval<T> input){
		assert input != null : "Input is null";
		return new PreviewBag<>(iInfo, input, oInfo, original, fInfo, flat);
	}
	
	
	
		
	
	/**
	 * @return The current input image
	 */
	public RandomAccessibleInterval<I> getInput(){
		return input; 
	}

	/**
	 * @return The original image (the image used as input at the start of the pipeline)
	 */
	@SuppressWarnings("unchecked")
	public <R extends Type<R>> RandomAccessibleInterval<R> getOriginal() {
		return original; 
	}


	/**
	 * @return The first flattened image (one channel of the original image for example)
	 */
	@SuppressWarnings("unchecked")
	public <R extends Type<R>> RandomAccessibleInterval<R> getFirstFlat() {
		return flat;
	}


	/**
	 * @return The type of the current input image
	 */
	public I getCurrentInputType() {
		return Views.iterable(input).firstElement();
	}
	
	
	
	/**
	 * @return A {@link MinimalImageInfo} instance for the current input image
	 */
	public MinimalImageInfo getInputInfo(){
		return iInfo;
	}
	
	/**
	 * @return A {@link MinimalImageInfo} instance for the starting input image (the image used as input at the start of the pipeline)
	 */
	public MinimalImageInfo getOriginalInfo(){
		return oInfo;
	}
	
	
	/**
	 * @return A {@link MinimalImageInfo} instance for the first flattened image (choice of one channel for example). May be null 
	 */
	public MinimalImageInfo getFlatInfo(){
		return fInfo;
	}



	
	
	
	
	
}
