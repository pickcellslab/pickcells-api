package org.pickcellslab.pickcells.api.img.io;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Set;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

/**
 * An {@link ImgsChecker} role is to determine if a list of image files (in the form of an {@link ImgsFileList})
 * share the same values for specific properties like calibration, bit depth...
 * 
 * @author Guillaume Blin
 *
 */
public interface ImgsChecker {

	/**
	 * An array containing the calibration (number of unit) per pixel in each dimension (in image order)
	 */
	public static final AKey<double[]> calibration = AKey.get("Calibration", double[].class);
	/**
	 * An array containing the number of pixel in each of the dimensions of the image (in image order)
	 */
	public static final AKey<long[]> dimensions = AKey.get("Dimensions", long[].class);
	/**
	 * The bit depth of the image (2^bitDepth)
	 */
	public static final AKey<Integer> bitDepth  = AKey.get("Bit Depth", Integer.class);

	/**
	 * The number of channels in the image
	 */
	public static final AKey<Integer> nChannels  = AKey.get("Channel Number", Integer.class);

	/**
	 * The extension of the image
	 */
	public static final AKey<String> extension  = AKey.get("Extension", String.class);

	/**
	 * The spatial unit of the image
	 */
	public static final AKey<String> sUnit  = AKey.get("Space Unit", String.class);

	/**
	 * The temporal unit of the image 
	 */
	public static final AKey<String> tUnit  = AKey.get("Time Unit", String.class);

	/**
	 * The dimensions ordering (an int[numDimensions] with entries corresponding to {@link Image#x}, y, c, z or t)
	 */
	public static final AKey<int[]> ordering = AKey.get("Ordering", int[].class);
	
	public static final AKey<String[]> units = AKey.get("units", String[].class);

	//TODO add more info such as laser power, microscope type, date ....


	/**
	 * Sets the {@link ImgFileList} to be checked.
	 * This method allows to reuse the same {@link ImgsChecke} on
	 * multiple ImgFileList.
	 * @param list The {@link ImgFileList} to check
	 */
	public void setList(ImgFileList list);


	/**
	 * @return The currently tested {@link ImgFileList}
	 */
	public ImgFileList currentList();


	/**
	 * @return {@code true} if the images in the currently tested {@link ImgFileList} have 
	 * consistent properties as defined in this {@link ImgChecker}, {@code false} otherwise
	 */
	public boolean isConsistent();

	/**
	 * @return A Set of {@link AKey} objects indicating which properties in the currently tested ImgFileList
	 * are not consistent
	 */
	public Set<AKey<?>> inconsistencies();


	/**
	 * @param property
	 * @return The value identified in the current ImgFileList or null if the property is unknown or inconsistent
	 */
	public <V> V value(AKey<V> property);

	/**
	 * @param property
	 * @return The value identified in the current ImgFileList or null if the property is unknown or inconsistent
	 */
	public <V> V value(AKey<V[]> property, int index);


	/**
	 * @return A Set of {@link AKey} objects indicating which properties are tested by this ImgsChecker
	 */
	public Set<AKey<?>> tested();


	public <V> V value(AKey<V> property, ImgFileList list, int dataset, int index);


	
	/**
	 * Converts the double[] returned by {@link ImgsChecker#calibration} to a double[5] as described by {@link Image#calibration}
	 * @param rawOrder
	 * @return The converted int[]
	 * @deprecated Use {@link MinimalImageInfo} instead
	 */
	public static double[] convert(int[] order, double[] rawCal){
		if(order.length!=5)
			throw new IllegalArgumentException("Order must have a length of 5 : "+order.length);
		double[] cal = new double[5];		
		for(int i = 0; i<order.length; i++)
			if(order[i]!=-1)
				cal[i] = rawCal[order[i]];	
			else
				cal[i] = 1;
		return cal;
	};

	/**
	 * Converts the int[] returned by {@link ImgsChecker#ordering} to an int[5] as described by {@link Image#order}
	 * @param rawOrder
	 * @return the converted int[]
	 */
	public static int[] convert(int[] rawOrder) {
		int[] order = new int[5];
		Arrays.fill(order,-1);
		for(int i = 0; i<order.length; i++){
			for(int j = 0; j<rawOrder.length; j++)
				if(rawOrder[j] == i)
					order[i] = j;
		}		
		return order;
	}

	

}
