package org.pickcellslab.pickcells.api.img.io;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Predicate;

/**
 * A Builder to configure an {@link ImgsChooser}
 * 
 * @author Guillaume Blin
 *
 */
public interface ImgsChooserBuilder {

	/**
	 * Sets the title of the dialog
	 * @param title The Title
	 */
	public ImgsChooserBuilder setTitle(String title);
	
	/**
	 * Sets the home directory of the final ImgsChooser
	 * @param path The path to the desired directory
	 */
	public ImgsChooserBuilder setHomeDirectory(String path);
	
	/**
	 * Sets a filter on the files that the user can choose from
	 * @param filter A {@link Predicate} for filtering
	 */
	public ImgsChooserBuilder setFileFilter(Predicate<String> filter);
	
	/**
	 * Creates restrictions on the number of files the user can choose
	 * @param number A {@link Predicate} defining the rules on the number of files to be chosen
	 */
	public ImgsChooserBuilder setFileNumber(Predicate<Integer> number);
	
	/**
	 * @return An {@link ImgsChooser} configured with all the options in this builder
	 */
	public ImgsChooser build();
	
}
