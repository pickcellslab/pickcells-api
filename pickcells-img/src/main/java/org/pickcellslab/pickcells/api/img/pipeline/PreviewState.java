package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.event.MouseListener;
import java.util.function.Function;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.view.ImageViewer;
import org.pickcellslab.pickcells.api.img.view.ImgLut;
import org.pickcellslab.pickcells.api.img.view.Lut16;
import org.pickcellslab.pickcells.api.img.view.Lut8;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.display.ColorTable;
import net.imglib2.type.numeric.RealType;

class PreviewState<T extends RealType<T>> implements ImageViewState<T> {


	private final ImageViewer<T> view;
	private final int state;
	private final String[] channels;

	private MouseListener current;

	public PreviewState(ImageViewer<T> view, String[] channels) {
		this.view = view;
		this.channels = channels;
		this.state = view.numChannels()-channels.length;		
	}



	@Override
	public T getType() {
		return view.getType();
	}

	@Override
	public boolean isVisible(int channel) {
		return view.isVisible(channel);
	}

	@Override
	public double getCurrentMinIntensity(int channel) {
		return view.getVoxelMeasureConverter(channel).apply(view.getCurrentMinIntensity(channel)).doubleValue();		
	}

	@Override
	public double getCurrentMaxIntensity(int channel) {
		return view.getVoxelMeasureConverter(channel).apply(view.getCurrentMaxIntensity(channel)).doubleValue();
	}



	@Override
	public int getPreviewIndex() {
		return state > 0 ? channels.length : -1;
	}



	@Override
	public int getAnnotationIndex() {
		return state == 2 ? channels.length+1 : -1;
	}



	@Override
	public void setLut(int channel, String lut) {
		
		if (channel >= view.numChannels()+state){
			System.out.println("PreviewState : wrong channel for setting Lut "+channel);
			return;
		}
		
		ImgLut<?> newLut;
		if(view.getType().getBitsPerPixel()>=16)
			newLut = Lut16.valueOf(lut);
		else
			newLut = Lut8.valueOf(lut); 
		
		if(newLut != null){
			System.out.println("PreviewState : Setting Lut "+newLut.name());
			view.setLut(channel, newLut.table());
		}
		
	}
	
	
	
	@Override
	public void setLut(int channel, ColorTable lut) {
		view.setLut(channel, lut);
	}
	
	
	

	
	void setVoxelMeasureConverter(Function<Double,Number> f, int channel){
		view.setVoxelMeasureConverter(f, channel);
	}



	@Override
	public void refreshView() {
		view.refresh();

	}

	@Override
	public int getSelectedZ() {
		return view.getCurrentSlice();
	}

	@Override
	public void setSelectedZ(int z) {
		if(view.order()[Image.z]!=-1){
			long[] pos = new long[view.access(0, view.numChannels()).numDimensions()];
			pos[view.order()[Image.z]] = z;
			view.setPosition(pos);
		}
		else
			view.refresh();
	}


	@Override
	public int getSelectedT() {
		return view.getCurrentFrame();
	}



	@Override
	public void setSelectedT(int t) {
		if(view.order()[Image.t]!=-1){
			long[] pos = new long[view.access(0, view.numChannels()).numDimensions()];
			pos[view.order()[Image.t]] = t;
			view.setPosition(pos);
		}
		else
			view.refresh();
	}



	@Override
	public String[] channelNames() {
		return channels;
	}





	public RandomAccessibleInterval<T> getChannels() {
		return view.access(0, view.numChannels()-state);
	}


	public RandomAccessibleInterval<T> getPreviewImage() {
		if(state == 0)
			return null;
		return view.access(view.numChannels()-(state));
	}


	@Override
	public RandomAccessibleInterval<T> getAnnotationImage() {
		if(state < 2)
			return null;
		return view.access(view.numChannels()-1);
	}



	@Override
	public int[] getOrder() {
		return view.order();
	}


	@Override
	public long[] getImageCoordinates(int x, int y) {
		return view.getImgCoordinates(x, y);
	}



	public void setMouseListener(MouseListener l) {
		if(current!=null)
			view.removeViewListener(current);
		if(l!=null)
			view.addViewListener(l);
		current = l;
	}



	@Override
	public void autoContrast(int channel) {
		view.adjustContrast(channel);
	}












}
