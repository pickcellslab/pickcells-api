package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.util.function.Function;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.view.ImageViewer;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.display.ColorTable;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.RealType;

/**
 * Provides info from an image visualisation
 * 
 * @author Guillaume Blin
 *
 */
public interface ImageViewState<T extends RealType<T>> {
	
	
	
	/**
	 * @return The {@link Type} of the displayed image
	 */
	public T getType();
	
	/**
	 * @param channel The index of the desired channel (can include preview or annotation which come after the channels of the imput image)
	 * @return true if the specified channel is visible, false otherwise
	 * @see #getPreviewIndex()
	 * @see #getAnnotationIndex()
	 */
	public boolean isVisible(int channel);
	
	/**
	 * @param channel
	 * @return The Min Intensity selected by the user for the given channel. NB : Unlike {@link ImageViewer#getCurrentMinIntensity(int)}, the 
	 * returned value will take into account any transform that may have been applied between the view and the actual underlying data.
	 * @see {{@link #setVoxelMeasureConverter(Function, int)}
	 */
	public double getCurrentMinIntensity(int channel);
	
	/**
	 * @param channel
	 * @return The Max Intensity selected by the user for the given channel. NB : Unlike {@link ImageViewer#getCurrentMaxIntensity(int)}, the 
	 * returned value will take into account any transform that may have been applied between the view and the actual underlying data.
	 * @see {{@link #setVoxelMeasureConverter(Function, int)}
	 */
	public double getCurrentMaxIntensity(int channel);
	
	
	/**
	 * @return The index of the preview channel or -1 if it is not present
	 */
	public int getPreviewIndex();
	
	
	/**
	 * @return The index of the Annotation channel or -1 if it is not present
	 */
	public int getAnnotationIndex();
	
	
	/**
	 * Notifies the view that its image has changes and it should refresh the display
	 */
	public void refreshView();
	
	/**
	 * @return The currently selected z slice
	 */
	public int getSelectedZ();
	
	/**
	 * Causes the view to position itself at the given z slices and refreshes the dislay
	 * @param z 
	 */
	public void setSelectedZ(int z);
	
	
	/**
	 * @return The currently selected time frame
	 */
	public int getSelectedT();
	
	/**
	 * Causes the view to position itself at the given time frame and refreshes the dislay
	 * @param z 
	 */
	public void setSelectedT(int t);
	
	
	
	
	/**
	 * Sets the Lookup Table for the channel at the provided index. If arguments are unknown, nothing happens
	 * @param channel
	 * @param lut
	 */
	public void setLut(int channel, String lut);
	
	
	/**
	 * Sets the Lookup Table for the channel at the provided index. If arguments are unknown, nothing happens
	 * @param channel
	 * @param lut
	 */
	public void setLut(int channel, ColorTable lut);
	
	
	
	/**
	 * Auto adjust the contrast of the given channel
	 * @param channel
	 */
	public void autoContrast(int channel);
	
	
	
	/**
	 * @return The channel names of the original image. NB: Does NOT include the overlay nor the preview channel.
	 */
	public String[] channelNames();
		
	/**
	 * @return A {@link RandomAccessibleInterval} allowing to read and write into the displayed Annotation Image. NB: will return null
	 * if no {@link ImgProcessing} has requested for this channel to be present in the view via {@link ImgProcessing#requiresAnnotationChannel()}
	 */
	public  RandomAccessibleInterval<T> getAnnotationImage();


	
	/**
	 * @return As in {@link Image#order} (for the view)
	 * @deprecated Use {@link MinimalImageInfo} system
	 */
	public int[] getOrder();
	
	/**
	 * @param x The on screen x coordinate (generally provided by a mouse event)
	 * @param y The on screen y coordinate (generally provided by a mouse event)
	 * @return a long[] holding the image coordinate corresponding to the given screen x and y coordinates. Note that all dimensions of the image
	 * will be included in order and set to the current values in the view.
	 * @see #getOrder()
	 */
	public long[] getImageCoordinates(int x, int y);
	
}
