package org.pickcellslab.pickcells.api.img.editors;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.ImageIcon;

import net.imglib2.Cursor;
import net.imglib2.RandomAccessible;

/**
 * Used to paint on a {@link RandomAccessible}. It defines a region which can be iterated over using {@link #cursor()} as well as a function
 * to determine the values to be set at each position within the region via {@link #computeAndSet(long[], Object)}. A shortcut exists to simply draw 
 * the region without consuming any returned value from the {@link Cursor} with {@link #draw()}.
 * 
 * @author Guillaume Blin
 *
 * @param <T> The type of values handled by this {@link Brush}
 */
public interface Brush extends Iterable<long[]> {

	
	public int numDimensions();
	
	/**
	 * Sets the center of this {@link Brush} to the provided position
	 * @param pos
	 */
	public void setPosition(long[] pos);
	
	/**
	 * @return Coordinates of the min bounding box given the current position of this {@link Brush}. Please do not modify the returned
	 * instance, the implementation is free to provide its own field.
	 */
	public long[] minBound();
	
	/**
	 * @return Coordinates of the max bounding box given the current position of this {@link Brush}. Please do not modify the returned
	 * instance, the implementation is free to provide its own field.
	 */
	public long[] maxBound();
	
	/**
	 * @return The current center of this {@link Brush}
	 */
	public long[] currentPosition();
	
		
	/**
	 * Modifies the provided value according to this {@link Brush} function. For example, sets a predefined color or compute a value
	 * based on the distance to the center to create a gradient, etc...
	 * @param t
	 * @return {@code true} if the actual value of t was modified, {@code false} otherwise.
	 */
	public boolean computeAndSet(long[] pos);
	
	public boolean revert(long[] coords);	
	
	
	public Brush createWithDims(long[] newDims);
	
	
	public ImageIcon cursorIcon();

	

	
}
