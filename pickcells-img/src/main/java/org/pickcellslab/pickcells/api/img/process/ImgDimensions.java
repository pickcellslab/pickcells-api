package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.view.ImgLut;
import org.pickcellslab.pickcells.api.img.view.Lut16;
import org.pickcellslab.pickcells.api.img.view.Lut8;

import net.imglib2.Cursor;
import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.IterableInterval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessible;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.RealRandomAccess;
import net.imglib2.converter.Converter;
import net.imglib2.display.ArrayColorTable;
import net.imglib2.display.ColorTable;
import net.imglib2.img.Img;
import net.imglib2.interpolation.InterpolatorFactory;
import net.imglib2.outofbounds.OutOfBoundsMirrorFactory;
import net.imglib2.realtransform.Scale;
import net.imglib2.type.NativeType;
import net.imglib2.type.Type;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public final class ImgDimensions {

	private ImgDimensions() {/*Not Instantiable*/}
	
	
	/**
	 * @param dims a long[] as in {@link Img#dimensions(long[])
	 * @param type The {@link Type} of the desired image
	 * @return true if an image with the given dimension and {@link Type} would fit in the current available RAM, false otherwise
	 */
	public static <T extends RealType<T>> boolean fitsInMemory(long[] dims, T type){

		long size = 1;
		for(int d = 0; d<dims.length; d++)
			size *= dims[d];

		int bitDepth = type.getBitsPerPixel()/8;
		long memorySize = size * bitDepth;

		System.gc();

		final MemoryUsage mu = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage ();
		long free = mu.getMax() - mu.getUsed();

		long expected = free - memorySize;

		if(expected<0)
			return false;

		long thresh = (long) ((1f - (float)expected/(float)free) * (float)memorySize) ;

		//System.out.println("Current free memory" + free/1000000);
		//System.out.println("Image size = " + memorySize/1000000);
		//System.out.println("Expected Free Memory after loading = " + expected/1000000);
		//System.out.println("Threshold = " + thresh/1000000);

		return expected >= thresh;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Adds extra hyperslices to an image.
	 * This method is useful for example to add a channel to an image and use for overlay
	 * @param img
	 * @param d The index of the dimension to increase
	 * @param i The number of hyperslices to add
	 * @return A copy of the original image with the required extra hyperslice
	 */
	public static < T extends NativeType< T > & RealType<T> > RandomAccessibleInterval<T> increase(ImgIO io, RandomAccessibleInterval<T> img, int d,  int i){


		final T type = Views.iterable(img).firstElement().createVariable();

		final List<RandomAccessibleInterval<T>> slices = new ArrayList<>((int)img.dimension(d)+i);

		for(int s = 0; s<img.dimension(d); s++)
			slices.add(Views.hyperSlice(img, d, s));

		for(int s = 0; s<i; s++)
			slices.add(io.createImg(slices.get(0), type));

		
		final RandomAccessibleInterval<T> stack = Views.stack(slices); //TODO test performance versus StackAccessMode.MOVE_ALL_SLICE_ACCESSES, 

		if(d != stack.numDimensions()-1){
			System.out.println("ImgDimensions: permuted dims: "+d+" and "+(stack.numDimensions()-1));
			RandomAccessibleInterval<T> permuted = stack;
			for(int j = d; j<stack.numDimensions(); j++)
				permuted = Views.permute(permuted, j, stack.numDimensions()-1);			
			return permuted;
		}
		else 
			return stack;

	}


	/**
	 * Same as {@link #increase(RandomAccessibleInterval, int, int)} except that the method uses the exact same factory as the provided image
	 * @param img
	 * @param d The index of the dimension to increase
	 * @param i The number of hyperslices to add
	 * @return A copy of the original image with the required extra hyperslice
	 */
	public static < T extends NativeType< T > & RealType<T> > RandomAccessibleInterval<T> increaseImg(ImgIO io, Img<T> img, int d,  int i){


		final T type = Views.iterable(img).firstElement().createVariable();

		final List<RandomAccessibleInterval<T>> slices = new ArrayList<>((int)img.dimension(d)+i);

		for(int s = 0; s<img.dimension(d); s++)
			slices.add(Views.hyperSlice(img, d, s));

		
		for(int s = 0; s<i; s++)
			slices.add(io.createImg(slices.get(0), type));

		//img.factory().create(dim, type));


		final RandomAccessibleInterval<T> stack = Views.stack(slices); //TODO test performance versus StackAccessMode.MOVE_ALL_SLICE_ACCESSES, 

		if(d != stack.numDimensions()-1){
			System.out.println("ImgDimensions: permuted dims: "+d+" and "+(stack.numDimensions()-1));
			RandomAccessibleInterval<T> permuted = stack;
			for(int j = d; j<stack.numDimensions(); j++)
				permuted = Views.permute(permuted, j, stack.numDimensions()-1);			
			return permuted;
		}
		else 
			return stack;

	}


	/**
	 * Moves the specified dimension to the last position and shifts the higher dimensions 1 notch down.
	 * @param img The {@link RandomAccessibleInterval} on which to perfoem the operation
	 * @param d The dimension which should be moved to fPos
	 * @param fPos The final position of the specified dimension.
	 * @return A modified view of the input RandomAccessibleInterval.
	 */
	public static  < T > RandomAccessibleInterval<T> moveDimension(RandomAccessibleInterval<T> img, int d, int fPos){					
		for(int j = d; j<fPos; j++)
			img = Views.permute(img, j, j+1);			
		return img;
	}






	/**
	 * Adds n channels to the given {@link RandomAccessibleInterval}. If the provided image is not already multichannel, 
	 * then an extra dimension is created. As a side effect, the given order is updated.
	 * @param img The image for which channels should be added
	 * @param order The int[] holding info about order of dimensions in the image as {@link Image#order}
	 * @param n The number of channels to be added
	 * @return The multichannel image.
	 */
	public static  < T extends NativeType< T > & RealType<T> > RandomAccessibleInterval<T> addChannel(ImgIO io, RandomAccessibleInterval<T> img, int[] order, int n){

		if(order[Image.c] == -1){
			
				T type = Views.iterable(img).firstElement().createVariable();
				
				List<RandomAccessibleInterval<T>> channels = new ArrayList<>();
				channels.add(img);
				
				for(int i = 0; i<n; i++)
					channels.add(io.createImg(img, type));
			
				RandomAccessibleInterval<T> stack = Views.stack(channels);
				order[Image.c] = stack.numDimensions()-1;
				
				return stack;			
		}
		else
			return increase(io, img,order[Image.c],n);
	}




	/**
	 * A convenience method to obtain an interval centered on the image on a given {@link RandomAccessibleInterval} with the desired sizes.
	 * NB: No data is copied.
	 * @param img
	 * @param sizes
	 * @return A {@link RandomAccessibleInterval} view of the source img centered and with the desired sizes. NB: if sizes are larger that the number
	 * of voxel in the source for a given dimension, the interval then covers the entire range of the source for this dimension.
	 */
	public static <T> RandomAccessibleInterval<T> createCenteredInterval(RandomAccessibleInterval<T> img, long[] sizes){

		assert sizes.length >= img.numDimensions();

		long[] min = new long[img.numDimensions()];
		long[] max = new long[img.numDimensions()];


		for(int i = 0; i<img.numDimensions(); i++){
			max[i] = img.dimension(i)-1;
			createCenteredBounds(min, max, i, sizes[i]);
		}

		Interval itrv = new FinalInterval(min, max);
		return Views.interval(img, itrv);		
	}






	public static <T> RandomAccessibleInterval<T> createCenteredInterval(RandomAccessibleInterval<T> img, int d, long size){

		long[] min = new long[img.numDimensions()];
		long[] max = new long[img.numDimensions()];

		long toAdd0 = (img.dimension(d)-size) /2;

		if(toAdd0<1)
			return img;

		min[d] = toAdd0;
		max[d] = toAdd0 + size;
		for(int i = 0; i<img.numDimensions(); i++)
			if(i!=d)
				max[i] = img.dimension(i)-1;

		Interval itrv = new FinalInterval(min, max);
		return Views.interval(img, itrv);		
	}


	public static void createCenteredBounds(long[] min, long[] max, int d, long size){

		long toAdd0 = (max[d]-min[d]-size) /2;

		if(toAdd0<1)
			return ;

		min[d] = toAdd0;
		max[d] = toAdd0 + size;

	}





	public static < T extends NativeType< T > & RealType<T> > Img<T> copy(ImgIO io, RandomAccessibleInterval<T> img){

		if(Img.class.isAssignableFrom(img.getClass()))
			return ((Img<T>)img).copy();

		// Create a copy of the image with one extra channel for the overlay
		T type = Views.iterable(img).firstElement().createVariable();
		Img<T> copy = io.createImg(img, type);
		copy(img,copy);

		return copy;
	}



	/**
	 * Copy from a source that is just RandomAccessible to an IterableInterval. Latter one defines
	 * size and location of the copy operation. It will query the same pixel locations of the
	 * IterableInterval in the RandomAccessible. It is up to the developer to ensure that these
	 * coordinates match.
	 *
	 * @param img - a RandomAccess as source that can be infinite
	 * @param copy - an IterableInterval as target
	 */
	public static < T extends Type< T > > void copy( final RandomAccessibleInterval<T> img,
			final RandomAccessibleInterval<T> copy )
	{
		Cursor< T > sourceCursor = Views.iterable(img).localizingCursor();
		RandomAccess< T > targetRandomAccess = copy.randomAccess();
		while ( sourceCursor.hasNext())
		{
			sourceCursor.fwd();
			targetRandomAccess.setPosition( sourceCursor );
			targetRandomAccess.get().set( sourceCursor.get() );
		}
	}


	public static <V extends RealType<V>, T extends RealType<T>> void copyReal(RandomAccessibleInterval<V> img, RandomAccessibleInterval<T> copy) {

		final Cursor< V > sourceCursor = Views.iterable(img).localizingCursor();
		final RandomAccess< T > targetRandomAccess = copy.randomAccess();

		while ( sourceCursor.hasNext())
		{
			sourceCursor.fwd();
			targetRandomAccess.setPosition( sourceCursor );
			targetRandomAccess.get().setReal( sourceCursor.get().getRealDouble() );
		}
	}




	/**
	 * Copies a "Min / Max" scaled version of the input image into the given recipient image. Please ensure that the recipient image
	 * spans the entire interval defined by the input image.
	 * @param img Input image to be copied
	 * @param copy Recipient image where the values will be copied.
	 */
	public static <V extends RealType<V>, T extends RealType<T>> void copyRealAndContrast(RandomAccessibleInterval<V> img, RandomAccessibleInterval<T> copy) {

		final IterableInterval<V> it = Views.iterable(img);
		V min = it.firstElement().createVariable();
		V max = it.firstElement().createVariable();
		ImgDimensions.computeMinMax(it, min, max);

		final RandomAccess< T > targetRandomAccess = copy.randomAccess();

		final double scale = targetRandomAccess.get().getMaxValue()/(max.getRealDouble()-min.getRealDouble());		
		final double translation = 0 - min.getRealDouble();


		Cursor< V > sourceCursor = it.localizingCursor();
		while ( sourceCursor.hasNext()){
			sourceCursor.fwd();
			targetRandomAccess.setPosition( sourceCursor );
			targetRandomAccess.get().setReal( (sourceCursor.get().getRealDouble() + translation) * scale );
		}

	}



	/**
	 * As {@link #copyRealAndContrast(RandomAccessibleInterval, RandomAccessibleInterval)} but also generate the inverse transformation performed during the copy operation.
	 * @param img Input image to be copied
	 * @param copy Recipient image where the values will be copied.
	 * @return A {@link Function} which represents the inverse transformation performed during the copy operation
	 */
	public static <V extends RealType<V>, T extends RealType<T>> Function<Double,Number> copyRealAndContrastWithOutput(RandomAccessibleInterval<V> img, RandomAccessibleInterval<T> copy) {

		final IterableInterval<V> it = Views.iterable(img);
		V min = it.firstElement().createVariable();
		V max = it.firstElement().createVariable();
		ImgDimensions.computeMinMax(it, min, max);

		final RandomAccess< T > targetRandomAccess = copy.randomAccess();

		final double scale = targetRandomAccess.get().getMaxValue()/(max.getRealDouble()-min.getRealDouble());		
		final double translation = 0 - min.getRealDouble();


		Cursor< V > sourceCursor = it.localizingCursor();
		while ( sourceCursor.hasNext()){
			sourceCursor.fwd();
			targetRandomAccess.setPosition( sourceCursor );
			targetRandomAccess.get().setReal( (sourceCursor.get().getRealDouble() + translation) * scale );
		}


		return t -> t/scale -translation;

	}






	public static <T extends RealType<T>, R extends NativeType<R> & RealType<R>> Img<R> hardCopy(ImgIO io, final RandomAccessibleInterval<T> view, R type){

		Objects.requireNonNull(view);
		
		Cursor<T> c = Views.iterable(view).localizingCursor();
		RandomAccess<T> r = view.randomAccess(view);

		//Image to be returned
		Img<R> copy =  io.createImg(view, type);

		Cursor<R> copyCursor = copy.cursor();

		while(c.hasNext()){
			c.next();
			copyCursor.next();
			r.setPosition(c);
			copyCursor.get().setReal(c.get().getRealDouble());    		
		}

		return copy;
	}




	public static <T extends RealType<T>, R extends RealType<R> & NativeType<R>> Img<R> hardCopy(ImgIO io, final RandomAccessibleInterval<T>  view, Converter<T,R> converter, R type){

		Cursor<T> c = Views.iterable(view).localizingCursor();
		RandomAccess<T> r = view.randomAccess(view);

		//Image to be returned
		Img<R> copy =  io.createImg(view, type);

		Cursor<R> copyCursor = copy.cursor();

		while(c.hasNext()){
			c.next();
			copyCursor.next();
			r.setPosition(c);   
			converter.convert(c.get(), copyCursor.get());
		}

		return copy;
	}




	public static <T extends Type<T>> Img<T> resample(Img<T> source, Scale scale, InterpolatorFactory<T,RandomAccessible<T>> ifac){

		if(source.numDimensions()!=scale.numDimensions())
			throw new IllegalArgumentException("The provided source and scale must have the same number of dimensions");

		//calculate new dimensions
		long[] oriDims = new long[source.numDimensions()];
		source.dimensions(oriDims);
		long[] dim = new long[source.numDimensions()];
		for(int d = 0; d<dim.length; d++)
			dim[d] = (long) (oriDims[d]*scale.getScale(d));

		final Img<T> result = source.factory().create(dim, source.firstElement().createVariable());


		final RealRandomAccess<T> inter = ifac.create(Views.extend(source, new OutOfBoundsMirrorFactory<T,Img<T>>(OutOfBoundsMirrorFactory.Boundary.SINGLE)));

		Cursor<T> rCursor = result.localizingCursor();

		while(rCursor.hasNext()){
			rCursor.fwd();
			scale.applyInverse(inter, rCursor);
			rCursor.get().set(inter.get());
		}

		return result;
	}



	public static <T extends RealType<T>> ColorTable[] createDefaultColorTables(IterableInterval<T> img, int channelDim){
		ColorTable[] tables = null;

		tables = new ColorTable[(int) img.dimension(channelDim)];
		ImgLut<?>[] luts = null;
		if(img.firstElement().getBitsPerPixel()==16)
			luts = Lut16.values();
		else
			luts = Lut8.values();
		for(int i = 0; i<img.dimension(channelDim); i++)
			tables[i] = (ArrayColorTable<?>) luts[i%luts.length].table();

		return tables;
	}

	public static <T extends RealType<T>> ColorTable[] createColorTables(T type, String[] luts){
		ColorTable[] tables = new ColorTable[luts.length];
		int bits = type.getBitsPerPixel();
		if(bits==16)
			for(int n = 0; n<luts.length; n++)
				tables[n] = Lut16.valueOf(luts[n]).table();
		else
			for(int n = 0; n<luts.length; n++)
				tables[n] = Lut8.valueOf(luts[n]).table();
		return tables;
	}



	public static <T extends RealType<T>> ColorTable[] createColorTables(RandomAccessibleInterval<T> img, String[] names){

		ColorTable[] tables = null;

		int bits = Views.iterable(img).firstElement().getBitsPerPixel();

		//TODO exception

		ImgLut<?>[] luts = new ImgLut<?>[names.length];
		if(bits==16)
			for(int n = 0; n<names.length; n++)
				luts[n] = Lut16.valueOf(names[n]);
		else
			for(int n = 0; n<names.length; n++)
				luts[n] = Lut8.valueOf(names[n]);


		//if(img.numDimensions()>3){
		tables = new ColorTable[names.length];

		for(int i = 0; i<names.length; i++)
			tables[i] = (ArrayColorTable<?>) luts[i].table();
		//}
		/*
		else{
			tables = new ArrayColorTable<?>[1];
			if(bits==16)
				tables[0] = (ArrayColorTable<?>) Lut16.values()[0].table();
			else
				tables[0] = (ArrayColorTable<?>) Lut8.values()[0].table();
		}
		 */
		return tables;
	}


	/**
	 * Compute the min and max for any {@link Iterable}, like an {@link Img}.
	 *
	 * The only functionality we need for that is to iterate. Therefore we need no {@link Cursor}
	 * that can localize itself, neither do we need a {@link RandomAccess}. So we simply use the
	 * most simple interface in the hierarchy.
	 *
	 * @param input - the input that has to just be {@link Iterable}
	 * @param min - the type that will have min
	 * @param max - the type that will have max
	 */
	public static < T extends Comparable< T > & Type< T > > void computeMinMax(
			final Iterable< T > input, final T min, final T max )
	{
		// create a cursor for the image (the order does not matter)
		final Iterator< T > iterator = input.iterator();

		// initialize min and max with the first image value
		T type = iterator.next();

		min.set( type );
		max.set( type );

		// loop over the rest of the data and determine min and max value
		while ( iterator.hasNext() )
		{
			// we need this type more than once
			type = iterator.next();

			if ( type.compareTo( min ) < 0 )
				min.set( type );

			if ( type.compareTo( max ) > 0 )
				max.set( type );
		}
	}







	/**
	 * @param reference
	 * @return A Function which rotates the given coordinate using the same matrix as the matrix required
	 * to rotate from (0,1,0) to the given reference
	 */
	public static Function<long[],long[]> rotation3D(final double[] reference, final long[] center){

		final RealMatrix I = MatrixUtils.createRealDiagonalMatrix(new double[]{1,1,1});

		final Vector3D ref = new Vector3D(reference[0],reference[1],reference[2]).normalize();
		final Vector3D v = ref.crossProduct(Vector3D.PLUS_J);//cross product

		final double s = v.getNorm(); // sine of angle
		final double c = ref.dotProduct(Vector3D.PLUS_J); // cosine of angle

		//skew-symmetric cross-product matrix of v
		final RealMatrix V = MatrixUtils.createRealMatrix(3,3);
		V.setEntry(0, 1, -v.getZ()); 	V.setEntry(0, 2, v.getY());
		V.setEntry(1, 0, v.getZ()); 	V.setEntry(1, 2, -v.getX());
		V.setEntry(2, 0, -v.getY()); 	V.setEntry(2, 1, v.getX());

		final RealMatrix R = I.add(V).add(V.power(2).scalarMultiply((1d-c)/Math.pow(s,2)));

		return (l) -> {
			long[] result = new long[3];
			Vector3D r = new Vector3D(R.operate(new double[]{l[0]-center[0],l[1]-center[1],l[2]-center[2]}));
			result[0] = Math.round(r.getX())+center[0];	result[1] = Math.round(r.getY())+center[1];	result[2] = Math.round(r.getZ())+center[2];
			return result;
		};
	}






}
