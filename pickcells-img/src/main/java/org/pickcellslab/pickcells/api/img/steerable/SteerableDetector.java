/*******************************************************************************
 * Copyright (c) 2012-2013 Biomedical Image Group (BIG), EPFL, Switzerland.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 *     Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 ******************************************************************************/
package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2019 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.awt.Color;

import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedByteType;

/**
 * This class implements a ridge detector based on the following publication:
 * <br><br>
 * M. Jacob, M. Unser, “Design of Steerable Filters for Feature Detection Using Canny-Like Criteria,”
 * IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 8, pp. 1007-1019
 * <br>
 * <hr>
 * NOTICE: This class was modified from a plugin originally implemented by François Aguet for ImageJ, and ported to Icy by Ricard Delgado-Gonzalo (ricard.delgado@gmail.com) 
 * and Zsuzsanna Püspöki (zsuzsanna.puspoki@epfl.ch).
 * <hr>
 */
public class SteerableDetector implements Runnable {

	// ----------------------------------------------------------------------------
	// IMAGE-RELATED FIELDS

	/** Rasterized input image. */
	private double[] input_ = null;

	/** Rasterized image containing the response of the filter. */
	private double[] response_ = null;
	/** Rasterized image containing an estimation of the local orientation. */
	private double[] orientation_ = null;

	/** With of the input image. */
	private int nx_ = 0;
	/** Height of the input image. */
	private int ny_ = 0;
	/** Depth of the input image. */
	private int nz_ = 1;
	/** Total number of pixels in the image stack. */
	private int size_ = 0;
	/** Number of pixels per image. */
	private int nxy_ = 0;

	// ----------------------------------------------------------------------------
	// PARAMETERS

	/** Order of the feature template. */
	private int M_ = 0;
	/**
	 * Standard deviation of the Gaussian on which the steerable templates are
	 * based.
	 */
	private double sigma_ = 0;
	/**
	 * Array of weights that control the linear combination of Gaussian
	 * derivatives which constitute the feature template.
	 */
	private double[] alpha_ = null;

	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a20_ = 0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a22_ = 0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a40_ = 0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a42_ = 0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a44_ = 0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a11_ = 0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a31_ = 0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a33_ = 0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a51_ = 0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template. Its value corresponds to one element of the
	 * <code>alpha_</code> array.
	 */
	private double a53_ = 0;

	/**
	 * First partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gx_ = null;
	/**
	 * First partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gy_ = null;
	/**
	 * Second partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxx_ = null;
	/**
	 * Second partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxy_ = null;
	/**
	 * Second partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gyy_ = null;
	/**
	 * Third partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxxx_ = null;
	/**
	 * Third partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxxy_ = null;
	/**
	 * Third partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxyy_ = null;
	/**
	 * Third partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gyyy_ = null;
	/**
	 * Fourth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxxxx_ = null;
	/**
	 * Fourth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxxxy_ = null;
	/**
	 * Fourth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxxyy_ = null;
	/**
	 * Fourth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxyyy_ = null;
	/**
	 * Fourth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gyyyy_ = null;
	/**
	 * Fifth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxxxxx_ = null;
	/**
	 * Fifth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxxxxy_ = null;
	/**
	 * Fifth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxxxyy_ = null;
	/**
	 * Fifth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxxyyy_ = null;
	/**
	 * Fifth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gxyyyy_ = null;
	/**
	 * Fifth partial derivative of the Gaussian kernel. The 'x' denotes first
	 * spatial dimension, and 'y' denotes second spatial dimension.
	 */
	private double[] gyyyyy_ = null;

	// ----------------------------------------------------------------------------
	// CONSTANTS

	/** PI. */
	private static final double PI = Math.PI;
	/** Tolerance when dividing by zero. */
	private static final double TOLERANCE = 1e-13;

	// ----------------------------------------------------------------------------
	// RUNNING FIELDS

	/**
	 * Controls the execution flow. If <code>true</code>, the filtering process
	 * stops.
	 */
	private boolean stop_ = false;

	/** Progress bar. */
	//private ProgressFrame pFrame_ = null;

	// ============================================================================
	// PUBLIC METHODS

	/**
	 * Initializes a new feature detector.
	 * 
	 * @param input
	 *            The input array, derived from the input image or stack. The
	 *            dimensions of the original image are passed as nx_, ny_, nz_.
	 * @param sigma
	 *            The standard deviation of the Gaussian on which the steerable
	 *            templates are based. This controls the feature width.
	 * @param M
	 *            The order of the feature template. Order up to 5 are supported
	 *            in the current implementation.
	 * @param alpha
	 *            The array of weights that control the linear combination of
	 *            Gaussian derivatives up to order M which constitute the
	 *            feature template. For more information, see the documentation
	 *            for the ParameterSet class.
	 */
	public SteerableDetector(double[] input, int nx, int ny, 
			double sigma, int M, double[] alpha) {

	
		
		input_ = input;
		M_ = M;
		sigma_ = sigma;
		alpha_ = alpha;
		nx_ = nx;
		ny_ = ny;
		nxy_ = nx * ny;
		size_ = nxy_ ;

		response_ = new double[size_];
		orientation_ = new double[size_];
	}

	// ----------------------------------------------------------------------------

	/** Computes the feature detection. */
	@Override
	public void run() {
		
		
		//pFrame_ = new ProgressFrame("Applying steerable filter");
		//pFrame_.setLength(size_);
		switch (M_) {
		case 1:
			a11_ = alpha_[0];
			filterM1();
			break;
		case 2:
			a20_ = alpha_[0];
			a22_ = alpha_[1];
			filterM2xx();
			break;
		case 3:
			a11_ = alpha_[0];
			a31_ = alpha_[1];
			a33_ = alpha_[2];
			filterM3();
			break;
		case 4:
			a20_ = alpha_[0];
			a22_ = alpha_[1];
			a40_ = alpha_[2];
			a42_ = alpha_[3];
			a44_ = alpha_[4];
			filterM4();
			break;
		case 5:
			a11_ = alpha_[0];
			a31_ = alpha_[1];
			a33_ = alpha_[2];
			a51_ = alpha_[3];
			a53_ = alpha_[4];
			filterM5();
			break;
		default:
			break;
		}
		//pFrame_.close();
	}

	// ----------------------------------------------------------------------------

	/** Stops the filtering process. */
	public void stop() {
		stop_ = true;
	}

	// ----------------------------------------------------------------------------

	
	/**
	 * Displays the orientation map color, where the hue indicates orientation
	 * and brightness is proportional to the strength of the feature response.
	 * @return A {@link RandomAccessibleInterval} where the last dimension corresponds
	 * to RGB channels.
	 */
	public RandomAccessibleInterval<UnsignedByteType> computeColorOrientation() {
		
		float[] h = new float[size_];
		float[] s = new float[size_];
		float[] b = new float[size_];
		double min = Double.MAX_VALUE;
		double max = -Double.MAX_VALUE;
		for (int i = 0; i < size_; i++) {
			if (response_[i] > max) {
				max = response_[i];
			}
			if (response_[i] < min) {
				min = response_[i];
			}
		}

		if (M_ % 2 != 0) {
			for (int i = 0; i < size_; i++) {
				h[i] = (float) ((orientation_[i] / (2.0 * PI) + 0.5));
				s[i] = 1;
				b[i] = (float) ((response_[i] - min) / (max - min));
			}
		} else {
			for (int i = 0; i < size_; i++) {
				h[i] = (float) ((orientation_[i] / PI + 0.5));
				s[i] = 1;
				b[i] = (float) ((response_[i] - min) / (max - min));
			}
		}
		Color c;

		int[] red = new int[size_];
		int[] green = new int[size_];
		int[] blue = new int[size_];

		for (int i = 0; i < h.length; ++i) {
			c = Color.getHSBColor(h[i], s[i], b[i]);
			red[i] = c.getRed();
			green[i] = c.getGreen();
			blue[i] = c.getBlue();
		}

		return Image2ArrayConverter
				.arraysToRGBSeq(red, green, blue, nx_, ny_, nz_);

	}

	// ----------------------------------------------------------------------------

	/** Performs non-maximum suppression on the filter response. */
	public <T extends RealType<T>> void computeNMS(RandomAccess<T> access) {
		
		double[] output = new double[size_];

		double ux, uy, GA, GA1, GA2, angle;
		SplineInterpolator interpolator;
		double[] slice = new double[nxy_];
		int idx = 0;
		for (int z = 0; z < nz_; z++) {

			System.arraycopy(response_, z * nxy_, slice, 0, nxy_);
			interpolator = new SplineInterpolator(slice, nx_, ny_, "linear");
			for (int y = 0; y < ny_; y++) {
				for (int x = 0; x < nx_; x++) {
					angle = orientation_[idx];
					ux = -Math.sin(angle); // cos(angle + pi/2)
					uy = Math.cos(angle);
					GA1 = interpolator.getValue(x + ux, y + uy);
					GA2 = interpolator.getValue(x - ux, y - uy);
					GA = response_[idx];
					if ((GA < GA1) || (GA < GA2)) {
						output[idx] = 0.0;
					} else {
						output[idx] = GA;
					}
					idx++;
				}
			}
		}
		Image2ArrayConverter.doubleArrToSeq(output, nx_, ny_, access);
	}

	// ----------------------------------------------------------------------------

	/**
	 * Computes the response to rotations of the feature template over 2 pi,
	 * in (2*pi)/nIncrements increments.
	 
	public <T extends RealType<T>> RandomAccessibleInterval<T> computeRotations(int nIncrements) {
		double[] rotations = new double[nxy_ * nIncrements];
		double radiantStep = 2.0 * PI / nIncrements;

		switch (M_) {
		case 1:
			for (int k = 0; k < nIncrements / 2; k++) {
				for (int i = 0; i < nxy_; i++) {
					rotations[i + k * nxy_] = pointRespM1(i, k * radiantStep);
					rotations[i + (k + nIncrements / 2) * nxy_] = -rotations[i
							+ k * nxy_];
				}
			}
			break;
		case 2:
			for (int k = 0; k < nIncrements / 2; k++) {
				for (int i = 0; i < nxy_; i++) {
					rotations[i + k * nxy_] = pointRespM2(i, k * radiantStep);
					rotations[i + (k + nIncrements / 2) * nxy_] = rotations[i
							+ k * nxy_];
				}
			}
			break;
		case 3:
			for (int k = 0; k < nIncrements / 2; k++) {
				for (int i = 0; i < nxy_; i++) {
					rotations[i + k * nxy_] = pointRespM3(i, k * radiantStep);
					rotations[i + (k + nIncrements / 2) * nxy_] = -rotations[i
							+ k * nxy_];
				}
			}
			break;
		case 4:
			for (int k = 0; k < nIncrements / 2; k++) {
				for (int i = 0; i < nxy_; i++) {
					rotations[i + k * nxy_] = pointRespM4(i, k * radiantStep);
					rotations[i + (k + nIncrements / 2) * nxy_] = rotations[i
							+ k * nxy_];
				}
			}
			break;
		case 5:
			for (int k = 0; k < nIncrements / 2; k++) {
				for (int i = 0; i < nxy_; i++) {
					rotations[i + k * nxy_] = pointRespM5(i, k * radiantStep);
					rotations[i + (k + nIncrements / 2) * nxy_] = -rotations[i
							+ k * nxy_];
				}
			}
			break;
		default:
			break;
		}
		//Image2ArrayConverter.doubleArrToSeq(rotations, nx_, ny_, access);
	
		return null;
	}
	*/
	
	

	// ============================================================================
	// PRIVATE METHODS

	private void filterM1() {
		double gxi, gyi;
		double temp;
		double[] tRoots = new double[2];

		int iz = 0;
		double[] current = new double[nxy_];

		for (int z = 0; z < nz_; z++) {
			System.arraycopy(input_, z * nxy_, current, 0, nxy_);
			computeBaseTemplates(current, nx_, ny_, M_, sigma_);
			for (int i = 0; i < nxy_ && !stop_; i++) {
				//pFrame_.incPosition();
				gxi = gx_[i];
				gyi = gy_[i];
				gxi = approxZero(gxi, TOLERANCE);
				gyi = approxZero(gyi, TOLERANCE);

				if (gxi == 0.0 && gyi == 0.0) {
					response_[iz] = 0.0;
					orientation_[iz] = 0.0;
				} else {
					if (gyi == 0.0) {
						tRoots[0] = PI / 2.0;
						tRoots[1] = opposite(tRoots[0]);
					} else {
						tRoots[0] = Math.atan(-gxi / gyi);
						tRoots[1] = opposite(tRoots[0]);
					}
					orientation_[iz] = tRoots[0];
					response_[iz] = Math.cos(tRoots[0]) * a11_ * gyi
							- Math.sin(tRoots[0]) * a11_ * gxi;
					temp = Math.cos(tRoots[1]) * a11_ * gyi
							- Math.sin(tRoots[1]) * a11_ * gxi;
					if (temp > response_[iz]) {
						response_[iz] = temp;
						orientation_[iz] = tRoots[1];
					}
				}
				iz++;
			}
		}
	}

	// ----------------------------------------------------------------------------

	private void filterM2xx() { // nonseparable implementation, linear solution
		double d, gxyi;
		double temp;
		double[] tRoots = new double[2];

		int iz = 0;
		double[] current = new double[nxy_];
		for (int z = 0; z < nz_; z++) {
			System.arraycopy(input_, z * nxy_, current, 0, nxy_);
			computeBaseTemplates(current, nx_, ny_, M_, sigma_);
			for (int i = 0; i < nxy_ && !stop_; i++) {
				//pFrame_.incPosition();
				d = approxZero(gxx_[i] - gyy_[i], TOLERANCE);
				gxyi = approxZero(gxy_[i], TOLERANCE);

				if (d == 0.0 && gxyi == 0.0) {
					response_[iz] = pointRespM2(i, 0.0);
					orientation_[iz] = 0.0;
				} else {
					if (gxyi == 0.0) { // sin(2t) = 0, cos(2t) = 1 -> 2t = pi/2
						tRoots[0] = 0.0;
						tRoots[1] = -PI / 2.0;
					} else {
						tRoots[0] = Math.atan(2.0 * gxyi / d) / 2.0;
						tRoots[1] = complement(tRoots[0]);
					}
					orientation_[iz] = tRoots[0];
					response_[iz] = pointRespM2(i, tRoots[0]);
					temp = pointRespM2(i, tRoots[1]);
					if (temp > response_[iz]) {
						response_[iz] = temp;
						orientation_[iz] = tRoots[1];
					}
				}
				iz++;
			}
		}
	}

	// ----------------------------------------------------------------------------

	private void filterM3() {

		double A, B, C, D;
		double temp;

		double a1 = 2.0 * a31_ - 3.0 * a33_;
		double a2 = 6.0 * a33_ - 7.0 * a31_;
		double g1, g2;

		int iz = 0;
		double[] current = new double[nxy_];
		for (int z = 0; z < nz_; z++) {
			System.arraycopy(input_, z * nxy_, current, 0, nxy_);
			computeBaseTemplates(current, nx_, ny_, M_, sigma_);
			for (int i = 0; i < nxy_ && !stop_; i++) {
			//	pFrame_.incPosition();
				g1 = -a11_ * gy_[i];
				g2 = -a11_ * gx_[i];

				A = g1 - a31_ * gyyy_[i] + a1 * gxxy_[i];
				B = g2 + a2 * gxyy_[i] + a1 * gxxx_[i];
				C = g1 + a2 * gxxy_[i] + a1 * gyyy_[i];
				D = g2 - a31_ * gxxx_[i] + a1 * gxyy_[i];

				A = approxZero(A, TOLERANCE);
				B = approxZero(B, TOLERANCE);
				C = approxZero(C, TOLERANCE);
				D = approxZero(D, TOLERANCE);

				if (A == 0.0) { // -> quadratic
					if (B == 0.0) { // -> linear
						if (C == 0.0) { // -> null, solve
							orientation_[iz] = 0.0;
							response_[iz] = pointRespM3(i, 0.0);
						} else { // solve linear
							if (D == 0.0) { // cos or sin can be 0
								double[] tRoots = new double[4];
								tRoots[0] = -PI / 2.0;
								tRoots[1] = 0.0;
								tRoots[2] = PI / 2.0;
								tRoots[3] = PI;
								response_[iz] = pointRespM3(i, tRoots[0]);
								orientation_[iz] = tRoots[0];
								for (int k = 1; k < 4; k++) {
									temp = pointRespM3(i, tRoots[k]);
									if (temp > response_[iz]) {
										response_[iz] = temp;
										orientation_[iz] = tRoots[k];
									}
								}
							} else {
								double[] tRoots = new double[2];
								tRoots[0] = Math.atan(-D / C);
								tRoots[1] = opposite(tRoots[0]);
								response_[iz] = pointRespM3(i, tRoots[0]);
								orientation_[iz] = tRoots[0];
								temp = pointRespM3(i, tRoots[1]);
								if (temp > response_[iz]) {
									response_[iz] = temp;
									orientation_[iz] = tRoots[1];
								}
							}
						}
					} else { // solve quadratic
						double[] xRoots = PolynomialUtils.quadraticRoots(C / B,
								D / B);
						double[] tRoots = new double[4];
						if (xRoots.length == 0 || xRoots[0] == 0.0
								|| xRoots[0] == -xRoots[1]) {
							tRoots[0] = -PI / 2.0;
							tRoots[1] = 0.0;
							tRoots[2] = PI / 2.0;
							tRoots[3] = PI;
						} else {
							tRoots[0] = Math.atan(xRoots[0]);
							tRoots[1] = Math.atan(xRoots[1]);
							tRoots[2] = opposite(tRoots[0]);
							tRoots[3] = opposite(tRoots[1]);
						}
						response_[iz] = pointRespM3(i, tRoots[0]);
						orientation_[iz] = tRoots[0];
						for (int k = 1; k < 4; k++) {
							temp = pointRespM3(i, tRoots[k]);
							if (temp > response_[iz]) {
								response_[iz] = temp;
								orientation_[iz] = tRoots[k];
							}
						}
					}
				} else { // solve cubic
					double[] xRoots = PolynomialUtils.cubicRoots(B / A, C / A,
							D / A);
					double[] tRoots = new double[2 * xRoots.length];
					double[] currentResponse = new double[tRoots.length];

					for (int k = 0; k < xRoots.length; k++) {
						tRoots[k] = Math.atan(xRoots[k]);
						tRoots[k + xRoots.length] = opposite(tRoots[k]);
					}

					for (int k = 0; k < tRoots.length; k++) {
						currentResponse[k] = pointRespM3(i, tRoots[k]);
					}
					sort(currentResponse, tRoots);

					if (xRoots.length == 3) {
						if (approxEqual(currentResponse[tRoots.length - 1],
								currentResponse[tRoots.length - 2], 0.000001)) {
							response_[iz] = currentResponse[tRoots.length - 3];
							orientation_[iz] = tRoots[tRoots.length - 3];
						} else {
							response_[iz] = currentResponse[tRoots.length - 1];
							orientation_[iz] = tRoots[tRoots.length - 1];
						}
					} else {
						response_[iz] = currentResponse[tRoots.length - 1];
						orientation_[iz] = tRoots[tRoots.length - 1];
					}
				}
				iz++;
			}
		}
	}

	// ----------------------------------------------------------------------------

	private void filterM4() {
		double g1, g2, g3;
		double A, B, C, D, E;
		double[] xRoots, tRoots;
		double temp;
		int N;
		double tolerance = 0.000000000001;

		double a1 = 2.0 * a44_ - a42_;
		double a2 = 2.0 * a40_ - a42_;
		double a3 = a22_ - a20_;
		double a4 = 6.0 * (a44_ - a42_ + a40_);
		int iz = 0;
		double[] current = new double[nxy_];
		for (int z = 0; z < nz_; z++) {
			System.arraycopy(input_, z * nxy_, current, 0, nxy_);
			computeBaseTemplates(current, nx_, ny_, M_, sigma_);
			for (int i = 0; i < nxy_ && !stop_; i++) {
			//	pFrame_.incPosition();
				g1 = a3 * gxy_[i];
				g2 = a3 * (gxx_[i] - gyy_[i]);
				g3 = a4 * gxxyy_[i];

				A = a1 * gxxxy_[i] - a2 * gxyyy_[i] + g1;
				B = a1 * gxxxx_[i] + a2 * gyyyy_[i] - g3 + g2;
				C = a4 * (gxyyy_[i] - gxxxy_[i]);
				D = -a1 * gyyyy_[i] - a2 * gxxxx_[i] + g3 + g2;
				E = a2 * gxxxy_[i] - a1 * gxyyy_[i] - g1;

				A = approxZero(A, tolerance);
				C = approxZero(C, tolerance);
				E = approxZero(E, tolerance);

				if (A == 0.0) { // -> cubic
					if (B == 0.0) { // -> quadratic
						if (C == 0.0) { // -> linear
							if (D == 0.0) { // solve null
								response_[iz] = pointRespM4(i, 0.0);
								orientation_[iz] = 0.0;
							} else { // solve linear
								orientation_[iz] = Math.atan(-E / D);
								response_[iz] = pointRespM4(i, orientation_[iz]);
							}
						} else { // solve quadratic
							xRoots = PolynomialUtils.quadraticRoots(D / C, E
									/ C);
							N = xRoots.length;
							if (N == 0) {
								response_[iz] = pointRespM4(i, 0.0);
								orientation_[iz] = 0.0;
							} else {
								tRoots = new double[2];
								tRoots[0] = Math.atan(xRoots[0]);
								tRoots[1] = Math.atan(xRoots[1]);
								response_[iz] = pointRespM4(i, tRoots[0]);
								orientation_[iz] = tRoots[0];
								temp = pointRespM4(i, tRoots[1]);
								if (temp > response_[iz]) {
									response_[iz] = temp;
									orientation_[iz] = tRoots[1];
								}
							}
						}
					} else { // solve cubic -> (B != 0)
						if ((C == 0.0) && (E == 0.0)) {
							double deltaQ = -D / B;
							if (deltaQ >= 0.0) { // Bx^3 + Dx = 0
								tRoots = new double[4];
								tRoots[0] = Math.atan(Math.sqrt(deltaQ));
								tRoots[1] = Math.atan(-Math.sqrt(deltaQ));
								tRoots[2] = 0.0;
								tRoots[3] = PI / 2.0;

								response_[iz] = pointRespM4(i, tRoots[0]);
								orientation_[iz] = tRoots[0];
								for (int k = 1; k < 4; k++) {
									temp = pointRespM4(i, tRoots[k]);
									if (temp > response_[iz]) {
										response_[iz] = temp;
										orientation_[iz] = tRoots[k];
									}
								}
							} else {
								tRoots = new double[2];
								tRoots[0] = 0.0;
								tRoots[1] = PI / 2.0;

								response_[iz] = pointRespM4(i, 0.0);
								orientation_[iz] = 0.0;
								temp = pointRespM4(i, PI / 2.0);
								if (temp > response_[iz]) {
									response_[iz] = temp;
									orientation_[iz] = PI / 2.0;
								}
							}
						} else {
							xRoots = PolynomialUtils.cubicRoots(C / B, D / B, E
									/ B);
							N = xRoots.length;
							tRoots = new double[N];
							for (int k = 0; k < N; k++) {
								tRoots[k] = Math.atan(xRoots[k]);
							}
							response_[iz] = pointRespM4(i, tRoots[0]);
							orientation_[iz] = tRoots[0];
							for (int k = 1; k < N; k++) {
								temp = pointRespM4(i, tRoots[k]);
								if (temp > response_[iz]) {
									response_[iz] = temp;
									orientation_[iz] = tRoots[k];
								}
							}
						}
					}
				} else { // solve quartic

					xRoots = PolynomialUtils.quarticRoots(B / A, C / A, D / A,
							E / A);
					N = xRoots.length;
					if (N == 0) {
						orientation_[iz] = 0.0;
						response_[iz] = pointRespM4(i, 0.0);
					} else {
						tRoots = new double[N];
						for (int k = 0; k < N; k++) {
							tRoots[k] = Math.atan(xRoots[k]);
						}
						response_[iz] = pointRespM4(i, tRoots[0]);
						orientation_[iz] = tRoots[0];
						for (int k = 1; k < N; k++) {
							temp = pointRespM4(i, tRoots[k]);
							if (temp > response_[iz]) {
								response_[iz] = temp;
								orientation_[iz] = tRoots[k];
							}
						}
					}
				}
				iz++;
			}
		}
	}

	// ----------------------------------------------------------------------------

	private void filterM5() {
		filterM1();
		double[] initialAngle = orientation_;

		double a1 = 2.0 * a31_ - 3.0 * a33_;
		double a2 = 4.0 * a51_ - 3.0 * a53_;
		double a3 = 6.0 * a33_ - 7.0 * a31_;
		double a4 = 12.0 * a51_ - 17.0 * a53_;
		double a5 = 6.0 * a53_ - 13.0 * a51_;
		double a6 = 3.0 * a33_ - 5.0 * a31_;
		double a7 = a31_ - 3.0 * a33_;
		double a8 = 30.0 * a53_ - 34.0 * a51_;
		double a53x2 = 2.0 * a53_;
		double a11x2 = 2.0 * a11_;
		double[] quinticC = new double[6];
		double[] quarticC = new double[5];
		double[] cubicC = new double[4];
		double[] quarticRoots;
		double[] roots, tRoots;
		int N;
		double x0;
		double[] f0;
		double[] croot;
		double theta;

		int iz = 0;
		double[] current = new double[nxy_];

		for (int z = 0; z < nz_; z++) {
			System.arraycopy(input_, z * nxy_, current, 0, nxy_);
			computeBaseTemplates(current, nx_, ny_, M_, sigma_);
			for (int i = 0; i < nxy_ && !stop_; i++) {
			//	pFrame_.incPosition();
				quinticC[0] = -a11_ * gx_[i] + a1 * gxyy_[i] - a31_ * gxxx_[i]
						- a51_ * gxxxxx_[i] + a2 * gxxxyy_[i] + a53x2
						* gxyyyy_[i];
				quinticC[1] = -a11_ * gy_[i] + a3 * gxxy_[i] + a5 * gxxxxy_[i]
						+ a1 * gyyy_[i] + a4 * gxxyyy_[i] + a53x2 * gyyyyy_[i];
				quinticC[2] = -a11x2 * gx_[i] + a7 * gxxx_[i] + a2 * gxxxxx_[i]
						+ a6 * gxyy_[i] + a8 * gxxxyy_[i] + a4 * gxyyyy_[i];
				quinticC[3] = -a11x2 * gy_[i] + a6 * gxxy_[i] + a4 * gxxxxy_[i]
						+ a7 * gyyy_[i] + a8 * gxxyyy_[i] + a2 * gyyyyy_[i];
				quinticC[4] = -a11_ * gx_[i] + a1 * gxxx_[i] + a53x2
						* gxxxxx_[i] + a3 * gxyy_[i] + a4 * gxxxyy_[i] + a5
						* gxyyyy_[i];
				quinticC[5] = -a11_ * gy_[i] + a1 * gxxy_[i] - a31_ * gyyy_[i]
						+ a53x2 * gxxxxy_[i] + a2 * gxxyyy_[i] - a51_
						* gyyyyy_[i];

				quinticC[5] = approxZero(quinticC[5], TOLERANCE);
				quinticC[4] = approxZero(quinticC[4], TOLERANCE);
				quinticC[3] = approxZero(quinticC[3], TOLERANCE);
				quinticC[2] = approxZero(quinticC[2], TOLERANCE);
				quinticC[1] = approxZero(quinticC[1], TOLERANCE);
				quinticC[0] = approxZero(quinticC[0], TOLERANCE); // check
																	// order!!
																	// ->
																	// highest
																	// degree
																	// coeff ->
																	// [5]

				if (quinticC[5] == 0.0) {
					if (quinticC[4] == 0.0) {
						if (quinticC[3] == 0.0) {
							if (quinticC[2] == 0.0) {
								if (quinticC[1] == 0.0) {
									roots = new double[1];
									roots[0] = 0.0;
								} else {
									roots = new double[1];
									roots[0] = -quinticC[0] / quinticC[1];
								}
							} else {
								roots = PolynomialUtils.quadraticRoots(
										quinticC[1] / quinticC[2], quinticC[0]
												/ quinticC[2]);
							}
						} else {
							roots = PolynomialUtils.cubicRoots(quinticC[2]
									/ quinticC[3], quinticC[1] / quinticC[3],
									quinticC[0] / quinticC[3]);
						}
					} else {
						roots = PolynomialUtils.quarticRoots(quinticC[3]
								/ quinticC[4], quinticC[2] / quinticC[4],
								quinticC[1] / quinticC[4], quinticC[0]
										/ quinticC[4]);
					}

					N = roots.length;

					if (N != 0) {
						tRoots = new double[2 * N + 4];
						for (int k = 0; k < N; k++) {
							tRoots[k] = Math.atan(roots[k]);
							tRoots[k + N] = opposite(tRoots[k]);
						}
						tRoots[2 * N] = 0.0;
						tRoots[2 * N + 1] = PI / 2.0;
						tRoots[2 * N + 2] = PI;
						tRoots[2 * N + 3] = -PI / 2.0;

						response_[iz] = pointRespM5(i, tRoots[0]);
						orientation_[iz] = tRoots[0];
						for (int k = 1; k < tRoots.length; k++) {
							double temp = pointRespM5(i, tRoots[k]);
							if (temp > response_[iz]) {
								response_[iz] = temp;
								orientation_[iz] = tRoots[k];
							}
						}
					} else {
						tRoots = new double[4];// {-PI/2.0, 0, PI/2.0, PI};
						tRoots[0] = 0.0;
						tRoots[1] = PI / 2.0;
						tRoots[2] = PI;
						tRoots[3] = -PI / 2.0;
						response_[iz] = pointRespM5(i, tRoots[0]);
						orientation_[iz] = tRoots[0];
						for (int k = 1; k < tRoots.length; k++) {
							double temp = pointRespM5(i, tRoots[k]);
							if (temp > response_[iz]) {
								response_[iz] = temp;
								orientation_[iz] = tRoots[k];
							}
						}
					}

				} else {
					theta = initialAngle[iz];
					x0 = Math.tan(theta);
					f0 = PolynomialUtils.evalPolyD(quinticC, x0);

					if (quinticC[0] == 0.0) { // root = 0.0
						quarticRoots = PolynomialUtils.quarticRoots(quinticC[4]
								/ quinticC[5], quinticC[3] / quinticC[5],
								quinticC[2] / quinticC[5], quinticC[1]
										/ quinticC[5]);
						N = quarticRoots.length;
						roots = new double[N + 1];
						roots[0] = 0.0;
						for (int k = 1; k <= N; k++) {
							roots[k] = quarticRoots[k - 1];
						}
						N++;
					} else if (f0[0] == 0.0) { // root = x0
						quarticC = PolynomialUtils.divPolyByRoot(quinticC, x0);
						quarticRoots = PolynomialUtils.quarticRoots(quarticC[3]
								/ quarticC[4], quarticC[2] / quarticC[4],
								quarticC[1] / quarticC[4], quarticC[0]
										/ quarticC[4]);
						N = quarticRoots.length;
						roots = new double[N + 1];
						roots[0] = x0;
						for (int k = 1; k <= N; k++) {
							roots[k] = quarticRoots[k - 1];
						}
						N++;
					} else {

						if (Math.abs(x0) >= 100.0) {
							x0 = 0.0;
						}
						croot = PolynomialUtils.laguerre(quinticC, x0);
						croot[0] = approxZero(croot[0], 1e-15);
						croot[1] = approxZero(croot[1], 1e-15);

						if (croot[1] == 0) { // real root
							quarticC = PolynomialUtils.divPolyByRoot(quinticC,
									croot[0]);
							quarticRoots = PolynomialUtils.quarticRoots(
									quarticC[3] / quarticC[4], quarticC[2]
											/ quarticC[4], quarticC[1]
											/ quarticC[4], quarticC[0]
											/ quarticC[4]);
							N = quarticRoots.length;
							roots = new double[N + 1];
							roots[0] = croot[0];
							for (int k = 1; k <= N; k++) {
								roots[k] = quarticRoots[k - 1];
							}
							N++;
						} else { // complex root
							cubicC = PolynomialUtils.divPolyByConjRoots(
									quinticC, croot[0], croot[1]);
							roots = PolynomialUtils.cubicRoots(cubicC[2]
									/ cubicC[3], cubicC[1] / cubicC[3],
									cubicC[0] / cubicC[3]);
							N = roots.length;
						}

					}

					tRoots = new double[2 * N];
					for (int k = 0; k < N; k++) {
						tRoots[k] = Math.atan(roots[k]);
						tRoots[k + N] = opposite(tRoots[k]);
					}

					response_[iz] = pointRespM5(i, tRoots[0]);
					orientation_[iz] = tRoots[0];
					for (int k = 1; k < tRoots.length; k++) {
						double temp = pointRespM5(i, tRoots[k]);
						if (temp > response_[iz]) {
							response_[iz] = temp;
							orientation_[iz] = tRoots[k];
						}
					}
				}
				iz++;
			}
		}
	}

	// ----------------------------------------------------------------------------

	static private void sort(double[] responses, double[] angles) {

		int N = responses.length;
		double temp;
		double min;
		int minIndex;
		for (int i = 0; i < N - 1; i++) {
			min = responses[i];
			minIndex = i;
			for (int k = i + 1; k < N; k++) {
				if (responses[k] < min) {
					min = responses[k];
					minIndex = k;
				}
			}
			temp = responses[i];
			responses[i] = min;
			responses[minIndex] = temp;
			temp = angles[i];
			angles[i] = angles[minIndex];
			angles[minIndex] = temp;
		}
	}

	// ----------------------------------------------------------------------------

	private double opposite(double theta) {
		if (theta > 0.0) { // values in (-PI,PI]
			return theta - PI;
		} else {
			return theta + PI;
		}
	}

	// ----------------------------------------------------------------------------

	private double complement(double theta) {
		if (theta > 0.0) {
			return theta - PI / 2.0;
		} else {
			return theta + PI / 2.0;
		}
	}

	// ----------------------------------------------------------------------------

	private double approxZero(double n, double tol) {
		if (Math.abs(n) < tol) {
			return 0.0;
		} else {
			return n;
		}
	}

	// ----------------------------------------------------------------------------

	private boolean approxEqual(double a, double b, double tol) {

		if (Math.abs(1 - a / b) < tol) {
			return true;
		} else {
			return false;
		}
	}

	// ----------------------------------------------------------------------------

	private void computeBaseTemplates(double[] input, int nx, int ny, int M,
			double sigma) {

		int wWidth = (int) (4.0 * sigma);
		int kLength = wWidth + 1;
		double[] aKernel = new double[kLength];
		double[] bKernel = new double[kLength];
		double g[] = new double[kLength];
		double d;
		double sigma2 = sigma * sigma;
		double sigma4 = sigma2 * sigma2;
		double sigma6 = sigma4 * sigma2;
		double sigma8 = sigma4 * sigma4;
		double sigma10 = sigma6 * sigma4;
		double sigma12 = sigma8 * sigma4;

		for (int i = 0; i < kLength; i++) {
			g[i] = Math.exp(-(i * i) / (2.0 * sigma2));
		}

		if (M == 1 || M == 3 || M == 5) {

			d = 2.0 * PI * sigma4;
			for (int i = 0; i < kLength; i++) {
				aKernel[i] = -i * g[i] / d;
			}

			gx_ = Convolver2D.convolveOddX(input, aKernel, nx, ny);
			gx_ = Convolver2D.convolveEvenY(gx_, g, nx, ny);
			gy_ = Convolver2D.convolveOddY(input, aKernel, nx, ny);
			gy_ = Convolver2D.convolveEvenX(gy_, g, nx, ny);

			if (M == 3 || M == 5) {

				d = 2.0 * PI * sigma8;
				for (int i = 0; i < kLength; i++) {
					aKernel[i] = (3.0 * i * sigma2 - i * i * i) * g[i] / d;
				}
				gxxx_ = Convolver2D.convolveOddX(input, aKernel, nx, ny);
				gxxx_ = Convolver2D.convolveEvenY(gxxx_, g, nx, ny);
				gyyy_ = Convolver2D.convolveOddY(input, aKernel, nx, ny);
				gyyy_ = Convolver2D.convolveEvenX(gyyy_, g, nx, ny);
				for (int i = 0; i < kLength; i++) {
					aKernel[i] = (sigma2 - i * i) * g[i] / d;
					bKernel[i] = i * g[i];
				}
				gxxy_ = Convolver2D.convolveEvenX(input, aKernel, nx, ny);
				gxxy_ = Convolver2D.convolveOddY(gxxy_, bKernel, nx, ny);
				gxyy_ = Convolver2D.convolveEvenY(input, aKernel, nx, ny);
				gxyy_ = Convolver2D.convolveOddX(gxyy_, bKernel, nx, ny);
			}
			if (M == 5) {

				d = 2.0 * PI * sigma12;
				for (int i = 0; i < kLength; i++) {
					aKernel[i] = -i
							* (i * i * i * i - 10.0 * i * i * sigma2 + 15.0 * sigma4)
							* g[i] / d;
				}
				gxxxxx_ = Convolver2D.convolveOddX(input, aKernel, nx, ny);
				gxxxxx_ = Convolver2D.convolveEvenY(gxxxxx_, g, nx, ny);
				gyyyyy_ = Convolver2D.convolveOddY(input, aKernel, nx, ny);
				gyyyyy_ = Convolver2D.convolveEvenX(gyyyyy_, g, nx, ny);
				for (int i = 0; i < kLength; i++) {
					aKernel[i] = (i * i * i * i - 6.0 * i * i * sigma2 + 3.0 * sigma4)
							* g[i] / d;
					bKernel[i] = -i * g[i];
				}
				gxxxxy_ = Convolver2D.convolveEvenX(input, aKernel, nx, ny);
				gxxxxy_ = Convolver2D.convolveOddY(gxxxxy_, bKernel, nx, ny);
				gxyyyy_ = Convolver2D.convolveEvenY(input, aKernel, nx, ny);
				gxyyyy_ = Convolver2D.convolveOddX(gxyyyy_, bKernel, nx, ny);
				for (int i = 0; i < kLength; i++) {
					aKernel[i] = i * (i * i - 3.0 * sigma2) * g[i] / d;
					bKernel[i] = (sigma2 - i * i) * g[i];
				}
				gxxxyy_ = Convolver2D.convolveOddX(input, aKernel, nx, ny);
				gxxxyy_ = Convolver2D.convolveEvenY(gxxxyy_, bKernel, nx, ny);
				gxxyyy_ = Convolver2D.convolveOddY(input, aKernel, nx, ny);
				gxxyyy_ = Convolver2D.convolveEvenX(gxxyyy_, bKernel, nx, ny);
			}
		} else { // (M == 2 || M == 4)

			d = 2.0 * PI * sigma6;
			for (int i = 0; i < kLength; i++) {
				aKernel[i] = (i * i - sigma2) * g[i] / d;
			}
			gxx_ = Convolver2D.convolveEvenX(input, aKernel, nx, ny);
			gxx_ = Convolver2D.convolveEvenY(gxx_, g, nx, ny);
			gyy_ = Convolver2D.convolveEvenY(input, aKernel, nx, ny);
			gyy_ = Convolver2D.convolveEvenX(gyy_, g, nx, ny);
			for (int i = 0; i < kLength; i++) {
				aKernel[i] = i * g[i];
				bKernel[i] = aKernel[i] / d;
			}
			gxy_ = Convolver2D.convolveOddX(input, aKernel, nx, ny);
			gxy_ = Convolver2D.convolveOddY(gxy_, bKernel, nx, ny);

			if (M == 4) {

				d = 2.0 * PI * sigma10;
				for (int i = 0; i < kLength; i++) {
					aKernel[i] = (i * i * i * i - 6.0 * i * i * sigma2 + 3.0 * sigma4)
							* g[i] / d;
				}
				gxxxx_ = Convolver2D.convolveEvenX(input, aKernel, nx, ny);
				gxxxx_ = Convolver2D.convolveEvenY(gxxxx_, g, nx, ny);
				gyyyy_ = Convolver2D.convolveEvenY(input, aKernel, nx, ny);
				gyyyy_ = Convolver2D.convolveEvenX(gyyyy_, g, nx, ny);
				for (int i = 0; i < kLength; i++) {
					aKernel[i] = i * (i * i - 3.0 * sigma2) * g[i] / d;
					bKernel[i] = i * g[i];
				}
				gxxxy_ = Convolver2D.convolveOddX(input, aKernel, nx, ny);
				gxxxy_ = Convolver2D.convolveOddY(gxxxy_, bKernel, nx, ny);
				gxyyy_ = Convolver2D.convolveOddY(input, aKernel, nx, ny);
				gxyyy_ = Convolver2D.convolveOddX(gxyyy_, bKernel, nx, ny);
				for (int i = 0; i < kLength; i++) {
					aKernel[i] = (sigma2 - i * i) * g[i];
					bKernel[i] = aKernel[i] / d;
				}
				gxxyy_ = Convolver2D.convolveEvenX(input, aKernel, nx, ny);
				gxxyy_ = Convolver2D.convolveEvenY(gxxyy_, bKernel, nx, ny);
			}
		}
	}

	// ----------------------------------------------------------------------------

	private double pointRespM1(int i, double angle) {

		double cosT = Math.cos(angle);
		double sinT = Math.sin(angle);

		double result = a11_ * (cosT * gy_[i] - sinT * gx_[i]);
		return result;
	}

	// ----------------------------------------------------------------------------

	private double pointRespM2(int i, double angle) {

		double cosT = Math.cos(angle);
		double sinT = Math.sin(angle);

		double result = cosT * cosT * (a22_ * gyy_[i] + a20_ * gxx_[i]) + cosT
				* sinT * 2.0 * (a20_ - a22_) * gxy_[i] + sinT * sinT
				* (a22_ * gxx_[i] + a20_ * gyy_[i]);
		return result;
	}

	// ----------------------------------------------------------------------------

	private double pointRespM3(int i, double angle) {

		double cosT = Math.cos(angle);
		double sinT = Math.sin(angle);
		double cosT2 = cosT * cosT;
		double sinT2 = sinT * sinT;

		double result = cosT
				* a11_
				* gy_[i]
				- sinT
				* a11_
				* gx_[i]
				+ cosT2
				* cosT
				* (a31_ * gxxy_[i] + a33_ * gyyy_[i])
				- sinT2
				* sinT
				* (a31_ * gxyy_[i] + a33_ * gxxx_[i])
				- cosT2
				* sinT
				* (3.0 * a33_ * gxyy_[i] - 2.0 * a31_ * gxyy_[i] + a31_
						* gxxx_[i])
				+ cosT
				* sinT2
				* (3.0 * a33_ * gxxy_[i] - 2.0 * a31_ * gxxy_[i] + a31_
						* gyyy_[i]);
		return result;
	}

	// ----------------------------------------------------------------------------

	private double pointRespM4(int i, double angle) {

		double cosT = Math.cos(angle);
		double sinT = Math.sin(angle);
		double cosTsinT = cosT * sinT;
		double cosT2 = cosT * cosT;
		double sinT2 = sinT * sinT;
		double a = (a20_ - a22_) * gxy_[i];

		double result = cosT2
				* cosT2
				* (a20_ * gxx_[i] + a22_ * gyy_[i] + a40_ * gxxxx_[i] + a42_
						* gxxyy_[i] + a44_ * gyyyy_[i])
				+ cosT2
				* cosTsinT
				* 2.0
				* (a + 2.0 * a40_ * gxxxy_[i] + a42_ * (gxyyy_[i] - gxxxy_[i]) - 2.0
						* a44_ * gxyyy_[i])
				+ cosT2
				* sinT2
				* ((a20_ + a22_) * (gxx_[i] + gyy_[i]) + a42_
						* (gxxxx_[i] + gyyyy_[i]) + (6.0 * (a40_ + a44_) - 4.0 * a42_)
						* gxxyy_[i])
				+ sinT2
				* cosTsinT
				* 2.0
				* (a + 2.0 * a40_ * gxyyy_[i] + a42_ * (gxxxy_[i] - gxyyy_[i]) - 2.0
						* a44_ * gxxxy_[i])
				+ sinT2
				* sinT2
				* (a20_ * gyy_[i] + a22_ * gxx_[i] + a40_ * gyyyy_[i] + a42_
						* gxxyy_[i] + a44_ * gxxxx_[i]);
		return result;
	}

	// ----------------------------------------------------------------------------

	private double pointRespM5(int i, double angle) {

		double cosT = Math.cos(angle);
		double sinT = Math.sin(angle);
		double cosT2 = cosT * cosT;
		double sinT2 = sinT * sinT;
		double cosT3 = cosT2 * cosT;
		double sinT3 = sinT2 * sinT;

		double result = cosT2
				* cosT3
				* (a11_ * gy_[i] + a31_ * gxxy_[i] + a33_ * gyyy_[i] + a51_
						* gxxxxy_[i] + a53_ * gxxyyy_[i])
				+ cosT2
				* cosT2
				* sinT
				* (-a11_ * gx_[i] - a31_ * gxxx_[i] - (3.0 * a33_ - 2.0 * a31_)
						* gxyy_[i] + a51_ * (4.0 * gxxxyy_[i] - gxxxxx_[i]) + a53_
						* (2.0 * gxyyyy_[i] - 3.0 * gxxxyy_[i]))
				+ cosT3
				* sinT2
				* (2.0 * a11_ * gy_[i] + (3.0 * a33_ - a31_) * gxxy_[i]
						+ (a33_ + a31_) * gyyy_[i] + a51_
						* (6.0 * gxxyyy_[i] - 4.0 * gxxxxy_[i]) + a53_
						* (gyyyyy_[i] - 6.0 * gxxyyy_[i] + 3.0 * gxxxxy_[i]))
				+ cosT2
				* sinT3
				* (-2.0 * a11_ * gx_[i] - (3.0 * a33_ - a31_) * gxyy_[i]
						- (a33_ + a31_) * gxxx_[i] - a51_
						* (6.0 * gxxxyy_[i] - 4.0 * gxyyyy_[i]) - a53_
						* (gxxxxx_[i] - 6.0 * gxxxyy_[i] + 3.0 * gxyyyy_[i]))
				+ cosT
				* sinT2
				* sinT2
				* (a11_ * gy_[i] + a31_ * gyyy_[i] + (3.0 * a33_ - 2.0 * a31_)
						* gxxy_[i] - a51_ * (4.0 * gxxyyy_[i] - gyyyyy_[i]) - a53_
						* (2.0 * gxxxxy_[i] - 3.0 * gxxyyy_[i]))
				- sinT2
				* sinT3
				* (a11_ * gx_[i] + a31_ * gxyy_[i] + a33_ * gxxx_[i] + a51_
						* gxyyyy_[i] + a53_ * gxxxyy_[i]);
		return result;
	}

	// ============================================================================
	// GETTERS

	/** Re */
	public boolean getStop() {
		return stop_;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Writes the image containing the response of the steerable filter.
	 */
	public <T extends RealType<T>> void getResponse(RandomAccess<T> access) {
		Image2ArrayConverter.doubleArrToSeq(response_, nx_, ny_, access);
	}

	// ----------------------------------------------------------------------------

	/** Writes the image containing the point-wise estimated orientation. */
	public <T extends RealType<T>> void getOrientation(RandomAccess<T> access) {
		Image2ArrayConverter.doubleArrToSeq(orientation_, nx_, ny_, access);
	}

	// ----------------------------------------------------------------------------

	/** Returns the order of the steerable filter. */
	public String getOrder() {

		switch (M_) {
		case 1:
			return "1st";
		case 2:
			return "2nd";
		case 3:
			return "3rd";
		case 4:
		case 5:
			return String.valueOf(M_) + "th";
		default:
			return "";
		}
	}

	
}
