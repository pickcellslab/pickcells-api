package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2019 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.view.Lut16;

import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.view.Views;

public class SteerableFilter<T extends RealType<T>> implements ImgProcessing<T,FloatType>{


	/** Stores all parameters used for the steerable templates. */
	private ParameterSet[][] pSet ;

	/**
	 * Array of weights that control the linear combination of Gaussian
	 * derivatives which constitute the feature template.
	 */
	private AlphaValues alphaValues ;

	/** Order of the feature template. */
	private int m = 1;

	/**
	 * Standard deviation of the Gaussian on which the steerable templates are
	 * based.
	 */
	private double sigma = 1.0;



	private SteerableDialog dialog;

	private PreviewBag<T> input;

	private final ImgIO io;
	
	private final NotificationFactory notif;




	public SteerableFilter(NotificationFactory notif, ImgIO io) {
		Objects.requireNonNull(notif);
		Objects.requireNonNull(io);
		this.notif = notif;
		this.io = io;
		alphaValues = new AlphaValues();
		pSet = ParameterSet.loadParameterSets(this.getClass()
				.getResourceAsStream("/parameters.xml"));
		alphaValues.setDefault(m, 1, pSet);
	}







	@Override
	public String name() {
		return "Steerable Filter";
	}





	@Override
	public String description() {
		return "Edge and Ridge Detection based on \n"
				+ "Jacob et al. Design of Steerable Filters for Feature Detection Using Canny-Like Criteria, IEEE Trans. Pattern Anal. Mach. Intell.";
	}

	@Override
	public boolean repeatable() {
		return false;
	}

	@Override
	public DimensionalityEffect dimensionalityEffect() {
		return DimensionalityEffect.FLAT_TO_FLAT;
	}




	@Override
	public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<T> input) {
		
		this.input = input;

		if(dialog== null){
			dialog = new SteerableDialog(notif);
		}
		
		state.setLut(state.getPreviewIndex(), Lut16.Grays.name());

		return dialog;
		
		
	}





	@Override
	public RandomAccessibleInterval<FloatType> validate() {
		
		
		sigma = dialog.getScale();
		int detectorType = dialog.getDetectorType();
		int  quality_ = dialog.getQuality();
		detectorType = 2 - detectorType;
		m = quality_ * 2 - detectorType % 2;			
		alphaValues.setDefault(m, m % 2 == 1 ? 1 : 2, pSet);
		double[] alpha = alphaValues.getAlpha(1, sigma, m);

		//System.out.println("SteerableFilterPipe: alpha = "+Arrays.toString(alpha));
		
		RandomAccessibleInterval<FloatType> img = ImgDimensions.hardCopy(io, input.getInput(), new FloatType());
		
		MinimalImageInfo info = input.getInputInfo();
		
		if(info.dimension(Image.t) > 1){
			for(int t = 0; t<info.dimension(Image.t); t++)
				executeForEachZSlice(Views.hyperSlice(img, info.index(Image.t), t), info.index(Image.z), sigma, m, alpha);
		}
		else
			executeForEachZSlice(img, info.index(Image.z), sigma, m, alpha);
		

		return img;

	}



	private static void executeForEachZSlice(RandomAccessibleInterval<FloatType> inputNoT, int zDim, double sigma, int m, double[] alpha){


		if(zDim >= 0){

			ExecutorService serv = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
			List<Future<Void>> threads = new ArrayList<>();



			for(int z = 0; z<inputNoT.dimension(zDim); z++){

				final int s = z;
				final Callable<Void> sliceComputation = ()->{

					RandomAccessibleInterval<FloatType> sSlice = Views.hyperSlice(inputNoT, zDim, s);
					run(sSlice, sigma, m, alpha);

					return null;
				};

				threads.add(serv.submit(sliceComputation));

			}

			threads.forEach(c->{
				try{
					c.get();
				}
				catch(Exception e){
					e.printStackTrace(); //TODO log
				}
			});

		}
		else
			run(inputNoT, sigma, m, alpha);		


	}




	@SuppressWarnings("unchecked")
	@Override
	public Function<ImageBag<T>, RandomAccessibleInterval<FloatType>> toPipe() {


		return (bag)->{

			// Create a copy only if not already a FloatType
			final T type = bag.getCurrentInputType();
			final RandomAccessibleInterval<FloatType> copy = 
					type instanceof FloatType ? 
							(RandomAccessibleInterval<FloatType>) bag.getInputFrame(0)
							:	ImgDimensions.hardCopy(io, bag.getInputFrame(0), new FloatType());

			final double[] alpha = alphaValues.getAlpha(1, sigma, m);
			
			final MinimalImageInfo info = bag.getInputFrameInfo();
			executeForEachZSlice(copy, info.index(Image.z), sigma, m, alpha);
						
			return copy;
			
		};

		
	}





	private static void run(RandomAccessibleInterval<FloatType> source, double sigma, int m, double[] alpha) {

		double[] input = Image2ArrayConverter.seqToDoubleArray(source);
		int nx = (int) source.dimension(0);
		int ny = (int) source.dimension(1);


		SteerableDetector detector = new SteerableDetector(input, nx, ny, sigma, m, alpha);

		detector.run();

		RandomAccess<FloatType> access = source.randomAccess();

		detector.getResponse(access);

	}







	@Override
	public boolean requiresAnnotationChannel() {
		return false;
	}




	


	@Override
	public void changeInput(PreviewBag<T> current) {
		this.input = current;
	}






	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}




	@Override
	public boolean requiresOriginal() {
		return false;
	}



	@Override
	public boolean requiresFlat() {
		return false;
	}







	@Override
	public FloatType outputType(T inputType) {
		return new FloatType();
	}














}



