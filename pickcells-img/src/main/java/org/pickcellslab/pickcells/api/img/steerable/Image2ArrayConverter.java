/*******************************************************************************
 * Copyright (c) 2012-2013 Biomedical Image Group (BIG), EPFL, Switzerland.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 *     Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 ******************************************************************************/
package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2019 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.imglib2.Cursor;
import net.imglib2.Dimensions;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessible;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.util.Intervals;
import net.imglib2.util.Util;
import net.imglib2.view.StackView.StackAccessMode;
import net.imglib2.view.Views;

/**
 * Utility class to convert arrays into {@link RandomAccessibleInterval}. 
 * <br><br>
 * This class was modified from a plugin originally implemented by François Aguet for ImageJ, and ported to Icy by Ricard Delgado-Gonzalo (ricard.delgado@gmail.com) 
 * and Zsuzsanna Püspöki (zsuzsanna.puspoki@epfl.ch).
 * 
 * @author Guillaume Blin
 */
class Image2ArrayConverter {



	/**
	 * Writes the content of a given double array to a 2D {@link RandomAccessible}
	 * @param array the double array which content needs to be written into the {@link RandomAccessible}. It is the caller's responsibility 
	 * to ensure that dimensions match.
	 * @param nx width of the {@link RandomAccessible} 
	 * @param ny height of the {@link RandomAccessible}
	 * @param access A {@link RandomAccess} on the 2D RandomAccessible the double array should be written
	 */
	static <T extends RealType<T>> void doubleArrToSeq(double[] array, int nx, int ny, RandomAccess<T> access) {
		
		long[] pos = new long[2];
		for (pos[1] = 0; pos[1] < ny; pos[1]++) {
			for (pos[0] = 0;  pos[0]< nx; pos[0]++) {
				access.setPosition(pos);
				double value = array[(int) (pos[0] + nx * pos[1])];
				access.get().setReal(value);
			}
		}
		
	}



	/** Transforms a {@link RandomAccessibleInterval} object to a rasterized <code>double</code> array. */
	static <T extends RealType<T>> double[] seqToDoubleArray(RandomAccessibleInterval<T> sequence) {

		int sizeX = (int) sequence.dimension(0);
		int sizeY = (int) sequence.dimension(1);
		RandomAccess<T> access = sequence.randomAccess();

		double[] pixelArray = new double[sizeX * sizeY];

		long[] pos = new long[2];
		for (pos[1] = 0; pos[1] < sizeY; pos[1]++) {
			for (pos[0] = 0;  pos[0]< sizeX; pos[0]++) {
				access.setPosition(pos);
				pixelArray[(int) (pos[0] + sizeX * pos[1]) ] = access.get().getRealDouble();
			}
		}

		return pixelArray;
	}




	/**
	 * Converts RGB arrays to a multi-channel {@link RandomAccessibleInterval}.
	 * @param R red channel
	 * @param G green channel
	 * @param B blue channel
	 * @param nx image width
	 * @param ny image height
	 * @param nz image depth
	 * @return The RGB image as a {@link RandomAccessibleInterval}
	 */
	@SuppressWarnings("unchecked")
	static RandomAccessibleInterval<UnsignedByteType> arraysToRGBSeq(int[] R, int[] G, int[] B, int nx,
			int ny, int nz) {

		Dimensions dims = null;
		if(nz == 1)		
			dims = Intervals.createMinMax(0,0,0,nx,ny,nz);
		else
			dims = Intervals.createMinMax(0,0,nx,ny);

		UnsignedByteType type = new UnsignedByteType();
		Img<UnsignedByteType> r = Util.getArrayOrCellImgFactory(dims, type).create(dims, type);
		Cursor<UnsignedByteType> rAccess = r.cursor();

		Img<UnsignedByteType> g = Util.getArrayOrCellImgFactory(dims, type).create(dims, type);
		Cursor<UnsignedByteType> gAccess = g.cursor();

		Img<UnsignedByteType> b = Util.getArrayOrCellImgFactory(dims, type).create(dims, type);
		Cursor<UnsignedByteType> bAccess = b.cursor();


		while(rAccess.hasNext()){
			UnsignedByteType rT = rAccess.next();	rT.set(R[rT.getIndex()]);
			UnsignedByteType gT = gAccess.next();	rT.set(R[gT.getIndex()]);
			UnsignedByteType bT = bAccess.next();	rT.set(R[bT.getIndex()]);
		}

		return Views.stack(StackAccessMode.DEFAULT, r,g,b);
	}





}
