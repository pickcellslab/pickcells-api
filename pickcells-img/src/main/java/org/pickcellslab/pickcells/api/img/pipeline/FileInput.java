package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.RandomAccessibleInterval;

class FileInput<T> implements ProcessingInput<T> {
	
	private final RandomAccessibleInterval<T> img;
	private final MinimalImageInfo info;
	private final File folder;
	private final String name;
	
	public FileInput(RandomAccessibleInterval<T> img, MinimalImageInfo info, String name, File folder) {
		this.img = img;
		this.info = info;
		this.name = name;
		this.folder = folder;
	}
	
	public RandomAccessibleInterval<T> getImg() {
		return img;
	}

	public MinimalImageInfo getInfo() {
		return info;
	}

	public String getName() {
		return name;
	}

	public File getFolder() {
		return folder;
	}
	
}
