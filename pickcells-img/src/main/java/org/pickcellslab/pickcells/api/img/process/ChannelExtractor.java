package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Function;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;
import org.pickcellslab.pickcells.api.img.view.Lut16;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public class ChannelExtractor<T extends RealType<T>> implements ImgProcessing<T, T> {

	private String[] channels;
	private ChannelDialog dialog;
	private PreviewBag<T> input;
	private ImageViewState<?> state;




	@Override
	public String name() {
		return "Channel Choice";
	}


	@Override
	public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state, PreviewBag<T> input) {
		this.input = input;
		this.state = state;
		if(dialog == null){
			channels = state.channelNames();
			dialog = new ChannelDialog();
		}
		return dialog;
	}


	@Override
	public void changeInput(PreviewBag<T> current) {
		this.input = current;			
	}



	@Override
	public RandomAccessibleInterval<T> validate() {

		RandomAccessibleInterval<T> result = null;
		if(input.getInputInfo().dimension(Image.c) == 1)
			result = input.getInput();
		else
			result = Views.hyperSlice(input.getInput(), input.getInputInfo().index(Image.c), dialog.getSelected());	

		state.setLut(state.getPreviewIndex(), Lut16.Grays.name());

		return result;
	}




	@Override
	public String description() {
		return "Extract one channel from a multi channel image";
	}

	@Override
	public boolean repeatable() {
		return false;
	}

	@Override
	public DimensionalityEffect dimensionalityEffect() {
		return DimensionalityEffect.CHANNELS_TO_FLAT;
	}



	@Override
	public Function<ImageBag<T>, RandomAccessibleInterval<T>> toPipe() {

		final int ch = dialog.getSelected();
		final boolean isMultiChannel = channels.length > 1;

		return (bag) ->{
			if(isMultiChannel){
				int cIndex = bag.getInputFrameInfo().index(Image.c);
				//System.out.println("ChannelExtractor : cIndex = "+cIndex);
				return Views.hyperSlice(bag.getInputFrame(0), cIndex, ch);
			}
			else
				return bag.getInputFrame(0);
		};


	}


	@SuppressWarnings("serial")
	private class ChannelDialog extends ValidityTogglePanel{

		private final JComboBox<String> combo;

		public ChannelDialog() {
			add(new JLabel(" Channel Choice "));
			combo = new JComboBox<>(channels);
			add(combo);
		}

		@Override
		public boolean validity() {
			return true;
		}

		public int getSelected(){
			return combo.getSelectedIndex();
		}
	}


	@Override
	public boolean requiresAnnotationChannel() {
		return false;
	}


	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}


	@Override
	public boolean requiresOriginal() {
		return false;
	}


	@Override
	public boolean requiresFlat() {
		return false;
	}


	@Override
	public boolean condition(MinimalImageInfo info){
		return info.dimension(Image.c)>1;
	}



	@Override
	public T outputType(T inputType) {
		return inputType;
	}


}
