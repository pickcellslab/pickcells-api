package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventListener;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;

import com.alee.extended.panel.GroupPanel;
import com.alee.extended.panel.GroupingType;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.tooltip.TooltipManager;
import com.alee.managers.tooltip.WebCustomTooltip;

import net.imagej.display.SourceOptimizedCompositeXYProjector;
import net.imglib2.IterableInterval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.converter.Converter;
import net.imglib2.converter.RealLUTConverter;
import net.imglib2.converter.TypeIdentity;
import net.imglib2.display.ColorTable;
import net.imglib2.display.projector.AbstractProjector2D;
import net.imglib2.display.projector.RandomAccessibleProjector2D;
import net.imglib2.display.projector.composite.CompositeXYProjector;
import net.imglib2.display.screenimage.awt.ARGBScreenImage;
import net.imglib2.realtransform.AffineTransform2D;
import net.imglib2.type.numeric.ARGBType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.ui.AffineTransformType2D;
import net.imglib2.ui.InteractiveDisplayCanvasComponent;
import net.imglib2.ui.PainterThread;
import net.imglib2.ui.PainterThread.Paintable;
import net.imglib2.ui.Renderer;
import net.imglib2.ui.TransformListener;
import net.imglib2.ui.overlay.BufferedImageOverlayRenderer;
import net.imglib2.ui.util.Defaults;
import net.imglib2.ui.util.GuiUtil;
import net.imglib2.ui.util.InterpolatingSource;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

@SuppressWarnings("serial")
public class ImageViewer<T extends RealType<T>>  extends JPanel implements Paintable, TransformListener<AffineTransform2D>{

	private InteractiveDisplayCanvasComponent< AffineTransform2D > canvas;
	private Renderer<AffineTransform2D> imageRenderer;
	private AffineTransform2D viewerTransform;
	private AffineTransformType2D transformType;

	//Create a long[] to adjust the position to render in the image
	private final long[] pos;
	private final JSlider sliderZ, sliderT;

	private InterpolatingSource<ARGBType, AffineTransform2D> interpolatingSource;

	private AbstractProjector2D projector;
	private ARGBScreenImage screenImage;

	private final ArrayList< Converter< T, ARGBType >> converters;

	private boolean isMultiChannel = false;  

	private RandomAccessibleInterval<T> img; 

	private final int[] order;


	// Image Interactions
	private final MouseListener voxelTooltips;
	private final Function<Double,Number>[] conversions;


	// Display Listener
	private final List<DisplayPositionListener> dpl = new ArrayList<>(1);
	private MinimalImageInfo info;



	/**
	 * @param interval The Interval to be displayed
	 * @param tables The Lookup tables to render each channel
	 * @throws IllegalArgumentException if the length of tables is smaller than the number of channels
	 */
	@SuppressWarnings("unchecked")
	public ImageViewer(final RandomAccessibleInterval<T> img, ColorTable[] tables, int[] ordering)
			throws IllegalArgumentException{

		//null checks
		Objects.requireNonNull(img, "img is null");
		Objects.requireNonNull(tables, "tables is null");
		Objects.requireNonNull(ordering, "order is null");
		
		// Check the coherence of luts, ordering and dimensions
		assert img.numDimensions() == Image.numDimensions(ordering) : "Inconsistent number of dimensions between img and ordering";
		assert ordering[Image.c] == -1 ? tables.length == 1 : tables.length == img.dimension(ordering[Image.c]) : "Check the number of channels/Lut";
		

	//	System.out.println("Order received by ImageViewer : "+Arrays.toString(ordering));
		long[] dims = new long[img.numDimensions()]; img.dimensions(dims);
		//System.out.println("Dims of the image received by ImageViewer : "+Arrays.toString(dims));

		// TODO info should be passed in constructor
		info = new MinimalImageInfo(ordering, dims);

	
		this.img = img;
		this.order = Arrays.copyOf(ordering, ordering.length);



		this.pos = new long[img.numDimensions()];

		//Setup environment
		this.setDoubleBuffered(true);

		//ScreenImage
		screenImage = new ARGBScreenImage((int)img.dimension(0), (int)img.dimension(1));

		//Transform the image into a a series of 2d plane if there are more than one channel
		//Converters for each channels
		converters = new ArrayList< Converter< T, ARGBType > >();      
		if(tables.length>1){
			isMultiChannel = true;

			try{
				for(int c = 0; c<img.dimension(order[Image.c]); c++){
					converters.add(new RealLUTConverter<T>(0, 1,tables[c]));
				}
			}catch(ArrayIndexOutOfBoundsException e){
				throw new IllegalArgumentException("The provided LUT array is to small");
			}

			//Building a projector which selects the XY planes to render

			if(IterableInterval.class.isAssignableFrom(img.getClass()))
				projector = new SourceOptimizedCompositeXYProjector<>((IterableInterval<T> & RandomAccessibleInterval<T>)img, screenImage, converters, order[Image.c]);	
			else
				projector = new CompositeXYProjector<>(img, screenImage, converters, order[Image.c]);			


			//System.out.println("Projector dimensions : "+projector.numDimensions());

			//To display all channels
			projector.map();
		}
		else{
			converters.add(new RealLUTConverter<T>(0, 1,tables[0]));
			projector = new RandomAccessibleProjector2D<T,ARGBType>(0, 1, img,
					screenImage, converters.get(0));
			projector.map();
		}

		//Canvas with the default transform handler
		canvas = new InteractiveDisplayCanvasComponent< AffineTransform2D >( 500, 500, DynamicTransformEventHandler2D.factory());

		transformType =  AffineTransformType2D.instance;
		viewerTransform = transformType.createTransform();    


		//Setup the RenderSource (wraps img with transform with converter of type T to ARGB)
		interpolatingSource = 
				new InterpolatingSource<ARGBType,AffineTransform2D>(
						Views.extendValue(screenImage,new ARGBType()), new AffineTransform2D(), new TypeIdentity<ARGBType>() );



		//Thread handler
		PainterThread painterThread = new PainterThread(this);

		//Setup the renderer
		final BufferedImageOverlayRenderer target = new BufferedImageOverlayRenderer();
		imageRenderer =         
				Defaults.rendererFactory( 
						AffineTransformType2D.instance, interpolatingSource )
				.create(target, painterThread);

		canvas.addOverlayRenderer( target );

		painterThread.start();




		// ---------------------------------------------------------------------------------------------------------------------------------
		// Setting up listeners

		canvas.addTransformListener( this );

		// Tooltips displaying voxels location and values

		voxelTooltips = new MouseAdapter(){

			WebCustomTooltip tt;

			@Override
			public void mouseClicked(MouseEvent e){

				if(SwingUtilities.isLeftMouseButton(e)){

					if(tt!=null)
						tt.closeTooltip();

					tt = TooltipManager.showOneTimeTooltip(
							ImageViewer.this,
							ImageViewer.this.getToolTipLocation(e), 
							ImageViewer.this.getToolTipText(e),
							TooltipWay.up);
					
				}

			}

			@Override
			public void mouseMoved(MouseEvent e) {
				if(tt!=null)
					tt.closeTooltip();
			}

			@Override
			public void mouseExited(MouseEvent e) {
				if(tt!=null)
					tt.closeTooltip();
			}

		};				

		this.addViewListener(voxelTooltips);







		// Key Binding to unzoom and recenter the image
		canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "Recenter");
		canvas.getActionMap().put("Recenter", new AbstractAction(){
			@Override
			public void actionPerformed(ActionEvent e) {			
				((DynamicTransformEventHandler2D) canvas.getTransformEventHandler()).centerImage(img.dimension(0), img.dimension(1));
			}			
		});








		// Create a slider for the z orientation
		//-----------------------------------------------------------------------------------------------------------

		if(order[Image.z]>0){

			//Slider for adjusting the plane
			sliderZ = new JSlider();
			sliderZ.setMinimum(0);
			sliderZ.setMaximum((int) img.dimension(order[Image.z])-1);

			sliderZ.addChangeListener(l->{
				SwingUtilities.invokeLater(()->{
					pos[order[Image.z]] = sliderZ.getValue();
					TooltipManager.setTooltip(sliderZ, "Current Z Slice = "+ sliderZ.getValue());
					projector.setPosition(pos);
					//new ArrayList<>(dpl).forEach(dl->dl.positionHasChanged(this, DisplayPosition.SLICE));
					refresh();
				});
			});


			// Key Binding to navigate slices and time frames
			canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("UP"), "UP");
			canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("DOWN"), "DOWN");

			canvas.getActionMap().put("UP", new AbstractAction(){
				@Override
				public void actionPerformed(ActionEvent e) {
					sliderZ.setValue(sliderZ.getValue()+1);
				}			
			});

			canvas.getActionMap().put("DOWN", new AbstractAction(){
				@Override
				public void actionPerformed(ActionEvent e) {
					sliderZ.setValue(sliderZ.getValue()-1);
				}			
			});

		}
		else sliderZ = null;







		// Create a slider for the z orientation
		//-----------------------------------------------------------------------------------------------------------

		if(order[Image.t]>0){

			//Slider for adjusting the plane
			sliderT = new JSlider();
			sliderT.setMinimum(0);
			sliderT.setMaximum((int) img.dimension(order[Image.t])-1);


			sliderT.addChangeListener(l->{
				SwingUtilities.invokeLater(()->{
					pos[order[Image.t]] = sliderT.getValue();
					TooltipManager.setTooltip(sliderT, "Current Frame = "+ sliderT.getValue());
					projector.setPosition(pos);
					//new ArrayList<>(dpl).forEach(dl->dl.positionHasChanged(this, DisplayPosition.FRAME));
					refresh();
				});
			});


			// Key Binding to navigate slices and time frames
			canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("RIGHT"), "RIGHT");
			canvas.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("LEFT"), "LEFT");

			canvas.getActionMap().put("RIGHT", new AbstractAction(){
				@Override
				public void actionPerformed(ActionEvent e) {
					sliderT.setValue(sliderT.getValue()+1);
				}			
			});

			canvas.getActionMap().put("LEFT", new AbstractAction(){
				@Override
				public void actionPerformed(ActionEvent e) {
					sliderT.setValue(sliderT.getValue()-1);
				}			
			});

		}
		else sliderT = null;




		// Create tooltip conversion functions
		conversions = new Function[converters.size()];
		final Function<Double,Number> identity = t->t;
		for(int i = 0; i<converters.size(); i++)
			conversions[i] = identity;






		// ------------------------------------------------------------------------------------------------------------------------------------
		//Adjust contrast

		for(int i = 0; i<converters.size(); i++){			
			this.adjustContrast(i);
			this.setVisible(i, true);	
		}



		// Center and map
		((DynamicTransformEventHandler2D) canvas.getTransformEventHandler()).centerImage(img.dimension(0), img.dimension(1));
		if(sliderZ!=null)
			sliderZ.setValue((sliderZ.getMaximum()-sliderZ.getMinimum())/2);
		if(sliderT!=null)
			sliderT.setValue((sliderT.getMaximum()-sliderT.getMinimum())/2);


		//Layout components

		setLayout(new BorderLayout(0, 0));

		add(canvas, BorderLayout.CENTER);

		if(sliderZ!=null || sliderT!=null){

			GroupPanel slidersPanel = new GroupPanel(false);
			add(slidersPanel, BorderLayout.SOUTH);


			if(sliderZ != null){

				JLabel lblZ = new JLabel("  Z  ");
				JLabel lblNewLabel = new JLabel("     ");
				GroupPanel zPanel = new GroupPanel(GroupingType.fillMiddle, lblZ, sliderZ, lblNewLabel);
				slidersPanel.add(zPanel);


			}

			if(sliderT != null){

				JLabel lblT = new JLabel("  T  ");
				JLabel lblNewLabel = new JLabel("     ");
				GroupPanel tPanel = new GroupPanel(GroupingType.fillMiddle, lblT, sliderT, lblNewLabel);
				slidersPanel.add(tPanel);

			}

		}

	}



	public void setVoxelMeasureConverter(Function<Double,Number> f, int channel){
		if(channel<conversions.length && f != null)
			conversions[channel] = f;
	}

	public Function<Double, Number> getVoxelMeasureConverter(int channel){
		return conversions[channel];
	}



	public void setVoxelTooltipsEnabled(boolean enabled){
		canvas.removeHandler(voxelTooltips);
		if(enabled)
			canvas.addHandler(voxelTooltips);
	}



	public void setRotationEnabled(boolean enabled){
		((DynamicTransformEventHandler2D)canvas.getTransformEventHandler()).setAllowRotation(enabled);
	}






	//Internal display events methods
	//--------------------------------------------------------------------------------------------------------------------

	@Override
	public void paint(){		
		if(imageRenderer!=null)
			imageRenderer.paint( viewerTransform );
		canvas.repaint();
	}



	@Override
	public void transformChanged(AffineTransform2D transform) {
		transformType.set( viewerTransform, transform );
		imageRenderer.requestRepaint();    
	}








	//Listener registration
	//--------------------------------------------------------------------------------------------------------------------


	/**
	 * Registers an {@link EventListener} on the canvas displaying the image
	 * @param lst The EventListener to register
	 */
	public void addViewListener(EventListener lst){
		canvas.addHandler(lst);
	}



	/**
	 * Regmove the {@link EventListener} from the canvas displaying the image
	 * @param lst The EventListener to remove
	 */
	public void removeViewListener(EventListener lst){
		canvas.removeHandler(lst);
	}



	public void addTransformListener(TransformListener<AffineTransform2D> l){
		canvas.addTransformListener(l);
	}


	public void removeTransformListener(TransformListener<AffineTransform2D> l){
		canvas.removeTransformListener(l);
	}


	public AffineTransform2D getCurrentTransform() {
		return ((DynamicTransformEventHandler2D)canvas.getTransformEventHandler()).getTransform();
	}



	public void addDisplayPositionListener(DisplayPositionListener l){
		if(l!=null)
			this.dpl.add(l);
	}

	public void removeDisplayPositionListener(DisplayPositionListener l){
		this.dpl.remove(l);
	}




	// Access to the display
	//-------------------------------------------------------------------------------------------------------------------


	/**
	 * @return The JComponent displaying the image
	 */
	public JComponent getImageDisplay(){
		return canvas;
	}






	//Information access
	//---------------------------------------------------------------------------------------------------------------------


	public int getCurrentSlice(){
		if(sliderZ == null)
			return 0;
		return sliderZ.getValue();
	}


	public int getCurrentFrame() {
		if(sliderT == null)
			return 0;
		return sliderT.getValue();
	}





	/**
	 * Transforms the provided on screen coordinates into image coordinates. If the image has more than 2 dimensions, 
	 * the currently displayed dimensions are written into the returned array.
	 * @param x The x coordinate in the on screen coordinate system
	 * @param y The y coordinate in the on screen coordinate system
	 * @return The image coordinates corresponding to the provided components coordinates
	 */
	public long[] getImgCoordinates(int x, int y){
		long[] c = new long[img.numDimensions()];
		projector.localize(c);
		float[] rec = new float[2];
		viewerTransform.applyInverse(rec, new float[]{x,y});
		c[0] = (long) rec[0];
		c[1] = (long) rec[1];		
		return c;
	}



	/**
	 * @return Writes to the provided long[] the image coordinates fo the upper left and 
	 * bottom right corner of the area currently visible in the view. 
	 */
	public void getDisplayedInterval(long[] min, long[] max) {
		//get upper left corner
		float[] rec = new float[2];
		viewerTransform.applyInverse(rec, new float[]{0,0});



		min[0] = (long) Math.max(rec[0],0);
		min[1] = (long) Math.max(rec[1],0);
		//get lower right corner
		viewerTransform.applyInverse(rec, new float[]{this.getSize().width - 1, this.getSize().height - 1});



		max[0] = (long) Math.min(rec[0],img.dimension(0)-1);
		max[1] = (long) Math.min(rec[1],img.dimension(1)-1);;

		//System.out.println("Min = " + Arrays.toString(min));
		//System.out.println("Max = " + Arrays.toString(max));

	}




	public double getGlobalMinIntensity(int i){
		//RealLUTConverter<T> channel = (RealLUTConverter<T>) converters.get(i);
		return 0;
	}

	public double getGlobalMaxIntensity(int i){
		RealLUTConverter<T> channel = (RealLUTConverter<T>) converters.get(i);
		return channel.getLUT().getLength();
	}


	public double getCurrentMinIntensity(int i){
		RealLUTConverter<T> channel = (RealLUTConverter<T>) converters.get(i);
		return channel.getMin();
	}

	public double getCurrentMaxIntensity(int i){
		RealLUTConverter<T> channel = (RealLUTConverter<T>) converters.get(i);
		return channel.getMax();
	}


	@SuppressWarnings("unchecked")
	public boolean isVisible(int channel){
		if(isMultiChannel)
			return ((CompositeXYProjector<T>) projector).isComposite(channel);			
		else
			return true;
	}


	public int numChannels(){
		return (int) img.dimension(order[Image.c]);
	}

	public long[] imageDimensions(){
		long[] receiver = new long[img.numDimensions()];
		img.dimensions(receiver);
		return receiver;
	}

	public IntervalView<T> access(int channel){
		return Views.hyperSlice(img, order[Image.c], channel);
	}

	/**
	 * @param from
	 * @param to
	 * @return An {@link IntervalView} of the displayed image from channel "from" to channel "to"
	 */
	public IntervalView<T> access(int from, int to){
		long[] min = new long[img.numDimensions()];
		long[] max = new long[img.numDimensions()];
		img.max(max);
		min[order[Image.c]] = from; max[order[Image.c]] = to;
		return Views.interval(img, min, max);
	}


	public T getType(){
		return Views.iterable(img).firstElement().createVariable();
	}


	//Controller methods
	//-------------------------------------------------------------------------------------------------------------------


	/**
	 * Sets the visibility of a specific channel
	 * @param channel The coordinate of the channel the visibility needs to be set 
	 * @param b determine the visibility of the channel
	 */
	@SuppressWarnings("unchecked")
	public void setVisible(int channel, boolean b){
		if(isMultiChannel){
			((CompositeXYProjector<T>) projector).setComposite(channel, b);
			projector.map();
			imageRenderer.requestRepaint();
		}
	}




	/**
	 * Call this method if you want to automatically auto adjust the contrast
	 * @param i The coordinate of the channel to adjust
	 */
	public void adjustContrast(int i) {	

		RandomAccessibleInterval<T> interval = null;

		int[] reorder = Arrays.copyOf(order, order.length);
		
		System.out.println("Reorder before C check "+Arrays.toString(reorder));
		
		// Cut along the desired channel
		if(isMultiChannel){
			System.out.println("Is Multichannel!");
			interval = Views.hyperSlice(img, order[Image.c], i);
			reorder = Image.removeDimension(order, Image.c);
		}
		else
			interval = img;


		// Check if it is large in X and Y
		if(img.dimension(0) > 1024 )
			interval = ImgDimensions.createCenteredInterval(interval, 0, 512);
		if(img.dimension(1) > 1024 )
			interval = ImgDimensions.createCenteredInterval(interval, 1, 512);

		System.out.println("Reorder before Z check "+Arrays.toString(reorder));
		
		// Check if it is large in the Z dimension
		if(reorder[Image.z]!=-1 && interval.dimension(reorder[Image.z]) > 20){
			System.out.println("Has Z!");
			interval = Views.hyperSlice(interval, reorder[Image.z], sliderZ.getValue());
			reorder = Image.removeDimension(reorder, Image.z);
		}

		//System.out.println("Reorder before t check "+Arrays.toString(reorder));
		
		// Get the first frame if it is time lapse
		if(reorder[Image.t]!=-1)
			interval = Views.hyperSlice(interval, reorder[Image.t], sliderT.getValue());


		this.adjustContrast(i,interval);
	}




	/**
	 * Call this method if you want to automatically adjust the contrast
	 * @param i The coordinate of the channel to adjust
	 */
	private void adjustContrast(int i, RandomAccessibleInterval<T> interval) {		

		IterableInterval<T> itr = Views.iterable(img);
		T min = itr.firstElement().createVariable();
		T max = min.createVariable();			

		System.out.println("Adjusting contrast... ");
		//	if(isMultiChannel)
		//		ImgDimensions.computeMinMax(Views.interval(Views.hyperSlice(img, order[Image.c], i), interval), min, max);
		//	else
		ImgDimensions.computeMinMax(Views.iterable(interval), min, max);


		System.out.println("Min Max done... ");

		RealLUTConverter<T> channel = (RealLUTConverter<T>) converters.get(i);
		channel.setMin(min.getRealDouble());
		channel.setMax(max.getRealDouble());		
		if(min.equals(max)){
			if(max.getRealDouble() == 65535)
				channel.setMin(65534);
			else if(min.getRealDouble() == 0)
				channel.setMax(1);

		}		
		projector.map();
		imageRenderer.requestRepaint(); 
	}




	public void setPosition(long[] pos){		
		if(order[Image.z]!=-1)
			sliderZ.setValue((int) pos[order[Image.z]]);
		if(order[Image.t]!=-1)
			sliderT.setValue((int)pos[order[Image.t]]);
		else{
			projector.setPosition(pos);
			projector.map();
		}
		imageRenderer.requestRepaint();
	}


	/**
	 * @return A long[] holding the current position of the display in each dimension.
	 * NB: The number of dimension is the number of dimension of the image.
	 */
	public long[] getCurrentPosition(){
		long[] pos = new long[projector.numDimensions()];
		projector.localize(pos);
		return pos;
	}



	public void refresh() {
		projector.map();
		imageRenderer.requestRepaint();
	}


	public void setLut(int channel, ColorTable table){
		((RealLUTConverter<T>) converters.get(channel)).setLUT(table);
		refresh();
	}	



	void setContrast(int i, float minimum, float maximum) {

		RealLUTConverter<T> channel = (RealLUTConverter<T>) converters.get(i);
		channel.setMin(minimum);
		channel.setMax(maximum);
		projector.map();
		imageRenderer.requestRepaint();

		//TODO notify listeners
	}


	public void revalidateChannels(){
		projector.map();
		imageRenderer.requestRepaint(); 
	}


	//Convenience methods to create controllers
	//------------------------------------------------------------------------------------------------------------------------


	public JPanel createChannelController(String[] names){
		return new ChannelControl_old(this,names);
	}




	/**
	 * A convenience method to display an ImageViewer with its channels controller in a new window
	 * @param title The title of the JFrame
	 * @param channels The names of the channels
	 * @return The JFrame displaying the image
	 */
	public JFrame displayWithChannelsController(String title, String[] channels){
		final GraphicsConfiguration gc = GuiUtil.getSuitableGraphicsConfiguration( GuiUtil.RGB_COLOR_MODEL );
		JFrame frame = new JFrame(title,gc);
		frame.getRootPane().setDoubleBuffered( true );		
		JPanel panel = createPanelWithChannelController(channels);
		frame.setContentPane(panel);
		frame.pack();
		frame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		frame.setVisible(true);
		revalidateChannels();
		return frame;
	}


	public JPanel createPanelWithChannelController(String[] channels){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.PAGE_AXIS));
		panel.add(this);
		panel.add(this.createChannelController(channels));
		return panel;		
	}



	
	public MinimalImageInfo getMinimalInfo(){
		return info;
	}


	@Deprecated
	public int[] order() {
		return Arrays.copyOf(order, order.length);
	}





	@Override
	public String getToolTipText(MouseEvent event) {
		Point p = new Point(event.getX(), event.getY());
		System.out.println("Tooltip required");
		if(!canvas.contains(p))        
			return super.getToolTipText(event);
		else{

			RandomAccess<T> access = img.randomAccess();
			long[] l = this.getImgCoordinates(event.getX(), event.getY());

			String tt = "<HTML> X : "+l[0]+" ; Y : "+l[1];
			if(order[Image.z]>-1)
				tt+= " ; Z "+l[order[Image.z]]; 
			if(order[Image.t]>-1)
				tt+= " ; T "+l[order[Image.t]]; 

			if(!isMultiChannel){
				access.setPosition(l);
				tt+="<br/> Value = "+conversions[0].apply(access.get().getRealDouble())+"</HTML>";
				return tt;
			}
			else{
				for(int c = 0; c<this.numChannels(); c++){
					l[Image.c] = c;
					access.setPosition(l);
					tt+="<br/>  Channel "+c+" = "+conversions[c].apply(access.get().getRealDouble());					
				}
				tt+="</HTML>";
				return tt;
			}

		}
	}


	public Point getToolTipLocation(MouseEvent e) {
		Point p = new Point(e.getX(), e.getY());
		return p;
	}



}
