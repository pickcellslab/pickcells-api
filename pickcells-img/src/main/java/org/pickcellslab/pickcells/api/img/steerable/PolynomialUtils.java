/*******************************************************************************
 * Copyright (c) 2012-2013 Biomedical Image Group (BIG), EPFL, Switzerland.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 *     Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 ******************************************************************************/
package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2019 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Class that implements several polynomial manipulation utilities. Based on the
 * <code>ijmath</code> package of Francois Aguet.
 * 
 * @version April 23, 2013
 * 
 * @author Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 * @author Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 * 
 * <hr>
 * NOTICE: This code was copied from a plugin originally implemented by François Aguet for ImageJ, and ported to Icy by Ricard Delgado-Gonzalo (ricard.delgado@gmail.com) 
 * and Zsuzsanna Püspöki (zsuzsanna.puspoki@epfl.ch). This plugin was based on the following publication:
 * <br><br>
 * M. Jacob, M. Unser, “Design of Steerable Filters for Feature Detection Using Canny-Like Criteria,”
 * IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 8, pp. 1007-1019
 * <hr>
 * 
 * 
 */
public class PolynomialUtils {

	// ============================================================================
	// PUBLIC METHODS

	/**
	 * Returns the real roots of a quadratic polynomial. The input is of the
	 * form: x^2 + a1*x + a0. The size of the returned array corresponds to the
	 * number of real roots.
	 */
	public static double[] quadraticRoots(double a1, double a0) {

		double[] roots;
		double delta = a1 * a1 - 4.0 * a0;
		if (delta < 0.0) {
			roots = new double[0];
		} else {
			roots = new double[2];
			delta = Math.sqrt(delta);
			roots[0] = (-a1 + delta) / 2.0;
			roots[1] = (-a1 - delta) / 2.0;
		}
		return roots;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Returns the real roots of a cubic polynomial. The input is of the form:
	 * x^3 + a2*x^2 + a1*x + a0. References: Numerical Recipes in C, pp.
	 * 184-185, http://mathworld.wolfram.com/CubicFormula.html The size of the
	 * returned array corresponds to the number of real roots.
	 */
	public static double[] cubicRoots(double a2, double a1, double a0) {

		double theta, A, B;
		double q = a2 * a2 - 3.0 * a1;
		double Q = q / 9.0;
		double R = (2.0 * a2 * a2 * a2 - 9.0 * a2 * a1 + 27.0 * a0) / 54.0;
		double roots[];
		double signR, delta;

		if (a0 == 0.0) {
			delta = a2 * a2 - 4.0 * a1;
			if (delta >= 0.0) {
				roots = new double[3];
				roots[0] = 0.0;
				roots[1] = (-a2 + Math.sqrt(delta)) / 2.0;
				roots[2] = (-a2 - Math.sqrt(delta)) / 2.0;
			} else {
				roots = new double[1];
				roots[0] = 0.0;
			}
		} else {
			if (R >= 0.0) {
				signR = 1.0;
			} else {
				signR = -1.0;
			}
			if (R * R <= Q * Q * Q) { // REF: R2 < Q3
				roots = new double[3];
				theta = Math.acos(R / Math.sqrt(Q * Q * Q));
				roots[0] = -2.0 * Math.sqrt(Q) * Math.cos(theta / 3.0)
						- (a2 / 3.0);
				roots[1] = -2.0 * Math.sqrt(Q)
						* Math.cos((theta + 2.0 * Math.PI) / 3.0) - (a2 / 3.0);
				roots[2] = -2.0 * Math.sqrt(Q)
						* Math.cos((theta - 2.0 * Math.PI) / 3.0) - (a2 / 3.0);
			} else {
				A = -signR
						* Math.pow(Math.abs(R) + Math.sqrt(R * R - Q * Q * Q),
								1.0 / 3.0);
				if (A == 0.0) {
					B = 0.0;
				} else {
					B = Q / A;
				}
				roots = new double[1];
				roots[0] = A + B - (a2 / 3.0);
			}
		}
		return roots;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Returns the real roots of a quartic polynomial. The input is of the form:
	 * x^4 + a3*x^3 + a2*x^2 + a1*x + a0. Reference:
	 * http://mathworld.wolfram.com/QuarticEquation.html The size of the
	 * returned array corresponds to the number of real roots.
	 */
	public static double[] quarticRoots(double a3, double a2, double a1,
			double a0) {

		double[] roots;

		if (a3 == 0.0 && a1 == 0.0) { // quadratic equation in x^2
			double[] x2roots = quadraticRoots(a2, a0);
			if (x2roots.length != 0) {
				if ((x2roots[0] >= 0.0) && (x2roots[1] >= 0.0)) {
					roots = new double[4];
					roots[0] = Math.sqrt(x2roots[0]);
					roots[1] = -roots[0];
					roots[2] = Math.sqrt(x2roots[1]);
					roots[3] = roots[2];
				} else if ((x2roots[0] < 0.0) && (x2roots[1] < 0.0)) {
					roots = new double[0];
				} else {
					roots = new double[2];
					if (x2roots[0] >= 0.0) {
						roots[0] = Math.sqrt(x2roots[0]);
						roots[1] = -roots[0];
					} else {
						roots[0] = Math.sqrt(x2roots[1]);
						roots[1] = -roots[0];
					}
				}
			} else {
				roots = new double[0];
			}
		} else { // solve quartic

			double[] crr = cubicRoots(-a2, a1 * a3 - 4.0 * a0, 4.0 * a2 * a0
					- a1 * a1 - a3 * a3 * a0);
			double y1 = crr[0];

			double deltaR = 0.25 * a3 * a3 - a2 + y1;
			double R;

			if (deltaR < 0.0) { // 4 complex roots
				roots = new double[0];
			} else {
				R = Math.sqrt(deltaR);
				double D, E;
				double deltaD, deltaE;
				if (R == 0.0) {
					deltaD = 0.75 * a3 * a3 - 2.0 * a2 + 2.0
							* Math.sqrt(y1 * y1 - 4.0 * a0);
					deltaE = 0.75 * a3 * a3 - 2.0 * a2 - 2.0
							* Math.sqrt(y1 * y1 - 4.0 * a0);
					if (deltaD >= 0.0) {
						D = Math.sqrt(deltaD);
						if (deltaE >= 0.0) {
							E = Math.sqrt(deltaE);
							roots = new double[4];
							roots[0] = -0.25 * a3 + 0.5 * D;
							roots[1] = -0.25 * a3 - 0.5 * D;
							roots[2] = -0.25 * a3 + 0.5 * E;
							roots[3] = -0.25 * a3 - 0.5 * E;
						} else {
							roots = new double[2];
							roots[0] = -0.25 * a3 + 0.5 * D;
							roots[1] = -0.25 * a3 - 0.5 * D;
						}
					} else {
						if (deltaE >= 0.0) {
							E = Math.sqrt(deltaE);
							roots = new double[2];
							roots[0] = -0.25 * a3 + 0.5 * E;
							roots[1] = -0.25 * a3 - 0.5 * E;
						} else {
							roots = new double[0];
						}
					}
				} else {
					deltaD = 0.75 * a3 * a3 - R * R - 2.0 * a2
							+ (a3 * a2 - 2.0 * a1 - 0.25 * a3 * a3 * a3) / R;
					deltaE = 0.75 * a3 * a3 - R * R - 2.0 * a2
							- (a3 * a2 - 2.0 * a1 - 0.25 * a3 * a3 * a3) / R;
					if (deltaD >= 0.0) {
						D = Math.sqrt(deltaD);
						if (deltaE >= 0.0) {
							E = Math.sqrt(deltaE);
							roots = new double[4];
							roots[0] = -0.25 * a3 + 0.5 * R + 0.5 * D;
							roots[1] = -0.25 * a3 + 0.5 * R - 0.5 * D;
							roots[2] = -0.25 * a3 - 0.5 * R + 0.5 * E;
							roots[3] = -0.25 * a3 - 0.5 * R - 0.5 * E;
						} else {
							roots = new double[2];
							roots[0] = -0.25 * a3 + 0.5 * R + 0.5 * D;
							roots[1] = -0.25 * a3 + 0.5 * R - 0.5 * D;
						}
					} else {
						if (deltaE >= 0.0) {
							E = Math.sqrt(deltaE);
							roots = new double[2];
							roots[0] = -0.25 * a3 - 0.5 * R + 0.5 * E;
							roots[1] = -0.25 * a3 - 0.5 * R - 0.5 * E;
						} else {
							roots = new double[0];
						}
					}
				}
			}
		}
		return roots;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Evaluates a polynomial and its first derivative in x0. f(x) = a[0] +
	 * a[1]*x + a[2]*x^2 + ...
	 * 
	 * @return A length two array containing f(x0) and f'(x0), respectively.
	 */
	public static double[] evalPolyD(double[] a, double x0) {
		int n = a.length;
		double[] f = new double[2];
		f[0] = a[n - 1];
		f[1] = 0.0;
		for (int i = n - 2; i >= 0; i--) {
			f[1] = f[1] * x0 + f[0];
			f[0] = f[0] * x0 + a[i];
		}
		return f;
	}

	// ----------------------------------------------------------------------------

	/** Divides a polynomial by a known root. */
	public static double[] divPolyByRoot(double[] coeffs, double root) {
		int nx = coeffs.length;
		double[] out = new double[nx - 1];
		double rem = coeffs[nx - 1];
		for (int i = nx - 2; i >= 0; i--) {
			out[i] = rem;
			rem = coeffs[i] + rem * root;
		}
		return out;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Divides a polynomial by a pair of known conjugate roots, i.e., (x - (a +
	 * bi))(x - (a - bi)).
	 */
	public static double[] divPolyByConjRoots(double[] coeffs, double a,
			double b) {
		double t1 = 2.0 * a;
		double t2 = a * a + b * b;
		int n = coeffs.length - 2;
		double[] out = new double[n];
		out[n - 1] = coeffs[n + 1];
		out[n - 2] = coeffs[n] + t1 * out[n - 1];
		for (int i = n - 1; i >= 2; i--) {
			out[i - 2] = coeffs[i] + t1 * out[i - 1] - t2 * out[i];
		}
		return out;
	}

	// ----------------------------------------------------------------------------

	/**
	 * Laguerre's root finding method for real-valued polynomials. f(x) = a[0] +
	 * a[1]*x + a[2]*x^2 + ... The returned root can be complex.
	 * 
	 * @return An array containing the real and imaginary part of the root a +
	 *         i*b, i.e., {a,b}.
	 */
	public static double[] laguerre(double[] a, double x0) {

		int N = a.length - 1; // degree of the polynomial
		double[] x = Complex.complex(x0, 0.0); // a (complex) root
		double[] f, df, d2f;
		double[] G, G2, H, sq, Gplus, Gminus, dx;
		double[] delta;
		double absx, error, abs_plus, abs_minus;
		final double[] fracl = { 0.0, 0.5, 0.25, 0.75, 0.125, 0.375, 0.625,
				0.875, 1.0 }; // fractions used to break limit cycles
		int MT = 10;
		int maxiter = 30;
		double tol = 1.0e-15;
		int[] converged = new int[1];
		converged[0] = -2;

		for (int iter = 1; iter <= maxiter; iter++) {

			f = Complex.complex(a[N], 0.0);
			df = Complex.complex(0.0, 0.0);
			d2f = Complex.complex(0.0, 0.0);
			absx = Complex.modulus(x);
			error = Complex.modulus(f);
			for (int k = N - 1; k >= 0; k--) {
				d2f = Complex.add(Complex.mul(d2f, x), df);
				df = Complex.add(Complex.mul(df, x), f);
				f = Complex
						.add(Complex.mul(f, x), Complex.complex(a[k], 0.0));
				error = Complex.modulus(f) + absx * error;
			}
			if (Complex.modulus(f) <= error * tol) { // x is a root
				converged[0] = 2;
				break;
			}
			G = Complex.div(df, f);
			G2 = Complex.mul(G, G);
			H = Complex.sub(G2, Complex.div(d2f, f));
			delta = Complex.mul(N - 1, Complex.sub(Complex.mul(N, H), G2));
			sq = Complex.sqrt(delta);
			Gplus = Complex.add(G, sq);
			Gminus = Complex.sub(G, sq);
			abs_plus = Complex.modulus(Gplus);
			abs_minus = Complex.modulus(Gminus);
			if (abs_minus > abs_plus) {
				Gplus = Gminus;
				abs_plus = abs_minus;
			} // Gplus is largest absolute denominator
			if (abs_plus > 0.0) {
				dx = Complex.div(Complex.complex(N, 0.0), Gplus);
			} else { // G = 0, H = 0, freak case
				dx = Complex.mul(1 + absx,
						Complex.complex(Math.cos(iter), Math.sin(iter)));
				dx = Complex.complex(1.0, 0.0);
			}
			if (iter % MT != 0) {
				x = Complex.sub(x, dx);
			} else { // fractional step to break limit cycles
				x = Complex.sub(x, Complex.mul(fracl[iter / MT], dx));
			}
		}
		return x;
	}
}
