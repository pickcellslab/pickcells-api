package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;

import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;


public class ImgProcessingRenderer implements ListCellRenderer<ImgProcessing<?,?>>{


	@Override
	public Component getListCellRendererComponent(JList<? extends ImgProcessing<?, ?>> list, ImgProcessing<?, ?> value,
			int index, boolean isSelected, boolean cellHasFocus) {
		
		JLabel label = new JLabel();

		if(value!=null){		
			label.setText(value.name());
			label.setToolTipText(value.description());
		}

		//Create a small padding
		Border paddingBorder = BorderFactory.createEmptyBorder(3,3,3,3);
		label.setBorder(paddingBorder);

		label.setForeground(Color.black);
		
		return label;
	}

}
