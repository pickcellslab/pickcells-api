/*******************************************************************************
 * Copyright (c) 2012-2013 Biomedical Image Group (BIG), EPFL, Switzerland.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 *     Zsuzsanna Puspoki (zsuzsanna.puspoki@epfl.ch)
 ******************************************************************************/
package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2019 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * 
 * Stores the parameters for the steerable templates. Based on the
 * <code>ParameterSet</code> package of Francois Aguet.
 * 
 * @version April 23, 2013
 * 
 * @author Ricard Delgado-Gonzalo (ricard.delgado@gmail.com)
 * 
 * 
 * <hr>
 * NOTICE: This code was copied from a plugin originally implemented by François Aguet for ImageJ, and ported to Icy by Ricard Delgado-Gonzalo (ricard.delgado@gmail.com) 
 * and Zsuzsanna Püspöki (zsuzsanna.puspoki@epfl.ch). This plugin was based on the following publication:
 * <br><br>
 * M. Jacob, M. Unser, “Design of Steerable Filters for Feature Detection Using Canny-Like Criteria,”
 * IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 26, no. 8, pp. 1007-1019
 * <hr>
 * 
 */
public class AlphaValues {

	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a11i_ = 0.0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a31i_ = 0.0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a33i_ = 0.0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a51i_ = 0.0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a53i_ = 0.0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a20i_ = 0.0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a22i_ = 0.0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a40i_ = 0.0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a42i_ = 0.0;
	/**
	 * Weights that control the linear combination of Gaussian derivatives to
	 * the feature template.
	 */
	private double a44i_ = 0.0;

	// ============================================================================
	// PUBLIC METHODS

	/** Default constructor. */
	public AlphaValues() {
	}

	// ----------------------------------------------------------------------------

	/**
	 * Sets the filter parameters to default values corresponding to order and
	 * set numbers.
	 * 
	 * @param M
	 *            Order of the filter.
	 * @param set
	 *            Identifier of the parameter set.
	 */
	public void setDefault(int M, int set, ParameterSet[][] pSet) {
		double[] w;
		switch (M) {
		case 1:
			w = pSet[0][set - 1].getWeights();
			a11i_ = w[0];
			break;
		case 2:
			w = pSet[1][set - 1].getWeights();
			a20i_ = w[0];
			a22i_ = w[1];
			break;
		case 3:
			w = pSet[2][set - 1].getWeights();
			a11i_ = w[0];
			a31i_ = w[1];
			a33i_ = w[2];
			break;
		case 4:
			w = pSet[3][set - 1].getWeights();
			a20i_ = w[0];
			a22i_ = w[1];
			a40i_ = w[2];
			a42i_ = w[3];
			a44i_ = w[4];
			break;
		case 5:
			w = pSet[4][set - 1].getWeights();
			a11i_ = w[0];
			a31i_ = w[1];
			a33i_ = w[2];
			a51i_ = w[3];
			a53i_ = w[4];
			break;
		default:
			break;
		}
	}

	// ----------------------------------------------------------------------------

	/**
	 * Constructs the set of weights for the filter given the set of basic
	 * parameters.
	 */
	public double[] getAlpha(int mode, double sigma, int M) {
		double[] alpha;
		if (mode == 1) {
			double sigma2 = sigma * sigma;
			switch (M) {
			case 1:
				alpha = new double[1];
				alpha[0] = a11i_;
				break;
			case 2:
				alpha = new double[2];
				alpha[0] = a20i_ * sigma;
				alpha[1] = a22i_ * sigma;
				break;
			case 3:
				alpha = new double[3];
				alpha[0] = a11i_;
				alpha[1] = a31i_ * sigma2;
				alpha[2] = a33i_ * sigma2;
				break;
			case 4:
				alpha = new double[5];
				alpha[0] = a20i_ * sigma;
				alpha[1] = a22i_ * sigma;
				alpha[2] = a40i_ * sigma * sigma2;
				alpha[3] = a42i_ * sigma * sigma2;
				alpha[4] = a44i_ * sigma * sigma2;
				break;
			case 5:
				alpha = new double[5];
				alpha[0] = a11i_;
				alpha[1] = a31i_ * sigma2;
				alpha[2] = a33i_ * sigma2;
				alpha[3] = a51i_ * sigma2 * sigma2;
				alpha[4] = a53i_ * sigma2 * sigma2;
				break;
			default:
				alpha = null;
				break;
			}
		} else {
			switch (M) {
			case 1:
				alpha = new double[1];
				alpha[0] = a11i_;
				break;
			case 2:
				alpha = new double[2];
				alpha[0] = a20i_;
				alpha[1] = a22i_;
				break;
			case 3:
				alpha = new double[3];
				alpha[0] = a11i_;
				alpha[1] = a31i_;
				alpha[2] = a33i_;
				break;
			case 4:
				alpha = new double[5];
				alpha[0] = a20i_;
				alpha[1] = a22i_;
				alpha[2] = a40i_;
				alpha[3] = a42i_;
				alpha[4] = a44i_;
				break;
			case 5:
				alpha = new double[5];
				alpha[0] = a11i_;
				alpha[1] = a31i_;
				alpha[2] = a33i_;
				alpha[3] = a51i_;
				alpha[4] = a53i_;
				break;
			default:
				alpha = null;
				break;
			}
		}
		return alpha;
	}
}
