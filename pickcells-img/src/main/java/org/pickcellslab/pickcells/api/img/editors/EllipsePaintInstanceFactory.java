package org.pickcellslab.pickcells.api.img.editors;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Objects;

import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.img.providers.OutOfLabelException;
import org.pickcellslab.pickcells.api.img.view.DefaultPaintInstance;

import net.imglib2.type.numeric.RealType;

public class EllipsePaintInstanceFactory<P extends RealType<P>,D extends RealType<D>> implements PaintInstanceFactory<P,D> {

	private final UITheme theme;
	private final long[] dfDims;
	
	
	public EllipsePaintInstanceFactory(UITheme theme, long[] defaultDimensions) {
		Objects.requireNonNull(theme);
		this.theme = theme;
		dfDims = Arrays.copyOf(defaultDimensions, defaultDimensions.length);
	}
	
	@Override
	public long[] defaultBrushDimensions() {
		return Arrays.copyOf(dfDims,dfDims.length);
	}

	@Override
	public void setDefaultBrushDimensions(long[] dims) {
		assert dims.length == dfDims.length : "dims length should be "+dfDims.length;
		for(int i = 0; i<dims.length; i++)
			dfDims[i] = dims[i];
	}
	
	
	
	@Override
	public PaintInstance<P,D> createEraserFor(PaintableAnnotation<P,D> a) {
		final PaintInstanceConsumer<P,D> consumer = a.getHandler();
		Brush brush = Brushes.selectiveEllipse(theme, consumer.getDataFrame(a), consumer.getPaintableFrame(a), dfDims, consumer.dataValue(a), consumer.eraseValue(a));
		PaintInstance<P,D> pi = new DefaultPaintInstance<>(brush, a);
		consumer.addEraser(pi);
		return pi;
	}

	@Override
	public PaintInstance<P,D> createExpanderFor(PaintableAnnotation<P,D> a) {
		final PaintInstanceConsumer<P,D> consumer = a.getHandler();
		Brush brush = Brushes.selectiveEllipse(theme, consumer.getDataFrame(a), consumer.getPaintableFrame(a), dfDims, consumer.nullDataValue(), consumer.expandValue(a));
		PaintInstance<P,D> pi = new DefaultPaintInstance<>(brush, a);
		consumer.addExpander(pi);
		return pi;
	}

	@Override
	public PaintInstance<P,D> createSplitterFor(PaintableAnnotation<P,D> a) {
		final PaintInstanceConsumer<P,D> consumer = a.getHandler();
		Brush brush = Brushes.selectiveEllipse(theme, consumer.getDataFrame(a), consumer.getPaintableFrame(a), dfDims, consumer.dataValue(a), consumer.splitValue(a));
		PaintInstance<P,D> pi = new DefaultPaintInstance<>(brush, a);
		consumer.addSplitter(pi);
		return pi;
	}

	@Override
	public PaintInstance<P,D> createAnnotationPainter(PaintInstanceConsumer<P,D> consumer, int time) throws OutOfLabelException {
		PaintableAnnotation<P,D> a = consumer.newPaintable(time);
		Brush brush = Brushes.selectiveEllipse(theme, consumer.getDataFrame(a), consumer.getPaintableFrame(a), dfDims, consumer.nullDataValue(), consumer.createValue(a));
		PaintInstance<P,D> pi = new DefaultPaintInstance<>(brush, a);		
		consumer.addAnnotation(pi);
		return pi;
	}

	

	


	

}
