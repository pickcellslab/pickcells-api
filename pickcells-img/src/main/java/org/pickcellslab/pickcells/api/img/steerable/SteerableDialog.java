package org.pickcellslab.pickcells.api.img.steerable;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;

import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;

@SuppressWarnings("serial")
class SteerableDialog extends ValidityTogglePanel{
	
	
	private final NotificationFactory notif;

	private final JComboBox<String> comboBox;
	private final JComboBox<String> qCombo;
	private final SpinnerNumberModel model;
	
	
	
	SteerableDialog(NotificationFactory notif) {
		
		this.notif = notif;
		
		
		// Controllers
		
		qCombo = new JComboBox<>();
		qCombo.addItem("Normal");
		qCombo.addItem("High");
		
		comboBox = new JComboBox<>();		
		comboBox.addItem("Nuclear Envelope");
		comboBox.addItem("Nuclear Content");
		
		comboBox.addActionListener(l->{
			int selected = getDetectorType();
			if(selected == 0){
				qCombo.removeItemAt(2);
			}
			else{
				qCombo.addItem("Highest");
			}
		});
		
		model = new SpinnerNumberModel(2.0,0.1,10,0.1);
		JSpinner spinner = new JSpinner(model);
		
		
		
		
		
		
		//   ===========================================================================================
		// Layout
		
		
		
	
		
		JLabel lblStainingType = new JLabel("Staining Type");
		
		
		
		JLabel lblScale = new JLabel("Scale");
		notif.setTooltip(lblScale, "Should be approximately the size (in pixels) of the width of a ridge");
		
		JLabel lblQuality = new JLabel("Quality");
		notif.setTooltip(lblQuality, "Higher quality is more sensitive to straight ridges but tends to create artefacts where\n" + 
				"ridges cross one another. It is also slower");
		
		
		GroupLayout gl_inputPanel = new GroupLayout(this);
		gl_inputPanel.setHorizontalGroup(
			gl_inputPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_inputPanel.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_inputPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblStainingType)
						.addComponent(lblScale)
						.addComponent(lblQuality))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_inputPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(qCombo, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(spinner)
						.addComponent(comboBox, 0, 145, Short.MAX_VALUE))
					.addContainerGap(24, Short.MAX_VALUE))
		);
		gl_inputPanel.setVerticalGroup(
			gl_inputPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_inputPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_inputPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblStainingType))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_inputPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblScale)
						.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_inputPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(qCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblQuality))
					.addContainerGap(56, Short.MAX_VALUE))
		);
		this.setLayout(gl_inputPanel);
		
		
		
	}


	public double getScale() {
		return model.getNumber().doubleValue();
	}
	
	
	public int getQuality() {
		return qCombo.getSelectedIndex()+1;
	}
	

	/**
	 * @return The chosen type of detector -> ridge (0) / edges (1)
	 */
	public int getDetectorType() {
		return comboBox.getSelectedIndex();
	}

	@Override
	public boolean validity() {
		return true;
	}
}
