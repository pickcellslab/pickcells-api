package org.pickcellslab.pickcells.api.img.process;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.numeric.RealType;

/**
 * Implements the Chamfer Distance Algorithm in 2D and 3D using a 3x3 kernel or 3x3x3 kernel respectively 
 * 
 * @author Guillaume Blin
 *
 */
public final class DistanceTransform {

	private DistanceTransform(){/*static methods only*/};



	public static int[][] createEuclidian2DFwd(){
		return new int[][]{
			{ 4, 3, 4},
			{ 3,-1,-1},
			{-1,-1,-1}			
		};
	}

	public static int[][] createEuclidian2DBckd(){
		return new int[][]{
			{-1,-1,-1},
			{-1,-1, 3},
			{ 4, 3, 4}			
		};
	}


	public static int[][][] createEuclidian3DFwd(){
		return new int[][][]{
			{{ 5, 4, 5},{ 4, 3, 4},{ 5, 4, 5}},
			{{ 4, 3, 4},{ 3,-1,-1},{-1,-1,-1}},
			{{-1,-1,-1},{-1,-1,-1},{-1,-1,-1}}
		};
	}

	public static int[][][] createEuclidian3DBckd(){
		return new int[][][]{
			{{-1,-1,-1},{-1,-1,-1},{-1,-1,-1}},			
			{{-1,-1,-1},{-1,-1, 3},{ 4, 3, 4}},
			{{ 5, 4, 5},{ 4, 3, 4},{ 5, 4, 5}}			
		};
	}


	/**
	 * 
	 * @param input A {@link RandomAccessibleInterval} where each background pixel is 0 and each foreground is set to the maximum of the {@link RealType} T
	 */
	public static <T extends RealType<T>> void run(RandomAccessibleInterval<T> input){
		if(input.numDimensions() > 2)
			run3D(input);
		else 
			run2D(input);
	}




	private static <T extends RealType<T>> void run2D(RandomAccessibleInterval<T> input) {

		// First pass
		int[][]  kernel = createEuclidian2DFwd();

		int[] u = new int[input.numDimensions()];
		int[] temp = new int[input.numDimensions()];
		RandomAccess<T> access = input.randomAccess();
		final int max = (int) access.get().getMaxValue();
			for(int y = 1; y<input.dimension(1)-1; y++){
				u[1] = y;
				for(int x = 1; x<input.dimension(0)-1; x++){
					u[0] = x;
					check2D(access, u, temp, kernel, max);
				}
			}
		

		//Second pass

		kernel = createEuclidian2DBckd();

			for(int y = (int) (input.dimension(1)-2); y>=1; y--){
				u[1] = y;
				for(int x = (int) (input.dimension(0)-2); x>=1; x--){
					u[0] = x;
					check2D(access, u, temp, kernel, max);
				}
			}
		
	}



	private static <T extends RealType<T>> void run3D(RandomAccessibleInterval<T> input) {
		// First pass
		int[][][] kernel = createEuclidian3DFwd();

		int[] u = new int[input.numDimensions()];
		int[] temp = new int[input.numDimensions()];
		RandomAccess<T> access = input.randomAccess();
		final int max = (int) access.get().getMaxValue();
		
		for(int z = 1; z<input.dimension(2)-1; z++){
			u[2] = z;
			for(int y = 1; y<input.dimension(1)-1; y++){
				u[1] = y;
				for(int x = 1; x<input.dimension(0)-1; x++){
					u[0] = x;
					check3D(access, u, temp, kernel, max);
				}
			}
		}

		//Second pass

		kernel = createEuclidian3DBckd();

		for(int z = (int) (input.dimension(2)-2); z>=1; z--){
			u[2] = z;
			for(int y = (int) (input.dimension(1)-2); y>=1; y--){
				u[1] = y;
				for(int x = (int) (input.dimension(0)-2); x>=1; x--){
					u[0] = x;
					check3D(access, u, temp, kernel, max);
				}
			}
		}
	}



	// Find the minimum of the sums -> current label + weight in fwd kernel
	private static <T extends RealType<T>> void check3D(RandomAccess<T> access,int[] u, int[] temp, int[][][] weights, int max){
		int min = max;
		for(int i = 0; i<weights.length; i++){
			temp[0] = u[0] + i -1;
			for(int j = 0; j<weights[i].length; j++){
				temp[1] = u[1] + j -1;
				for(int k = 0; k<weights[i][j].length; k++){					
					if(weights[i][j][k] != -1){
						temp[2] = u[2] + k -1;
						access.setPosition(temp);
						min = Math.min(((int) access.get().getRealDouble() + weights[i][j][k]), min);
					}
				}
			}
		}
		access.setPosition(u);
		min = Math.min((int) access.get().getRealDouble() , min);
		access.get().setReal(min);
	}



	private static <T extends RealType<T>> void check2D(RandomAccess<T> access, int[] u, int[] temp, int[][] weights, int max){
		int min = max;
		for(int i = 0; i<weights.length; i++){
			temp[0] = u[0] + i -1;
			for(int j = 0; j<weights[i].length; j++){
				if(weights[i][j] != -1){
					temp[1] = u[1] + j -1;
					access.setPosition(temp);
					min = Math.min(((int) access.get().getRealDouble() + weights[i][j]), min);
				}
			}
		}
		access.setPosition(u);
		min = Math.min((int) access.get().getRealDouble() , min);
		access.get().setReal(min);
	}


}
