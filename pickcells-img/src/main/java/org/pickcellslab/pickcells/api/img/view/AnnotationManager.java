package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.Interval;

/**
 * Stores {@link Annotation}s and provides methods to obtain them based on image coordinates
 * An {@link AnnotationManager} may hold {@link Annotation}s contained within a lower dimensionality than the annotated image,
 * for example 3D shapes in a 3D time resolved dataset. For large {@link Annotation} database (such as a large 3D time resolved segmented dataset),
 * the {@link AnnotationManager} is responsible for caching behaviour and should notify {@link AnnotationDrawer} accordingly via 
 * {@link AnnotationDrawer#annotationsReDrawRequired()}
 * @author Guillaume Blin
 *
 */
public interface AnnotationManager extends  DisplayPositionListener{

			
	/**
	 * @return A {@link MinimalImageInfo} for the image this {@link AnnotationManager} holds the {@link Annotation}s for.
	 */
	public MinimalImageInfo annotatedImageInfo();
	
	
	public List<AnnotationStatus> possibleAnnotationStates();
	
	/**
	 * @param meta Null is allowed in which case
	 * @param filter
	 * @param decompose
	 * @return A Map with a List of {@link Dimension} for each {@link Annotation#representedType()}.
	 */
	public Map<String, List<Dimension<Annotation,?>>> possibleAnnotationDimensions(Predicate<Dimension> filter, boolean decompose);
	
	/**
	 * @param clickPos A long[] with the dimensionality of {@link #annotationsInfo()}
	 * @return The {@link Annotation} at the given location or null, if none are present.
	 */
	public Annotation getAnnotationAt(long[] clickPos);

	public void toggleSelectedAt(long[] pos);
	
	public void toggleSelected(Annotation previous);

	public void inverseSelection();

	public void clearSelection();
	
	
	public boolean annotationsAreVisible();

	public void toggleAnnotationsVisible();

	
	public void addAnnotationStatusListener(AnnotationStatusListener l);

	public void removeAnnotationStatusListener(AnnotationStatusListener l);
	
	
	
	public Stream<Annotation> annotations(long[] min, long[] max);
	

	public Stream<? extends Annotation> stream(String type);
	
	
	
	/**
	 * @param stop An {@link AtomicBoolean} allowing the plane iteration to stop before the full iteration
	 * @param consumer A {@link BiConsumer} of the positions (2d) and {@link Annotation} encountered during the iteration.
	 * NB: if no Annotation is found at a given location, Annotation is null, pos is never null.
	 */
	public void produceCurrentPlaneIteration(AtomicBoolean stop, BiConsumer<long[], Annotation> consumer);
	
	/**
	 * @param stop An {@link AtomicBoolean} allowing the plane iteration to stop before the full iteration
	 * @param consumer A {@link BiConsumer} of the positions (2d) and {@link Annotation} encountered during the iteration.
	 * NB: if no Annotation is found at a given location, Annotation is null, pos is never null.
	 */
	public void produceCurrentPlaneIteration(AtomicBoolean stop, BiConsumer<long[], Annotation> consumer, Interval itrv);
	
	
	
	public void addAnnotationDrawer(AnnotationDrawer l);

	public void removeAnnotationDrawer(AnnotationDrawer l);

}
