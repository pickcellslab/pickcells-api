package org.pickcellslab.pickcells.api.img.providers;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;

import net.imglib2.Interval;
import net.imglib2.Point;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;



/**
 * A SegmentationImageProvider can be configured in 2 ways:
 * -> by setting a prototype instance via {@link #setPrototype(SegmentedObject)}, in this case
 * this provider will create as many instance as there are labels found in the segmentation image.
 * -> by setting a list of DataItem via {@link #setKnownAssociatedData(List)}, in this case this provider will assume that
 * the provided items correspond to the labels found in the segmentation image and will return one of the provided item when
 * {@link #getItem(Number)} is called
 * @author Guillaume Blin
 *
 */
public interface SegmentationImageProvider<T extends RealType<T>> extends ImageProvider<T>{


	
	
	//------------------------ Provider Setup -------------------// 
	
	
	/**
	 * Sets the {@link SegmentedObject} instance to use as a prototype for each encountered label in the 
	 * segmentation image.
	 * @param prototype
	 */
	public void setPrototype(LabelsImage labels, SegmentedObject prototype, boolean computeFeatures);
	
		
	
	
	/**
	 * Injects a list of {DataItem} that are known to be associated with the image this ImageProvider provides.
	 * This will prevent the ImageProvider to recalculate features that are already contained in the data
	 * @param items
	 */
	public <S extends SegmentedObject> void setKnownAssociatedData(Map<Integer, List<S>> data);
	
	public <S extends SegmentedObject> void setKnownAssociatedData(int frame, List<S> data);
	
	
	
	
	//------------------------ Provider Info and Capabilities -------------------//
	
	
	/**
	 * @return The {@link LabelsImage} storing the information about the segmentation Image being provided
	 * by the SegmentationImageProvider
	 */
	public LabelsImage origin();


	public Map<AKey<?>, String> computedFeatures();
	
	
	public long numFrames();
	
	
	
	
	//------------------------ Access to Underlying Image -------------------// 
	
	/**
	 * @return The underlying {@link Img}, NB: should be read only! 
	 */
	@Override
	public Img<T> provide();

	public RandomAccessibleInterval<T> getFrame(int t);

	public RandomAccessibleInterval<T> getSlice(int time, int slice);
		
	
	/**
	 * @param label
	 * @param time
	 * @return An {@link Interval} on the desired time frame which includes the full surface with the given label. The number of dimensions of
	 * this interval are the same as the number of dimensions per frame.
	 */
	public IntervalView<T> interval(float label, int time);

	
	public T variable(float l);
	
	
	
	
	//------------------------ Editing ----------------------// 
	
	public List<SegmentedObject> write(float label, Iterator<long[]> it, int time);
	
	public SegmentedObject merge(List<SegmentedObject> toMerge);
	
	public Pair<SegmentedObject,SegmentedObject> splitZ(SegmentedObject toSplit, long slice) throws OutOfLabelException;
	
	public void delete(SegmentedObject toDelete);
	
	public float newLabel(int time) throws OutOfLabelException;
	
	public void updateOnDisk() throws IOException;
	
	
	/**
	 * Will convert the underlying image into an image with a higher bit depth. For example, if the current image is 8 bits,
	 * this method will convert the image into a 16 bit image.
	 * 
	 * @throws IOException
	 */
	public void convertToNextBitDepth() throws IOException;
	
	/**
	 * Will convert the underlying image into an image with the lowest possible bit depth which can contain all the labels
	 * present in this Segmentation image without data loss.
	 * 
	 * @throws IOException
	 */
	public void convertToLowestPossibleBitDepth() throws IOException;	
	
	
	//------------------------ Access to Segmented Objects ----------------------// 
	
	
	
	public Optional<SegmentedObject> getAnyItem();
	
	
	/**
	 * 
	 * @param l
	 * @return A {@link WritableDataItem} associated with the specified label in the segmentation image.
	 * This item can be used to either retrieve known features about the labeled image or to store new
	 * features
	 */	
	public Optional<SegmentedObject> getItem(float l, int time);

	/**
	 * @return A {@link Collection} containing all the labels found in the image (for the currently selected time frame)
	 * @see #setTimePosition(int)
	 */
	public Stream<Float> labels(int time);
	
	/**
	 * @return A {@link Stream} containing all the labels found in the image (for the given time frame zslice)
	 */
	public Stream<Float> labels(int time, int slice);

		
	/**
	 * @return A {@link Collection} containing all the labels found in the given interval
	 */
	public Stream<Float> labels(long[] min, long[] max, int time);	
	
	/**
	 * @return A {@link Collection} containing all the labels found in the given interval
	 */
	public Stream<Float> labels(long[] min, long[] max, int time, int slice);
	
	
	
	
	
	//------------------------ Convenience Methods to iterate over useful locations  ----------------------// 
	
	public void processPoints(float label, int time, Consumer<long[]> consumer);

	public void processOutline(float label, int time, Consumer<long[]> consumer);

	public void processOutline(long[] ls, long[] ls2, float float1, int time, Consumer<long[]> consumer);

	/**
	 * @param s
	 * @param resolution
	 * @param simplify
	 * @return A List of float[] corresponding to a triangle mesh representing the surface of the object. (In real space)
	 */
	public List<float[]> getMesh(SegmentedObject s, int resolution, boolean simplify, boolean calibrated);

	public List<float[]> getMesh(long[] bbMin, long[] bbMax, float label, int time, int resolution, boolean simplify, boolean calibrated);



	

	
	/**
	 * @param p The location of interest in the image
	 * @return An {@link Optional} holding the data found at the specified location. NB: the Point must have thesame number
	 * of dimension as the underlying image (minus channel).
	 * @see #getData(long[], int)
	 */
	public Optional<SegmentedObject> getData(Point p);
	
	/**
	 * @param p The location of interest in the image (2D or 3D, no channel, no time)
	 * @param time The time frame where the data is found
	 * @return An {@link Optional} holding the data found at the specified location.
	 */
	public Optional<SegmentedObject> getData(long[] p, int time);
	



	



}
