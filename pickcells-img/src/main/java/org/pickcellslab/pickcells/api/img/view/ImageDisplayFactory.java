package org.pickcellslab.pickcells.api.img.view;

import org.pickcellslab.foundationj.annotations.Core;
import org.pickcellslab.foundationj.annotations.SameScopeAs;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

@Core
@SameScopeAs(ImgIO.class)
public interface ImageDisplayFactory {

	/**
	 * @return a new {@link ImageDisplay} instance
	 */
	public ImageDisplay newDisplay();
	
}
