package org.pickcellslab.pickcells.api.img.view;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.simplecharts.SimpleHistogramFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;

@CoreImpl
public class ImageDiplayFactoryImpl implements ImageDisplayFactory {

	
	private final SimpleHistogramFactory hFctry;
	private final UITheme theme;
	private final NotificationFactory notifier;
	
	public ImageDiplayFactoryImpl(SimpleHistogramFactory hFctry, UITheme theme, NotificationFactory notifier) {
		this.hFctry = hFctry;
		this.theme = theme;
		this.notifier = notifier;
	}
	
	@Override
	public ImageDisplay newDisplay() {
		return new ImageViewer2(hFctry, theme, notifier);
	}

}
