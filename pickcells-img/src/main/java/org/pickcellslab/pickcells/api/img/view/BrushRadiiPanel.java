package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.alee.extended.panel.GroupPanel;

public class BrushRadiiPanel extends GroupPanel {

	private final long[] newDims;

	public BrushRadiiPanel(long[] current, String[] dims) {

		super(15, false);
		
		assert current != null;
		assert dims != null;

		newDims = Arrays.copyOf(current, current.length);
		
		int limit = Math.min(current.length, dims.length);

		for(int i = 0; i< limit; i++){

			
			JLabel lblDims = new JLabel(dims[i]);
			add(lblDims);

			JSpinner spinner = new JSpinner();
			SpinnerNumberModel m = new SpinnerNumberModel(newDims[i], 1, 128, 1);
			spinner.setModel(m);
			final int j = i;
			m.addChangeListener(l-> newDims[j] = m.getNumber().longValue());
			
			add(new GroupPanel(15, lblDims, spinner));

		}






	}

	public long[] getNewDims() {
		return newDims;
	}

}
