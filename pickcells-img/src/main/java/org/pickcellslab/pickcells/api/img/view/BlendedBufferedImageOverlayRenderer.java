package org.pickcellslab.pickcells.api.img.view;

import java.awt.CompositeContext;

/*
 * #%L
 * ImgLib2: a general-purpose, multidimensional image processing library.
 * %%
 * Copyright (C) 2009 - 2015 Tobias Pietzsch, Stephan Preibisch, Barry DeZonia,
 * Stephan Saalfeld, Curtis Rueden, Albert Cardona, Christian Dietz, Jean-Yves
 * Tinevez, Johannes Schindelin, Jonathan Hale, Lee Kamentsky, Larry Lindsey, Mark
 * Hiner, Michael Zinsmaier, Martin Horn, Grant Harris, Aivar Grislis, John
 * Bogovic, Steffen Jaensch, Stefan Helfrich, Jan Funke, Nick Perry, Mark Longair,
 * Melissa Linkert and Dimiter Prodanov.
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

import org.jdesktop.swingx.graphics.BlendComposite;

import net.imglib2.ui.OverlayRenderer;
import net.imglib2.ui.overlay.BufferedImageOverlayRenderer;

/**
 * An extension of {@link BufferedImageOverlayRenderer} which allows to change the alpha value of the image rendered on screen.
 * Maybe useful when multiple {@link OverlayRenderer}s are used on a single canvas.
 * 
 */
public class BlendedBufferedImageOverlayRenderer extends BufferedImageOverlayRenderer{

	private final BlendComposite composite = BlendComposite.Add;


	public BufferedImage blend( final Graphics g, BufferedImage bi)
	{

		synchronized ( this )
		{
			if ( pending )
			{
				final BufferedImage tmp = bufferedImage;
				bufferedImage = pendingImage;
				pendingImage = tmp;
				pending = false;
			}
		}
		if ( bufferedImage != null )
		{
			final ColorModel cm = bufferedImage.getColorModel();
			final boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
			final WritableRaster raster = bufferedImage.copyData(null);
			final BufferedImage toReturn = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
			if(bi!=null){
				CompositeContext context = composite.createContext(bi.getColorModel(), toReturn.getColorModel(), ( ( Graphics2D ) g ).getRenderingHints());
				context.compose(bi.getRaster(), toReturn.getRaster(), toReturn.getRaster());
			}

			//System.out.println( String.format( "g.drawImage() :%4d ms", watch.nanoTime() / 1000000 ) );
			return toReturn;
		}
		
		return null;

	}

}
