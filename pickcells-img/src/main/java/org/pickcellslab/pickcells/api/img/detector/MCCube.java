package org.pickcellslab.pickcells.api.img.detector;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.mines.jtk.util.Array;
import net.imglib2.Cursor;
import net.imglib2.Dimensions;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.util.Util;
import net.imglib2.view.Views;

/**
 * 
 * Marching Cube implementation to obtain the isosurface of objects in an image
 * 
 *TODO add license : This class was obtained from ImageJ3D Viewer and modified to remove dependencies
 * 
 */
public final class MCCube {

	private static Logger log = LoggerFactory.getLogger(MCCube.class);

	// vertexes
	private float[][] v;

	// interpolated values
	private float[][] e;

	private boolean nonZeroFound;

	private int cubeSize = 3;

	public MCCube() {
		this.v = new float[8][3];
		this.e = new float[12][3];
	}

	/**
	 * initializes a MCCube object
	 *        _________           0______x
	 *       /v0    v1/|         /|
	 *      /________/ |        / | 
	 *      |v3    v2| /v5    y/  |z 
	 *      |________|/	
	 *       v7    v6
	 */
	private void init(long x, long y, long z){
		int half = cubeSize;
		v[0][0] = x; 			v[0][1] = y;		 	v[0][2] = z;
		v[1][0] = x + half;		v[1][1] = y;		 	v[1][2] = z;
		v[2][0] = x + half; 	v[2][1] = y + half;	 	v[2][2] = z;
		v[3][0] = x ; 			v[3][1] = y + half; 	v[3][2] = z;
		v[4][0] = x ; 			v[4][1] = y; 			v[4][2] = z + half;
		v[5][0] = x + half;		v[5][1] = y; 			v[5][2] = z + half;
		v[6][0] = x + half;		v[6][1] = y + half; 	v[6][2] = z + half;
		v[7][0] = x ; 			v[7][1] = y + half; 	v[7][2] = z + half;
	} 

	/**
	 * computes the interpolated point along a specified edge whose 
	 * intensity equals the reference value
	 * @param v1 first extremity of the edge
	 * @param v2 second extremity of the edge
	 * @param result stores the resulting edge
	 * return the point on the edge where intensity equals the isovalue; 
	 * @return false if the interpolated point is beyond edge boundaries
	 */
	private boolean computeEdge( 
			float[] v1, float i1,
			float[] v2, float i2,
			float[] result, 
			float label) {

		if(i1!=label)
			i1=0;
		if(i2!=label)
			i2=0;

		// 30 --- 50 --- 70 : t=0.5
		// 70 --- 50 --- 30 : t=0.5
		///int i1 = car.intensity(v1);
		///int i2 = car.intensity(v2);
		if(i2 < i1)
			return computeEdge(v2, i2, v1, i1, result, label);

		float t = (label - i1) / (float) (i2 - i1);
		if (t >= 0 && t <= 1) {
			// v1 + t*(v2-v1)
			for(int i = 0; i<3; i++)
				result[i] = t*(v2[i]-v1[i])+v1[i];
			
			//System.out.println("================================");
			//System.out.println("label "+label);
			//System.out.println("t "+t);
			//System.out.println("v1 "+Arrays.toString(v1));
			//System.out.println("i1 "+i1);
			//System.out.println("v2 "+Arrays.toString(v2));
			//System.out.println("i2 "+i2);
			//System.out.println(Arrays.toString(result));
			
			return true;
		}
		result[0] = -1; result[1] = -1; result[2] = -1;
		//System.out.println("================================");
		//System.out.println("v1 "+Arrays.toString(v1));
		//System.out.println("i1 "+i1);
		//System.out.println("v2 "+Arrays.toString(v2));
		//System.out.println("i2 "+i2);
		//System.out.println(Arrays.toString(result));
		return false;
	}

	/**
	 * computes interpolated values along each edge of the cube 
	 * (null if interpolated value doesn't belong to the edge)
	 */
	private <T extends RealType<T>> void computeEdges(final RandomAccess<T> access, float label) {



		access.setPosition(new long[]{(long) v[0][0],(long) v[0][1],(long) v[0][2]});  float i0 = access.get().getRealFloat();
		access.setPosition(new long[]{(long) v[1][0],(long) v[1][1],(long) v[1][2]});  float i1 = access.get().getRealFloat();
		access.setPosition(new long[]{(long) v[2][0],(long) v[2][1],(long) v[2][2]});  float i2 = access.get().getRealFloat();
		access.setPosition(new long[]{(long) v[3][0],(long) v[3][1],(long) v[3][2]});  float i3 = access.get().getRealFloat();
		access.setPosition(new long[]{(long) v[4][0],(long) v[4][1],(long) v[4][2]});  float i4 = access.get().getRealFloat();
		access.setPosition(new long[]{(long) v[5][0],(long) v[5][1],(long) v[5][2]});  float i5 = access.get().getRealFloat();
		access.setPosition(new long[]{(long) v[6][0],(long) v[6][1],(long) v[6][2]});  float i6 = access.get().getRealFloat();
		access.setPosition(new long[]{(long) v[7][0],(long) v[7][1],(long) v[7][2]});  float i7 = access.get().getRealFloat();

		if(i0!=0||i1!=0||i2!=0||i3!=0||i4!=0||i5!=0||i6!=0||i7!=0)
			nonZeroFound = true;

		this.computeEdge(v[0], i0, v[1], i1, e[0], label);
		this.computeEdge(v[1], i1, v[2], i2, e[1], label);
		this.computeEdge(v[2], i2, v[3], i3, e[2], label);
		this.computeEdge(v[3], i3, v[0], i0, e[3], label);

		this.computeEdge(v[4], i4, v[5], i5, e[4], label);
		this.computeEdge(v[5], i5, v[6], i6, e[5], label);
		this.computeEdge(v[6], i6, v[7], i7, e[6], label);
		this.computeEdge(v[7], i7, v[4], i4, e[7], label);

		this.computeEdge(v[0], i0, v[4], i4, e[8], label);
		this.computeEdge(v[1], i1, v[5], i5, e[9], label);
		this.computeEdge(v[3], i3, v[7], i7, e[10], label);
		this.computeEdge(v[2], i2, v[6], i6, e[11], label);
	}


	/**
	 * indicates if a number corresponds to an ambiguous case
	 * @param n number of the case to test
	 * @return true if the case if ambiguous
	 */
	private static boolean isAmbigous(int n) {
		boolean result = false;
		for (int index = 0; index < MCCube.ambigous.length; index++) {
			result |= MCCube.ambigous[index] == n;
		}
		return result;
	}

	/**
	 * computes the case number of the cube
	 * @return the number of the case corresponding to the cube
	 */
	private int caseNumber(final Carrier car) {
		int caseNumber = 0;

		for (int index = -1; 
				++index < v.length; 
				caseNumber += 
						(car.intensity(v[index]) - car.label >= 0) //Get intensity
						? 1 << index
								: 0);
		return caseNumber;
	}

	private void getTriangles(List<float[]> list, final Carrier car){
		int cn = caseNumber(car);
		boolean directTable = !(isAmbigous(cn));
		//directTable = true;

		// address in the table
		int offset = directTable ? cn*15 : (255-cn)*15;
		for(int index = 0; index < 5; index++){
			// if there's a triangle
			if (faces[offset] != -1) {
				// pick up vertices of the current triangle
				list.add(Array.copy(this.e[faces[offset+0]]));
				list.add(Array.copy(this.e[faces[offset+1]]));
				list.add(Array.copy(this.e[faces[offset+2]]));
				
			//	System.out.println("e1 "+ Arrays.toString(this.e[faces[offset]]));
			//	System.out.println("e2 "+ Arrays.toString(this.e[faces[offset+1]]));
			//	System.out.println("e3 "+ Arrays.toString(this.e[faces[offset+2]]));
			} 
			offset += 3;
		}
		
	//	System.out.println(list.size());
	}

	public <T extends RealType<T>> List<float[]> getTriangles(
			RandomAccessibleInterval<? extends T> img,
			long[] min,
			long[] max,
			float label,
			int cubeSize) {

		this.cubeSize = cubeSize;
		nonZeroFound = false;

		log.trace("Computing edges for a new segmented object...............................");

		//Extend the interval to make sure there is no OutOfBoundException//TODO		
		//get dimensions
		if(img.numDimensions()<3 || min.length != max.length || min.length != img.numDimensions())
			throw new IllegalArgumentException(
					"The provided Interval must possess at least 3 dimensions"
							+ " and the provided min and max must possess the same length");



		//Make a hardcopy remove non label voxels and smooth
		//Img<FloatType> img = ImgDimensions.hardCopy(Views.interval(interval, min, max),new FloatType());
		long[] dim = new long[min.length];
		dim[0] = max[0]-min[0]+10;
		dim[1] = max[1]-min[1]+10;
		dim[2] = max[2]-min[2]+10;


		Dimensions d = Views.interval(img, new long[dim.length], dim);
		Img<FloatType> copy = Util.getArrayOrCellImgFactory(d,new FloatType()).create(d, new FloatType());

		long[] copyMin = new long[dim.length];		long[] copyMax = new long[dim.length]; 
		copyMin[0] = 5;	copyMax[0] = dim[0] - 5;
		copyMin[1] = 5;	copyMax[1] = dim[1] - 5;
		copyMin[2] = 5;	copyMax[2] = dim[2] - 5;

		Cursor<FloatType> copyCursor = Views.interval(copy, copyMin, copyMax).localizingCursor();

		Cursor<? extends T> imgCursor = Views.interval(img, min, max).localizingCursor();




		//System.out.println("copyMin = "+Arrays.toString(copyMin));
		//System.out.println("copyMax = "+Arrays.toString(copyMax));
		//System.out.println("min = "+Arrays.toString(min));
		//System.out.println("max = "+Arrays.toString(max));

		while(imgCursor.hasNext()){
			imgCursor.next();
			copyCursor.next();
			T type = imgCursor.get();
			if(type.getRealFloat() == label){
				copyCursor.get().setReal(255);
			}				
		}

		/*
		try {

			Gauss3.gauss(0.1, Views.extendZero(copy), copy);

		} catch (IncompatibleTypeException e) {
			// This should not happen
			e.printStackTrace();
		}
		 */

		RandomAccess<FloatType> access = Views.extendZero(copy).randomAccess();

		Carrier car = new Carrier();		
		car.access = access;
		car.label = 255;

		//The list holding the triangles
		List<float[]> tri = new ArrayList<float[]>(1000);



		Cursor<FloatType> c = Views.iterable(Views.subsample(copy, cubeSize)).localizingCursor();//copy.localizingCursor();
		long[] current = new long[min.length];
		while(c.hasNext()){		
			c.next();
			c.localize(current);
			init(current[0]*cubeSize, current[1]*cubeSize, current[2]*cubeSize);
			computeEdges(access, 255);
			getTriangles(tri, car);
		}
		
		
		
		/*
		for(long z = copyMin[2]-cubeSize; z < copyMax[2]+cubeSize; z+=cubeSize){
			for(long x = copyMin[0]-cubeSize; x < copyMax[0]+cubeSize; x+=cubeSize){
				for(long y = copyMin[1]-cubeSize; y < copyMax[1]+cubeSize; y+=cubeSize){
					cube.init(x, y, z);
					cube.computeEdges(access, label);
					cube.getTriangles(tri, car);
				}
			}
		}
		 */

		float[] back = new float[]{min[0]-5,min[1]-5,min[2]-5};
		tri.forEach(p->{
			
			for(int i = 0;i<3;i++)
				p[i] += back[i];
			
		});


		/**
		// convert pixel coordinates
		for(int i = 0; i < tri.size(); i++) {
			Point3f p = (Point3f)tri.get(i);
			p[0] = (float) (p[0] * volume.pw + volume.minCoord[0]);
			p[1] = (float) (p[1] * volume.ph + volume.minCoord[1]);
			p[2] = (float) (p[2] * volume.pd + volume.minCoord[2]);
		}	
		 */
		//log.debug("Non Zero Values found : "+nonZeroFound);
		//log.debug("Triangle size = "+tri.size());

		return tri;
	}

	/** 
	 * An encapsulating class to avoid thread collisions on static fields.
	 */
	private static final class Carrier {
		float label;
		RandomAccess<FloatType> access;

		final float intensity(final float[] p) {
			//	if(p[0] < min[0] || p[1] < min[1] || p[2] < min[2]
			//		|| p[0] >= max[0] || p[1] >= max[1] || p[2] >= max[2])
			//	return 0;

			access.setPosition(new long[]{(long) p[0],(long) p[1],(long) p[2]});
			float i = access.get().getRealFloat();
			if(i!=label)
				return 0;
			return i;
			/*
			 * float i = access.get().getRealFloat();
				if(i!=label)
				return 0;
			 */
		}
	}

	protected static final int ambigous[] = {
			250,
			245,
			237,
			231,
			222,
			219,
			189,
			183,
			175,
			126,
			123,
			95,
			234,
			233,
			227,
			214,
			213,
			211,
			203,
			199,
			188,
			186,
			182,
			174,
			171,
			158,
			151,
			124,
			121,
			117,
			109,
			107,
			93,
			87,
			62,
			61,
			229,
			218,
			181,
			173,
			167,
			122,
			94,
			91,
			150,
			170,
			195,
			135,
			149,
			154,
			163,
			166,
			169,
			172,
			180,
			197,
			202,
			210,
			225,
			165
	};        

	// triangles to be drawn in each case
	private static final int faces[] =
		{
				-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 8, 3, 1, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				9, 2, 11, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				2, 8, 3, 2, 11, 8, 11, 9, 8, -1, -1, -1, -1, -1, -1,
				3, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 10, 2, 8, 10, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 9, 0, 2, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 10, 2, 1, 9, 10, 9, 8, 10, -1, -1, -1, -1, -1, -1,
				3, 11, 1, 10, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 11, 1, 0, 8, 11, 8, 10, 11, -1, -1, -1, -1, -1, -1,
				3, 9, 0, 3, 10, 9, 10, 11, 9, -1, -1, -1, -1, -1, -1,
				9, 8, 11, 11, 8, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1,
				1, 2, 11, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				3, 4, 7, 3, 0, 4, 1, 2, 11, -1, -1, -1, -1, -1, -1,
				9, 2, 11, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1,
				2, 11, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1,
				8, 4, 7, 3, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				10, 4, 7, 10, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1,
				9, 0, 1, 8, 4, 7, 2, 3, 10, -1, -1, -1, -1, -1, -1,
				4, 7, 10, 9, 4, 10, 9, 10, 2, 9, 2, 1, -1, -1, -1,
				3, 11, 1, 3, 10, 11, 7, 8, 4, -1, -1, -1, -1, -1, -1,
				1, 10, 11, 1, 4, 10, 1, 0, 4, 7, 10, 4, -1, -1, -1,
				4, 7, 8, 9, 0, 10, 9, 10, 11, 10, 0, 3, -1, -1, -1,
				4, 7, 10, 4, 10, 9, 9, 10, 11, -1, -1, -1, -1, -1, -1,
				9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1,
				1, 2, 11, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				3, 0, 8, 1, 2, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1,
				5, 2, 11, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1,
				2, 11, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1,
				9, 5, 4, 2, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 10, 2, 0, 8, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1,
				0, 5, 4, 0, 1, 5, 2, 3, 10, -1, -1, -1, -1, -1, -1,
				2, 1, 5, 2, 5, 8, 2, 8, 10, 4, 8, 5, -1, -1, -1,
				11, 3, 10, 11, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1,
				4, 9, 5, 0, 8, 1, 8, 11, 1, 8, 10, 11, -1, -1, -1,
				5, 4, 0, 5, 0, 10, 5, 10, 11, 10, 0, 3, -1, -1, -1,
				5, 4, 8, 5, 8, 11, 11, 8, 10, -1, -1, -1, -1, -1, -1,
				9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1,
				0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1,
				1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				9, 7, 8, 9, 5, 7, 11, 1, 2, -1, -1, -1, -1, -1, -1,
				11, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1,
				8, 0, 2, 8, 2, 5, 8, 5, 7, 11, 5, 2, -1, -1, -1,
				2, 11, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1,
				7, 9, 5, 7, 8, 9, 3, 10, 2, -1, -1, -1, -1, -1, -1,
				9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 10, -1, -1, -1,
				2, 3, 10, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1,
				10, 2, 1, 10, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1,
				9, 5, 8, 8, 5, 7, 11, 1, 3, 11, 3, 10, -1, -1, -1,
				5, 7, 0, 5, 0, 9, 7, 10, 0, 1, 0, 11, 10, 11, 0,
				10, 11, 0, 10, 0, 3, 11, 5, 0, 8, 0, 7, 5, 7, 0,
				10, 11, 5, 7, 10, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				11, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 8, 3, 5, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				9, 0, 1, 5, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 8, 3, 1, 9, 8, 5, 11, 6, -1, -1, -1, -1, -1, -1,
				1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1,
				9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1,
				5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1,
				2, 3, 10, 11, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				10, 0, 8, 10, 2, 0, 11, 6, 5, -1, -1, -1, -1, -1, -1,
				0, 1, 9, 2, 3, 10, 5, 11, 6, -1, -1, -1, -1, -1, -1,
				5, 11, 6, 1, 9, 2, 9, 10, 2, 9, 8, 10, -1, -1, -1,
				6, 3, 10, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1,
				0, 8, 10, 0, 10, 5, 0, 5, 1, 5, 10, 6, -1, -1, -1,
				3, 10, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1,
				6, 5, 9, 6, 9, 10, 10, 9, 8, -1, -1, -1, -1, -1, -1,
				5, 11, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				4, 3, 0, 4, 7, 3, 6, 5, 11, -1, -1, -1, -1, -1, -1,
				1, 9, 0, 5, 11, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1,
				11, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1,
				6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1,
				1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1,
				8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1,
				7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9,
				3, 10, 2, 7, 8, 4, 11, 6, 5, -1, -1, -1, -1, -1, -1,
				5, 11, 6, 4, 7, 2, 4, 2, 0, 2, 7, 10, -1, -1, -1,
				0, 1, 9, 4, 7, 8, 2, 3, 10, 5, 11, 6, -1, -1, -1,
				9, 2, 1, 9, 10, 2, 9, 4, 10, 7, 10, 4, 5, 11, 6,
				8, 4, 7, 3, 10, 5, 3, 5, 1, 5, 10, 6, -1, -1, -1,
				5, 1, 10, 5, 10, 6, 1, 0, 10, 7, 10, 4, 0, 4, 10,
				0, 5, 9, 0, 6, 5, 0, 3, 6, 10, 6, 3, 8, 4, 7,
				6, 5, 9, 6, 9, 10, 4, 7, 9, 7, 10, 9, -1, -1, -1,
				11, 4, 9, 6, 4, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				4, 11, 6, 4, 9, 11, 0, 8, 3, -1, -1, -1, -1, -1, -1,
				11, 0, 1, 11, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1,
				8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 11, -1, -1, -1,
				1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1,
				3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1,
				0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1,
				11, 4, 9, 11, 6, 4, 10, 2, 3, -1, -1, -1, -1, -1, -1,
				0, 8, 2, 2, 8, 10, 4, 9, 11, 4, 11, 6, -1, -1, -1,
				3, 10, 2, 0, 1, 6, 0, 6, 4, 6, 1, 11, -1, -1, -1,
				6, 4, 1, 6, 1, 11, 4, 8, 1, 2, 1, 10, 8, 10, 1,
				9, 6, 4, 9, 3, 6, 9, 1, 3, 10, 6, 3, -1, -1, -1,
				8, 10, 1, 8, 1, 0, 10, 6, 1, 9, 1, 4, 6, 4, 1,
				3, 10, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1,
				6, 4, 8, 10, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				7, 11, 6, 7, 8, 11, 8, 9, 11, -1, -1, -1, -1, -1, -1,
				0, 7, 3, 0, 11, 7, 0, 9, 11, 6, 7, 11, -1, -1, -1,
				11, 6, 7, 1, 11, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1,
				11, 6, 7, 11, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1,
				1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1,
				2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9,
				7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1,
				7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				2, 3, 10, 11, 6, 8, 11, 8, 9, 8, 6, 7, -1, -1, -1,
				2, 0, 7, 2, 7, 10, 0, 9, 7, 6, 7, 11, 9, 11, 7,
				1, 8, 0, 1, 7, 8, 1, 11, 7, 6, 7, 11, 2, 3, 10,
				10, 2, 1, 10, 1, 7, 11, 6, 1, 6, 7, 1, -1, -1, -1,
				8, 9, 6, 8, 6, 7, 9, 1, 6, 10, 6, 3, 1, 3, 6,
				0, 9, 1, 10, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				7, 8, 0, 7, 0, 6, 3, 10, 0, 10, 6, 0, -1, -1, -1,
				7, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				7, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				3, 0, 8, 10, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 1, 9, 10, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				8, 1, 9, 8, 3, 1, 10, 7, 6, -1, -1, -1, -1, -1, -1,
				11, 1, 2, 6, 10, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 2, 11, 3, 0, 8, 6, 10, 7, -1, -1, -1, -1, -1, -1,
				2, 9, 0, 2, 11, 9, 6, 10, 7, -1, -1, -1, -1, -1, -1,
				6, 10, 7, 2, 11, 3, 11, 8, 3, 11, 9, 8, -1, -1, -1,
				7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1,
				2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1,
				1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1,
				11, 7, 6, 11, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1,
				11, 7, 6, 1, 7, 11, 1, 8, 7, 1, 0, 8, -1, -1, -1,
				0, 3, 7, 0, 7, 11, 0, 11, 9, 6, 11, 7, -1, -1, -1,
				7, 6, 11, 7, 11, 8, 8, 11, 9, -1, -1, -1, -1, -1, -1,
				6, 8, 4, 10, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				3, 6, 10, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1,
				8, 6, 10, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1,
				9, 4, 6, 9, 6, 3, 9, 3, 1, 10, 3, 6, -1, -1, -1,
				6, 8, 4, 6, 10, 8, 2, 11, 1, -1, -1, -1, -1, -1, -1,
				1, 2, 11, 3, 0, 10, 0, 6, 10, 0, 4, 6, -1, -1, -1,
				4, 10, 8, 4, 6, 10, 0, 2, 9, 2, 11, 9, -1, -1, -1,
				11, 9, 3, 11, 3, 2, 9, 4, 3, 10, 3, 6, 4, 6, 3,
				8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1,
				0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1,
				1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1,
				8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 11, 1, -1, -1, -1,
				11, 1, 0, 11, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1,
				4, 6, 3, 4, 3, 8, 6, 11, 3, 0, 3, 9, 11, 9, 3,
				11, 9, 4, 6, 11, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				4, 9, 5, 7, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 8, 3, 4, 9, 5, 10, 7, 6, -1, -1, -1, -1, -1, -1,
				5, 0, 1, 5, 4, 0, 7, 6, 10, -1, -1, -1, -1, -1, -1,
				10, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1,
				9, 5, 4, 11, 1, 2, 7, 6, 10, -1, -1, -1, -1, -1, -1,
				6, 10, 7, 1, 2, 11, 0, 8, 3, 4, 9, 5, -1, -1, -1,
				7, 6, 10, 5, 4, 11, 4, 2, 11, 4, 0, 2, -1, -1, -1,
				3, 4, 8, 3, 5, 4, 3, 2, 5, 11, 5, 2, 10, 7, 6,
				7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1,
				9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1,
				3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1,
				6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8,
				9, 5, 4, 11, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1,
				1, 6, 11, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4,
				4, 0, 11, 4, 11, 5, 0, 3, 11, 6, 11, 7, 3, 7, 11,
				7, 6, 11, 7, 11, 8, 5, 4, 11, 4, 8, 11, -1, -1, -1,
				6, 9, 5, 6, 10, 9, 10, 8, 9, -1, -1, -1, -1, -1, -1,
				3, 6, 10, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1,
				0, 10, 8, 0, 5, 10, 0, 1, 5, 5, 6, 10, -1, -1, -1,
				6, 10, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1,
				1, 2, 11, 9, 5, 10, 9, 10, 8, 10, 5, 6, -1, -1, -1,
				0, 10, 3, 0, 6, 10, 0, 9, 6, 5, 6, 9, 1, 2, 11,
				10, 8, 5, 10, 5, 6, 8, 0, 5, 11, 5, 2, 0, 2, 5,
				6, 10, 3, 6, 3, 5, 2, 11, 3, 11, 5, 3, -1, -1, -1,
				5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1,
				9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1,
				1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8,
				1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 3, 6, 1, 6, 11, 3, 8, 6, 5, 6, 9, 8, 9, 6,
				11, 1, 0, 11, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1,
				0, 3, 8, 5, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				11, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				10, 5, 11, 7, 5, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				10, 5, 11, 10, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1,
				5, 10, 7, 5, 11, 10, 1, 9, 0, -1, -1, -1, -1, -1, -1,
				11, 7, 5, 11, 10, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1,
				10, 1, 2, 10, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1,
				0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 10, -1, -1, -1,
				9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 10, 7, -1, -1, -1,
				7, 5, 2, 7, 2, 10, 5, 9, 2, 3, 2, 8, 9, 8, 2,
				2, 5, 11, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1,
				8, 2, 0, 8, 5, 2, 8, 7, 5, 11, 2, 5, -1, -1, -1,
				9, 0, 1, 5, 11, 3, 5, 3, 7, 3, 11, 2, -1, -1, -1,
				9, 8, 2, 9, 2, 1, 8, 7, 2, 11, 2, 5, 7, 5, 2,
				1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1,
				9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1,
				9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				5, 8, 4, 5, 11, 8, 11, 10, 8, -1, -1, -1, -1, -1, -1,
				5, 0, 4, 5, 10, 0, 5, 11, 10, 10, 3, 0, -1, -1, -1,
				0, 1, 9, 8, 4, 11, 8, 11, 10, 11, 4, 5, -1, -1, -1,
				11, 10, 4, 11, 4, 5, 10, 3, 4, 9, 4, 1, 3, 1, 4,
				2, 5, 1, 2, 8, 5, 2, 10, 8, 4, 5, 8, -1, -1, -1,
				0, 4, 10, 0, 10, 3, 4, 5, 10, 2, 10, 1, 5, 1, 10,
				0, 2, 5, 0, 5, 9, 2, 10, 5, 4, 5, 8, 10, 8, 5,
				9, 4, 5, 2, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				2, 5, 11, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1,
				5, 11, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1,
				3, 11, 2, 3, 5, 11, 3, 8, 5, 4, 5, 8, 0, 1, 9,
				5, 11, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1,
				8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1,
				0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1,
				9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				4, 10, 7, 4, 9, 10, 9, 11, 10, -1, -1, -1, -1, -1, -1,
				0, 8, 3, 4, 9, 7, 9, 10, 7, 9, 11, 10, -1, -1, -1,
				1, 11, 10, 1, 10, 4, 1, 4, 0, 7, 4, 10, -1, -1, -1,
				3, 1, 4, 3, 4, 8, 1, 11, 4, 7, 4, 10, 11, 10, 4,
				4, 10, 7, 9, 10, 4, 9, 2, 10, 9, 1, 2, -1, -1, -1,
				9, 7, 4, 9, 10, 7, 9, 1, 10, 2, 10, 1, 0, 8, 3,
				10, 7, 4, 10, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1,
				10, 7, 4, 10, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1,
				2, 9, 11, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1,
				9, 11, 7, 9, 7, 4, 11, 2, 7, 8, 7, 0, 2, 0, 7,
				3, 7, 11, 3, 11, 2, 7, 4, 11, 1, 11, 0, 4, 0, 11,
				1, 11, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1,
				4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1,
				4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				9, 11, 8, 11, 10, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				3, 0, 9, 3, 9, 10, 10, 9, 11, -1, -1, -1, -1, -1, -1,
				0, 1, 11, 0, 11, 8, 8, 11, 10, -1, -1, -1, -1, -1, -1,
				3, 1, 11, 10, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 2, 10, 1, 10, 9, 9, 10, 8, -1, -1, -1, -1, -1, -1,
				3, 0, 9, 3, 9, 10, 1, 2, 9, 2, 10, 9, -1, -1, -1,
				0, 2, 10, 8, 0, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				3, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				2, 3, 8, 2, 8, 11, 11, 8, 9, -1, -1, -1, -1, -1, -1,
				9, 11, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				2, 3, 8, 2, 8, 11, 0, 1, 8, 1, 11, 8, -1, -1, -1,
				1, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
		};
}
