package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import net.imglib2.Interval;
import net.imglib2.IterableInterval;
import net.imglib2.type.numeric.RealType;

interface ChannelControlModel<T extends RealType<T>> extends ChannelControl<T>{

	
	/**
	 * Sets the {@link Interval} used by this {@link ChannelControlModel} to obtain image statistics.
	 * @param itrv
	 */
	public void setProbingInterval(IterableInterval<T> itrv);
	
	
	
}
