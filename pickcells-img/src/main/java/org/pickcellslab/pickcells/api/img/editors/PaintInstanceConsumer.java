package org.pickcellslab.pickcells.api.img.editors;

import org.pickcellslab.pickcells.api.img.providers.OutOfLabelException;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.pickcells.api.img.view.Annotation;

import net.imglib2.RandomAccessibleInterval;

/**
 * The working partner of {@link PaintInstanceFactory} which provides information regarding the {@link PaintInstance} it consumes.
 * 
 * @author Guillaume Blin
 *
 * @param <D> The type of data which are 'read' by the {@link PaintInstance} supported by this consumer (same as data frame returned by this consumer)
 * @param <P> The type of the paint deposited by the {@link PaintInstance} supported by this consumer (same as paintable frame returned by this consumer)
 */
public interface PaintInstanceConsumer<P,D> {

		
	public PaintableAnnotation<P,D> getAnnotationAt(long[] clickPos);
	
	public void addEraser(PaintInstance<P,D> pi);
	

	public void addExpander(PaintInstance<P,D> pi);


	public void addSplitter(PaintInstance<P,D> pi);
	
	
	public void addAnnotation(PaintInstance<P,D> pi);

	public RandomAccessibleInterval<D> getDataFrame(Annotation a);

	public RandomAccessibleInterval<P> getPaintableFrame(Annotation a);


	public D dataValue(Annotation a);

	public D nullDataValue();
	
	public P eraseValue(Annotation a);

	public P expandValue(PaintableAnnotation<P,D> a);

	public P splitValue(PaintableAnnotation<P,D> a);
	
	public P createValue(PaintableAnnotation<P,D> a);	
	
	public P nullPaintValue();

	public PaintableAnnotation<P,D> newPaintable(int time) throws OutOfLabelException;

	
	
}
