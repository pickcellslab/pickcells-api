package org.pickcellslab.pickcells.api.img.view;

import java.lang.reflect.Array;
import java.util.Objects;
import java.util.function.Function;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.datamodel.dimensions.KeyDimension;

public class AnnotationDimension<E> implements Dimension<Annotation,E>{

	
	@SuppressWarnings("rawtypes")
	private final AKey key;
	private final String name;
	private final int index;
	private final String description;


	public AnnotationDimension(AKey<E> k, String description){

		Objects.requireNonNull(k,"the provided AKey cannot be null");
		if(k.type().isArray()) {
			throw new IllegalArgumentException("A length must be specified if the provided AKey points to an array value");
		}
		this.key = k;
		this.index = -1;
		this.name = key.name;

		if(null == description)
			description = "NaN";
		this.description = description;
	}



	public AnnotationDimension(AKey<E[]> k, int index, String description){
		this(k, index, description, i-> index == -1 ? "" : " "+i);
	}



	public AnnotationDimension(AKey<E[]> k, int index, String description, Function<Integer,String> indexNames){

		Objects.requireNonNull(k,"the provided AKey cannot be null");
		Objects.requireNonNull(indexNames,"the provided indexNames cannot be null");

		if(!k.type().isArray())
			throw new IllegalArgumentException("The provided AKey does not point to an array type");
		if(index<0)
			throw new IllegalArgumentException("The provided index is negative, use KeyArrayDimension instead if you need a dimension"
					+ " which retrieves the entire array the AKey points at");
		if(index<0)
			throw new IllegalArgumentException("The lengt");

		this.key = k;
		this.index = index;
		this.name = key.name+indexNames.apply(index);

		if(null == description)
			description = "Not Available";
		this.description = description;
	}



	@Override
	public int index() {
		return index;
	}

	@Override
	public String name() {
		return name;
	}



	@Override
	public dType dataType() {
		dType t = key.dType();
		return t;
	}


	@Override
	public String toString() {
		return name;
	}


	@Override
	public boolean equals(Object o){
		if(o instanceof KeyDimension)
			return key.equals(((AnnotationDimension<?>)o).key) && index == ((Dimension<?,?>)o).index();
		else return false;
	}


	@Override
	public int hashCode(){
		return key.hashCode() + Integer.hashCode(index);
	}





	@SuppressWarnings("unchecked")
	@Override
	public E apply(Annotation i) {
		if(i==null)	return null;
		final Object r = i.getProperty(key);
		if(index==-1)			
			return (E) r;
		else
			if(r == null)
				return null;
		final int length = Array.getLength(r);
		if(index>=length)
			return null;
		return (E) Array.get(r, index);
	}





	@SuppressWarnings("unchecked")
	@Override
	public Class<E> getReturnType() {
		if(index!=-1)
			return key.type().getComponentType();
		return (Class<E>) key.type();
	}



	@Override
	public String info() {
		return description;
	}



	@Override
	public int length() {
		return -1;
	}

}
