package org.pickcellslab.pickcells.api.img.editors;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.ImageIcon;

public interface PaintInstance<P,D> {

	
	public PaintableAnnotation<P,D> getAnnotation();
		
	/**
	 * Draws with the current {@link Brush} at the specified coordinates in the image with the current Label. Will update bounding box and center.
	 * @param coords
	 */
	public void draw(long[] coords);
	
		
	/**
	 * Ctrl-Z type of step
	 */
	public void undo();
	
	/**
	 * Ctrl-Shift-Z type of step
	 */
	public void redo();
	
	/**
	 * Erase all the currently drawn voxels.
	 */
	public void clear();
	
	
	/**
	 * @return Coordinates of the bounding box minimum
	 */
	public long[] min();
	
	/**
	 * @return Coordinates of the bounding box maximum
	 */
	public long[] max();
	
	public Brush getBrush();
	
	public void setBrush(Brush brush);
	
	public ImageIcon getBrushIcon();
	
	public int getBrushSizeX();
	
	public int getBrushSizeY();
	
	
	public void cancel();
	
	
	public void addPaintInstanceListener(PaintInstanceListener l);
	
	public void removePaintInstanceListener(PaintInstanceListener l);
}
