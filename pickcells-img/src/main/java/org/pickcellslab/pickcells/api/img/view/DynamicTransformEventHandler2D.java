package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import net.imglib2.realtransform.AffineTransform2D;
import net.imglib2.ui.TransformEventHandler;
import net.imglib2.ui.TransformEventHandler2D;
import net.imglib2.ui.TransformListener;

/**
 * 
 * This class is a version of {@link TransformEventHandler2D} where the rotation transform can be disabled and the image centered on the screen
 * 
 * A {@link TransformEventHandler} that changes an {@link AffineTransform2D} in
 * response to mouse and keyboard events.
 * 
 */
public class DynamicTransformEventHandler2D extends MouseAdapter implements DynamicTransformEventHandler< AffineTransform2D >
{
	final static private DynamicTransformEventHandlerFactory< AffineTransform2D > factory = new DynamicTransformEventHandlerFactory< AffineTransform2D >()
	{
		@Override
		public DynamicTransformEventHandler< AffineTransform2D > create( final TransformListener< AffineTransform2D > transformListener ){
			return new DynamicTransformEventHandler2D( transformListener );
		}
	};

	public static DynamicTransformEventHandlerFactory< AffineTransform2D > factory(){
		return factory;
	}

	
	
	
	/**
	 * Current source to screen transform.
	 */
	final protected AffineTransform2D affine = new AffineTransform2D();

	/**
	 * Whom to notify when the {@link #affine current transform} is changed.
	 */
	protected TransformListener< AffineTransform2D > listener;

	/**
	 * Copy of {@link #affine current transform} when mouse dragging started.
	 */
	final protected AffineTransform2D affineDragStart = new AffineTransform2D();

	/**
	 * Coordinates where mouse dragging started.
	 */
	protected double oX, oY;

	/**
	 * The screen size of the canvas (the component displaying the image and
	 * generating mouse events).
	 */
	protected int canvasW = 1, canvasH = 1;

	/**
	 * Screen coordinates to keep centered while zooming or rotating with the
	 * keyboard. For example set these to
	 * <em>(screen-width/2, screen-height/2)</em>
	 */
	protected int centerX = 0, centerY = 0;

	private boolean allowRotate = true;

	public DynamicTransformEventHandler2D( final TransformListener< AffineTransform2D > listener )
	{
		this.listener = listener;
	}

	@Override
	public AffineTransform2D getTransform()
	{
		synchronized ( affine )
		{
			return affine.copy();
		}
	}

	@Override
	public void setTransform( final AffineTransform2D transform )
	{
		synchronized ( affine )
		{
			affine.set( transform );	
			update();
		}
		
	}
	
	
	@Override
	public void centerImage(long imgSizeX, long imgSizeY){
		
		centerX = (int) (imgSizeX/2);
		centerY = (int) (imgSizeY/2);
		
		AffineTransform2D tr = new AffineTransform2D();
		//tr.translate(new double[]{canvasW - centerX, canvasH - centerY});
		tr.scale((double)canvasW/(double)imgSizeX);		
		this.setTransform(tr);
	}
	
	
	

	@Override
	public void setCanvasSize( final int width, final int height, final boolean updateTransform )
	{
		if ( updateTransform )
		{
			synchronized ( affine )
			{
				affine.set( affine.get( 0, 2 ) - canvasW / 2, 0, 2 );
				affine.set( affine.get( 1, 2 ) - canvasH / 2, 1, 2 );
				affine.scale( ( double ) width / canvasW );
				affine.set( affine.get( 0, 2 ) + width / 2, 0, 2 );
				affine.set( affine.get( 1, 2 ) + height / 2, 1, 2 );
				update();
			}
		}
		canvasW = width;
		canvasH = height;
		centerX = width / 2;
		centerY = height / 2;
	}

	@Override
	public void setTransformListener( final TransformListener< AffineTransform2D > transformListener )
	{
		listener = transformListener;
	}

	@Override
	public String getHelpString()
	{
		return helpString;
	}

	/**
	 * notifies {@link #listener} that the current transform changed.
	 */
	protected void update()
	{
		if ( listener != null )
			listener.transformChanged( affine );
	}

	/**
	 * One step of rotation (radian).
	 */
	final private static double step = Math.PI / 180;

	final private static String NL = System.getProperty( "line.separator" );

	final private static String helpString =
			"Mouse control:" + NL + " " + NL +
			"rotate the image by left-click and dragging the image in the canvas, " + NL +
			"move the image by middle-or-right-click and dragging the image in the canvas, " + NL +
			"zoom in and out using the mouse-wheel." + NL + " " + NL +
			"Key control:" + NL + " " + NL +
			"SHIFT - Rotate and zoom 10x faster." + NL +
			"CTRL - Rotate and zoom 10x slower.";

	/**
	 * Return rotate/translate/scale speed resulting from modifier keys.
	 * 
	 * Normal speed is 1. SHIFT is faster (10). CTRL is slower (0.1).
	 * 
	 * @param modifiers
	 * @return speed resulting from modifier keys.
	 */
	private static double keyModfiedSpeed( final int modifiers )
	{
		if ( ( modifiers & KeyEvent.SHIFT_DOWN_MASK ) != 0 )
			return 10;
		else if ( ( modifiers & KeyEvent.CTRL_DOWN_MASK ) != 0 )
			return 0.1;
		else
			return 1;
	}

	@Override
	public void mousePressed( final MouseEvent e )
	{
		synchronized ( affine )
		{
			oX = e.getX();
			oY = e.getY();
			affineDragStart.set( affine );
		}
	}

	
	public void setAllowRotation(boolean allow){
		allowRotate = allow;
	}
	
	
	@Override
	public void mouseDragged( final MouseEvent e )
	{
		synchronized ( affine )
		{
			final int modifiers = e.getModifiersEx();

			if ( ( modifiers & MouseEvent.BUTTON1_DOWN_MASK ) != 0 ) // rotate
			{
				if(allowRotate){

					affine.set( affineDragStart );

					final double dX = e.getX() - centerX;
					final double dY = e.getY() - centerY;
					final double odX = oX - centerX;
					final double odY = oY - centerY;
					final double theta = Math.atan2( dY, dX ) - Math.atan2( odY, odX );

					rotate( theta );
				}
			}
			else if ( ( modifiers & ( MouseEvent.BUTTON2_DOWN_MASK | MouseEvent.BUTTON3_DOWN_MASK ) ) != 0 ) // translate
			{
				affine.set( affineDragStart );

				final double dX = oX - e.getX();
				final double dY = oY - e.getY();

				affine.set( affine.get( 0, 2 ) - dX, 0, 2 );
				affine.set( affine.get( 1, 2 ) - dY, 1, 2 );
			}

			update();
		}
	}

	/**
	 * Scale by factor s. Keep screen coordinates (x, y) fixed.
	 */
	private void scale( final double s, final double x, final double y )
	{
		// center shift
		affine.set( affine.get( 0, 2 ) - x, 0, 2 );
		affine.set( affine.get( 1, 2 ) - y, 1, 2 );

		// scale
		affine.scale( s );

		// center un-shift
		affine.set( affine.get( 0, 2 ) + x, 0, 2 );
		affine.set( affine.get( 1, 2 ) + y, 1, 2 );
	}

	/**
	 * Rotate by d radians. Keep screen coordinates ({@link #centerX},
	 * {@link #centerY}) fixed.
	 */
	private void rotate( final double d )
	{
		// center shift
		affine.set( affine.get( 0, 2 ) - centerX, 0, 2 );
		affine.set( affine.get( 1, 2 ) - centerY, 1, 2 );

		// rotate
		affine.rotate( d );

		// center un-shift
		affine.set( affine.get( 0, 2 ) + centerX, 0, 2 );
		affine.set( affine.get( 1, 2 ) + centerY, 1, 2 );
	}

	@Override
	public void mouseWheelMoved( final MouseWheelEvent e )
	{
		synchronized ( affine )
		{
			final int modifiers = e.getModifiersEx();
			final double v = keyModfiedSpeed( modifiers );
			final int s = e.getWheelRotation();

			final double dScale = 1.0 + 0.05 * v;
			if ( s > 0 )
				scale( 1.0 / dScale, e.getX(), e.getY() );
			else
				scale( dScale, e.getX(), e.getY() );

			update();
		}
	}

	
}

