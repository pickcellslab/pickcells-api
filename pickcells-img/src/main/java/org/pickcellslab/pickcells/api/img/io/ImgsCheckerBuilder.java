package org.pickcellslab.pickcells.api.img.io;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.foundationj.datamodel.AKey;

/**
 * A Builder to configure an {@link ImgsChecker}
 * 
 * @author Guillaume Blin
 *
 */
public interface ImgsCheckerBuilder {

	
	/**
	 * Adds a check to be performed by the final ImgsChecker. The ImgsChecker will 
	 * test if all the images in the list share the same value for the specified property.
	 * @param property the property to be checked. Note: in order to be sure that the
	 * property is handled by the implementation, the caller is advised to choose among the 
	 * properties listed in {@link ImgsChecker}.
	 * @return The updated {@link ImgsCheckerBuilder}
	 */
	public ImgsCheckerBuilder addCheck(AKey<?> property);
	
	/**
	 * Adds a check to be performed by the final ImgsChecker. The ImgsChecker will 
	 * test if all the images in the list share the same value for the specified property.
	 * Here the property type must be an array and the provided index is a valid index in this array.
	 * @param property the property to be checked. Note: in order to be sure that the
	 * property is handled by the implementation, the caller is advised to choose among the 
	 * properties listed in {@link ImgsChecker}.
	 * @return The updated {@link ImgsCheckerBuilder}
	 */
	public ImgsCheckerBuilder addCheck(AKey<?> property, int index);
	
	/**
	 * Adds a check to be performed by the final ImgsChecker. The ImgsChecker will 
	 * test if all the images in the list share the provided value for the specified property.
	 * @param property the property to be checked. Note: in order to be sure that the
	 * property is handled by the implementation, the caller is advised to choose among the 
	 * properties listed in {@link ImgsChecker}.
	 * @param value The value the property must be equal to
	 * @return The updated {@link ImgsCheckerBuilder}
	 */
	public <V> ImgsCheckerBuilder addCheck(AKey<V> property, V value);
	
	/**
	 * Adds a check to be performed by the final ImgsChecker. The ImgsChecker will 
	 * test if all the images in the list share the provided value for the specified property.
	 * Here the property type must be an array and the provided index is a valid index in this array.
	 * @param property the property to be checked. Note: in order to be sure that the
	 * property is handled by the implementation, the caller is advised to choose among the 
	 * properties listed in {@link ImgsChecker}.
	 * @param value The value the property must be equal to
	 * @return The updated {@link ImgsCheckerBuilder}
	 */
	public <V> ImgsCheckerBuilder addCheck(AKey<V[]> property, V value, int index);
	
	/**
	 * Builds an {@link ImgsChecker} configured with options specified in this builder
	 * @param list An {@link ImgFileList} to be tested
	 * @return The configured ImgsChecker.
	 */
	public ImgsChecker build(ImgFileList list);
}
