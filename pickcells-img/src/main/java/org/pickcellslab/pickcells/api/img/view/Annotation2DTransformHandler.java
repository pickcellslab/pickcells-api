package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.function.BiFunction;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.math3.util.FastMath;
import org.pickcellslab.foundationj.datamodel.threads.ModeratedConsumerThread;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;

import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.converter.TypeIdentity;
import net.imglib2.display.screenimage.awt.ARGBScreenImage;
import net.imglib2.realtransform.AffineTransform2D;
import net.imglib2.type.numeric.ARGBType;
import net.imglib2.ui.AffineTransformType2D;
import net.imglib2.ui.PainterThread;
import net.imglib2.ui.Renderer;
import net.imglib2.ui.util.Defaults;
import net.imglib2.ui.util.InterpolatingSource;
import net.imglib2.view.Views;

class Annotation2DTransformHandler implements AnnotationDrawer, ChangeListener, AppearanceControlChangeListener {


	private final MinimalImageInfo info;

	private final BlendedBufferedImageOverlayRenderer blender;
	private final Renderer<AffineTransform2D> renderer;
	private final AnnotationProjector2D<ARGBType> projector;
	private final ModeratedConsumerThread<Interval> projThread;


	private final ArrayList<ChangeListener> lstrs = new ArrayList<>();
	private final ChangeEvent evt = new ChangeEvent(this);

	private final AnnotationManager mgr;

	private final AnnotationAppearanceControlManager model;

	private AnnotationAppearanceControl currentControl;



	Annotation2DTransformHandler(AnnotationAppearanceControlManager model, PainterThread painterThread) {

		this.mgr = model.getAnnotationManager();
		mgr.addAnnotationDrawer(this);


		this.info = mgr.annotatedImageInfo();


		this.model = model;		
		model.addAppearanceControlChangeListener(this);


		this.currentControl = model.getActiveControl();
		currentControl.addChangeListener(this);


		//ScreenImage
		final ARGBScreenImage screenImage = new ARGBScreenImage((int)info.dimension(Image.x), (int)info.dimension(Image.y));


		projector = new AnnotationProjector2D<>(mgr, screenImage, currentControl.getConverter(), new ARGBType());




		//Setup the RenderSource (wraps img with transform with converter of type T to ARGB)
		InterpolatingSource<ARGBType, AffineTransform2D> interpolatingSource = 
				new InterpolatingSource<ARGBType,AffineTransform2D>(
						Views.extendValue(screenImage, new ARGBType()), new AffineTransform2D(), new TypeIdentity<ARGBType>() );

		//Thread handler
		projThread = new ModeratedConsumerThread<>((itrv)->{
			projector.map(itrv);
			lstrs.forEach(l->l.stateChanged(evt));
		});
		projThread.setCombiner(combiner());

		//Setup the renderer
		blender = new BlendedBufferedImageOverlayRenderer();

		//renderer = new SimpleRenderer.Factory<>(AffineTransformType2D.instance, interpolatingSource, false, 4).create(target, painterThread);
		renderer = Defaults.rendererFactory(AffineTransformType2D.instance, interpolatingSource ).create(blender, painterThread);

		projThread.start();

	}


	BlendedBufferedImageOverlayRenderer getRenderer(){
		return blender;
	}



	AnnotationManager getManager() {
		return mgr;
	}


	AnnotationAppearanceControlManager getAppearanceControl(){
		return model;
	}


	

	void refresh(AffineTransform2D viewerTransform) {	
		projThread.requestRerun(projector.defaultInterval());		
	}


	void paint(AffineTransform2D viewerTransform) {		
		renderer.paint(viewerTransform);
	}




	void addChangeListener(ChangeListener l){
		lstrs.add(l);
	}

	void removeChangeListener(ChangeListener l){
		lstrs.remove(l);
	}




	@Override
	public void annotationsReDrawRequired(Interval itrv) {
		projThread.requestRerun(itrv);
	}


	@Override
	public void stateChanged(ChangeEvent e) {
		projThread.requestRerun(projector.defaultInterval());
	}
	
	
	
	@Override
	public void appearanceModelChanged(AnnotationAppearanceControlManager source) {
		currentControl.removeChangeListener(this);
		currentControl = source.getActiveControl();
		currentControl.addChangeListener(this);	
		projector.setConverter(currentControl.getConverter());
		projThread.requestRerun(projector.defaultInterval());
	}



	private BiFunction<Interval,Interval,Interval> combiner(){
		return (i1,i2)->{

			final long[] i1Min = new long[2], i1Max = new long[2], i2Min = new long[2], i2Max = new long[2];
			//System.out.println("AnnotationProjector2D : Creating Union");				
			//Create union
			i1.min(i1Min);	i1.max(i1Max);	i2.min(i2Min);	i2.max(i2Max);
			for(int i = 0; i<i1Min.length; i++){
				i1Min[i] = FastMath.min(i1Min[i], i2Min[i]);
				i1Max[i] = FastMath.max(i1Max[i], i2Max[i]);					
			}
			//System.out.println("AnnotationProjector2D : Union done");
			return new FinalInterval(i1Min, i1Max);			
		};
	}



}
