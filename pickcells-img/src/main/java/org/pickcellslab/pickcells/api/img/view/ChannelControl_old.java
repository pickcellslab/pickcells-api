package org.pickcellslab.pickcells.api.img.view;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import com.alee.managers.tooltip.TooltipManager;


@SuppressWarnings("serial")
class ChannelControl_old extends JPanel {



	private ImageViewer<?> view;



	ChannelControl_old(ImageViewer<?> view, String[] names) {

		this.view = view;
		
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scroll = new JScrollPane();
		JPanel pane = new JPanel();
		pane.setLayout(new BoxLayout(pane,BoxLayout.PAGE_AXIS));
		
		//Add a ChannelBox for each channel
		for(int c = 0; c<names.length; c++)			
			pane.add(new ChannelBox(names[c],c));
		
		scroll.setViewportView(pane);
		this.add(scroll,BorderLayout.CENTER);
	}



	private class ChannelBox extends JPanel{

		private ChannelBox(String channel, int pos){
			
			//Controllers
			//----------------------------------------------------------------------------------------------------------------
			
			JCheckBox cb = new JCheckBox(channel);
			cb.addChangeListener(l->{
				view.setVisible(pos, cb.isSelected());
			});
			cb.setPreferredSize(new Dimension(100,25));
			cb.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
			
			if(view.isVisible(pos))
				cb.setSelected(true);
			
			int min = (int) view.getGlobalMinIntensity(pos);
			int max = (int) view.getGlobalMaxIntensity(pos);
			
			RangeSlider rangeSlider = new RangeSlider(min, max);
			rangeSlider.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
						
			rangeSlider.setValue((int) view.getCurrentMinIntensity(pos));
			rangeSlider.setUpperValue((int) view.getCurrentMaxIntensity(pos));
			
			rangeSlider.addMouseWheelListener(l->{
				
				int w = l.getWheelRotation();
				if(l.isControlDown()){//lower value
					int novel = rangeSlider.getValue() + w;
					novel = novel < 0 ? 0 : novel;
					rangeSlider.setValue(novel);
				}else{
					int novel = rangeSlider.getUpperValue() + w;
					novel = novel > rangeSlider.getMaximum() ? rangeSlider.getMaximum() : novel;
					rangeSlider.setUpperValue(novel);
				}
			});
			
			rangeSlider.addChangeListener(l->{
				view.setContrast(pos, rangeSlider.getValue(), rangeSlider.getUpperValue());
				TooltipManager.setTooltip(rangeSlider, "Range : "+rangeSlider.getValue()+" - "+ rangeSlider.getUpperValue());
			});
			
			
			JButton lutBtn = new JButton("LUT");
			
			lutBtn.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
			lutBtn.addActionListener(l->{
				ImgLut<?>[] luts = null;
				if(view.getType().getBitsPerPixel()>=16)
					luts = Lut16.values();
				else
					luts = Lut8.values();
				
				ImgLut<?> choice 
				= (ImgLut<?>) JOptionPane.showInputDialog(
						this,
						"Choose the LUT for "+channel,
						"Lookup Table ... ",
						JOptionPane.PLAIN_MESSAGE,
						null,
						luts,
						luts[0]);
				
				if(choice != null)
					view.setLut(pos, choice.table());
				
			});
			
			
			//Layout
			//---------------------------------------------------------------------------------------------------------------
			
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
			add(cb,BorderLayout.WEST);
			add(rangeSlider,BorderLayout.CENTER);
			add(lutBtn,BorderLayout.EAST);
		}
	}

}
