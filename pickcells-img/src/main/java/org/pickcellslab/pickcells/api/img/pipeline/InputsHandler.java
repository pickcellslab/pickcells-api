package org.pickcellslab.pickcells.api.img.pipeline;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.function.Function;

import org.pickcellslab.pickcells.api.img.io.ImgIO;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.Type;

/**
 * An {@link InputsHandler} is an {@link Iterable} of {@link ProcessingInput}s. 
 * It also provides a method to obtain the result of a pipeline for a ProcessingInput which may have been
 * used for preview.
 * 
 * @author Guillaume Blin
 *
 * @param <I> The {@link Type} of the input image of the {@link ProcessingInput} delivered by this handler
 * @param <O> The {@link Type} of the result image created when running the pipeline on the previewed image
 * @param <P> The type of {@link ProcessingInput} delivered by this handler.
 */
public interface InputsHandler<I,O extends Type<O>, P extends ProcessingInput<I>> extends Iterable<P> {

	
	/**
	 * Adds an extra step to the pipeline. <b>NB:</b> This will only have an effect if a full image has been used for preview.
	 * @param step The extra step to be added
	 * @return A new {@link InputsHandler} instance with the extra step computed if required.
	 */
	<E extends Type<E>> InputsHandler<I,E,P> addStep(Function<RandomAccessibleInterval<O>, RandomAccessibleInterval<E>> step);

	
	/**
	 * @return The name of the previewed image
	 */
	public String nameOfPreview();
	
	/**
	 * Runs a given pipeline on the image used for preview, or do nothing and returns null if no image was used for preview
	 * @param pipeline The {@link ProcessingStep} used as pipeline.
	 * @param io An instance of {@link ImgIO} for loading the input image and saving the result to disk
	 * @param namingConvention A {@link Function} to transform the name of the input image into a name for the result image
	 * @param fctry The factory for the {@link ImgWriteManager} to be used for writing the result to disk.
	 * @param runner The {@link ProcessingRunner} to use to run the pipeline
	 * @param outType The {@link Type} of the result image.
	 * @return The result of the pipeline on the previewed image, or null if no image were used for preview
	 * @throws IOException
	 */
	ExtendedImageInfo<O> getPreviewedResult(ProcessingStep<I, RandomAccessibleInterval<O>> pipeline,
			ImgIO io,
			Function<String, String> namingConvention,
			ImgWriteManagerFactory<O> fctry,
			ProcessingRunner<I, O, P> runner,
			O outType) throws IOException;
	
}
